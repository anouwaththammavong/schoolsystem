export type TeacherAssessments = {
  teacherAssessments: TeacherAssessmentsClass;
};

export type TeacherAssessmentsClass = {
  teacher_assessments: TeacherAssessment[];
  totalAssessment: number;
};

export type TeacherAssessment = {
  _id: string;
  teacher: Teacher;
  attendance_class: string;
  behavior: string;
  assess_teaching: string;
  assessment_date: Date;
  note: string;
};

export type Teacher = {
  _id: string;
  teacher_id: string;
  name: string;
  last_name: string;
  age: string;
  teacher_tel: string;
  date_of_birth: Date;
  teacher_village: string;
  teacher_district: string;
  teacher_province: string;
  nationality: string;
  graduated_from_institute: string;
  gender: string;
  degree: string;
  subjects: Subject[];
  class_teacher: string;
};

export type Subject = {
  title: string;
  _id: string;
};

export type CreateTeacherAssessment = {
  teacher: string;
  attendance_class: string;
  behavior: string;
  assess_teaching: string;
  assessment_date: Date;
  note?: string;
};

export type UpdateTeacherAssessment = {
  teacher: string;
  attendance_class: string;
  behavior: string;
  assess_teaching: string;
  assessment_date: Date;
  note?: string;
};

export interface TeacherAssessmentState {
  readonly teacherAssessments: TeacherAssessments;
  // Get Teacher Assessments
  readonly isGetTeacherAssessmentsLoading: boolean;
  readonly isGetTeacherAssessmentsSuccess: boolean;
  readonly getTeacherAssessmentsError: {
    isError: boolean;
    message?: string;
  };
  // Create Teacher Assessment
  readonly isCreateTeacherAssessmentLoading: boolean;
  readonly isCreateTeacherAssessmentSuccess: boolean;
  readonly createTeacherAssessmentError: {
    isError: boolean;
    message?: string;
  };
  // update Teacher Assessment
  readonly isUpdateTeacherAssessmentLoading: boolean;
  readonly isUpdateTeacherAssessmentSuccess: boolean;
  readonly updateTeacherAssessmentError: {
    isError: boolean;
    message?: string;
  };
  // delete Teacher Assessment
  readonly isDeleteTeacherAssessmentLoading: boolean;
  readonly isDeleteTeacherAssessmentSuccess: boolean;
  readonly deleteTeacherAssessmentError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: TeacherAssessmentState = {
  teacherAssessments: {
    teacherAssessments: {
      teacher_assessments: [],
      totalAssessment: 0,
    },
  },
  isGetTeacherAssessmentsLoading: false,
  isGetTeacherAssessmentsSuccess: false,
  getTeacherAssessmentsError: {
    isError: false,
  },
  isCreateTeacherAssessmentLoading: false,
  isCreateTeacherAssessmentSuccess: false,
  createTeacherAssessmentError: {
    isError: false,
  },
  isUpdateTeacherAssessmentLoading: false,
  isUpdateTeacherAssessmentSuccess: false,
  updateTeacherAssessmentError: {
    isError: false,
  },
  isDeleteTeacherAssessmentLoading: false,
  isDeleteTeacherAssessmentSuccess: false,
  deleteTeacherAssessmentError: {
    isError: false,
  },
};

export enum TeacherAssessmentActionTypes {
  GET_TEACHER_ASSESSMENTS_REQUEST = "@@teacher_assessment/GET_TEACHER_ASSESSMENTS_REQUEST",
  GET_TEACHER_ASSESSMENTS_SUCCESS = "@@teacher_assessment/GET_TEACHER_ASSESSMENTS_SUCCESS",
  GET_TEACHER_ASSESSMENTS_ERROR = "@@teacher_assessment/GET_TEACHER_ASSESSMENTS_ERROR",
  CREATE_TEACHER_ASSESSMENT_REQUEST = "@@teacher_assessment/CREATE_TEACHER_ASSESSMENT_REQUEST",
  CREATE_TEACHER_ASSESSMENT_SUCCESS = "@@teacher_assessment/CREATE_TEACHER_ASSESSMENT_SUCCESS",
  CREATE_TEACHER_ASSESSMENT_ERROR = "@@teacher_assessment/CREATE_TEACHER_ASSESSMENT_ERROR",
  CREATE_TEACHER_ASSESSMENT_RESET = "@@teacher_assessment/CREATE_TEACHER_ASSESSMENT_RESET",
  UPDATE_TEACHER_ASSESSMENT_REQUEST = "@@teacher_assessment/UPDATE_TEACHER_ASSESSMENT_REQUEST",
  UPDATE_TEACHER_ASSESSMENT_SUCCESS = "@@teacher_assessment/UPDATE_TEACHER_ASSESSMENT_SUCCESS",
  UPDATE_TEACHER_ASSESSMENT_ERROR = "@@teacher_assessment/UPDATE_TEACHER_ASSESSMENT_ERROR",
  UPDATE_TEACHER_ASSESSMENT_RESET = "@@teacher_assessment/UPDATE_TEACHER_ASSESSMENT_RESET",
  DELETE_TEACHER_ASSESSMENT_REQUEST = "@@teacher_assessment/DELETE_TEACHER_ASSESSMENT_REQUEST",
  DELETE_TEACHER_ASSESSMENT_SUCCESS = "@@teacher_assessment/DELETE_TEACHER_ASSESSMENT_SUCCESS",
  DELETE_TEACHER_ASSESSMENT_ERROR = "@@teacher_assessment/DELETE_TEACHER_ASSESSMENT_ERROR",
  DELETE_TEACHER_ASSESSMENT_RESET = "@@teacher_assessment/DELETE_TEACHER_ASSESSMENT_RESET",
}

export type UseTeacherAssessment = {
  teacherAssessmentState: TeacherAssessmentState;
  getTeacherAssessment: (
    limit: number,
    offset: number,
    teacher_id?: string,
    assessment_date?: Date
  ) => void;
  createTeacherAssessment: (teacherAssessment: CreateTeacherAssessment) => void;
  createTeacherAssessmentReset: () => void;
  updateTeacherAssessment: (
    _id: string,
    teacherAssessment: UpdateTeacherAssessment
  ) => void;
  updateTeacherAssessmentReset: () => void;
  deleteTeacherAssessment: (_id: string) => void;
  deleteTeacherAssessmentReset: () => void;
};

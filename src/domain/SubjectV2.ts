export type SubjectV2s = {
    subjectV2s: SubjectV2sClass;
  };
  
  export type SubjectV2sClass = {
    subjectV2s: SubjectV2[];
    totalCount: number;
  };
  
  export type SubjectV2 = {
    _id: string;
    subject_id: string;
    subject_name: string;
  };
  
  export type CreateSubjectV2Form = {
    subject_id: string;
    subject_name: string;
  };
  
  export type UpdateSubjectV2Form = {
    subject_id: string;
    subject_name: string;
  };
  
  export interface SubjectV2State {
    readonly subjectV2s: SubjectV2s;
    // GET SubjectV2
    readonly isGetSubjectV2sLoading: boolean;
    readonly isGetSubjectV2sSuccess: boolean;
    readonly getSubjectV2sError: {
      isError: boolean;
      message?: string;
    };
    // Create SubjectV2
    readonly isCreateSubjectV2Loading: boolean;
    readonly isCreateSubjectV2Success: boolean;
    readonly createSubjectV2Error: {
      isError: boolean;
      message?: string;
    };
    // update SubjectV2
    readonly isUpdateSubjectV2Loading: boolean;
    readonly isUpdateSubjectV2Success: boolean;
    readonly updateSubjectV2Error: {
      isError: boolean;
      message?: string;
    };
    // delete SubjectV2
    readonly isDeleteSubjectV2Loading: boolean;
    readonly isDeleteSubjectV2Success: boolean;
    readonly deleteSubjectV2Error: {
      isError: boolean;
      message?: string;
    };
  }
  
  export const initialState: SubjectV2State = {
    subjectV2s: {
      subjectV2s: {
        subjectV2s: [],
        totalCount: 0,
      },
    },
    isGetSubjectV2sLoading: false,
    isGetSubjectV2sSuccess: false,
    getSubjectV2sError: {
      isError: false,
    },
    isCreateSubjectV2Loading: false,
    isCreateSubjectV2Success: false,
    createSubjectV2Error: {
      isError: false,
    },
    isUpdateSubjectV2Loading: false,
    isUpdateSubjectV2Success: false,
    updateSubjectV2Error: {
      isError: false,
    },
    isDeleteSubjectV2Loading: false,
    isDeleteSubjectV2Success: false,
    deleteSubjectV2Error: {
      isError: false,
    },
  };
  
  export enum SubjectV2ActionTypes {
    GET_SUBJECT_V2S_REQUEST = "@@subject_v2/GET_SUBJECT_V2S_REQUEST",
    GET_SUBJECT_V2S_SUCCESS = "@@subject_v2/GET_SUBJECT_V2S_SUCCESS",
    GET_SUBJECT_V2S_ERROR = "@@subject_v2/GET_SUBJECT_V2S_ERROR",
    CREATE_SUBJECT_V2_REQUEST = "@@subject_v2/CREATE_SUBJECT_V2_REQUEST",
    CREATE_SUBJECT_V2_SUCCESS = "@@subject_v2/CREATE_SUBJECT_V2_SUCCESS",
    CREATE_SUBJECT_V2_ERROR = "@@subject_v2/CREATE_SUBJECT_V2_ERROR",
    CREATE_SUBJECT_V2_RESET = "@@subject_v2/CREATE_SUBJECT_V2_RESET",
    UPDATE_SUBJECT_V2_REQUEST = "@@subject_v2/UPDATE_SUBJECT_V2_REQUEST",
    UPDATE_SUBJECT_V2_SUCCESS = "@@subject_v2/UPDATE_SUBJECT_V2_SUCCESS",
    UPDATE_SUBJECT_V2_ERROR = "@@subject_v2/UPDATE_SUBJECT_V2_ERROR",
    UPDATE_SUBJECT_V2_RESET = "@@subject_v2/UPDATE_SUBJECT_V2_RESET",
    DELETE_SUBJECT_V2_REQUEST = "@@subject_v2/DELETE_SUBJECT_V2_REQUEST",
    DELETE_SUBJECT_V2_SUCCESS = "@@subject_v2/DELETE_SUBJECT_V2_SUCCESS",
    DELETE_SUBJECT_V2_ERROR = "@@subject_v2/DELETE_SUBJECT_V2_ERROR",
    DELETE_SUBJECT_V2_RESET = "@@subject_v2/DELETE_SUBJECT_V2_RESET",
  }
  
  export type UseSubjectV2 = {
    subjectV2State: SubjectV2State;
    getSubjectV2s: (limit: number, offset: number) => void;
    createSubjectV2: (subjectV2: CreateSubjectV2Form) => void;
    createSubjectV2Reset: () => void;
    updateSubjectV2: (_id: string, subjectV2: UpdateSubjectV2Form) => void;
    updateSubjectV2Reset: () => void;
    deleteSubjectV2: (_id: string) => void;
    deleteSubjectV2Reset: () => void;
  };
  
export type StudentAssessments = {
  studentAssessments: StudentAssessmentsClass;
}

export type StudentAssessmentsClass = {
  studentAssessments: StudentAssessment[];
  totalStudents:      number;
}

export type StudentAssessment = {
  _id:              string;
  student:          Student;
  attendance_class: string;
  behavior:         string;
  assess_learning:  string;
  generation:       string;
  note: string;
  roomDetails:      RoomDetails;
  levelDetails:     LevelDetails;
}

export type LevelDetails = {
  _id:        string;
  level_id:   string;
  level_name: string;
}

export type RoomDetails = {
  _id:       string;
  level_id:  string;
  room_id:   string;
  room_name: string;
}

export type Student = {
  _id:                 string;
  student_id:          string;
  name:                string;
  last_name:           string;
  age:                 string;
  student_tel:         string;
  date_of_birth:       Date;
  student_village:     string;
  student_district:    string;
  student_province:    string;
  nationality:         string;
  father_name:         string;
  father_job:          string;
  mother_name:         string;
  mother_job:          string;
  father_tel:          string;
  mother_tel:          string;
  student_past_school: string;
  student_number:      string;
  room_id:             string;
  level_id:            string;
  status:              string;
  note:                string;
  school_entry_date:   Date;
  gender:              string;
  generation:          string;
  new_student:         boolean;
}

export type CreateStudentAssessmentForm = {
  class_room: string;
  class_level: string;
  student: string;
  attendance_class: string;
  behavior: string;
  assess_learning: string;
  note?: string;
};

export type UpdateStudentAssessmentForm = {
  attendance_class: string;
  behavior: string;
  assess_learning: string;
  note?: string;
};

export interface StudentAssessmentState {
  readonly studentAssessments: StudentAssessments;
  // Get Student Assessments
  readonly isGetStudentAssessmentsLoading: boolean;
  readonly isGetStudentAssessmentsSuccess: boolean;
  readonly getStudentAssessmentsError: {
    isError: boolean;
    message?: string;
  };
  // Create Student Assessment
  readonly isCreateStudentAssessmentLoading: boolean;
  readonly isCreateStudentAssessmentSuccess: boolean;
  readonly createStudentAssessmentError: {
    isError: boolean;
    message?: string;
  };
  // update Student Assessment
  readonly isUpdateStudentAssessmentLoading: boolean;
  readonly isUpdateStudentAssessmentSuccess: boolean;
  readonly updateStudentAssessmentError: {
    isError: boolean;
    message?: string;
  };
   // delete Student Assessment
   readonly isDeleteStudentAssessmentLoading: boolean;
   readonly isDeleteStudentAssessmentSuccess: boolean;
   readonly deleteStudentAssessmentError: {
     isError: boolean;
     message?: string;
   };
}

export const initialState: StudentAssessmentState = {
  studentAssessments: {
    studentAssessments: {
      studentAssessments: [],
      totalStudents: 0,
    },
  },
  isGetStudentAssessmentsLoading: false,
  isGetStudentAssessmentsSuccess: false,
  getStudentAssessmentsError: {
    isError: false,
  },
  isCreateStudentAssessmentLoading: false,
  isCreateStudentAssessmentSuccess: false,
  createStudentAssessmentError: {
    isError: false,
  },
  isUpdateStudentAssessmentLoading: false,
  isUpdateStudentAssessmentSuccess: false,
  updateStudentAssessmentError: {
    isError: false,
  },
  isDeleteStudentAssessmentLoading: false,
  isDeleteStudentAssessmentSuccess: false,
  deleteStudentAssessmentError: {
    isError: false,
  },
};

export enum StudentAssessmentActionTypes {
  GET_STUDENT_ASSESSMENTS_REQUEST = "@@student_assessment/GET_STUDENT_ASSESSMENTS_REQUEST",
  GET_STUDENT_ASSESSMENTS_SUCCESS = "@@student_assessment/GET_STUDENT_ASSESSMENTS_SUCCESS",
  GET_STUDENT_ASSESSMENTS_ERROR = "@@student_assessment/GET_STUDENT_ASSESSMENTS_ERROR",
  CREATE_STUDENT_ASSESSMENT_REQUEST = "@@student_assessment/CREATE_STUDENT_ASSESSMENT_REQUEST",
  CREATE_STUDENT_ASSESSMENT_SUCCESS = "@@student_assessment/CREATE_STUDENT_ASSESSMENT_SUCCESS",
  CREATE_STUDENT_ASSESSMENT_ERROR = "@@student_assessment/CREATE_STUDENT_ASSESSMENT_ERROR",
  CREATE_STUDENT_ASSESSMENT_RESET = "@@student_assessment/CREATE_STUDENT_ASSESSMENT_RESET",
  UPDATE_STUDENT_ASSESSMENT_REQUEST = "@@student_assessment/UPDATE_STUDENT_ASSESSMENT_REQUEST",
  UPDATE_STUDENT_ASSESSMENT_SUCCESS = "@@student_assessment/UPDATE_STUDENT_ASSESSMENT_SUCCESS",
  UPDATE_STUDENT_ASSESSMENT_ERROR = "@@student_assessment/UPDATE_STUDENT_ASSESSMENT_ERROR",
  UPDATE_STUDENT_ASSESSMENT_RESET = "@@student_assessment/UPDATE_STUDENT_ASSESSMENT_RESET",
  DELETE_STUDENT_ASSESSMENT_REQUEST = "@@student_assessment/DELETE_STUDENT_ASSESSMENT_REQUEST",
  DELETE_STUDENT_ASSESSMENT_SUCCESS = "@@student_assessment/DELETE_STUDENT_ASSESSMENT_SUCCESS",
  DELETE_STUDENT_ASSESSMENT_ERROR = "@@student_assessment/DELETE_STUDENT_ASSESSMENT_ERROR",
  DELETE_STUDENT_ASSESSMENT_RESET = "@@student_assessment/DELETE_STUDENT_ASSESSMENT_RESET",
}

export type UseStudentAssessment = {
  studentAssessmentState: StudentAssessmentState;
  getStudentAssessment: (
    limit: number,
    offset: number,
    level_id: string,
    room_id: string,
    generation: string
  ) => void;
  createStudentAssessment: (
    studentAssessment: CreateStudentAssessmentForm
  ) => void;
  createStudentAssessmentReset: () => void;
  updateStudentAssessment: (
    _id: string,
    studentAssessment: UpdateStudentAssessmentForm
  ) => void;
  updateStudentAssessmentReset: () => void;
  deleteStudentAssessment: (_id: string) => void;
  deleteStudentAssessmentReset: () => void;
};

export type SchoolFees = {
  school_fees: SchoolFeesClass;
};

export type SchoolFeesClass = {
  schoolFees: SchoolFee[];
  totalCount: number;
};

export type SchoolFee = {
  _id: string;
  school_fee_id: string;
  academic_year_no: SchoolFeeNo;
  level_id: LevelID;
  book_fee: number;
  uniform_fee: number;
  total_fee: number;
  term_fee: number;
};

export type SchoolFeeNo = {
  _id: string;
  academic_year: string;
  academic_year_no: string;
};

export type LevelID = {
  _id: string;
  level_id: string;
  level_name: string;
};

export type CreateSchoolFee = {
  school_fee_id: string;
  academic_year_no: string;
  level_id: string;
  book_fee: number;
  uniform_fee: number;
  term_fee: number;
  total_fee: number;
};

export type UpdateSchoolFee = {
  school_fee_id: string;
  academic_year_no: string;
  level_id: string;
  book_fee: number;
  uniform_fee: number;
  term_fee: number;
  total_fee: number;
};

export interface SchoolFeeState {
  readonly schoolFees: SchoolFees;
  // GET  SchoolFee
  readonly isGetSchoolFeesLoading: boolean;
  readonly isGetSchoolFeesSuccess: boolean;
  readonly getSchoolFeesError: {
    isError: boolean;
    message?: string;
  };
  // Create SchoolFee
  readonly isCreateSchoolFeeLoading: boolean;
  readonly isCreateSchoolFeeSuccess: boolean;
  readonly createSchoolFeeError: {
    isError: boolean;
    message?: string;
  };
  // update SchoolFee
  readonly isUpdateSchoolFeeLoading: boolean;
  readonly isUpdateSchoolFeeSuccess: boolean;
  readonly updateSchoolFeeError: {
    isError: boolean;
    message?: string;
  };
  // delete SchoolFee
  readonly isDeleteSchoolFeeLoading: boolean;
  readonly isDeleteSchoolFeeSuccess: boolean;
  readonly deleteSchoolFeeError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: SchoolFeeState = {
  schoolFees: {
    school_fees: {
      schoolFees: [],
      totalCount: 0,
    },
  },
  isGetSchoolFeesLoading: false,
  isGetSchoolFeesSuccess: false,
  getSchoolFeesError: {
    isError: false,
  },
  isCreateSchoolFeeLoading: false,
  isCreateSchoolFeeSuccess: false,
  createSchoolFeeError: {
    isError: false,
  },
  isUpdateSchoolFeeLoading: false,
  isUpdateSchoolFeeSuccess: false,
  updateSchoolFeeError: {
    isError: false,
  },
  isDeleteSchoolFeeLoading: false,
  isDeleteSchoolFeeSuccess: false,
  deleteSchoolFeeError: {
    isError: false,
  },
};

export enum SchoolFeeActionTypes {
  // Get School fee
  GET_SCHOOL_FEES_REQUEST = "@@school_fee/GET_SCHOOL_FEES_REQUEST",
  GET_SCHOOL_FEES_SUCCESS = "@@school_fee/GET_SCHOOL_FEES_SUCCESS",
  GET_SCHOOL_FEES_ERROR = "@@school_fee/GET_SCHOOL_FEES_ERROR",
  CREATE_SCHOOL_FEE_REQUEST = "@@school_fee/CREATE_SCHOOL_FEE_REQUEST",
  CREATE_SCHOOL_FEE_SUCCESS = "@@school_fee/CREATE_SCHOOL_FEE_SUCCESS",
  CREATE_SCHOOL_FEE_ERROR = "@@school_fee/CREATE_SCHOOL_FEE_ERROR",
  CREATE_SCHOOL_FEE_RESET = "@@school_fee/CREATE_SCHOOL_FEE_RESET",
  UPDATE_SCHOOL_FEE_REQUEST = "@@school_fee/UPDATE_SCHOOL_FEE_REQUEST",
  UPDATE_SCHOOL_FEE_SUCCESS = "@@school_fee/UPDATE_SCHOOL_FEE_SUCCESS",
  UPDATE_SCHOOL_FEE_ERROR = "@@school_fee/UPDATE_SCHOOL_FEE_ERROR",
  UPDATE_SCHOOL_FEE_RESET = "@@school_fee/UPDATE_SCHOOL_FEE_RESET",
  DELETE_SCHOOL_FEE_REQUEST = "@@school_fee/DELETE_SCHOOL_FEE_REQUEST",
  DELETE_SCHOOL_FEE_SUCCESS = "@@school_fee/DELETE_SCHOOL_FEE_SUCCESS",
  DELETE_SCHOOL_FEE_ERROR = "@@school_fee/DELETE_SCHOOL_FEE_ERROR",
  DELETE_SCHOOL_FEE_RESET = "@@school_fee/DELETE_SCHOOL_FEE_RESET",
}

export type UseSchoolFee = {
  schoolFeeState: SchoolFeeState;
  getSchoolFees: (limit: number, offset: number, level_id?: string, academic_year_no?: string) => void;
  createSchoolFee: (schoolFee: CreateSchoolFee) => void;
  createSchoolFeeReset: () => void;
  updateSchoolFee: (_id: string, schoolFee: UpdateSchoolFee) => void;
  updateSchoolFeeReset: () => void;
  deleteSchoolFee: (_id: string) => void;
  deleteSchoolFeeReset: () => void;
};

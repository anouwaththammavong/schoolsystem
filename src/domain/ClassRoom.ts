export type ClassRooms = {
  classrooms: ClassroomsClass;
};

export type ClassroomsClass = {
  classRooms: ClassRoom[];
  totalCount: number;
};

export type ClassRoom = {
  _id: string;
  level_id: LevelID;
  room_id: string;
  room_name: string;
};

export type LevelID = {
  _id: string;
  level_id: string;
  level_name: string;
};

export type CreateClassRoom = {
  room_id: string;
  room_name: string;
  level_id: string;
};

export type UpdateClassRoom = {
  room_id: string;
  room_name: string;
  level_id: string;
};

export interface ClassRoomState {
  readonly classRooms: ClassRooms;
  // GET ClassRoom
  readonly isGetClassRoomsLoading: boolean;
  readonly isGetClassRoomsSuccess: boolean;
  readonly getClassRoomsError: {
    isError: boolean;
    message?: string;
  };
  // Create ClassRoom
  readonly isCreateClassRoomLoading: boolean;
  readonly isCreateClassRoomSuccess: boolean;
  readonly createClassRoomError: {
    isError: boolean;
    message?: string;
  };
  // update ClassRoom
  readonly isUpdateClassRoomLoading: boolean;
  readonly isUpdateClassRoomSuccess: boolean;
  readonly updateClassRoomError: {
    isError: boolean;
    message?: string;
  };
  // delete ClassRoom
  readonly isDeleteClassRoomLoading: boolean;
  readonly isDeleteClassRoomSuccess: boolean;
  readonly deleteClassRoomError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: ClassRoomState = {
  classRooms: {
    classrooms: {
      classRooms: [],
      totalCount: 0,
    },
  },
  isGetClassRoomsLoading: false,
  isGetClassRoomsSuccess: false,
  getClassRoomsError: {
    isError: false,
  },
  isCreateClassRoomLoading: false,
  isCreateClassRoomSuccess: false,
  createClassRoomError: {
    isError: false,
  },
  isUpdateClassRoomLoading: false,
  isUpdateClassRoomSuccess: false,
  updateClassRoomError: {
    isError: false,
  },
  isDeleteClassRoomLoading: false,
  isDeleteClassRoomSuccess: false,
  deleteClassRoomError: {
    isError: false,
  },
};

export enum ClassRoomActionTypes {
  GET_CLASS_ROOMS_REQUEST = "@@class_room/GET_CLASS_ROOMS_REQUEST",
  GET_CLASS_ROOMS_SUCCESS = "@@class_room/GET_CLASS_ROOMS_SUCCESS",
  GET_CLASS_ROOMS_ERROR = "@@class_room/GET_CLASS_ROOMS_ERROR",
  CREATE_CLASS_ROOM_REQUEST = "@@class_room/CREATE_CLASS_ROOM_REQUEST",
  CREATE_CLASS_ROOM_SUCCESS = "@@class_room/CREATE_CLASS_ROOM_SUCCESS",
  CREATE_CLASS_ROOM_ERROR = "@@class_room/CREATE_CLASS_ROOM_ERROR",
  CREATE_CLASS_ROOM_RESET = "@@class_room/CREATE_CLASS_ROOM_RESET",
  UPDATE_CLASS_ROOM_REQUEST = "@@class_room/UPDATE_CLASS_ROOM_REQUEST",
  UPDATE_CLASS_ROOM_SUCCESS = "@@class_room/UPDATE_CLASS_ROOM_SUCCESS",
  UPDATE_CLASS_ROOM_ERROR = "@@class_room/UPDATE_CLASS_ROOM_ERROR",
  UPDATE_CLASS_ROOM_RESET = "@@class_room/UPDATE_CLASS_ROOM_RESET",
  DELETE_CLASS_ROOM_REQUEST = "@@class_room/DELETE_CLASS_ROOM_REQUEST",
  DELETE_CLASS_ROOM_SUCCESS = "@@class_room/DELETE_CLASS_ROOM_SUCCESS",
  DELETE_CLASS_ROOM_ERROR = "@@class_room/DELETE_CLASS_ROOM_ERROR",
  DELETE_CLASS_ROOM_RESET = "@@class_room/DELETE_CLASS_ROOM_RESET",
}

export type UseClassRoom = {
  classRoomState: ClassRoomState;
  getClassRooms: (limit: number, offset: number) => void;
  createClassRoom: (classRoom: CreateClassRoom) => void;
  createClassRoomReset: () => void;
  updateClassRoom: (_id: string, classRoom: UpdateClassRoom) => void;
  updateClassRoomReset: () => void;
  deleteClassRoom: (_id: string) => void;
  deleteClassRoomReset: () => void;
};

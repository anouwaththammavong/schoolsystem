export type TeacherAbsence = {
  teacherAbsence: TeacherAbsenceClass;
}

export type TeacherAbsenceClass = {
  absences:      Absence[];
  totalAbsences: number;
  totalBe_reasonable: number,
	totalNotBe_reasonable: number
}

export type Absence = {
  _id:            string;
  teacher:        Teacher;
  be_reasonable: string;
  absence_allday: string;
  absence_date:   Date;
  note:           string;
}

export type Teacher = {
  _id:           string;
  teacher_id:    string;
  name:          string;
  last_name:     string;
  age:           string;
  gender:        string;
  degree:        string;
  subjects:      Subject[];
  date_of_birth: Date;
  class_teacher: string;
}

export type Subject = {
  title: string;
  _id:   string;
}

export type CreateTeacherAbsenceForm = {
  teacher: string;
  be_reasonable: string;
  absence_allday: string;
  absence_date: Date;
  note?: string;
};

export type UpdateTeacherAbsenceForm = {
  be_reasonable: string;
  absence_allday: string;
  absence_date: Date;
  note?: string;
};

export interface TeacherAbsenceState {
  readonly teacherAbsences: TeacherAbsence;
  // Get Teacher Absence
  readonly isGetTeacherAbsencesLoading: boolean;
  readonly isGetTeacherAbsencesSuccess: boolean;
  readonly getTeacherAbsencesError: {
    isError: boolean;
    message?: string;
  };
  // Create Teacher Absence
  readonly isCreateTeacherAbsenceLoading: boolean;
  readonly isCreateTeacherAbsenceSuccess: boolean;
  readonly createTeacherAbsenceError: {
    isError: boolean;
    message?: string;
  };
  // update Teacher Absence
  readonly isUpdateTeacherAbsenceLoading: boolean;
  readonly isUpdateTeacherAbsenceSuccess: boolean;
  readonly updateTeacherAbsenceError: {
    isError: boolean;
    message?: string;
  };
  // delete Teacher Absence
  readonly isDeleteTeacherAbsenceLoading: boolean;
  readonly isDeleteTeacherAbsenceSuccess: boolean;
  readonly deleteTeacherAbsenceError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: TeacherAbsenceState = {
  teacherAbsences: {
    teacherAbsence: {
      absences: [],
      totalAbsences: 0,
      totalBe_reasonable: 0,
      totalNotBe_reasonable: 0,
    },
  },
  isGetTeacherAbsencesLoading: false,
  isGetTeacherAbsencesSuccess: false,
  getTeacherAbsencesError: {
    isError: false,
  },
  isCreateTeacherAbsenceLoading: false,
  isCreateTeacherAbsenceSuccess: false,
  createTeacherAbsenceError: {
    isError: false,
  },
  isUpdateTeacherAbsenceLoading: false,
  isUpdateTeacherAbsenceSuccess: false,
  updateTeacherAbsenceError: {
    isError: false,
  },
  isDeleteTeacherAbsenceLoading: false,
  isDeleteTeacherAbsenceSuccess: false,
  deleteTeacherAbsenceError: {
    isError: false,
  },
};

export enum TeacherAbsenceActionTypes {
  GET_TEACHER_ABSENCES_REQUEST = "@@teacher_absence/GET_TEACHER_ABSENCES_REQUEST",
  GET_TEACHER_ABSENCES_SUCCESS = "@@teacher_absence/GET_TEACHER_ABSENCES_SUCCESS",
  GET_TEACHER_ABSENCES_ERROR = "@@teacher_absence/GET_TEACHER_ABSENCES_ERROR",
  CREATE_TEACHER_ABSENCE_REQUEST = "@@teacher_absence/CREATE_TEACHER_ABSENCE_REQUEST",
  CREATE_TEACHER_ABSENCE_SUCCESS = "@@teacher_absence/CREATE_TEACHER_ABSENCE_SUCCESS",
  CREATE_TEACHER_ABSENCE_ERROR = "@@teacher_absence/CREATE_TEACHER_ABSENCE_ERROR",
  CREATE_TEACHER_ABSENCE_RESET = "@@teacher_absence/CREATE_TEACHER_ABSENCE_RESET",
  UPDATE_TEACHER_ABSENCE_REQUEST = "@@teacher_absence/UPDATE_TEACHER_ABSENCE_REQUEST",
  UPDATE_TEACHER_ABSENCE_SUCCESS = "@@teacher_absence/UPDATE_TEACHER_ABSENCE_SUCCESS",
  UPDATE_TEACHER_ABSENCE_ERROR = "@@teacher_absence/UPDATE_TEACHER_ABSENCE_ERROR",
  UPDATE_TEACHER_ABSENCE_RESET = "@@teacher_absence/UPDATE_TEACHER_ABSENCE_RESET",
  DELETE_TEACHER_ABSENCE_REQUEST = "@@teacher_absence/DELETE_TEACHER_ABSENCE_REQUEST",
  DELETE_TEACHER_ABSENCE_SUCCESS = "@@teacher_absence/DELETE_TEACHER_ABSENCE_SUCCESS",
  DELETE_TEACHER_ABSENCE_ERROR = "@@teacher_absence/DELETE_TEACHER_ABSENCE_ERROR",
  DELETE_TEACHER_ABSENCE_RESET = "@@teacher_absence/DELETE_TEACHER_ABSENCE_RESET",
}

export type UseTeacherAbsence = {
  teacherAbsenceState: TeacherAbsenceState;
  getTeacherAbsence: (
    limit: number,
    offset: number,
    teacher_id: string,
    absence_date?: Date,
    be_reasonable?: string,
  ) => void;
  createTeacherAbsence: (teacherAbsence: CreateTeacherAbsenceForm) => void;
  createTeacherAbsenceReset: () => void;
  updateTeacherAbsence: (
    _id: string,
    teacherAbsence: UpdateTeacherAbsenceForm
  ) => void;
  updateTeacherAbsenceReset: () => void;
  deleteTeacherAbsence: (_id: string) => void;
  deleteTeacherAbsenceReset: () => void;
};

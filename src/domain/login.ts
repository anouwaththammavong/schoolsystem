import moment, { Moment } from "moment";

export type Gender = "male" | "female" | "other";
export type GenderHold = "male" | "female" | "other" | "";

export interface IAuth {
	identified: string;
	password: string;
}
export interface ILoginAuthHold {
	identified: string;
	password: string;
}
export interface IRegisterHold {
	first_name: string;
	last_name: string;
	email?: string;
	phone: string;
	birth_day: string;
	gender: GenderHold;
	password: string;
	confirm_password: string;
}
export interface IPatchProfile {
	first_name: string;
	last_name: string;
	email?: string;
	phone: string;
	birth_day: string;
	gender: GenderHold;
}
export interface Token {
	// accessToken: string;
	token: string;
	refreshToken: string;
}

export interface IVerifyOtp {
	phone: string;
	code: string;
}
export interface IVerifyOtp {
	phone: string;
	code: string;
}

export interface IUser {
	id?: string;
	first_name: string;
	last_name: string;
	phone: string;
	email: string;
	birth_day: string;
	gender: Gender;
	role: "admin" | "organizer" | "visitor";
	enable: boolean;
	disabled_by?: string;
	created_at?: Moment;
	created_by: string;
	updated_at?: Moment;
	updated_by: string;
	password?: string;
}
export interface IVerifyOtp {
	phone: string;
	code: string;
}

export interface IUser {
	id?: string;
	first_name: string;
	last_name: string;
	phone: string;
	email: string;
	birth_day: string;
	gender: Gender;
	role: "admin" | "organizer" | "visitor";
	enable: boolean;
	photo: string;
	event_total: number;
	staff_total: number;
	disabled_by?: string;
	created_at?: Moment;
	created_by: string;
	updated_at?: Moment;
	updated_by: string;
	password?: string;
}

export interface AuthState {
	readonly token: Token;
	readonly iLoginAuthHold: ILoginAuthHold;
	readonly registerHold: IRegisterHold;
	readonly isLoginLoading: boolean;
	readonly isLoginSuccess: boolean;
	readonly loginError: {
		isError: boolean;
		message?: string;
	};
	readonly isRegisterLoading: boolean;
	readonly isRegisterSuccess: boolean;
	readonly registerError: {
		isError: boolean;
		message?: string;
	};
	readonly isVerifyOtpLoading: boolean;
	readonly isVerifyOtpSuccess: boolean;
	readonly verifyOtpError: {
		isError: boolean;
		message?: string;
	};
	readonly profileHold?: IPatchProfile;
	readonly isUpdateProfileLoading: boolean;
	readonly isUpdateProfileSuccess: boolean;
	readonly updateProfileError: {
		isError: boolean;
		message?: string;
	};
	readonly isGetMeLoading: boolean;
	readonly isGetMeSuccess: boolean;
	readonly getMeError: {
		isError: boolean;
		message?: string;
	};
	readonly profile?: IUser;
}

export const initialState: AuthState = {
	token: {
		token: "",
		// accessToken: "",
		refreshToken: "",
	},
	iLoginAuthHold: {
		identified: "",
		password: "",
	},
	registerHold: {
		first_name: "",
		last_name: "",
		phone: "",
		email: "",
		birth_day: moment().subtract(7, "year").toString(),
		gender: "female",
		password: "",
		confirm_password: "",
	},
	isLoginLoading: false,
	isLoginSuccess: false,
	loginError: {
		isError: false,
	},
	isRegisterLoading: false,
	isRegisterSuccess: false,
	registerError: {
		isError: false,
	},
	isVerifyOtpLoading: false,
	isVerifyOtpSuccess: false,
	verifyOtpError: {
		isError: false,
	},
	isUpdateProfileLoading: false,
	isUpdateProfileSuccess: false,
	updateProfileError: {
		isError: false,
	},
	isGetMeLoading: false,
	isGetMeSuccess: false,
	getMeError: {
		isError: false,
	},
};

export enum AuthActionTypes {
	LOGIN_AUTH_REQUEST = "@@auth/LOGIN_AUTH_REQUEST",
	LOGIN_AUTH_SUCCESS = "@@auth/LOGIN_AUTH_SUCCESS",
	LOGIN_AUTH_ERROR = "@@auth/LOGIN_AUTH_ERROR",
	REGISTER_REQUEST = "@@auth/REGISTER_REQUEST",
	REGISTER_SUCCESS = "@@auth/REGISTER_SUCCESS",
	REGISTER_ERROR = "@@auth/REGISTER_ERROR",
	UPDATE_REGISTER_HOLD = "@@auth/UPDATE_REGISTER_HOLD",
	VERIFY_OTP_REQUEST = "@@auth/VERIFY_OTP_REQUEST",
	VERIFY_OTP_SUCCESS = "@@auth/VERIFY_OTP_SUCCESS",
	VERIFY_OTP_ERROR = "@@auth/VERIFY_OTP_ERROR",
	SEND_OTP_FOR_RESET_PASSWORD_REQUEST = "@@auth/SEND_OTP_FOR_RESET_PASSWORD_REQUEST",
	SEND_OTP_FOR_RESET_PASSWORD_SUCCESS = "@@auth/SEND_OTP_FOR_RESET_PASSWORD_SUCCESS",
	SEND_OTP_FOR_RESET_PASSWORD_ERROR = "@@auth/SEND_OTP_FOR_RESET_PASSWORD_ERROR",
	VERIFY_OTP_FOR_RESET_PASSWORD_REQUEST = "@@auth/VERIFY_OTP_FOR_RESET_PASSWORD_REQUEST",
	VERIFY_OTP_FOR_RESET_PASSWORD_SUCCESS = "@@auth/VERIFY_OTP_FOR_RESET_PASSWORD_SUCCESS",
	VERIFY_OTP_FOR_RESET_PASSWORD_ERROR = "@@auth/VERIFY_OTP_FOR_RESET_PASSWORD_ERROR",
	GET_ME_REQUEST = "@@auth/GET_ME_REQUEST",
	GET_ME_SUCCESS = "@@auth/GET_ME_SUCCESS",
	GET_ME_ERROR = "@@auth/GET_ME_ERROR",
	LOGOUT = "@@auth/LOGOUT",
}

export type UseLogin = {
	profile?: IUser;
	getMeLoading: boolean;
	login: (auth: IAuth) => void;
	updateRegisterHold: (register: IRegisterHold) => void;
	register: (register: IRegisterHold) => void;
	verifyOtp: (verifyOtp: IVerifyOtp) => void;
	getMe: () => void;
	logout: () => void;
  };
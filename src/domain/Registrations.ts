export type Registrations = {
  registrations: RegistrationsClass;
};

export type RegistrationsClass = {
  registrations: Registration[];
  totalCount: number;
};

export type Registration = {
  _id: string;
  registration_id: string;
  student_id: StudentID;
  room_id: RoomID;
  level_id: LevelID;
  academic_year_no: AcademicYearNo;
  school_fee_id: RegistrationID;
  registration_date: Date;
  isPaid: string;
  __v: number;
};

export type AcademicYearNo = {
  _id: string;
  academic_year: string;
  academic_year_no: string;
  __v: number;
};

export type LevelID = {
  _id: string;
  level_id: string;
  level_name: string;
  __v: number;
};

export type RoomID = {
  _id: string;
  level_id: string;
  room_id: string;
  room_name: string;
  __v: number;
};

export type RegistrationID = {
  _id: string;
  school_fee_id: string;
  academic_year_no: string;
  level_id: string;
  book_fee: number;
  uniform_fee: number;
  term_fee: number;
  total_fee: number;
  __v: number;
};

export type StudentID = {
  _id: string;
  student_id: string;
  name: string;
  last_name: string;
  age: string;
  student_tel: string;
  date_of_birth: Date;
  student_village: string;
  student_district: string;
  student_province: string;
  nationality: string;
  father_name: string;
  father_job: string;
  mother_name: string;
  mother_job: string;
  father_tel: string;
  mother_tel: string;
  student_past_school: string;
  student_number: string;
  room_id: RoomID;
  level_id: LevelID;
  status: string;
  note: string;
  school_entry_date: Date;
  gender: string;
  generation: string;
  __v: number;
  new_student: boolean;
};

export type CreateOldStudentRegistration = {
  registration_id: string;
  student_id: string;
  room_id: string;
  level_id: string;
  academic_year_no: string;
  school_fee_id: string;
  registration_date: Date;
  isPaid: string;
};

export type CreateNewStudentRegistration = {
  registration_id: string;
  student_id: string;
  academic_year_no: string;
  school_fee_id: string;
  registration_date: Date;
  isPaid: string;
};

export type UpdateRegistration = {
  registration_id: string;
  student_id: string;
  room_id: string;
  level_id: string;
  academic_year_no: string;
  school_fee_id: string;
  registration_date: Date;
  isPaid: string;
};

export type UpdateNewRegistration = {
  registration_id: string;
  student_id: string;
  academic_year_no: string;
  school_fee_id: string;
  registration_date: Date;
  isPaid: string;
};

export interface RegistrationState {
  readonly registrations: Registrations;
  // GET  Registration
  readonly isGetRegistrationsLoading: boolean;
  readonly isGetRegistrationsSuccess: boolean;
  readonly getRegistrationsError: {
    isError: boolean;
    message?: string;
  };
  // Create Old Student Registration
  readonly isCreateOldStudentRegistrationLoading: boolean;
  readonly isCreateOldStudentRegistrationSuccess: boolean;
  readonly createOldStudentRegistrationError: {
    isError: boolean;
    message?: string;
  };
  // Create New Student Registration
  readonly isCreateNewStudentRegistrationLoading: boolean;
  readonly isCreateNewStudentRegistrationSuccess: boolean;
  readonly createNewStudentRegistrationError: {
    isError: boolean;
    message?: string;
  };
  // update Registration
  readonly isUpdateRegistrationLoading: boolean;
  readonly isUpdateRegistrationSuccess: boolean;
  readonly updateRegistrationError: {
    isError: boolean;
    message?: string;
  };
  // update New Registration
  readonly isUpdateNewRegistrationLoading: boolean;
  readonly isUpdateNewRegistrationSuccess: boolean;
  readonly updateNewRegistrationError: {
    isError: boolean;
    message?: string;
  };
  // delete Registration
  readonly isDeleteRegistrationLoading: boolean;
  readonly isDeleteRegistrationSuccess: boolean;
  readonly deleteRegistrationError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: RegistrationState = {
  registrations: {
    registrations: {
      registrations: [],
      totalCount: 0,
    },
  },
  isGetRegistrationsLoading: false,
  isGetRegistrationsSuccess: false,
  getRegistrationsError: {
    isError: false,
  },
  isCreateOldStudentRegistrationLoading: false,
  isCreateOldStudentRegistrationSuccess: false,
  createOldStudentRegistrationError: {
    isError: false,
  },
  isCreateNewStudentRegistrationLoading: false,
  isCreateNewStudentRegistrationSuccess: false,
  createNewStudentRegistrationError: {
    isError: false,
  },
  isUpdateRegistrationLoading: false,
  isUpdateRegistrationSuccess: false,
  updateRegistrationError: {
    isError: false,
  },
  isUpdateNewRegistrationLoading: false,
  isUpdateNewRegistrationSuccess: false,
  updateNewRegistrationError: {
    isError: false,
  },
  isDeleteRegistrationLoading: false,
  isDeleteRegistrationSuccess: false,
  deleteRegistrationError: {
    isError: false,
  },
};

export enum RegistrationActionTypes {
  // Get School fee
  GET_REGISTRATIONS_REQUEST = "@@registration/GET_REGISTRATIONS_REQUEST",
  GET_REGISTRATIONS_SUCCESS = "@@registration/GET_REGISTRATIONS_SUCCESS",
  GET_REGISTRATIONS_ERROR = "@@registration/GET_REGISTRATIONS_ERROR",
  CREATE_OLD_STUDENT_REGISTRATION_REQUEST = "@@registration/CREATE_OLD_STUDENT_REGISTRATION_REQUEST",
  CREATE_OLD_STUDENT_REGISTRATION_SUCCESS = "@@registration/CREATE_OLD_STUDENT_REGISTRATION_SUCCESS",
  CREATE_OLD_STUDENT_REGISTRATION_ERROR = "@@registration/CREATE_OLD_STUDENT_REGISTRATION_ERROR",
  CREATE_OLD_STUDENT_REGISTRATION_RESET = "@@registration/CREATE_OLD_STUDENT_REGISTRATION_RESET",
  CREATE_NEW_STUDENT_REGISTRATION_REQUEST = "@@registration/CREATE_NEW_STUDENT_REGISTRATION_REQUEST",
  CREATE_NEW_STUDENT_REGISTRATION_SUCCESS = "@@registration/CREATE_NEW_STUDENT_REGISTRATION_SUCCESS",
  CREATE_NEW_STUDENT_REGISTRATION_ERROR = "@@registration/CREATE_NEW_STUDENT_REGISTRATION_ERROR",
  CREATE_NEW_STUDENT_REGISTRATION_RESET = "@@registration/CREATE_NEW_STUDENT_REGISTRATION_RESET",
  UPDATE_REGISTRATION_REQUEST = "@@registration/UPDATE_REGISTRATION_REQUEST",
  UPDATE_REGISTRATION_SUCCESS = "@@registration/UPDATE_REGISTRATION_SUCCESS",
  UPDATE_REGISTRATION_ERROR = "@@registration/UPDATE_REGISTRATION_ERROR",
  UPDATE_REGISTRATION_RESET = "@@registration/UPDATE_REGISTRATION_RESET",
  UPDATE_NEW_REGISTRATION_REQUEST = "@@registration/UPDATE_NEW_REGISTRATION_REQUEST",
  UPDATE_NEW_REGISTRATION_SUCCESS = "@@registration/UPDATE_NEW_REGISTRATION_SUCCESS",
  UPDATE_NEW_REGISTRATION_ERROR = "@@registration/UPDATE_NEW_REGISTRATION_ERROR",
  UPDATE_NEW_REGISTRATION_RESET = "@@registration/UPDATE_NEW_REGISTRATION_RESET",
  DELETE_REGISTRATION_REQUEST = "@@registration/DELETE_REGISTRATION_REQUEST",
  DELETE_REGISTRATION_SUCCESS = "@@registration/DELETE_REGISTRATION_SUCCESS",
  DELETE_REGISTRATION_ERROR = "@@registration/DELETE_REGISTRATION_ERROR",
  DELETE_REGISTRATION_RESET = "@@registration/DELETE_REGISTRATION_RESET",
}

export type UseRegistration = {
  registrationState: RegistrationState;
  getRegistrations: (
    limit: number,
    offset: number,
    new_student: boolean,
    student_id?: string,
    academic_year_no?: string,
    room_id?: string,
    isPaid?: string
  ) => void;
  createOldStudentRegistration: (
    registration: CreateOldStudentRegistration
  ) => void;
  createOldStudentRegistrationReset: () => void;
  createNewStudentRegistration: (
    registration: CreateNewStudentRegistration
  ) => void;
  createNewStudentRegistrationReset: () => void;
  updateRegistration: (_id: string, registration: UpdateRegistration) => void;
  updateRegistrationReset: () => void;
  updateNewRegistration: (
    _id: string,
    registration: UpdateNewRegistration
  ) => void;
  updateNewRegistrationReset: () => void;
  deleteRegistration: (_id: string) => void;
  deleteRegistrationReset: () => void;
};

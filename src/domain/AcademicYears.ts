export type AcademicYears = {
    academicYears: AcademicYearsClass;
}

export type AcademicYearsClass = {
    academicYear:       AcademicYear[];
    totalAcademicYears: number;
}

export type AcademicYear = {
    _id:              string;
    academic_year:    string;
    academic_year_no: string;
}

export type CreateAcademicYear = {
    academic_year: string,
    academic_year_no: string
}

export type UpdateAcademicYear = {
    academic_year: string,
    academic_year_no: string
}

export interface AcademicYearState {
    readonly academic_years: AcademicYears;
    // GET  AcademicYear
    readonly isGetAcademicYearsLoading: boolean;
    readonly isGetAcademicYearsSuccess: boolean;
    readonly getAcademicYearsError: {
      isError: boolean;
      message?: string;
    };
    // Create AcademicYear
    readonly isCreateAcademicYearLoading: boolean;
    readonly isCreateAcademicYearSuccess: boolean;
    readonly createAcademicYearError: {
      isError: boolean;
      message?: string;
    };
    // update AcademicYear
    readonly isUpdateAcademicYearLoading: boolean;
    readonly isUpdateAcademicYearSuccess: boolean;
    readonly updateAcademicYearError: {
      isError: boolean;
      message?: string;
    };
    // delete AcademicYear
    readonly isDeleteAcademicYearLoading: boolean;
    readonly isDeleteAcademicYearSuccess: boolean;
    readonly deleteAcademicYearError: {
      isError: boolean;
      message?: string;
    };
  }

  export const initialState: AcademicYearState = {
    academic_years: {
      academicYears: {
        academicYear: [],
        totalAcademicYears: 0,
      },
    },
    isGetAcademicYearsLoading: false,
    isGetAcademicYearsSuccess: false,
    getAcademicYearsError: {
      isError: false,
    },
    isCreateAcademicYearLoading: false,
    isCreateAcademicYearSuccess: false,
    createAcademicYearError: {
      isError: false,
    },
    isUpdateAcademicYearLoading: false,
    isUpdateAcademicYearSuccess: false,
    updateAcademicYearError: {
      isError: false,
    },
    isDeleteAcademicYearLoading: false,
    isDeleteAcademicYearSuccess: false,
    deleteAcademicYearError: {
      isError: false,
    },
  };

  export enum AcademicYearActionTypes {
    // Get Academic Year
    GET_ACADEMIC_YEARS_REQUEST = "@@academic_year/GET_ACADEMIC_YEARS_REQUEST",
    GET_ACADEMIC_YEARS_SUCCESS = "@@academic_year/GET_ACADEMIC_YEARS_SUCCESS",
    GET_ACADEMIC_YEARS_ERROR = "@@academic_year/GET_ACADEMIC_YEARS_ERROR",
    CREATE_ACADEMIC_YEAR_REQUEST = "@@academic_year/CREATE_ACADEMIC_YEAR_REQUEST",
    CREATE_ACADEMIC_YEAR_SUCCESS = "@@academic_year/CREATE_ACADEMIC_YEAR_SUCCESS",
    CREATE_ACADEMIC_YEAR_ERROR = "@@academic_year/CREATE_ACADEMIC_YEAR_ERROR",
    CREATE_ACADEMIC_YEAR_RESET = "@@academic_year/CREATE_ACADEMIC_YEAR_RESET",
    UPDATE_ACADEMIC_YEAR_REQUEST = "@@academic_year/UPDATE_ACADEMIC_YEAR_REQUEST",
    UPDATE_ACADEMIC_YEAR_SUCCESS = "@@academic_year/UPDATE_ACADEMIC_YEAR_SUCCESS",
    UPDATE_ACADEMIC_YEAR_ERROR = "@@academic_year/UPDATE_ACADEMIC_YEAR_ERROR",
    UPDATE_ACADEMIC_YEAR_RESET = "@@academic_year/UPDATE_ACADEMIC_YEAR_RESET",
    DELETE_ACADEMIC_YEAR_REQUEST = "@@academic_year/DELETE_ACADEMIC_YEAR_REQUEST",
    DELETE_ACADEMIC_YEAR_SUCCESS = "@@academic_year/DELETE_ACADEMIC_YEAR_SUCCESS",
    DELETE_ACADEMIC_YEAR_ERROR = "@@academic_year/DELETE_ACADEMIC_YEAR_ERROR",
    DELETE_ACADEMIC_YEAR_RESET = "@@academic_year/DELETE_ACADEMIC_YEAR_RESET",
  }

  export type UseAcademicYear = {
    academicYearState: AcademicYearState;
    getAcademicYears: (
      limit: number,
      offset: number,
    ) => void;
    createAcademicYear: (academicYear: CreateAcademicYear) => void;
    createAcademicYearReset: () => void;
    updateAcademicYear: (_id: string, academicYear: UpdateAcademicYear) => void;
    updateAcademicYearReset: () => void;
    deleteAcademicYear: (_id: string) => void;
    deleteAcademicYearReset: () => void;
  };
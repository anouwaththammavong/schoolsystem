import { User } from "./user";

export interface IProfile extends User { };

export interface IChangePasswordHold {
	oldPassword: string;
	newPassword: string;
}

export interface IAuth {
	identified: string;
	password: string;
}

export interface ILoginAuthHold {
	identified: string;
	password: string;
}

export interface Token {
	// accessToken: string;
	accessToken: string;
	refreshToken: string;
}

export interface IUser extends User { };

export interface AuthState {
	readonly token?: Token;
	readonly iLoginAuthHold: ILoginAuthHold;
	readonly isLoginLoading: boolean;
	readonly isLoginSuccess: boolean;
	readonly loginError: {
		isError: boolean;
		message?: string;
	};
}

export interface ProfileState {
	readonly token?: Token;
	readonly iLoginAuthHold: ILoginAuthHold;
	readonly isLoginLoading: boolean;
	readonly isLoginSuccess: boolean;
	readonly loginError: {
		isError: boolean;
		message?: string;
	};
	readonly profile?: IProfile;
	readonly isGetMeLoading: boolean;
	readonly isGetMeSuccess: boolean;
	readonly getMeError: {
		isError: boolean;
		message?: string;
	};
	readonly isChangePasswordLoading: boolean;
	readonly isChangePasswordSuccess: boolean;
	readonly changePasswordError: {
		isError: boolean;
		message?: string;
	};
}

export const initialState: ProfileState = {
	iLoginAuthHold: {
		identified: "",
		password: "",
	},
	isLoginLoading: false,
	isLoginSuccess: false,
	loginError: {
		isError: false,
	},
	isGetMeLoading: false,
	isGetMeSuccess: false,
	getMeError: {
		isError: false,
	},
	isChangePasswordLoading: false,
	isChangePasswordSuccess: false,
	changePasswordError: {
		isError: false,
	},
};

export enum ProfileActionTypes {
	LOGIN_AUTH_REQUEST = "@@auth/LOGIN_AUTH_REQUEST",
	LOGIN_AUTH_SUCCESS = "@@auth/LOGIN_AUTH_SUCCESS",
	LOGIN_AUTH_ERROR = "@@auth/LOGIN_AUTH_ERROR",
	GET_ME_REQUEST = "@@auth/GET_ME_REQUEST",
	GET_ME_SUCCESS = "@@auth/GET_ME_SUCCESS",
	GET_ME_ERROR = "@@auth/GET_ME_ERROR",
	CHANGE_PASSWORD_REQUEST = "@@auth/CHANGE_PASSWORD_REQUEST",
	CHANGE_PASSWORD_SUCCESS = "@@auth/CHANGE_PASSWORD_SUCCESS",
	CHANGE_PASSWORD_ERROR = "@@auth/CHANGE_PASSWORD_ERROR",
	LOGOUT = "@@auth/LOGOUT",
}

export type UseProfile = {
	profileState: ProfileState;
	login: (auth: IAuth) => void;
	getMe: () => void;
	changePassword: (changePasswordHold: IChangePasswordHold) => void;
	logout: () => void;
};

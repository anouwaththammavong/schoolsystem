export type StudentAbsence = {
  studentAbsence: PurpleStudentAbsence;
};

export type PurpleStudentAbsence = {
  studentAbsence: FluffyStudentAbsence;
};

export type FluffyStudentAbsence = {
  studentAbsence: TentacledStudentAbsence[];
  totalStudentsAbsent: number;
};

export type TentacledStudentAbsence = {
  _id:               string;
  student_id:        string;
  name:              string;
  last_name:         string;
  age:               string;
  student_number:    string;
  date_of_birth:     Date;
  room_id:           RoomID;
  level_id:          LevelID;
  status:            string;
  school_entry_date: Date;
  gender:            string;
  generation:        string;
  note:              string;
  studentAbsence: StickyStudentAbsence[];
};

export type LevelID = {
  _id:        string;
  level_id:   string;
  level_name: string;
}

export type RoomID = {
  _id:       string;
  level_id:  string;
  room_id:   string;
  room_name: string;
}

export type StickyStudentAbsence = {
  _id: string;
  student_absence_term1: number;
  student_absence_term2: number;
  student_sum_all_term1_and_term2: number;
};

export type StudentIdAbsence = {
  studentAbsence: StudentAbsenceDetail;
}

export type StudentAbsenceDetail = {
  absences:              Absence[];
  totalAbsences:         number;
  totalBe_reasonable:    number;
  totalNotBe_reasonable: number;
}

export type Absence = {
  _id:            string;
  student:        Student;
  room_id:    RoomIDStudentAbsenceDetail;
  be_reasonable:  string;
  absence_allday: string;
  absence_date:   Date;
  term:           string;
  note:           string;
}

export type Student = {
  _id:                 string;
  student_id:          string;
  name:                string;
  last_name:           string;
  age:                 string;
  student_tel:         string;
  date_of_birth:       Date;
  student_village:     string;
  student_district:    string;
  student_province:    string;
  nationality:         string;
  father_name:         string;
  father_job:          string;
  mother_name:         string;
  mother_job:          string;
  father_tel:          string;
  mother_tel:          string;
  student_past_school: string;
  student_number:      string;
  room_id:             string;
  level_id:            string;
  status:              string;
  note:                string;
  school_entry_date:   Date;
  gender:              string;
  generation:          string;
  __v:                 number;
  new_student:         boolean;
}

export type RoomIDStudentAbsenceDetail = {
  _id:       string;
  level_id:  LevelID;
  room_id:   string;
  room_name: string;
}

export type CreateStudentAbsenceForm = {
  student: string;
  room_id: string;
  be_reasonable: string;
  absence_allday: string;
  absence_date: Date;
  term: string;
  // generation: string;
  note: string;
};

export type UpdateStudentAbsenceForm = {
  be_reasonable: string;
  absence_allday: string;
  absence_date: Date;
  term: string;
  note: string;
};

export interface StudentAbsenceState {
  // Get All Student Absence
  readonly studentAbsence: StudentAbsence;
  // Get Student Id Absence
  readonly studentIdAbsence: StudentIdAbsence;
  // GET All Student Absences
  readonly isGetAllStudentAbsencesLoading: boolean;
  readonly isGetAllStudentAbsencesSuccess: boolean;
  readonly getAllStudentAbsencesError: {
    isError: boolean;
    message?: string;
  };
  // GET Student Id Absences
  readonly isGetStudentIdAbsencesLoading: boolean;
  readonly isGetStudentIdAbsencesSuccess: boolean;
  readonly getStudentIdAbsencesError: {
    isError: boolean;
    message?: string;
  };
  // Create Student Absences
  readonly isCreateStudentAbsenceLoading: boolean;
  readonly isCreateStudentAbsenceSuccess: boolean;
  readonly createStudentAbsenceError: {
    isError: boolean;
    message?: string;
  };
  // update Student Absences
  readonly isUpdateStudentAbsenceLoading: boolean;
  readonly isUpdateStudentAbsenceSuccess: boolean;
  readonly updateStudentAbsenceError: {
    isError: boolean;
    message?: string;
  };
  // delete Student Absences
  readonly isDeleteStudentAbsenceLoading: boolean;
  readonly isDeleteStudentAbsenceSuccess: boolean;
  readonly deleteStudentAbsenceError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: StudentAbsenceState = {
  studentAbsence: {
    studentAbsence: {
      studentAbsence: {
        studentAbsence: [],
        totalStudentsAbsent: 0,
      },
    },
  },
  studentIdAbsence: {
   studentAbsence: {
    absences:[],
    totalAbsences:0,
    totalBe_reasonable:0,
    totalNotBe_reasonable:0
   }
  },
  isGetAllStudentAbsencesLoading: false,
  isGetAllStudentAbsencesSuccess: false,
  getAllStudentAbsencesError: {
    isError: false,
  },
  isGetStudentIdAbsencesLoading: false,
  isGetStudentIdAbsencesSuccess: false,
  getStudentIdAbsencesError: {
    isError: false,
  },
  isCreateStudentAbsenceLoading: false,
  isCreateStudentAbsenceSuccess: false,
  createStudentAbsenceError: {
    isError: false,
  },
  isUpdateStudentAbsenceLoading: false,
  isUpdateStudentAbsenceSuccess: false,
  updateStudentAbsenceError: {
    isError: false,
  },
  isDeleteStudentAbsenceLoading: false,
  isDeleteStudentAbsenceSuccess: false,
  deleteStudentAbsenceError: {
    isError: false,
  },
};

export enum StudentAbsenceActionTypes {
  GET_ALL_STUDENT_ABSENCES_REQUEST = "@@student_absence/GET_ALL_STUDENT_ABSENCES_REQUEST",
  GET_ALL_STUDENT_ABSENCES_SUCCESS = "@@student_absence/GET_ALL_STUDENT_ABSENCES_SUCCESS",
  GET_ALL_STUDENT_ABSENCES_ERROR = "@@student_absence/GET_ALL_STUDENT_ABSENCES_ERROR",
  // Get Student Id Absence
  GET_STUDENT_ID_ABSENCES_REQUEST = "@@student_absence/GET_STUDENT_ID_ABSENCES_REQUEST",
  GET_STUDENT_ID_ABSENCES_SUCCESS = "@@student_absence/GET_STUDENT_ID_ABSENCES_SUCCESS",
  GET_STUDENT_ID_ABSENCES_ERROR = "@@student_absence/GET_STUDENT_ID_ABSENCES_ERROR",
  CREATE_STUDENT_ABSENCE_REQUEST = "@@student_absence/CREATE_STUDENT_ABSENCE_REQUEST",
  CREATE_STUDENT_ABSENCE_SUCCESS = "@@student_absence/CREATE_STUDENT_ABSENCE_SUCCESS",
  CREATE_STUDENT_ABSENCE_ERROR = "@@student_absence/CREATE_STUDENT_ABSENCE_ERROR",
  CREATE_STUDENT_ABSENCE_RESET = "@@student_absence/CREATE_STUDENT_ABSENCE_RESET",
  UPDATE_STUDENT_ABSENCE_REQUEST = "@@student_absence/UPDATE_STUDENT_ABSENCE_REQUEST",
  UPDATE_STUDENT_ABSENCE_SUCCESS = "@@student_absence/UPDATE_STUDENT_ABSENCE_SUCCESS",
  UPDATE_STUDENT_ABSENCE_ERROR = "@@student_absence/UPDATE_STUDENT_ABSENCE_ERROR",
  UPDATE_STUDENT_ABSENCE_RESET = "@@student_absence/UPDATE_STUDENT_ABSENCE_RESET",
  DELETE_STUDENT_ABSENCE_REQUEST = "@@student_absence/DELETE_STUDENT_ABSENCE_REQUEST",
  DELETE_STUDENT_ABSENCE_SUCCESS = "@@student_absence/DELETE_STUDENT_ABSENCE_SUCCESS",
  DELETE_STUDENT_ABSENCE_ERROR = "@@student_absence/DELETE_STUDENT_ABSENCE_ERROR",
  DELETE_STUDENT_ABSENCE_RESET = "@@student_absence/DELETE_STUDENT_ABSENCE_RESET",
}

export type UseStudentAbsence = {
  studentAbsenceState: StudentAbsenceState;
  getAllStudentAbsences: (
    limit: number,
    offset: number,
    room_id: string,
    generation: string,
    student_id?: string
  ) => void;
  getStudentIdAbsences: (
    limit: number,
    offset: number,
    student_id: string,
    room_id: string,
    absence_date?: Date,
    term?: string,
    be_reasonable?: string,
  ) => void;
  createStudentAbsence: (studentAbsence: CreateStudentAbsenceForm) => void;
  createStudentAbsenceReset: () => void;
  updateStudentAbsence: (
    _id: string,
    studentAbsence: UpdateStudentAbsenceForm
  ) => void;
  updateStudentAbsenceReset: () => void;
  deleteStudentAbsence: (_id: string) => void;
  deleteStudentAbsenceReset: () => void;
};

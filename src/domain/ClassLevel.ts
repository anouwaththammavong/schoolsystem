export type ClassLevels = {
  classLevels: ClassLevelsClass;
};

export type ClassLevelsClass = {
  classLevels: ClassLevel[];
  totalCount: number;
};

export type ClassLevel = {
  _id: string;
  level_id: string;
  level_name: string;
};

export type CreateClassLevelForm = {
  level_id: string;
  level_name: string;
};

export type UpdateClassLevelForm = {
  level_id: string;
  level_name: string;
};

export interface ClassLevelState {
  readonly classLevels: ClassLevels;
  // GET ClassLevel
  readonly isGetClassLevelsLoading: boolean;
  readonly isGetClassLevelsSuccess: boolean;
  readonly getClassLevelsError: {
    isError: boolean;
    message?: string;
  };
  // Create ClassLevel
  readonly isCreateClassLevelLoading: boolean;
  readonly isCreateClassLevelSuccess: boolean;
  readonly createClassLevelError: {
    isError: boolean;
    message?: string;
  };
  // update ClassLevel
  readonly isUpdateClassLevelLoading: boolean;
  readonly isUpdateClassLevelSuccess: boolean;
  readonly updateClassLevelError: {
    isError: boolean;
    message?: string;
  };
  // delete ClassLevel
  readonly isDeleteClassLevelLoading: boolean;
  readonly isDeleteClassLevelSuccess: boolean;
  readonly deleteClassLevelError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: ClassLevelState = {
  classLevels: {
    classLevels: {
      classLevels: [],
      totalCount: 0,
    },
  },
  isGetClassLevelsLoading: false,
  isGetClassLevelsSuccess: false,
  getClassLevelsError: {
    isError: false,
  },
  isCreateClassLevelLoading: false,
  isCreateClassLevelSuccess: false,
  createClassLevelError: {
    isError: false,
  },
  isUpdateClassLevelLoading: false,
  isUpdateClassLevelSuccess: false,
  updateClassLevelError: {
    isError: false,
  },
  isDeleteClassLevelLoading: false,
  isDeleteClassLevelSuccess: false,
  deleteClassLevelError: {
    isError: false,
  },
};

export enum ClassLevelActionTypes {
  GET_CLASS_LEVELS_REQUEST = "@@class_level/GET_CLASS_LEVELS_REQUEST",
  GET_CLASS_LEVELS_SUCCESS = "@@class_level/GET_CLASS_LEVELS_SUCCESS",
  GET_CLASS_LEVELS_ERROR = "@@class_level/GET_CLASS_LEVELS_ERROR",
  CREATE_CLASS_LEVEL_REQUEST = "@@class_level/CREATE_CLASS_LEVEL_REQUEST",
  CREATE_CLASS_LEVEL_SUCCESS = "@@class_level/CREATE_CLASS_LEVEL_SUCCESS",
  CREATE_CLASS_LEVEL_ERROR = "@@class_level/CREATE_CLASS_LEVEL_ERROR",
  CREATE_CLASS_LEVEL_RESET = "@@class_level/CREATE_CLASS_LEVEL_RESET",
  UPDATE_CLASS_LEVEL_REQUEST = "@@class_level/UPDATE_CLASS_LEVEL_REQUEST",
  UPDATE_CLASS_LEVEL_SUCCESS = "@@class_level/UPDATE_CLASS_LEVEL_SUCCESS",
  UPDATE_CLASS_LEVEL_ERROR = "@@class_level/UPDATE_CLASS_LEVEL_ERROR",
  UPDATE_CLASS_LEVEL_RESET = "@@class_level/UPDATE_CLASS_LEVEL_RESET",
  DELETE_CLASS_LEVEL_REQUEST = "@@class_level/DELETE_CLASS_LEVEL_REQUEST",
  DELETE_CLASS_LEVEL_SUCCESS = "@@class_level/DELETE_CLASS_LEVEL_SUCCESS",
  DELETE_CLASS_LEVEL_ERROR = "@@class_level/DELETE_CLASS_LEVEL_ERROR",
  DELETE_CLASS_LEVEL_RESET = "@@class_level/DELETE_CLASS_LEVEL_RESET",
}

export type UseClassLevel = {
  classLevelState: ClassLevelState;
  getClassLevels: (limit: number, offset: number) => void;
  createClassLevel: (classLevel: CreateClassLevelForm) => void;
  createClassLevelReset: () => void;
  updateClassLevel: (_id: string, classLevel: UpdateClassLevelForm) => void;
  updateClassLevelReset: () => void;
  deleteClassLevel: (_id: string) => void;
  deleteClassLevelReset: () => void;
};

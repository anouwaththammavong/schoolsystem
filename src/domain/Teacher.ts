export type Teacher = {
  teachers: Teachers;
};

export type Teachers = {
  teachers: TeacherElement[];
  totalTeachers: number;
};

export type TeacherElement = {
  _id: string;
  teacher_id: string;
  name: string;
  last_name: string;
  age: string;
  teacher_tel: string;
  date_of_birth: Date;
  teacher_village: string;
  teacher_district: string;
  teacher_province: string;
  nationality: string;
  graduated_from_institute: string;
  gender: string;
  degree: string;
  subjects: Subject[];
  class_teacher: ClassTeacher;
};

export type ClassTeacher = {
  _id: string;
  level_id: LevelID;
  room_id: string;
  room_name: string;
};

export type LevelID = {
  _id: string;
  level_id: string;
  level_name: string;
};

export type Subject = {
  title: Title;
  _id: string;
};

export type Title = {
  _id: string;
  subject_id: string;
  subject_name: string;
};

// Create Teacher
export type CreateTeacher = {
  teacher_id: string;
  name: string;
  last_name: string;
  age: string;
  teacher_tel: string;
  date_of_birth: string;
  teacher_village: string;
  teacher_district: string;
  teacher_province: string;
  nationality: string;
  graduated_from_institute: string;
  gender: string;
  degree: string;
  subjects: createSubjectTeacher[];
  class_teacher?: string;
};

export type createSubjectTeacher = {
  title: string;
};

export interface TeacherState {
  readonly teachers: Teacher;
  // GET Teacher
  readonly isGetTeachersLoading: boolean;
  readonly isGetTeachersSuccess: boolean;
  readonly getTeachersError: {
    isError: boolean;
    message?: string;
  };
  // Create Teacher
  readonly isCreateTeacherLoading: boolean;
  readonly isCreateTeacherSuccess: boolean;
  readonly CreateTeacherError: {
    isError: boolean;
    message?: string;
  };
  // update Teacher
  readonly isUpdateTeacherLoading: boolean;
  readonly isUpdateTeacherSuccess: boolean;
  readonly updateTeacherError: {
    isError: boolean;
    message?: string;
  };
  // delete Teacher
  readonly isDeleteTeacherLoading: boolean;
  readonly isDeleteTeacherSuccess: boolean;
  readonly deleteTeacherError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: TeacherState = {
  teachers: {
    teachers: {
      teachers: [],
      totalTeachers: 0,
    },
  },
  isGetTeachersLoading: false,
  isGetTeachersSuccess: false,
  getTeachersError: {
    isError: false,
  },
  isCreateTeacherLoading: false,
  isCreateTeacherSuccess: false,
  CreateTeacherError: {
    isError: false,
  },
  isUpdateTeacherLoading: false,
  isUpdateTeacherSuccess: false,
  updateTeacherError: {
    isError: false,
  },
  isDeleteTeacherLoading: false,
  isDeleteTeacherSuccess: false,
  deleteTeacherError: {
    isError: false,
  },
};

export enum TeacherActionTypes {
  // Get Teacher
  GET_TEACHERS_REQUEST = "@@teacher/GET_TEACHERS_REQUEST",
  GET_TEACHERS_SUCCESS = "@@teacher/GET_TEACHERS_SUCCESS",
  GET_TEACHERS_ERROR = "@@teacher/GET_TEACHERS_ERROR",
  CREATE_TEACHERS_REQUEST = "@@teacher/CREATE_TEACHERS_REQUEST",
  CREATE_TEACHERS_SUCCESS = "@@teacher/CREATE_TEACHERS_SUCCESS",
  CREATE_TEACHERS_ERROR = "@@teacher/CREATE_TEACHERS_ERROR",
  CREATE_TEACHERS_RESET = "@@teacher/CREATE_TEACHERS_RESET",
  UPDATE_TEACHERS_REQUEST = "@@teacher/UPDATE_TEACHERS_REQUEST",
  UPDATE_TEACHERS_SUCCESS = "@@teacher/UPDATE_TEACHERS_SUCCESS",
  UPDATE_TEACHERS_ERROR = "@@teacher/UPDATE_TEACHERS_ERROR",
  UPDATE_TEACHERS_RESET = "@@teacher/UPDATE_TEACHERS_RESET",
  DELETE_TEACHERS_REQUEST = "@@teacher/DELETE_TEACHERS_REQUEST",
  DELETE_TEACHERS_SUCCESS = "@@teacher/DELETE_TEACHERS_SUCCESS",
  DELETE_TEACHERS_ERROR = "@@teacher/DELETE_TEACHERS_ERROR",
  DELETE_TEACHERS_RESET = "@@teacher/DELETE_TEACHERS_RESET",
}

export type UseTeacher = {
  teacherState: TeacherState;
  getTeachers: (limit: number, offset: number, teacher_id?: string) => void;
  createTeacher: (teacher: CreateTeacher) => void;
  createTeacherReset: () => void;
  updateTeacher: (_id: string, teacher: CreateTeacher) => void;
  updateTeacherReset: () => void;
  deleteTeacher: (_id: string) => void;
  deleteTeacherReset: () => void;
};
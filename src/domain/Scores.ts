export type Scores = {
  scores: ScoresClass;
};

export type ScoresClass = {
  scores: Score[];
  totalScore: number;
};

export type Score = {
  _id: string;
  student: Student;
  room_id: RoomID;
  level_id: LevelID;
  generation: string;
  month: Date;
  term: string;
  subjects: ScoreSubject[];
  total_point: number;
  rank: number;
};

export type LevelID = {
  _id: string;
  level_id: string;
  level_name: string;
};

export type RoomID = {
  _id: string;
  level_id: string;
  room_id: string;
  room_name: string;
};

export type Student = {
  _id: string;
  student_id: string;
  name: string;
  last_name: string;
  age: string;
  student_tel: string;
  date_of_birth: Date;
  student_village: string;
  student_district: string;
  student_province: string;
  nationality: string;
  father_name: string;
  father_job: string;
  mother_name: string;
  mother_job: string;
  father_tel: string;
  mother_tel: string;
  student_past_school: string;
  student_number: string;
  room_id: string;
  level_id: string;
  status: string;
  note: string;
  school_entry_date: Date;
  gender: string;
  generation: string;
  new_student: boolean;
};

export type ScoreSubject = {
  title: Title;
  point: number;
  teacher: Teacher;
  _id: string;
};

export type Teacher = {
  _id: string;
  teacher_id: string;
  name: string;
  last_name: string;
  age: string;
  teacher_tel: string;
  date_of_birth: Date;
  teacher_village: string;
  teacher_district: string;
  teacher_province: string;
  nationality: string;
  graduated_from_institute: string;
  gender: string;
  degree: string;
  subjects: TeacherSubject[];
  class_teacher?: string;
};

export type TeacherSubject = {
  title: string;
  _id: string;
};

export type Title = {
  _id: string;
  subject_id: string;
  subject_name: string;
};

export type CreateScore = {
  student: string;
  room_id: string;
  level_id: string;
  generation: string;
  month: Date;
  term: string;
  subjects: Subject[];
  rank?: string;
};

export type UpdateScore = {
  student: string;
  room_id: string;
  level_id: string;
  generation: string;
  month: Date;
  term: string;
  subjects: Subject[];
  rank?: string;
};

export type Subject = {
  title: string;
  point: number;
  teacher: string;
};

export interface ScoreState {
  readonly scores: Scores;
  readonly isGetScoresLoading: boolean;
  readonly isGetScoresSuccess: boolean;
  readonly getScoresError: {
    isError: boolean;
    message?: string;
  };
  readonly isCreateScoreLoading: boolean;
  readonly isCreateScoreSuccess: boolean;
  readonly createScoreError: {
    isError: boolean;
    message?: string;
  };
  readonly isUpdateScoreLoading: boolean;
  readonly isUpdateScoreSuccess: boolean;
  readonly updateScoreError: {
    isError: boolean;
    message?: string;
  };
  readonly isDeleteScoreLoading: boolean;
  readonly isDeleteScoreSuccess: boolean;
  readonly deleteScoreError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: ScoreState = {
  scores: {
    scores: {
      scores: [],
      totalScore: 0,
    },
  },
  isGetScoresLoading: false,
  isGetScoresSuccess: false,
  getScoresError: {
    isError: false,
  },
  isCreateScoreLoading: false,
  isCreateScoreSuccess: false,
  createScoreError: {
    isError: false,
  },
  isUpdateScoreLoading: false,
  isUpdateScoreSuccess: false,
  updateScoreError: {
    isError: false,
  },
  isDeleteScoreLoading: false,
  isDeleteScoreSuccess: false,
  deleteScoreError: {
    isError: false,
  },
};

export enum ScoreActionTypes {
  // Get STUDENT
  GET_SCORES_REQUEST = "@@score/GET_SCORES_REQUEST",
  GET_SCORES_SUCCESS = "@@score/GET_SCORES_SUCCESS",
  GET_SCORES_ERROR = "@@score/GET_SCORES_ERROR",
  UPDATE_SCORE_REQUEST = "@@score/UPDATE_SCORE_REQUEST",
  UPDATE_SCORE_SUCCESS = "@@score/UPDATE_SCORE_SUCCESS",
  UPDATE_SCORE_ERROR = "@@score/UPDATE_SCORE_ERROR",
  UPDATE_SCORE_RESET = "@@score/UPDATE_SCORE_RESET",
  CREATE_SCORE_REQUEST = "@@score/CREATE_SCORE_REQUEST",
  CREATE_SCORE_SUCCESS = "@@score/CREATE_SCORE_SUCCESS",
  CREATE_SCORE_ERROR = "@@score/CREATE_SCORE_ERROR",
  CREATE_SCORE_RESET = "@@score/CREATE_SCORE_RESET",
  DELETE_SCORE_REQUEST = "@@score/DELETE_SCORE_REQUEST",
  DELETE_SCORE_SUCCESS = "@@score/DELETE_SCORE_SUCCESS",
  DELETE_SCORE_ERROR = "@@score/DELETE_SCORE_ERROR",
  DELETE_SCORE_RESET = "@@score/DELETE_SCORE_RESET",
}

export type UseScore = {
  scoreState: ScoreState;
  getScores: (
    limit: number,
    offset: number,
    room_id: string,
    month: Date,
    generation: string,
    term?: string
  ) => void;
  createScore: (score: CreateScore) => void;
  createScoreReset: () => void;
  updateScore: (
    _id: string,
    score: UpdateScore
  ) => void;
  updateScoreReset: () => void;
  deleteScore: (_id: string) => void;
  deleteScoreReset: () => void;
};

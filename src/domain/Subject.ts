export type Subject = {
  subjects: Subjects;
};

export type Subjects = {
  subjects: SubjectsSubject[];
  totalStudents: number;
};

export type SubjectsSubject = {
  _id: string;
  student_id: string;
  name: string;
  last_name: string;
  age: string;
  student_number: string;
  date_of_birth: Date;
  class_room: string;
  class_level: string;
  status: string;
  school_entry_date: Date;
  gender: string;
  generation: string;
  note: string;
  subjects: SubjectSubject[];
};

export type SubjectSubject = {
  _id: string;
  student: string;
  class_room: string;
  class_level: string;
  term: string;
  laos: number;
  english: number;
  geography: number;
  social: number;
  physical_education: number;
  history: number;
  math: number;
  chemical: number;
  physics: number;
  biology: number;
  total_score: number;
  academic_results: string;
  rank: string;
};

export type UpdateSubjectForm = {
    laos: number;
    english: number;
    geography: number;
    social: number;
    physical_education: number;
    history: number;
    math: number;
    chemical: number;
    physics: number;
    biology: number;
    rank?: number;
}

export interface SubjectState {
  readonly subjects: Subject;
  // GET Subjects
  readonly isGetSubjectsLoading: boolean;
  readonly isGetSubjectsSuccess: boolean;
  readonly getSubjectsError: {
    isError: boolean;
    message?: string;
  };
  // update Subject
  readonly isUpdateSubjectLoading: boolean;
  readonly isUpdateSubjectSuccess: boolean;
  readonly updateSubjectError: {
    isError: boolean;
    message?: string;
  };
  // delete Subject
  readonly isDeleteSubjectLoading: boolean;
  readonly isDeleteSubjectSuccess: boolean;
  readonly deleteSubjectError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: SubjectState = {
  subjects: {
    subjects: {
      subjects: [],
      totalStudents: 0,
    },
  },
  isGetSubjectsLoading: false,
  isGetSubjectsSuccess: false,
  getSubjectsError: {
    isError: false,
  },
  isUpdateSubjectLoading: false,
  isUpdateSubjectSuccess: false,
  updateSubjectError: {
    isError: false,
  },
  isDeleteSubjectLoading: false,
  isDeleteSubjectSuccess: false,
  deleteSubjectError: {
    isError: false,
  },
};

export enum SubjectActionTypes {
  GET_Subjects_REQUEST = "@@subject/GET_Subjects_REQUEST",
  GET_Subjects_SUCCESS = "@@subject/GET_Subjects_SUCCESS",
  GET_Subjects_ERROR = "@@subject/GET_Subjects_ERROR",
  UPDATE_Subjects_REQUEST = "@@subject/UPDATE_Subjects_REQUEST",
  UPDATE_Subjects_SUCCESS = "@@subject/UPDATE_Subjects_SUCCESS",
  UPDATE_Subjects_ERROR = "@@subject/UPDATE_Subjects_ERROR",
  UPDATE_Subjects_RESET = "@@subject/UPDATE_Subjects_RESET",
  DELETE_Subjects_REQUEST = "@@subject/DELETE_Subjects_REQUEST",
  DELETE_Subjects_SUCCESS = "@@subject/DELETE_Subjects_SUCCESS",
  DELETE_Subjects_ERROR = "@@subject/DELETE_Subjects_ERROR",
  DELETE_Subjects_RESET = "@@subject/DELETE_Subjects_RESET",
}

export type UseSubject = {
  subjectState: SubjectState;
  getSubjects: (
    limit: number,
    offset: number,
    class_level: string,
    class_room: string,
    term: string,
    generation: string
  ) => void;
  updateSubject: (_id: string, subject: UpdateSubjectForm) => void;
  updateSubjectReset: () => void;
  deleteSubject: (_id: string) => void;
  deleteSubjectReset: () => void;
};

export type Students = {
  students: StudentsClass;
};

export type StudentsClass = {
  students: Student[];
  totalCount: number;
  uniqueGenerations: UniqueGeneration[];
};

export type Student = {
  _id: string;
  student_id: string;
  name: string;
  last_name: string;
  age: string;
  student_tel: string;
  date_of_birth: Date;
  student_village: string;
  student_district: string;
  student_province: string;
  nationality: string;
  father_name: string;
  father_job: string;
  mother_name: string;
  mother_job: string;
  father_tel: string;
  mother_tel: string;
  student_past_school: string;
  student_number: string;
  room_id: RoomID;
  level_id: LevelID;
  status: string;
  note: string;
  school_entry_date: Date;
  gender: string;
  generation: string;
};

export type LevelID = {
  _id: string;
  level_id: string;
  level_name: string;
};

export type RoomID = {
  _id: string;
  level_id: string;
  room_id: string;
  room_name: string;
};

export type UniqueGeneration = {
  generation: string;
};

// Create student
export type CreateStudent = {
  student_id: string;
  name: string;
  last_name: string;
  age: string;
  student_tel: string;
  date_of_birth: Date;
  student_village: string;
  student_district: string;
  student_province: string;
  nationality: string;
  father_name: string;
  father_job: string;
  mother_name: string;
  mother_job: string;
  father_tel: string;
  mother_tel: string;
  student_past_school: string;
  student_number: string;
  room_id: string;
  level_id: string;
  status: string;
  note: string;
  school_entry_date: Date;
  gender: string;
  generation: string;
};

export interface StudentState {
  readonly students: Students;
  // GET STUDENT
  readonly isGetStudentsLoading: boolean;
  readonly isGetStudentsSuccess: boolean;
  readonly getStudentsError: {
    isError: boolean;
    message?: string;
  };
  // Create Student
  readonly isCreateStudentLoading: boolean;
  readonly isCreateStudentSuccess: boolean;
  readonly CreateStudentError: {
    isError: boolean;
    message?: string;
  };
  // update Student
  readonly isUpdateStudentLoading: boolean;
  readonly isUpdateStudentSuccess: boolean;
  readonly updateStudentError: {
    isError: boolean;
    message?: string;
  };
  // delete Student
  readonly isDeleteStudentLoading: boolean;
  readonly isDeleteStudentSuccess: boolean;
  readonly deleteStudentError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: StudentState = {
  students: {
    students: {
      students: [],
      totalCount: 0,
      uniqueGenerations: [],
    },
  },
  isGetStudentsLoading: false,
  isGetStudentsSuccess: false,
  getStudentsError: {
    isError: false,
  },
  isCreateStudentLoading: false,
  isCreateStudentSuccess: false,
  CreateStudentError: {
    isError: false,
  },
  isUpdateStudentLoading: false,
  isUpdateStudentSuccess: false,
  updateStudentError: {
    isError: false,
  },
  isDeleteStudentLoading: false,
  isDeleteStudentSuccess: false,
  deleteStudentError: {
    isError: false,
  },
};

export enum StudentActionTypes {
  // Get STUDENT
  GET_STUDENTS_REQUEST = "@@student/GET_STUDENTS_REQUEST",
  GET_STUDENTS_SUCCESS = "@@student/GET_STUDENTS_SUCCESS",
  GET_STUDENTS_ERROR = "@@student/GET_STUDENTS_ERROR",
  CREATE_STUDENTS_REQUEST = "@@student/CREATE_STUDENTS_REQUEST",
  CREATE_STUDENTS_SUCCESS = "@@student/CREATE_STUDENTS_SUCCESS",
  CREATE_STUDENTS_ERROR = "@@student/CREATE_STUDENTS_ERROR",
  CREATE_STUDENTS_RESET = "@@student/CREATE_STUDENTS_RESET",
  UPDATE_STUDENTS_REQUEST = "@@student/UPDATE_STUDENTS_REQUEST",
  UPDATE_STUDENTS_SUCCESS = "@@student/UPDATE_STUDENTS_SUCCESS",
  UPDATE_STUDENTS_ERROR = "@@student/UPDATE_STUDENTS_ERROR",
  UPDATE_STUDENTS_RESET = "@@student/UPDATE_STUDENTS_RESET",
  DELETE_STUDENTS_REQUEST = "@@student/DELETE_STUDENTS_REQUEST",
  DELETE_STUDENTS_SUCCESS = "@@student/DELETE_STUDENTS_SUCCESS",
  DELETE_STUDENTS_ERROR = "@@student/DELETE_STUDENTS_ERROR",
  DELETE_STUDENTS_RESET = "@@student/DELETE_STUDENTS_RESET",
}

export type UseStudent = {
  studentState: StudentState;
  getStudents: (
    limit: number,
    offset: number,
    student_id?: string,
    generation?: string,
    level_id?: string,
    new_student?: string
  ) => void;
  createStudent: (student: CreateStudent) => void;
  createStudentReset: () => void;
  updateStudent: (_id: string, student: CreateStudent) => void;
  updateStudentReset: () => void;
  deleteStudent: (_id: string) => void;
  deleteStudentReset: () => void;
};

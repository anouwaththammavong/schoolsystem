import { Moment } from "moment";

export type Users = {
  users: UserElement[],
  totalUser: number
}

export type UserElement = {
  _id: string;
  username: string;
  isAdmin: boolean;
  role: string;
};

export type User = {
  username: string;
  userRole: string;
};

export type ICreateUser = {
  username: string;
  password: string;
  role: string;
};

export type UpdateUser = {
  username: string;
  password: string;
  role: string;
};

export type FilterUser = {
  name: string;
  category: string;
};

export type UserForm = Omit<User, "category"> & {
  categoryId: string;
};

export type IGetUserPickUpItem = {
  pickUpNo: string;
  date: string;
  qty: number;
  shopName: string;
  id: UniqueId;
  no: string;
  name: string;
  detail: string;
  code: string;
  color: string;
  category: string;
};

export type IGetUserClaimItem = {
  claimNo: string;
  date: string;
  qty: number;
  shopName: string;
  id: UniqueId;
  no: string;
  name: string;
  detail: string;
  code: string;
  color: string;
  category: string;
};

export interface UserState {
  readonly users: Users;
  readonly user?: User;
  readonly isGetUsersLoading: boolean;
  readonly isGetUsersSuccess: boolean;
  readonly getUsersError: {
    isError: boolean;
    message?: string;
  };
  readonly isCreateUserLoading: boolean;
  readonly isCreateUserSuccess: boolean;
  readonly createUserError: {
    isError: boolean;
    message?: string;
  };
  readonly isUpdateUserLoading: boolean;
  readonly isUpdateUserSuccess: boolean;
  readonly updateUserError: {
    isError: boolean;
    message?: string;
  };
  readonly userPickUpItems: {
    data: IGetUserPickUpItem[];
    all: number;
  };
  readonly isGetUserPickUpItemsLoading: boolean;
  readonly isGetUserPickUpItemsSuccess: boolean;
  readonly getUserPickUpItemsError: {
    isError: boolean;
    message?: string;
  };
  readonly userClaimItems: {
    data: IGetUserClaimItem[];
    all: number;
  };
  readonly isGetUserClaimItemsLoading: boolean;
  readonly isGetUserClaimItemsSuccess: boolean;
  readonly getUserClaimItemsError: {
    isError: boolean;
    message?: string;
  };
  readonly isGetUserLoading: boolean;
  readonly isGetUserSuccess: boolean;
  readonly getUserError: {
    isError: boolean;
    message?: string;
  };
  readonly allUsers: {
    data: User[];
    all: number;
  };
  readonly isGetAllUsersLoading: boolean;
  readonly isGetAllUsersSuccess: boolean;
  readonly getAllUsersError: {
    isError: boolean;
    message?: string;
  };
  readonly isUpdateUserStatusLoading: boolean;
  readonly isUpdateUserStatusSuccess: boolean;
  readonly updateUserStatusError: {
    isError: boolean;
    message?: string;
  };
}
export const initialState: UserState = {
  users: {
    users: [],
    totalUser: 0
  },
  user: undefined,
  isGetUsersLoading: false,
  isGetUsersSuccess: false,
  getUsersError: {
    isError: false,
  },
  isCreateUserLoading: false,
  isCreateUserSuccess: false,
  createUserError: {
    isError: false,
  },
  isUpdateUserLoading: false,
  isUpdateUserSuccess: false,
  updateUserError: {
    isError: false,
  },
  userPickUpItems: {
    data: [],
    all: 0,
  },
  isGetUserPickUpItemsLoading: false,
  isGetUserPickUpItemsSuccess: false,
  getUserPickUpItemsError: {
    isError: false,
  },
  userClaimItems: {
    data: [],
    all: 0,
  },
  isGetUserClaimItemsLoading: false,
  isGetUserClaimItemsSuccess: false,
  getUserClaimItemsError: {
    isError: false,
  },
  isGetUserLoading: false,
  isGetUserSuccess: false,
  getUserError: {
    isError: false,
  },
  allUsers: {
    data: [],
    all: 0,
  },
  isGetAllUsersLoading: false,
  isGetAllUsersSuccess: false,
  getAllUsersError: {
    isError: false,
  },
  isUpdateUserStatusLoading: false,
  isUpdateUserStatusSuccess: false,
  updateUserStatusError: {
    isError: false,
  },
};

export type Category = {
  id: UniqueId;
  name: string;
};

export enum UserActionTypes {
  GET_USERS_REQUEST = "@@user/GET_USERS_REQUEST",
  GET_USERS_SUCCESS = "@@user/GET_USERS_SUCCESS",
  GET_USERS_ERROR = "@@user/GET_USERS_ERROR",
  CREATE_USER_REQUEST = "@@user/CREATE_USER_REQUEST",
  CREATE_USER_SUCCESS = "@@user/CREATE_USER_SUCCESS",
  CREATE_USER_ERROR = "@@user/CREATE_USER_ERROR",
  CREATE_USER_RESET = "@@user/CREATE_USER_RESET",
  UPDATE_USER_REQUEST = "@@user/UPDATE_USER_REQUEST",
  UPDATE_USER_SUCCESS = "@@user/UPDATE_USER_SUCCESS",
  UPDATE_USER_ERROR = "@@user/UPDATE_USER_ERROR",
  UPDATE_USER_RESET = "@@user/UPDATE_USER_RESET",
  GET_USER_PICK_UP_ITEMS_REQUEST = "@@product/GET_USER_PICK_UP_ITEMS_REQUEST",
  GET_USER_PICK_UP_ITEMS_SUCCESS = "@@product/GET_USER_PICK_UP_ITEMS_SUCCESS",
  GET_USER_PICK_UP_ITEMS_ERROR = "@@product/GET_USER_PICK_UP_ITEMS_ERROR",
  GET_USER_CLAIM_ITEMS_REQUEST = "@@product/GET_USER_CLAIM_ITEMS_REQUEST",
  GET_USER_CLAIM_ITEMS_SUCCESS = "@@product/GET_USER_CLAIM_ITEMS_SUCCESS",
  GET_USER_CLAIM_ITEMS_ERROR = "@@product/GET_USER_CLAIM_ITEMS_ERROR",
  GET_USER_REQUEST = "@@user/GET_USER_REQUEST",
  GET_USER_SUCCESS = "@@user/GET_USER_SUCCESS",
  GET_USER_ERROR = "@@user/GET_USER_ERROR",
  GET_ALL_USERS_REQUEST = "@@product/GET_ALL_USERS_REQUEST",
  GET_ALL_USERS_SUCCESS = "@@product/GET_ALL_USERS_SUCCESS",
  GET_ALL_USERS_ERROR = "@@product/GET_ALL_USERS_ERROR",
  UPDATE_USER_STATUS_REQUEST = "@@user/UPDATE_USER_STATUS_REQUEST",
  UPDATE_USER_STATUS_SUCCESS = "@@user/UPDATE_USER_STATUS_SUCCESS",
  UPDATE_USER_STATUS_ERROR = "@@user/UPDATE_USER_STATUS_ERROR",
  UPDATE_USER_STATUS_RESET = "@@user/UPDATE_USER_STATUS_RESET",
}

export type UseUser = {
  userState: UserState;
  getUsers: (limit:number,offset:number) => void;
  createUser: (user: ICreateUser) => void;
  createUserReset: () => void;
  updateUser: (_id:string , user: UpdateUser) => void;
  updateUserReset: () => void;
  getUserPickUpItems: (id: string, limit: number, offset: number) => void;
  getUserClaimItems: (id: string, limit: number, offset: number) => void;
  getUser: (id: string) => void;
  getAllUsers: () => void;
  updateUserStatus: (userId: string) => void;
  updateUserStatusReset: () => void;
};

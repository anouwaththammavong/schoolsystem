export type HomeSlider = {
  target_type: string;
  url: string;
  is_active: string;
  store: string;
  admin_id: string;
  _id: string;
  slider_name: string;
  slider_img: string;
  start_date: string;
  end_date: string;
  slider_date: string;
};

export type CreateHomeSlider = {
  name: string;
  start_date: string;
  end_date: string;
  target_type: string;
  url?: string;
  store_id?: string;
  image: File;
};

// export type AddHomeSlider = {
//     name: string;
//     target_type: string;
//     url: string;
//     store_id: string;
//     image: string;
// }

export interface HomeSliderState {
  readonly homeSliders: HomeSlider[];
  // GET HOME SLIDER
  readonly isGetHomeSlidersLoading: boolean;
  readonly isGetHomeSlidersSuccess: boolean;
  readonly getHomeSlidersError: {
    isError: boolean;
    message?: string;
  };
  // Create Home Slider
  readonly isCreateHomeSliderLoading: boolean;
  readonly isCreateHomeSliderSuccess: boolean;
  readonly createHomeSliderError: {
    isError: boolean;
    message?: string;
  };
  // Update Home Slider
  readonly isUpdateHomeSliderLoading: boolean;
  readonly isUpdateHomeSliderSuccess: boolean;
  readonly updateHomeSliderError: {
    isError: boolean;
    message?: string;
  };
}

export const initialState: HomeSliderState = {
  homeSliders: [],

  // Get Home Slider
  isGetHomeSlidersLoading: false,
  isGetHomeSlidersSuccess: false,
  getHomeSlidersError: {
    isError: false,
    message: "",
  },

  // Create Home Slider
  isCreateHomeSliderLoading: false,
  isCreateHomeSliderSuccess: false,
  createHomeSliderError: {
    isError: false,
    message: "",
  },

  // Update Home Slider
    isUpdateHomeSliderLoading: false,
    isUpdateHomeSliderSuccess: false,
    updateHomeSliderError: {
    isError: false,
    message: " ",
  },

};

export enum HomeSliderActionTypes {
  // Get Home Slider
  GET_HOME_SLIDERS_REQUEST = "@@homeSlider/GET_HOME_SLIDERS_REQUEST",
  GET_HOME_SLIDERS_SUCCESS = "@@homeSlider/GET_HOME_SLIDERS_SUCCESS",
  GET_HOME_SLIDERS_ERROR = "@@homeSlider/GET_HOME_SLIDERS_ERROR",
  // Create Home Slider
  CREATE_HOME_SLIDER_REQUEST = "@@homeSlider/CREATE_HOME_SLIDER_REQUEST",
  CREATE_HOME_SLIDER_SUCCESS = "@@homeSlider/CREATE_HOME_SLIDER_SUCCESS",
  CREATE_HOME_SLIDER_ERROR = "@@homeSlider/CREATE_HOME_SLIDER_ERROR",
  CREATE_HOME_SLIDER_RESET = "@@homeSlider/CREATE_HOME_SLIDER_RESET",
  // Update Home Slider
  UPDATE_HOME_SLIDER_REQUEST = "@@homeSlider/UPDATE_HOME_SLIDER_REQUEST",
  UPDATE_HOME_SLIDER_SUCCESS = "@@homeSlider/UPDATE_HOME_SLIDER_SUCCESS",
  UPDATE_HOME_SLIDER_ERROR = "@@homeSlider/UPDATE_HOME_SLIDER_ERROR",
  UPDATE_HOME_SLIDER_RESET = "@@homeSlider/UPDATE_HOME_SLIDER_RESET",
}

export type UseHomeSlider = {
  homeSliderState: HomeSliderState;
  getHomeSliders: () => void;
  createHomeSlider: (createHomeSlider: FormData) => void;
  createHomeSliderReset: () => void;
  updateHomeSlider: (updateHomeSlider: FormData) => void;
  updateHomeSliderReset: () => void;
};

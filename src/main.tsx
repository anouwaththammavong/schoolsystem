import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { Provider } from 'react-redux';
import store, {history} from './application/store';
import { HistoryRouter as Router } from 'redux-first-history/rr6';
import './index.css';
import {SnackbarProvider} from "notistack"

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <Router history={history}>
      <SnackbarProvider maxSnack={3} autoHideDuration={3000}>
        <App />
        </SnackbarProvider>
      </Router>
    </Provider>
  </React.StrictMode>
);

import * as React from 'react';
import { Card, CardContent, Grid, Stack } from '@mui/material';
import LoginForm from './components/LoginForm';
import LoginHeaderLogo from './components/LoginHeaderLogo';
import LoginHeaderText from './components/LoginHeaderText';
import LocalStorageService from '../../../application/service/LocalStorageService';
import { useNavigate } from 'react-router-dom';
import { useProfile } from '../../hooks/useProfile';

const Login = () => {

    const navigate = useNavigate();

    const {
        login,
        profileState,
    } = useProfile();

    const { isLoginSuccess, token } = profileState;

    React.useEffect(() => {
        if (isLoginSuccess && token) {
            LocalStorageService.setToken({
                token: token.accessToken,
                // accessToken: token.accessToken,
                refreshToken: token.refreshToken,
            });
            navigate('/')
        }
    }, [isLoginSuccess, token])

    return (
        <Grid
            container
            spacing={3}
            alignItems='center'
            justifyContent='center'
            bgcolor={'rgb(250, 250, 251)'}
            height={'100vh'}
        >
            <Grid
                item
                xs={11}
                md={5}
                lg={4}
                xl={3}
            >
                <Card sx={{borderRadius:"8px"}} >
                    <CardContent
                        style={{ padding: 40 }}
                    >
                        <Stack
                            spacing={2}
                        >
                            <LoginHeaderLogo />
                            <LoginHeaderText />
                            <LoginForm
                                profileState={profileState}
                                login={login}
                            />
                        </Stack>
                    </CardContent>
                </Card>
            </Grid>
        </Grid >
    );
};

export default Login;

import * as React from 'react';
import { Box, Button, Grid, Link, Stack, TextField, Typography } from '@mui/material';
import { Controller, useForm } from 'react-hook-form';
import { IAuth, ProfileState } from '../../../../domain/profile';

interface Props {
    login: (auth: IAuth) => void;
    profileState: ProfileState;
}

const LoginForm: React.FC<Props> = ({ login, profileState }) => {
    const { loginError, isLoginLoading, iLoginAuthHold } = profileState;

    const {
        handleSubmit,
        control,
        formState: { errors }
    } = useForm({
        defaultValues: iLoginAuthHold,
    });

    const handleLogin = (data: any) => {
        login({ ...data })
    }

    return (
        <Box>
            <form
                style={{ width: '100%' }}
                onSubmit={handleSubmit(handleLogin)}
            >
                <Stack
                    spacing={3}
                >
                    {
                        loginError.isError && loginError.message &&
                        <Typography
                            variant={'h5'}
                            component='div'
                            color={'error.main'}
                            sx={
                                { textAlign: 'center' }
                            }
                        >
                            {
                                ErrorStatus(loginError.message)
                            }
                        </Typography>
                    }
                    <Box>
                        <Controller
                            control={control}
                            name='identified'
                            rules={{
                                required: true
                            }}
                            render={({ field }) =>
                                <TextField
                                    {...field}
                                    disabled={isLoginLoading}
                                    size='small'
                                    type='text'
                                    label={'ຊື່ຜູ້ໃຊ້'}
                                    fullWidth
                                    // variant='filled'
                                />
                            }
                        />

                        {
                            errors.identified && errors.identified.type === 'required' &&
                            <Typography
                                variant='subtitle2'
                                color='error'
                                marginTop={1}
                            >
                                ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ
                            </Typography>
                        }
                    </Box>
                    <Box>
                        <Controller
                            control={control}
                            name='password'
                            rules={{
                                required: true,
                            }}
                            render={({ field }) =>
                                <TextField
                                    {...field}
                                    disabled={isLoginLoading}
                                    size='small'
                                    type='password'
                                    label={'ລະຫັດຜ່ານ'}
                                    fullWidth
                                    // variant='filled'
                                />
                            }
                        />

                        {
                            ((errors && errors.password && errors.password.type === 'required')) &&
                            <Typography
                                variant='subtitle2'
                                color='error'
                                marginTop={1}
                            >
                                ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ
                            </Typography>
                        }
                    </Box>
                    {/* <Box>
                        <Link
                            href={'/forgot-password'}
                        >
                            <Typography
                                variant={'subtitle2'}
                                component='div'
                                color='text.disabled'
                            >
                                ລືມລະຫັດຜ່ານ?
                            </Typography>
                        </Link>
                    </Box> */}
                    <Box>
                        <Button
                            type='submit'
                            variant='contained'
                            fullWidth
                            disabled={isLoginLoading}
                            color='inherit'
                            sx={{backgroundColor:"rgb(22, 119, 255)",color:"#fff",":hover":{background:"rgb(6 94 217);"}}}
                        >
                            ເຂົ້າສູ່ລະບົບ
                        </Button>
                    </Box>
                    <Box>
                        <Grid
                            item
                            xs={12}
                            lg={6}
                        >
                            <Typography
                                variant={'subtitle2'}
                                component='div'
                                color='text.disabled'
                            >
                            </Typography>
                        </Grid>
                        <Grid
                            item
                            xs={12}
                            lg={6}
                        >
                            <Link
                                href={'/register'}
                            >
                                <Typography
                                    variant={'subtitle2'}
                                    component='div'
                                    color='text.primary'
                                >
                                </Typography>
                            </Link>
                        </Grid>
                    </Box>
                </Stack>
            </form>
        </Box>
    );
};

const ErrorStatus = (status: string): string => {
    if (status === 'invalid_username_password') {
        return 'ຊື່ຜູ້ໃຊ້ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ';
    } else if (status === 'bad_request') {
        return 'Bad Request';
    } else if (status === 'server_error') {
        return 'Internal server error';
    } else {
        return 'No connection';
    }
}

export default LoginForm;

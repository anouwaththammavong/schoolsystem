import * as React from 'react';
import { Box, Typography } from '@mui/material';

interface Props { }

const LoginHeaderText: React.FC<Props> = ({ }) => {

    return (
        <Box>
            <Typography
                variant='h6'
                fontWeight={'bold'}
                marginBottom={"4px"}
            >
                ເຂົ້າສູ່ລະບົບ
            </Typography>
        </Box>
    );
};

export default LoginHeaderText;

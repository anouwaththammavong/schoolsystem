import * as React from 'react';
import { Box, styled } from '@mui/material';
import methani from '../../../../assets/images/methani.png'

const Img = styled('img')(() => ({
    width: '50%',
    height: '100px',
    marginInline: 'auto',
    display: 'block',
    objectFit:"cover"
}));

interface Props { }

const LoginHeaderLogo: React.FC<Props> = ({ }) => {

    return (
        <Box>
            <Img
                src={methani}
                alt='Waiting'
            />
        </Box>
    );
};

export default LoginHeaderLogo;

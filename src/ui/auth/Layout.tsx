import { FC, memo } from 'react';
import {  Box, CssBaseline } from '@mui/material';
import { Outlet } from 'react-router-dom';
import { styled } from '@mui/material/styles';

const Main = styled('main', {
  shouldForwardProp: (prop) => prop !== 'open',
})<{}>(() => ({
  flexGrow: 1,
  height: '100%',
  minHeight: '100%',
  width: '100%',
  minWidth: '100%',
}));
const Auth: FC = (): JSX.Element => {
  return (
    <Box
      id="auth-layout"
      sx={{
        display: 'flex',
        height: '100%',
        width: '100%',
      }}
    >
      <CssBaseline />
      <Main>
        <Outlet />
      </Main>
    </Box>
  );
};

export default memo(Auth);

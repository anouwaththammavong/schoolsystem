import * as React from 'react';
import { changePasswordRequestAction, getMeRequestAction, loginRequestAction, logoutAction } from '../../application/store/profile/reducer';
import { useAppDispatch, useAppSelector } from '../../application/store/hook';
import { IAuth, IChangePasswordHold, UseProfile } from '../../domain/profile';

export const useProfile = (): UseProfile => {
  const dispatch = useAppDispatch();
  const { profile } = useAppSelector((state) => state);

  return {
    profileState: profile,
    login: (auth: IAuth) => {
      dispatch(loginRequestAction({ auth: auth }))
    },
    getMe: () => {
      dispatch(getMeRequestAction());
    },
    changePassword(changePasswordHold: IChangePasswordHold) {
      dispatch(changePasswordRequestAction({ changePasswordHold: changePasswordHold }));
    },
    logout() {
      dispatch(logoutAction());
    },
  };
}

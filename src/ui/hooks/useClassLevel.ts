import {
  createClassLevelRequestAction,
  createClassLevelResetAction,
  deleteClassLevelRequestAction,
  deleteClassLevelResetAction,
  getClassLevelsRequestAction,
  updateClassLevelRequestAction,
  updateClassLevelResetAction,
} from "../../application/store/classLevel/Reducer";
import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import { UseClassLevel } from "../../domain/ClassLevel";
import {
  CreateClassLevelForm,
  UpdateClassLevelForm,
} from "../../domain/ClassLevel";

export const useClassLevel = (): UseClassLevel => {
  const { classLevel } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    classLevelState: classLevel,
    getClassLevels(limit: number, offset: number) {
      dispatch(
        getClassLevelsRequestAction({
          limit: limit,
          offset: offset,
        })
      );
    },
    createClassLevel(classLevel: CreateClassLevelForm) {
      dispatch(createClassLevelRequestAction({ classLevel: classLevel }));
    },
    createClassLevelReset() {
      dispatch(createClassLevelResetAction());
    },
    updateClassLevel(_id: string, classLevel: UpdateClassLevelForm) {
      dispatch(
        updateClassLevelRequestAction({ _id: _id, classLevel: classLevel })
      );
    },
    updateClassLevelReset() {
      dispatch(updateClassLevelResetAction());
    },
    deleteClassLevel(_id: string) {
      dispatch(deleteClassLevelRequestAction({ _id: _id }));
    },
    deleteClassLevelReset() {
      dispatch(deleteClassLevelResetAction());
    },
  };
};

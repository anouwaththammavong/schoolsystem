import { UseSchoolFee } from "../../domain/SchoolFees";
import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createSchoolFeeRequestAction,
  createSchoolFeeResetAction,
  deleteSchoolFeeRequestAction,
  deleteSchoolFeeResetAction,
  getSchoolFeesRequestAction,
  updateSchoolFeeRequestAction,
  updateSchoolFeeResetAction,
} from "../../application/store/schoolFee/Reducer";
import { CreateSchoolFee, UpdateSchoolFee } from "../../domain/SchoolFees";

export const useSchoolFee = (): UseSchoolFee => {
  const { schoolFee } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    schoolFeeState: schoolFee,
    getSchoolFees(
      limit: number,
      offset: number,
      level_id?: string,
      academic_year_no?: string
    ) {
      dispatch(
        getSchoolFeesRequestAction({
          limit: limit,
          offset: offset,
          level_id: level_id,
          academic_year_no: academic_year_no,
        })
      );
    },
    createSchoolFee(schoolFee: CreateSchoolFee) {
      dispatch(createSchoolFeeRequestAction({ schoolFee: schoolFee }));
    },
    createSchoolFeeReset() {
      dispatch(createSchoolFeeResetAction());
    },
    updateSchoolFee(_id: string, schoolFee: UpdateSchoolFee) {
      dispatch(
        updateSchoolFeeRequestAction({
          _id: _id,
          schoolFee: schoolFee,
        })
      );
    },
    updateSchoolFeeReset() {
      dispatch(updateSchoolFeeResetAction());
    },
    deleteSchoolFee(_id: string) {
      dispatch(deleteSchoolFeeRequestAction({ _id: _id }));
    },
    deleteSchoolFeeReset() {
      dispatch(deleteSchoolFeeResetAction());
    },
  };
};

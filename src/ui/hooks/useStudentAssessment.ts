import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createStudentAssessmentRequestAction,
  createStudentAssessmentResetAction,
  deleteStudentAssessmentRequestAction,
  deleteStudentAssessmentResetAction,
  getStudentAssessmentsRequestAction,
  updateStudentAssessmentRequestAction,
  updateStudentAssessmentResetAction,
} from "../../application/store/studentAssessment/Reducer";
import {
  UseStudentAssessment,
  CreateStudentAssessmentForm,
  UpdateStudentAssessmentForm,
} from "../../domain/StudentAssessments";

export const useStudentAssessment = (): UseStudentAssessment => {
  const { studentAssessment } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    studentAssessmentState: studentAssessment,
    getStudentAssessment(
      limit: number,
      offset: number,
      level_id: string,
      room_id: string,
      generation: string
    ) {
      dispatch(
        getStudentAssessmentsRequestAction({
          limit,
          offset,
          level_id,
          room_id,
          generation,
        })
      );
    },
    createStudentAssessment(studentAssessment: CreateStudentAssessmentForm) {
      dispatch(
        createStudentAssessmentRequestAction({
          studentAssessment: studentAssessment,
        })
      );
    },
    createStudentAssessmentReset() {
      dispatch(createStudentAssessmentResetAction());
    },
    updateStudentAssessment(
      _id: string,
      studentAssessment: UpdateStudentAssessmentForm
    ) {
      dispatch(
        updateStudentAssessmentRequestAction({
          _id: _id,
          studentAssessment: studentAssessment,
        })
      );
    },
    updateStudentAssessmentReset() {
      dispatch(updateStudentAssessmentResetAction());
    },
    deleteStudentAssessment(_id:string) {
      dispatch(
        deleteStudentAssessmentRequestAction({
          _id: _id
        })
      );
    },
    deleteStudentAssessmentReset() {
      dispatch(deleteStudentAssessmentResetAction());
    },
  };
};

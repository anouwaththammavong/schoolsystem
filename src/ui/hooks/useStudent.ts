import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createStudentRequestAction,
  createStudentResetAction,
  deleteStudentRequestAction,
  deleteStudentResetAction,
  getStudentsRequestAction,
  updateStudentRequestAction,
  updateStudentResetAction,
} from "../../application/store/student/Reducer";
import { UseStudent } from "../../domain/Student";
import { CreateStudent } from "../../domain/Student";

export const useStudent = (): UseStudent => {
  const { student } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    studentState: student,
    getStudents(
      limit: number,
      offset: number,
      student_id?: string,
      generation?: string,
      level_id?: string,
      new_student?: string
    ) {
      dispatch(
        getStudentsRequestAction({
          limit: limit,
          offset: offset,
          student_id: student_id,
          generation: generation,
          level_id: level_id,
          new_student: new_student,
        })
      );
    },
    createStudent(student: CreateStudent) {
      dispatch(createStudentRequestAction({ student: student }));
    },
    createStudentReset() {
      dispatch(createStudentResetAction());
    },
    updateStudent(_id: string, student: CreateStudent) {
      dispatch(updateStudentRequestAction({ _id: _id, student: student }));
    },
    updateStudentReset() {
      dispatch(updateStudentResetAction());
    },
    deleteStudent(_id: string) {
      dispatch(deleteStudentRequestAction({ _id: _id }));
    },
    deleteStudentReset() {
      dispatch(deleteStudentResetAction());
    },
  };
};

import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createTeacherRequestAction,
  createTeacherResetAction,
  deleteTeacherRequestAction,
  deleteTeacherResetAction,
  getTeachersRequestAction,
  updateTeacherRequestAction,
  updateTeacherResetAction,
} from "../../application/store/teacher/Reducer";
import { UseTeacher } from "../../domain/Teacher";
import { CreateTeacher } from "../../domain/Teacher";

export const useTeacher = (): UseTeacher => {
  const { teacher } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    teacherState: teacher,
    getTeachers(limit: number, offset: number, teacher_id?: string) {
      dispatch(
        getTeachersRequestAction({
          limit: limit,
          offset: offset,
          teacher_id: teacher_id,
        })
      );
    },
    createTeacher(teacher: CreateTeacher) {
      dispatch(createTeacherRequestAction({ teacher: teacher }));
    },
    createTeacherReset() {
      dispatch(createTeacherResetAction());
    },
    updateTeacher(_id: string, teacher: CreateTeacher) {
      dispatch(updateTeacherRequestAction({ _id: _id, teacher: teacher }));
    },
    updateTeacherReset() {
      dispatch(updateTeacherResetAction());
    },
    deleteTeacher(_id: string) {
      dispatch(deleteTeacherRequestAction({ _id: _id }));
    },
    deleteTeacherReset() {
      dispatch(deleteTeacherResetAction());
    },
  };
};

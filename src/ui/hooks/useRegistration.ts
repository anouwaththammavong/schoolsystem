import { UseRegistration } from "../../domain/Registrations";
import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createNewStudentRegistrationRequestAction,
  createNewStudentRegistrationResetAction,
  createOldStudentRegistrationRequestAction,
  createOldStudentRegistrationResetAction,
  deleteRegistrationRequestAction,
  deleteRegistrationResetAction,
  getRegistrationsRequestAction,
  updateNewRegistrationRequestAction,
  updateNewRegistrationResetAction,
  updateRegistrationRequestAction,
  updateRegistrationResetAction,
} from "../../application/store/registration/Reducer";
import {
  CreateOldStudentRegistration,
  CreateNewStudentRegistration,
  UpdateRegistration,
  UpdateNewRegistration
} from "../../domain/Registrations";

export const useRegistration = (): UseRegistration => {
  const { registration } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    registrationState: registration,
    getRegistrations(
      limit: number,
      offset: number,
      new_student: boolean,
      student_id?: string,
      academic_year_no?: string,
      room_id?: string,
      isPaid?: string
    ) {
      dispatch(
        getRegistrationsRequestAction({
          limit: limit,
          offset: offset,
          new_student: new_student,
          student_id: student_id,
          academic_year_no: academic_year_no,
          room_id: room_id,
          isPaid: isPaid,
        })
      );
    },
    createOldStudentRegistration(registration: CreateOldStudentRegistration) {
      dispatch(
        createOldStudentRegistrationRequestAction({
          registration: registration,
        })
      );
    },
    createOldStudentRegistrationReset() {
      dispatch(createOldStudentRegistrationResetAction());
    },
    createNewStudentRegistration(registration: CreateNewStudentRegistration) {
      dispatch(
        createNewStudentRegistrationRequestAction({
          registration: registration,
        })
      );
    },
    createNewStudentRegistrationReset() {
      dispatch(createNewStudentRegistrationResetAction());
    },
    updateRegistration(_id: string, registration: UpdateRegistration) {
      dispatch(
        updateRegistrationRequestAction({
          _id: _id,
          registration: registration,
        })
      );
    },
    updateRegistrationReset() {
      dispatch(updateRegistrationResetAction());
    },
    updateNewRegistration(_id: string, registration: UpdateNewRegistration) {
      dispatch(
        updateNewRegistrationRequestAction({
          _id: _id,
          registration: registration,
        })
      );
    },
    updateNewRegistrationReset() {
      dispatch(updateNewRegistrationResetAction());
    },
    deleteRegistration(_id: string) {
      dispatch(deleteRegistrationRequestAction({ _id: _id }));
    },
    deleteRegistrationReset() {
      dispatch(deleteRegistrationResetAction());
    },
  };
};

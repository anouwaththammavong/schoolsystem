import {
  createAcademicYearRequestAction,
  createAcademicYearResetAction,
  deleteAcademicYearRequestAction,
  deleteAcademicYearResetAction,
  getAcademicYearsRequestAction,
  updateAcademicYearRequestAction,
  updateAcademicYearResetAction,
} from "../../application/store/academicYear/Reducer";
import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import { UseAcademicYear } from "../../domain/AcademicYears";
import {
  CreateAcademicYear,
  UpdateAcademicYear,
} from "../../domain/AcademicYears";

export const useAcademicYear = (): UseAcademicYear => {
  const { academicYear } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    academicYearState: academicYear,
    getAcademicYears(limit: number, offset: number) {
      dispatch(
        getAcademicYearsRequestAction({
          limit: limit,
          offset: offset,
        })
      );
    },
    createAcademicYear(academicYear: CreateAcademicYear) {
      dispatch(createAcademicYearRequestAction({ academicYear: academicYear }));
    },
    createAcademicYearReset() {
      dispatch(createAcademicYearResetAction());
    },
    updateAcademicYear(_id: string, academicYear: UpdateAcademicYear) {
      dispatch(
        updateAcademicYearRequestAction({
          _id: _id,
          academicYear: academicYear,
        })
      );
    },
    updateAcademicYearReset() {
      dispatch(updateAcademicYearResetAction());
    },
    deleteAcademicYear(_id: string) {
      dispatch(deleteAcademicYearRequestAction({ _id: _id }));
    },
    deleteAcademicYearReset() {
      dispatch(deleteAcademicYearResetAction());
    },
  };
};

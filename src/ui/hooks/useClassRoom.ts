import {
  createClassRoomRequestAction,
  createClassRoomResetAction,
  deleteClassRoomRequestAction,
  deleteClassRoomResetAction,
  getClassRoomsRequestAction,
  updateClassRoomRequestAction,
  updateClassRoomResetAction,
} from "../../application/store/classRoom/Reducer";
import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import { UseClassRoom } from "../../domain/ClassRoom";
import { CreateClassRoom, UpdateClassRoom } from "../../domain/ClassRoom";

export const useClassRoom = (): UseClassRoom => {
  const { classRoom } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    classRoomState: classRoom,
    getClassRooms(limit: number, offset: number) {
      dispatch(
        getClassRoomsRequestAction({
          limit: limit,
          offset: offset,
        })
      );
    },
    createClassRoom(classRoom: CreateClassRoom) {
      dispatch(createClassRoomRequestAction({ classRoom: classRoom }));
    },
    createClassRoomReset() {
      dispatch(createClassRoomResetAction());
    },
    updateClassRoom(_id: string, classRoom: UpdateClassRoom) {
      dispatch(
        updateClassRoomRequestAction({ _id: _id, classRoom: classRoom })
      );
    },
    updateClassRoomReset() {
      dispatch(updateClassRoomResetAction());
    },
    deleteClassRoom(_id: string) {
      dispatch(deleteClassRoomRequestAction({ _id: _id }));
    },
    deleteClassRoomReset() {
      dispatch(deleteClassRoomResetAction());
    },
  };
};

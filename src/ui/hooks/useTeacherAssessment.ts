import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createTeacherAssessmentRequestAction,
  createTeacherAssessmentResetAction,
  deleteTeacherAssessmentRequestAction,
  deleteTeacherAssessmentResetAction,
  getTeacherAssessmentsRequestAction,
  updateTeacherAssessmentRequestAction,
  updateTeacherAssessmentResetAction,
} from "../../application/store/teacherAssessment/Reducer";
import {
  UseTeacherAssessment,
  CreateTeacherAssessment,
  UpdateTeacherAssessment,
} from "../../domain/TeacherAssessments";

export const useTeacherAssessment = (): UseTeacherAssessment => {
  const { teacherAssessment } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    teacherAssessmentState: teacherAssessment,
    getTeacherAssessment(
      limit: number,
      offset: number,
      teacher_id?: string,
      assessment_date?: Date
    ) {
      dispatch(
        getTeacherAssessmentsRequestAction({
          limit,
          offset,
          teacher_id,
          assessment_date,
        })
      );
    },
    createTeacherAssessment(teacherAssessment: CreateTeacherAssessment) {
      dispatch(
        createTeacherAssessmentRequestAction({
          teacherAssessment: teacherAssessment,
        })
      );
    },
    createTeacherAssessmentReset() {
      dispatch(createTeacherAssessmentResetAction());
    },
    updateTeacherAssessment(
      _id: string,
      teacherAssessment: UpdateTeacherAssessment
    ) {
      dispatch(
        updateTeacherAssessmentRequestAction({
          _id: _id,
          teacherAssessment: teacherAssessment,
        })
      );
    },
    updateTeacherAssessmentReset() {
      dispatch(updateTeacherAssessmentResetAction());
    },
    deleteTeacherAssessment(_id:string) {
      dispatch(
        deleteTeacherAssessmentRequestAction({
          _id: _id
        })
      );
    },
    deleteTeacherAssessmentReset() {
      dispatch(deleteTeacherAssessmentResetAction());
    },
  };
};

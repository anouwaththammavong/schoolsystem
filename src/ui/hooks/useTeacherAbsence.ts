import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createTeacherAbsenceRequestAction,
  createTeacherAbsenceResetAction,
  deleteTeacherAbsenceRequestAction,
  deleteTeacherAbsenceResetAction,
  getTeacherAbsencesRequestAction,
  updateTeacherAbsenceRequestAction,
  updateTeacherAbsenceResetAction,
} from "../../application/store/teacherAbsence/Reducer";
import {
  UseTeacherAbsence,
  CreateTeacherAbsenceForm,
  UpdateTeacherAbsenceForm,
} from "../../domain/TeacherAbsence";

export const useTeacherAbsence = (): UseTeacherAbsence => {
  const { teacherAbsence } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    teacherAbsenceState: teacherAbsence,
    getTeacherAbsence(
      limit: number,
      offset: number,
      teacher_id: string,
      absence_date?: Date,
      be_reasonable?: string
    ) {
      dispatch(
        getTeacherAbsencesRequestAction({
          limit,
          offset,
          teacher_id,
          absence_date,
          be_reasonable,
        })
      );
    },
    createTeacherAbsence(teacherAbsence: CreateTeacherAbsenceForm) {
      dispatch(createTeacherAbsenceRequestAction({ teacherAbsence }));
    },
    createTeacherAbsenceReset() {
      dispatch(createTeacherAbsenceResetAction());
    },
    updateTeacherAbsence(
      _id: string,
      teacherAbsence: UpdateTeacherAbsenceForm
    ) {
      dispatch(
        updateTeacherAbsenceRequestAction({
          _id: _id,
          teacherAbsence: teacherAbsence,
        })
      );
    },
    updateTeacherAbsenceReset() {
      dispatch(updateTeacherAbsenceResetAction());
    },
    deleteTeacherAbsence(_id: string) {
      dispatch(deleteTeacherAbsenceRequestAction({ _id: _id }));
    },
    deleteTeacherAbsenceReset() {
      dispatch(deleteTeacherAbsenceResetAction());
    },
  };
};

import {
  createSubjectV2RequestAction,
  createSubjectV2ResetAction,
  deleteSubjectV2RequestAction,
  deleteSubjectV2ResetAction,
  getSubjectV2sRequestAction,
  updateSubjectV2RequestAction,
  updateSubjectV2ResetAction,
} from "../../application/store/subjectV2/Reducer";
import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import { UseSubjectV2 } from "../../domain/SubjectV2";
import {
  CreateSubjectV2Form,
  UpdateSubjectV2Form,
} from "../../domain/SubjectV2";

export const useSubjectV2 = (): UseSubjectV2 => {
  const { subjectV2 } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    subjectV2State: subjectV2,
    getSubjectV2s(limit: number, offset: number) {
      dispatch(
        getSubjectV2sRequestAction({
          limit: limit,
          offset: offset,
        })
      );
    },
    createSubjectV2(subjectV2: CreateSubjectV2Form) {
      dispatch(createSubjectV2RequestAction({ subjectV2: subjectV2 }));
    },
    createSubjectV2Reset() {
      dispatch(createSubjectV2ResetAction());
    },
    updateSubjectV2(_id: string, subjectV2: UpdateSubjectV2Form) {
      dispatch(
        updateSubjectV2RequestAction({ _id: _id, subjectV2: subjectV2 })
      );
    },
    updateSubjectV2Reset() {
      dispatch(updateSubjectV2ResetAction());
    },
    deleteSubjectV2(_id: string) {
      dispatch(deleteSubjectV2RequestAction({ _id: _id }));
    },
    deleteSubjectV2Reset() {
      dispatch(deleteSubjectV2ResetAction());
    },
  };
};

import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createStudentAbsenceRequestAction,
  createStudentAbsenceResetAction,
  deleteStudentAbsenceRequestAction,
  deleteStudentAbsenceResetAction,
  getAllStudentAbsencesRequestAction,
  getStudentIdAbsencesRequestAction,
  updateStudentAbsenceRequestAction,
  updateStudentAbsenceResetAction,
} from "../../application/store/studentAbsence/Reducer";
import { UseStudentAbsence } from "../../domain/StudentAbsence";
import {
  CreateStudentAbsenceForm,
  UpdateStudentAbsenceForm,
} from "../../domain/StudentAbsence";

export const useStudentAbsence = (): UseStudentAbsence => {
  const { studentAbsence } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    studentAbsenceState: studentAbsence,
    getAllStudentAbsences(
      limit: number,
      offset: number,
      room_id: string,
      generation: string,
      student_id?: string
    ) {
      dispatch(
        getAllStudentAbsencesRequestAction({
          limit: limit,
          offset: offset,
          room_id: room_id,
          generation: generation,
          student_id: student_id,
        })
      );
    },
    getStudentIdAbsences(
      limit: number,
      offset: number,
      student_id: string,
      room_id: string,
      absence_date?: Date,
      term?: string,
      be_reasonable?: string
    ) {
      dispatch(
        getStudentIdAbsencesRequestAction({
          limit: limit,
          offset: offset,
          room_id: room_id,
          student_id: student_id,
          absence_date: absence_date,
          term: term,
          be_reasonable: be_reasonable
        })
      );
    },
    createStudentAbsence(studentAbsence: CreateStudentAbsenceForm) {
      dispatch(
        createStudentAbsenceRequestAction({ studentAbsence: studentAbsence })
      );
    },
    createStudentAbsenceReset() {
      dispatch(createStudentAbsenceResetAction());
    },
    updateStudentAbsence(
      _id: string,
      studentAbsence: UpdateStudentAbsenceForm
    ) {
      dispatch(
        updateStudentAbsenceRequestAction({
          _id: _id,
          studentAbsence: studentAbsence,
        })
      );
    },
    updateStudentAbsenceReset() {
      dispatch(updateStudentAbsenceResetAction());
    },
    deleteStudentAbsence(_id: string) {
      dispatch(deleteStudentAbsenceRequestAction({ _id: _id }));
    },
    deleteStudentAbsenceReset() {
      dispatch(deleteStudentAbsenceResetAction());
    },
  };
};

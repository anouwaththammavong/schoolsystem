import * as React from "react";
import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createUserRequestAction,
  createUserResetAction,
  getAllUsersRequestAction,
  getUserClaimItemsRequestAction,
  getUserPickUpItemsRequestAction,
  getUserRequestAction,
  getUsersRequestAction,
  updateUserRequestAction,
  updateUserResetAction,
  updateUserStatusRequestAction,
  updateUserStatusResetAction,
} from "../../application/store/user/reducer";
import { User, UpdateUser, UseUser, ICreateUser } from "../../domain/user";

export const useUser = (): UseUser => {
  const { user } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    userState: user,
    getUsers: (limit: number, offset: number) => {
      dispatch(getUsersRequestAction({ limit, offset }));
    },
    createUser: (user: ICreateUser) => {
      dispatch(createUserRequestAction({ user: user }));
    },
    createUserReset() {
      dispatch(createUserResetAction());
    },
    updateUser: (_id: string, user: UpdateUser) => {
      dispatch(updateUserRequestAction({ _id: _id, user: user }));
    },
    updateUserReset() {
      dispatch(updateUserResetAction());
    },
    getUserPickUpItems(id: string, limit: number, offset: number) {
      dispatch(
        getUserPickUpItemsRequestAction({
          id: id,
          limit: limit,
          offset: offset,
        })
      );
    },
    getUserClaimItems(id: string, limit: number, offset: number) {
      dispatch(
        getUserClaimItemsRequestAction({ id: id, limit: limit, offset: offset })
      );
    },
    getUser(id: string) {
      dispatch(getUserRequestAction({ id: id }));
    },
    getAllUsers() {
      dispatch(getAllUsersRequestAction());
    },
    updateUserStatus: (userId: string) => {
      dispatch(updateUserStatusRequestAction({ userId: userId }));
    },
    updateUserStatusReset() {
      dispatch(updateUserStatusResetAction());
    },
  };
};

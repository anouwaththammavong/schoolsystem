import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  deleteSubjectRequestAction,
  deleteSubjectResetAction,
  getSubjectsRequestAction,
  updateSubjectRequestAction,
  updateSubjectResetAction,
} from "../../application/store/subject/Reducer";
import { UseSubject, UpdateSubjectForm } from "../../domain/Subject";

export const useSubject = (): UseSubject => {
  const { subject } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    subjectState: subject,
    getSubjects(
      limit: number,
      offset: number,
      class_level: string,
      class_room: string,
      term: string,
      generation: string
    ) {
      dispatch(
        getSubjectsRequestAction({
          limit: limit,
          offset: offset,
          class_level: class_level,
          class_room: class_room,
          term: term,
          generation: generation,
        })
      );
    },
    updateSubject(_id: string, subject: UpdateSubjectForm) {
      dispatch(updateSubjectRequestAction({ _id: _id, subject: subject }));
    },
    updateSubjectReset() {
      dispatch(updateSubjectResetAction());
    },
    deleteSubject(_id: string) {
      dispatch(deleteSubjectRequestAction({ _id: _id }));
    },
    deleteSubjectReset() {
      dispatch(deleteSubjectResetAction());
    },
  };
};

import { useAppDispatch, useAppSelector } from "../../application/store/hook";
import {
  createScoreRequestAction,
  createScoreResetAction,
  deleteScoreRequestAction,
  deleteScoreResetAction,
  getScoresRequestAction,
  updateScoreRequestAction,
  updateScoreResetAction,
} from "../../application/store/score/Reducer";
import { UseScore, CreateScore, UpdateScore } from "../../domain/Scores";

export const useScore = (): UseScore => {
  const { score } = useAppSelector((state) => state);
  const dispatch = useAppDispatch();

  return {
    scoreState: score,
    getScores(
      limit: number,
      offset: number,
      room_id: string,
      month: Date,
      generation: string,
      term?: string
    ) {
      dispatch(
        getScoresRequestAction({
          limit,
          offset,
          room_id,
          month,
          generation,
          term
        })
      );
    },
    createScore(score: CreateScore) {
      dispatch(
        createScoreRequestAction({
          score: score,
        })
      );
    },
    createScoreReset() {
      dispatch(createScoreResetAction());
    },
    updateScore(_id: string, score: UpdateScore) {
      dispatch(
        updateScoreRequestAction({
          _id: _id,
          score: score,
        })
      );
    },
    updateScoreReset() {
      dispatch(updateScoreResetAction());
    },
    deleteScore(_id: string) {
      dispatch(
        deleteScoreRequestAction({
          _id: _id,
        })
      );
    },
    deleteScoreReset() {
      dispatch(deleteScoreResetAction());
    },
  };
};

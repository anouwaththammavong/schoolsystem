import { FC, NamedExoticComponent, useEffect, useState } from "react";
import { Routes, Route, useNavigate, Navigate } from "react-router-dom";

import Admin from "../admin/Layout";
import Auth from "../auth/Layout";
import routers, { Router, LayoutKey } from "../config/configRoutes";
import NotFoundImage from "../admin/components/NotFoundImage";
import { useProfile } from "../hooks/useProfile";
import LoadingImage from "../admin/components/LoadingImage";
import { IUser } from "../../domain/profile";

export type Layout = {
  [key in LayoutKey]: NamedExoticComponent;
};
const layout: Layout = {
  ADMIN: Admin,
  AUTH: Auth,
};
const Layouts: FC = () => {
  const [cleanUp, setCleanUp] = useState(false);
  const navigate = useNavigate();

  const { profileState, getMe } = useProfile();

  const { getMeError, profile, isGetMeLoading, isLoginSuccess } = profileState;

  useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getMe();
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, isLoginSuccess]);

  useEffect(() => {
    if (getMeError.isError) {
      navigate("/login");
    }
  }, [getMeError]);

  return (
    <>
      <Routes>
        {profile && (
          <Route
            path="/"
            element={
              <Navigate
                replace
                to={
                  profile.userRole === "ADMIN" ? "/student" : "/absence_student"
                }
              />
            }
          />
        )}
        {Object.keys(layout).map((key) => {
          const Layout = layout[key as LayoutKey];
          return (
            <Route element={<Layout />} key={key}>
              {mapRoutePath(
                routers[key as LayoutKey],
                key as LayoutKey,
                profile
              )}
            </Route>
          );
        })}
        {!isGetMeLoading && <Route path="*" element={<NotFoundImage />} />}
        {isGetMeLoading && <Route path="*" element={<LoadingImage />} />}
      </Routes>
    </>
  );
};

const mapRoutePath = (
  routes: Array<Router>,
  layoutKey: LayoutKey,
  profile?: IUser
) => {
  return routes.map((route) => {
    if (layoutKey === "ADMIN") {
      if (profile && route.role && route.role.includes(profile.userRole)) {
        return (
          <Route
            index={route.index}
            key={route.id}
            path={route.path}
            element={route.component}
          />
        );
      }
      return <></>;
    } else {
      return (
        <Route
          index={route.index}
          key={route.id}
          path={route.path}
          element={route.component}
        />
      );
    }
  });
};

export default Layouts;

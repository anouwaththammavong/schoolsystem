import { ImageOutlined, PictureAsPdf } from '@mui/icons-material';
import { Box, Stack, styled, Typography } from '@mui/material';
import React, { FC, useRef, useState } from 'react';
import { AlertSnackbar } from './ui-tool/AlertSnackbar';
import './index.css';

const UploadBox = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(2),
    borderWidth: 2,
    borderColor: '#ccc',
    borderStyle: 'dashed',
    backgroundColor: 'rgba(255, 255, 255, 0.09)',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out',
    '&:hover': {
        borderColor: '#aaa',
    },
}));

const Img = styled('img')({
    width: '300px',
});

interface Props {
    name: string;
    desc: string;
    title: string;
    setFile: React.Dispatch<React.SetStateAction<File | undefined>>;
}

const UploadDocument: FC<Props> = ({
    name,
    desc,
    title,
    setFile
}) => {
    const [alert, setAlert] = useState({
        isOpenAlert: false,
        alertText: ''
    });
    const imgRef = useRef<HTMLImageElement>(null)

    const [imgSrc, setImgSrc] = React.useState<{
        src: string | ArrayBuffer | null;
        type: string;
        name: string;
    }>()

    const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        if (event.dataTransfer.files[0].size > 2000000) {
            setAlert({
                alertText: 'ຮູບທີ່ທ່ານຕ້ອງການອັບໂຫຼດມີຂະໜາດເກີນກວ່າທີ່ກຳນົດ',
                isOpenAlert: true
            })
        } else {
            if (event.dataTransfer.files && event.dataTransfer.files.length > 0) {
                const reader = new FileReader()
                const file = event.dataTransfer.files[0];
                setFile(file);
                reader.addEventListener('load', () => {
                    if (reader.result) {
                        return setImgSrc({ src: reader.result, type: file.type, name: file.name })
                    } else {
                        setAlert({
                            alertText: 'ເກີດຂໍ້ຜິດພາດ, ກະລຸນາລອງໃໝ່',
                            isOpenAlert: true
                        })
                    }
                })
                reader.readAsDataURL(event.dataTransfer.files[0])
            }
        }
    };

    const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
    };

    const handleFileSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0].size > 2000000) {
            setAlert({
                alertText: 'ຮູບທີ່ທ່ານຕ້ອງການອັບໂຫຼດມີຂະໜາດເກີນກວ່າທີ່ກຳນົດ',
                isOpenAlert: true
            })
        } else {
            if (e.target.files && e.target.files.length > 0) {
                const reader = new FileReader()
                const file = e.target.files[0];
                setFile(file);
                reader.addEventListener('load', () => {
                    if (reader.result) {
                        return setImgSrc({ src: reader.result, type: file.type, name: file.name })
                    } else {
                        setAlert({
                            alertText: 'ເກີດຂໍ້ຜິດພາດ, ກະລຸນາລອງໃໝ່',
                            isOpenAlert: true
                        })
                    }
                })
                reader.readAsDataURL(e.target.files[0])
            }
        }
    };

    const handleCloseAlert = () => {
        setAlert({
            alertText: '',
            isOpenAlert: false
        })
    }

    return (
        <Box
            alignItems='center'
        >
            <label htmlFor={`contained-button-file-${name}`}>
                <UploadBox
                    onDragOver={handleDragOver}
                    onDrop={handleDrop}
                >
                    <ImageOutlined
                        fontSize='large'
                    />
                    <p>ລາກ ແລະ ວາງຮູບພາບ ຫຼື ຄຣິກເພື່ອອັບໂຫຼດ</p>
                </UploadBox>
            </label>
            <input
                accept='image/*, application/pdf'
                style={{ display: 'none' }}
                id={`contained-button-file-${name}`}
                type='file'
                onChange={handleFileSelect}
            />
            <Typography
                textAlign={'center'}
                marginBottom={3}
                marginTop={1}
            >
                {desc}
            </Typography>
            <Typography>
                {title === "document" ? "ເອກະສານ:" : "ຮູບພາບ:"}
            </Typography>
            {imgSrc?.type.includes('image') &&
                <Stack
                    padding={3}
                    spacing={1}
                >
                    <Img
                        ref={imgRef}
                        alt='Crop me'
                        src={imgSrc?.src?.toString()}
                    />
                    <Typography>
                        {imgSrc.name}
                    </Typography>
                </Stack>
            }
            {imgSrc?.type.includes('pdf') &&
                <Stack
                    padding={3}
                    direction={'row'}
                    spacing={1}
                >
                    <PictureAsPdf
                        fontSize='large'
                    />
                    <Typography>
                        {imgSrc.name}
                    </Typography>
                </Stack>
            }
            <AlertSnackbar
                isOpenAlert={alert.isOpenAlert}
                handleCloseAlert={handleCloseAlert}
                alertText={alert.alertText}
                severity='error'
            />
        </Box>
    );
};

export default UploadDocument;

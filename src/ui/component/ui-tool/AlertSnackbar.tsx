import { Alert, AlertColor, Snackbar } from '@mui/material';
import { FC } from 'react';

interface Props {
  isOpenAlert: boolean;
  handleCloseAlert: () => void;
  severity: AlertColor;
  alertText: string;
}
export const AlertSnackbar: FC<Props> = ({ isOpenAlert, handleCloseAlert, severity, alertText }) => {
  return (
    <Snackbar
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      open={isOpenAlert}
      autoHideDuration={3000}
      onClose={() => handleCloseAlert()}
    >
      <Alert
        onClose={() => handleCloseAlert()}
        severity={severity}
        variant='filled'
        sx={{ width: '100%' }}
      >
        {alertText}
      </Alert>
    </Snackbar>
  )
};

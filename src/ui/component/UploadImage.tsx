import { ImageOutlined } from '@mui/icons-material';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, styled, Typography } from '@mui/material';
import { FC, RefObject, useEffect, useRef, useState } from 'react';
import ReactCrop, { Crop, PixelCrop, centerCrop, makeAspectCrop } from 'react-image-crop';
import { canvasPreview } from './canvasPreview';
import { AlertSnackbar } from './ui-tool/AlertSnackbar';
import './index.css';

const UploadBox = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(2),
    borderWidth: 2,
    borderColor: '#ccc',
    borderStyle: 'dashed',
    backgroundColor: 'rgba(255, 255, 255, 0.09)',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out',
    '&:hover': {
        borderColor: '#aaa',
    },
}));

interface Props {
    canvasRef: RefObject<HTMLCanvasElement>;
    imageRatio: number;
    name: string;
    currentImage: string;
    previewImageWidth: string;
    desc: string;
}

const UploadImage: FC<Props> = ({ canvasRef, imageRatio, name, currentImage, previewImageWidth, desc }) => {
    const [alert, setAlert] = useState({
        isOpenAlert: false,
        alertText: ''
    });
    const [crop, setCrop] = useState<Crop>()
    const [imgSrc, setImgSrc] = useState('')
    const [isCropDialogOpen, setIsCropDialogOpen] = useState(false);
    const [completedCrop, setCompletedCrop] = useState<PixelCrop>()
    const imgRef = useRef<HTMLImageElement>(null)

    const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        if (event.dataTransfer.files[0].size > 2000000) {
            setAlert({
                alertText: 'ຮູບທີ່ທ່ານຕ້ອງການອັບໂຫຼດມີຂະໜາດເກີນກວ່າທີ່ກຳນົດ',
                isOpenAlert: true
            })
        } else {
            if (event.dataTransfer.files && event.dataTransfer.files.length > 0) {
                setCrop(undefined) // Makes crop preview update between images.
                const reader = new FileReader()
                reader.addEventListener('load', () =>
                    setImgSrc(reader.result?.toString() || ''),
                )
                reader.readAsDataURL(event.dataTransfer.files[0])
            }
        }
    };

    const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
    };

    const handleFileSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files.length > 0) {
            setCrop(undefined) // Makes crop preview update between images.
            const reader = new FileReader()
            reader.addEventListener('load', () =>
                setImgSrc(reader.result?.toString() || ''),
            )
            reader.readAsDataURL(e.target.files[0])
        }
    };

    function onImageLoad(e: React.SyntheticEvent<HTMLImageElement>) {
        const { width, height } = e.currentTarget
        setCrop(centerAspectCrop(width, height, imageRatio))
    }

    function centerAspectCrop(
        mediaWidth: number,
        mediaHeight: number,
        aspect: number,
    ) {
        return centerCrop(
            makeAspectCrop(
                {
                    unit: '%',
                    width: 100,
                },
                aspect,
                mediaWidth,
                mediaHeight,
            ),
            mediaWidth,
            mediaHeight,
        )
    }

    const handleCancleCropImage = () => {
        setCompletedCrop(undefined)
        setIsCropDialogOpen(false)
    }

    const handleConfirmCrop = () => {
        if (
            completedCrop?.width &&
            completedCrop?.height &&
            imgRef.current &&
            canvasRef.current
        ) {
            canvasPreview(
                imgRef.current,
                canvasRef.current,
                completedCrop,
                1,
                0,
            )

            setIsCropDialogOpen(false)
        }
    }

    const handleCloseAlert = () => {
        setAlert({
            alertText: '',
            isOpenAlert: false
        })
    }

    useEffect(() => {
        setIsCropDialogOpen(!!imgSrc);
    }, [imgSrc])

    return (
        <Box
            alignItems='center'
        >
            <label htmlFor={`contained-button-file-${name}`}>
                <UploadBox
                    onDragOver={handleDragOver}
                    onDrop={handleDrop}
                >
                    <ImageOutlined
                        fontSize='large'
                    />
                    <p>ລາກ ແລະ ວາງຮູບພາບ ຫຼື ຄຣິກເພື່ອອັບໂຫຼດ</p>
                </UploadBox>
            </label>
            <input
                accept='image/*'
                style={{ display: 'none' }}
                id={`contained-button-file-${name}`}
                type='file'
                onChange={handleFileSelect}
            />
            <Typography
                textAlign={'center'}
                marginBottom={3}
                marginTop={1}
            >
                {desc}
            </Typography>
            <Dialog
                open={isCropDialogOpen}
                fullWidth
                maxWidth='lg'
            >
                <DialogTitle>
                    ຕັດຮູບພາບ
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        <ReactCrop
                            crop={crop}
                            onChange={(_, percentCrop) => setCrop(percentCrop)}
                            onComplete={(c) => setCompletedCrop(c)}
                            aspect={imageRatio}
                        >
                            <img
                                ref={imgRef}
                                alt='Crop me'
                                src={imgSrc}
                                onLoad={onImageLoad}
                            />
                        </ReactCrop>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={() => handleCancleCropImage()}
                        variant='outlined'
                    >
                        ຍົກເລີກ
                    </Button>
                    <Button
                        onClick={() => handleConfirmCrop()}
                        variant='contained'
                    >
                        ບັນທຶກ
                    </Button>
                </DialogActions>
            </Dialog>
            {completedCrop && (
                <Box
                    textAlign={'center'}
                >
                    <div>
                        <canvas
                            ref={canvasRef}
                            style={{
                                border: '1px dashed white',
                                objectFit: 'contain',
                                width: previewImageWidth,
                            }}
                        />
                    </div>
                </Box>
            )}
            {!completedCrop && (
                <Box
                    textAlign={'center'}
                >
                    <div>
                        <img
                            src={currentImage}
                            style={{
                                width: previewImageWidth
                            }}
                        />
                    </div>
                </Box>
            )}
            <AlertSnackbar
                isOpenAlert={alert.isOpenAlert}
                handleCloseAlert={handleCloseAlert}
                alertText={alert.alertText}
                severity='error'
            />
        </Box>
    );
};

export default UploadImage;

import * as React from "react";
import { Registration, RegistrationState } from "../../../../../domain/Registrations";
import { Grid, Paper, Skeleton, TableFooter, Typography } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { formatMoney } from "../../../../../application/service/Utils";
import moment from "moment";

type Props = {
  registrationState: RegistrationState;
  registrationHold: Registration | undefined;
};

const RegistrationNewStudentBillReport = React.forwardRef<HTMLDivElement, Props>(
  ({ registrationState, registrationHold }, ref) => {
    const { isGetRegistrationsLoading, isGetRegistrationsSuccess } = registrationState;

    return (
      <div ref={ref}>
        {isGetRegistrationsLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
            <Skeleton />
            <Skeleton />
          </Paper>
        )}
        {isGetRegistrationsSuccess && registrationHold && (
          <Grid container alignItems="center" gap={5}>
            <Grid
              item
              xs={12}
              display={"flex"}
              direction={"column"}
              alignItems={"center"}
              gap={2}
            >
              <Typography variant={"h5"}>ໃບບິນການຊຳລະເງິນຄ່າທຳນຽມຮຽນ</Typography>
              <Typography variant={"h5"}>ໂຮງຮຽນປະຖົມເມທາທິການທີ່</Typography>
            </Grid>
            <Grid item xs={12} display={"flex"} direction={"column"} gap={1}>
              <Typography variant={"body1"}>
                ຊື່ ແລະ ນາມສະກຸນນັກຮຽນ:{" "}
                {registrationHold.student_id.gender === "ຊາຍ" ? "ທ້າວ" : "ນາງ"}
                {registrationHold.student_id.name}{" "}
                {registrationHold.student_id.last_name}
              </Typography>
              <Typography variant={"body1"}>
                ລະຫັດນັກຮຽນ: {registrationHold.student_id.student_id}
              </Typography>
              <Typography variant={"body1"}>
                {registrationHold.room_id.room_name} {" ຊັ້ນຮຽນ "}
                {registrationHold.level_id.level_name}
              </Typography>
              <Typography variant={"body1"}>
                {" ປີການສຶກສາ "}
                {registrationHold.academic_year_no.academic_year} {" ສົກຮຽນທີ່ "}
                {registrationHold.academic_year_no.academic_year_no}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <TableContainer sx={{ border: "1px solid #000" }}>
                <Table sx={{ minWidth: 700 }} aria-label="spanning table">
                  <TableHead>
                    <TableRow>
                      <TableCell><Typography>ລາຍການທີ່</Typography></TableCell>
                      <TableCell align="right"><Typography>ລາຍການ</Typography></TableCell>
                      <TableCell align="right"><Typography>ຈຳນວນເງິນ</Typography></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell>1</TableCell>
                      <TableCell align="right">ຄ່າປຶ້ມ</TableCell>
                      <TableCell align="right">
                        {formatMoney(registrationHold.school_fee_id.book_fee)}
                        ກີບ
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>2</TableCell>
                      <TableCell align="right">ຄ່າເຄື່ອງແບບ</TableCell>
                      <TableCell align="right">
                        {formatMoney(registrationHold.school_fee_id.uniform_fee)}
                        ກີບ
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>3</TableCell>
                      <TableCell align="right">ຄ່າຮຽນ</TableCell>
                      <TableCell align="right">
                        {formatMoney(registrationHold.school_fee_id.term_fee)}
                        ກີບ
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell rowSpan={3} />
                    </TableRow>
                    <TableRow>
                      <TableCell align="right">ລວມເປັນເງິນທັງໝົດ</TableCell>
                      <TableCell align="right">
                        {formatMoney(registrationHold.school_fee_id.total_fee)}
                        ກີບ
                      </TableCell>
                    </TableRow>
                  </TableBody>
                  <TableFooter>
                    <TableRow>
                      <TableCell align="right" sx={{ border: "none" }}></TableCell>
                      <TableCell align="right" sx={{ border: "none" }}></TableCell>
                      <TableCell
                        sx={{
                          border: "none",
                          borderLeft: "1px solid rgba(224, 224, 224, 1)",
                        }}
                      >
                        <Typography textAlign={"center"}>ສຳລັບພະນັກງານ</Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="right" rowSpan={3} sx={{ border: "none" }}></TableCell>
                      <TableCell align="right" sx={{ border: "none" }}></TableCell>
                      <TableCell
                        align="left"
                        sx={{
                          border: "none",
                          borderLeft: "1px solid rgba(224, 224, 224, 1)",
                        }}
                      >
                        <Typography>
                          ວັນທີ {moment(new Date()).format("DD/MM/YYYY")}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="right" sx={{ border: "none" }}></TableCell>
                      <TableCell
                        align="right"
                        sx={{
                          border: "none",
                          borderLeft: "1px solid rgba(224, 224, 224, 1)",
                        }}
                      >
                        <Typography textAlign={"center"}>(ປະທັບກາ)</Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="right" sx={{ border: "none" }}></TableCell>
                      <TableCell
                        align="right"
                        sx={{
                          border: "none",
                          borderLeft: "1px solid rgba(224, 224, 224, 1)",
                        }}
                      ></TableCell>
                    </TableRow>
                  </TableFooter>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
        )}
      </div>
    );
  }
);

export default RegistrationNewStudentBillReport;

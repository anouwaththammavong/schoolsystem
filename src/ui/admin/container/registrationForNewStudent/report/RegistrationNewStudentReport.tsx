import * as React from "react";
import { RegistrationState } from "../../../../../domain/Registrations";
import {Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";
import { formatMoney } from "../../../../../application/service/Utils";

type Props = {
    registrationState: RegistrationState;
};

// const RegistrationReport = React.forwardRef<HTMLDivElement, Props>((props, ref) => {
//     return (
//       <div ref={ref}>RegistrationReport</div>
//     );
//   });

const RegistrationReport = React.forwardRef<HTMLDivElement, Props>(
  ({ registrationState }, ref) => {
    const [registrationTable, setRegistrationTable] = React.useState<Array<any>>([]);

    const { registrations, isGetRegistrationsLoading, isGetRegistrationsSuccess, getRegistrationsError } = registrationState;
    
    React.useEffect(() => {
        setRegistrationTable(
            registrations.registrations.registrations.map((regis) => {
            return {
              ໄອດີຂອງນັກຮຽນ: <Typography>{regis.student_id.student_id}</Typography>,
              "ຊື່ ແລະ ນາມສະກຸນ": <Typography>{regis.student_id.name} {regis.student_id.last_name}</Typography>,
              ລົງທະບຽນຫ້ອງຮຽນ: <Typography>{regis.room_id.room_name}</Typography>,
              ລົງທະບຽນຊັ້ນຮຽນທີ່: <Typography>{regis.level_id.level_name}</Typography>,
              ປີການສຶກສາທີ່: <Typography>{regis.academic_year_no.academic_year}</Typography>,
              ລວມເງິນທັງໝົດຂອງການລົງທະບຽນ: <Typography>{formatMoney(regis.school_fee_id.total_fee)} ກີບ</Typography>,
              ຈ່າຍເງິນແລ້ວບໍ: <Typography>{regis.isPaid === "isPaid" ? "ຈ່າຍແລ້ວ" : "ບໍ່ທັນຈ່າຍ"}</Typography>,
            };
          })
        );
      }, [registrations]);
    return (
      <div ref={ref} style={{marginTop:"20px"}}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"} >ລາຍງານຂໍ້ມູນນັກຮຽນທີ່ລົງທະບຽນ</Typography>
        <TableComponentForRePort
          isLoading={isGetRegistrationsLoading}
          isLoadSuccess={isGetRegistrationsSuccess}
          isLoadError={getRegistrationsError.isError}
          data={registrationTable}
          all={registrations.registrations.totalCount}
        />
      </div>
    );
  }
);

export default RegistrationReport;

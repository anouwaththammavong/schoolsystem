import * as React from "react";
import { useForm } from "react-hook-form";
import { useAcademicYear } from "../../../hooks/useAcademicYear";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import RegistrationNewStudentPageHeaderText from "./RegistrationNewStudentPageHeaderText";
import { useClassRoom } from "../../../hooks/useClassRoom";
import { Registration } from "../../../../domain/Registrations";
import { useRegistration } from "../../../hooks/useRegistration";
import RegistrationFilterForm from "./RegistrationFilterForm";
import CreateNewStudentRegistrationForm from "./CreateNewStudentRegistrationForm";
import RegistrationTable from "./RegistrationTable";
import { useSchoolFee } from "../../../hooks/useSchoolFee";
import UpdateRegistrationForm from "./UpdateRegistrationForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  registration_id: string;
  anchorEl: null | HTMLElement;
}

const RegistrationNewStudentPage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm<{
    student_id: string;
    academic_year_no: string;
    room_id: string;
    isPaid: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [
    isOpenCreateNewStudentRegistrationDrawer,
    setIsOpenCreateNewStudentRegistrationDrawer,
  ] = React.useState(false);
  const [isOpenUpdateRegistrationDrawer, setIsOpenUpdateRegistrationDrawer] =
    React.useState(false);
  const [registrationHold, setRegistrationHold] = React.useState<
    Registration | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    registration_id: "",
    anchorEl: null,
  });

  const {
    registrationState,
    getRegistrations,
    createNewStudentRegistration,
    createNewStudentRegistrationReset,
    updateNewRegistration,
    updateNewRegistrationReset,
    deleteRegistration,
    deleteRegistrationReset,
  } = useRegistration();

  const { getAcademicYears, academicYearState } = useAcademicYear();

  const { getClassRooms, classRoomState } = useClassRoom();

  const { getSchoolFees, schoolFeeState } = useSchoolFee();

  const {
    registrations,
    isCreateNewStudentRegistrationSuccess,
    createNewStudentRegistrationError,
    isUpdateNewRegistrationSuccess,
    updateNewRegistrationError,
    isDeleteRegistrationSuccess,
    deleteRegistrationError,
  } = registrationState;

  const handleOpenCreateNewStudentRegistrationDrawer = () => {
    setIsOpenCreateNewStudentRegistrationDrawer(true);
  };

  const handleCloseCreateNewStudentRegistrationDrawer = () => {
    setIsOpenCreateNewStudentRegistrationDrawer(false);
  };

  const handleBlurSearch = (student_id: string) => {
    setValue("student_id", student_id);
    handleSearch();
  };

  const handleChangeAcademicYear = (academic_year_no: string) => {
    setValue("academic_year_no", academic_year_no);
    handleSearch();
  };

  const handleChangeClassRoom = (room_id: string) => {
    setValue("room_id", room_id);
    handleSearch();
  };

  const handleChangeIsPaid = (isPaid: string) => {
    setValue("isPaid", isPaid);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getRegistrations(
      rowPerPage,
      0,
      true,
      data.student_id,
      data.academic_year_no,
      data.room_id,
      data.isPaid
    );
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    registration_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      registration_id: registration_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      registration_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateRegistrationDrawer = () => {
    const registration = registrations.registrations.registrations.filter(
      (regis) => regis._id === openItemMenu.registration_id
    )[0];
    if (registration) {
      setRegistrationHold(registration);
      setIsOpenUpdateRegistrationDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        registration_id: "",
        anchorEl: null,
      });
    }
  };

  const handleOpenRegistrationNewStudentBillReport = () => {
    const registration = registrations.registrations.registrations.filter(
      (regis) => regis._id === openItemMenu.registration_id
    )[0];
    if (registration) {
      setRegistrationHold(registration);
      setOpenItemMenu({
        isOpen: false,
        registration_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateRegistrationDrawer = () => {
    setIsOpenUpdateRegistrationDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getRegistrations(
      rowPerPage,
      newPage * rowPerPage,
      true,
      data.student_id,
      data.academic_year_no,
      data.room_id,
      data.isPaid
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getRegistrations(
      parseInt(event.target.value, 10),
      0,
      true,
      data.student_id,
      data.academic_year_no,
      data.room_id,
      data.isPaid
    );
  };

  const handleOpenDeleteRegistrationAlertSweet = () => {
    const registration = openItemMenu.registration_id;
    if (registration) {
      setOpenItemMenu({
        isOpen: false,
        registration_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງການລົງທະບຽນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && registration) {
        deleteRegistration(registration);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateNewStudentRegistrationSuccess) {
      handleCloseCreateNewStudentRegistrationDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົງທະບຽນສຳເລັດ!",
        icon: "success",
      });
      createNewStudentRegistrationReset();
      getRegistrations(defaultLimit, 0, true);
    } else if (createNewStudentRegistrationError.isError) {
      handleCloseCreateNewStudentRegistrationDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createNewStudentRegistrationError.message,
        icon: "error",
      });
      createNewStudentRegistrationReset();
      getRegistrations(defaultLimit, 0, true);
    }
  }, [
    isCreateNewStudentRegistrationSuccess,
    createNewStudentRegistrationError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isUpdateNewRegistrationSuccess) {
      handleCloseUpdateRegistrationDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂການລົງທະບຽນສຳເລັດ!",
        icon: "success",
      });
      updateNewRegistrationReset();
      getRegistrations(defaultLimit, 0, true);
    } else if (updateNewRegistrationError.isError) {
      handleCloseUpdateRegistrationDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateNewRegistrationError.message,
        icon: "error",
      });
      updateNewRegistrationReset();
      getRegistrations(defaultLimit, 0, true);
    }
  }, [
    isUpdateNewRegistrationSuccess,
    updateNewRegistrationError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isDeleteRegistrationSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບການລົງທະບຽນສຳເລັດ!",
        icon: "success",
      });
      deleteRegistrationReset();
      getRegistrations(defaultLimit, 0, true);
    } else if (deleteRegistrationError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteRegistrationReset();
      getRegistrations(defaultLimit, 0, true);
    }
  }, [
    isDeleteRegistrationSuccess,
    deleteRegistrationError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getClassRooms(0, 0);
      getAcademicYears(0, 0);
      getSchoolFees(0, 0);
      getRegistrations(defaultLimit, 0, true);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
        <RegistrationNewStudentPageHeaderText
          handleOpenCreateNewStudentRegistrationDrawer={
            handleOpenCreateNewStudentRegistrationDrawer
          }
        />
        <RegistrationFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleSearch={handleSearch}
          handleBlurSearch={handleBlurSearch}
          handleChangeClassRoom={handleChangeClassRoom}
          handleChangeAcademicYear={handleChangeAcademicYear}
          handleChangeIsPaid={handleChangeIsPaid}
          registrationState={registrationState}
          classRoomState={classRoomState}
          academicYearState={academicYearState}
        />
        <CreateNewStudentRegistrationForm
          registrationState={registrationState}
          schoolFeeState={schoolFeeState}
          academicYearState={academicYearState}
          isOpenCreateNewStudentRegistrationDrawer={
            isOpenCreateNewStudentRegistrationDrawer
          }
          handleCloseCreateNewStudentRegistrationDrawer={
            handleCloseCreateNewStudentRegistrationDrawer
          }
          createNewStudentRegistration={createNewStudentRegistration}
        />

        {registrationHold && (
          <UpdateRegistrationForm
            registrationState={registrationState}
            schoolFeeState={schoolFeeState}
            academicYearState={academicYearState}
            isOpenUpdateRegistrationDrawer={isOpenUpdateRegistrationDrawer}
            handleCloseUpdateRegistrationDrawer={
              handleCloseUpdateRegistrationDrawer
            }
            updateNewRegistration={updateNewRegistration}
            registrationHold={registrationHold}
          />
        )}
        <RegistrationTable
          registrationState={registrationState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateRegistrationDrawer={
            handleOpenUpdateRegistrationDrawer
          }
          handleOpenDeleteRegistrationAlertSweet={
            handleOpenDeleteRegistrationAlertSweet
          }
          handleOpenRegistrationNewStudentBillReport={
            handleOpenRegistrationNewStudentBillReport
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
          registrationHold={registrationHold}
          isOpenUpdateRegistrationDrawer={isOpenUpdateRegistrationDrawer}
        />
      </Stack>
    </Box>
  );
};

export default RegistrationNewStudentPage;

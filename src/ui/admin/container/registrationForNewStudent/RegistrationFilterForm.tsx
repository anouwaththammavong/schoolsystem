import * as React from "react";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { RegistrationState } from "../../../../domain/Registrations";
import { AcademicYearState } from "../../../../domain/AcademicYears";
import { ClassRoomState } from "../../../../domain/ClassRoom";
import { Print, Search } from "@mui/icons-material";
import ReactToPrint from "react-to-print";
import RegistrationNewStudentReport from "./report/RegistrationNewStudentReport";

type Props = {
  control: Control<{
    student_id: string;
    academic_year_no: string;
    room_id: string;
    isPaid: string;
  }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleBlurSearch: (student_id: string) => void;
  handleChangeAcademicYear: (academic_year_no: string) => void;
  handleChangeIsPaid: (isPaid: string) => void;
  handleChangeClassRoom: (room_id: string) => void;
  registrationState: RegistrationState;
  classRoomState: ClassRoomState;
  academicYearState: AcademicYearState;
};

const RegistrationFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleBlurSearch,
  handleChangeClassRoom,
  handleChangeAcademicYear,
  handleChangeIsPaid,
  registrationState,
  classRoomState,
  academicYearState,
}: Props) => {
  const componentRef = React.useRef(null);
  const { isGetRegistrationsLoading, isGetRegistrationsSuccess } =
    registrationState;
  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;
  const {
    academic_years,
    isGetAcademicYearsLoading,
    isGetAcademicYearsSuccess,
  } = academicYearState;

  const isPaid: { title: string; isPaid: string }[] = [
    {
      title: "ຈ່າຍແລ້ວ",
      isPaid: "isPaid",
    },
    {
      title: "ຍັງບໍ່ທັນຈ່າຍ",
      isPaid: "notPaid",
    },
  ];

  return (
    <Box>
      {isGetRegistrationsLoading ||
        isGetClassRoomsLoading ||
        (isGetAcademicYearsLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        ))}
      {isGetRegistrationsSuccess &&
        isGetClassRoomsSuccess &&
        isGetAcademicYearsSuccess && (
          <form onSubmit={handleSubmit(handleSearch)}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="student_id"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      label="ຄົ້ນຫາໄອດີນັກຮຽນ"
                      size="small"
                      fullWidth
                      onBlur={(e) => handleBlurSearch(e.target.value)}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="start">
                            <Search />
                          </InputAdornment>
                        ),
                      }}
                      variant="filled"
                    />
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="room_id"
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ຫ້ອງຮຽນທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeClassRoom(e.target.value)
                        }
                      >
                        {classRooms.classrooms.classRooms.map((val, key) => (
                          <MenuItem key={key} value={val._id}>
                            {val.room_name} {"ຊັ້ນຮຽນ"}
                            {val.level_id.level_name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="academic_year_no"
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ປີການສຶກສາທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeAcademicYear(e.target.value)
                        }
                      >
                        {academic_years.academicYears.academicYear.map(
                          (val, key) => (
                            <MenuItem key={key} value={val._id}>
                              {val.academic_year}
                            </MenuItem>
                          )
                        )}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="isPaid"
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>
                        ເລືອກ ເບິ່ງຂໍ້ມູນນັກຮຽນທີ່ຈ່າຍເງິນຫຼືບໍ
                      </InputLabel>
                      <Select
                        {...field}
                        size="small"
                        // value={String(field.value)}
                        onChange={(e: SelectChangeEvent<string>) => {
                          // const value = e.target.value === "true";
                          handleChangeIsPaid(e.target.value);
                          // field.onChange(value);
                        }}
                      >
                        {isPaid.map((val, key) => (
                          <MenuItem key={key} value={val.isPaid}>
                            {val.title}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetRegistrationsLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetRegistrationsLoading && (
                        <CircularProgress
                          color="inherit"
                          size={"2rem"}
                          sx={{ marginLeft: 1 }}
                        />
                      )}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນການລົງທະບຽນ"
                pageStyle="print"
              />
              <Box display={"none"}>
              <RegistrationNewStudentReport registrationState={registrationState}  ref={componentRef} />
              </Box>
            </Grid>
            </Grid>
          </form>
        )}
    </Box>
  );
};

export default RegistrationFilterForm;

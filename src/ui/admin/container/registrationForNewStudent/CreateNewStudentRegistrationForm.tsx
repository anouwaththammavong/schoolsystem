import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Skeleton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm, useWatch } from "react-hook-form";
import {
  RegistrationState,
  CreateNewStudentRegistration,
} from "../../../../domain/Registrations";
// import { ClassLevel, ClassLevelState } from "../../../../domain/ClassLevel";
import {
  AcademicYear,
  AcademicYearState,
} from "../../../../domain/AcademicYears";
import { SchoolFee, SchoolFeeState } from "../../../../domain/SchoolFees";
import moment from "moment";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

type Props = {
  registrationState: RegistrationState;
  // classLevelState: ClassLevelState;
  academicYearState: AcademicYearState;
  schoolFeeState: SchoolFeeState;
  isOpenCreateNewStudentRegistrationDrawer: boolean;
  handleCloseCreateNewStudentRegistrationDrawer: () => void;
  createNewStudentRegistration: (registration: CreateNewStudentRegistration) => void;
};

const CreateNewStudentRegistrationForm = ({
  registrationState,
  // classLevelState,
  academicYearState,
  schoolFeeState,
  isOpenCreateNewStudentRegistrationDrawer,
  handleCloseCreateNewStudentRegistrationDrawer,
  createNewStudentRegistration,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const {
    isCreateNewStudentRegistrationLoading,
    isCreateNewStudentRegistrationSuccess,
  } = registrationState;

  // const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
  //   classLevelState;

  const {
    academic_years,
    isGetAcademicYearsLoading,
    isGetAcademicYearsSuccess,
  } = academicYearState;

  const { schoolFees, isGetSchoolFeesLoading, isGetSchoolFeesSuccess } =
    schoolFeeState;

  const {
    handleSubmit,
    control,
    setValue,
    getValues,
    reset,
    formState: { errors },
  } = useForm<CreateNewStudentRegistration>();

  const isPaid: { title: string; isPaid: string }[] = [
    {
      title: "ຈ່າຍແລ້ວ",
      isPaid: "isPaid",
    },
    {
      title: "ຍັງບໍ່ທັນຈ່າຍ",
      isPaid: "notPaid",
    },
  ];

  const handleChangeRegistration_date = (registration_date: any) => {
    if (registration_date) {
      setValue("registration_date", registration_date);
    }
  };

  const handleCreateNewStudentRegistration = (
    data: CreateNewStudentRegistration
  ) => {
    createNewStudentRegistration({
      registration_id: data.registration_id,
      student_id: data.student_id,
      academic_year_no: data.academic_year_no,
      school_fee_id: data.school_fee_id,
      registration_date: data.registration_date,
      isPaid: data.isPaid,
    });
  };

  React.useEffect(() => {
    if (isCreateNewStudentRegistrationSuccess) {
      reset({
        registration_id: "",
        student_id: "",
        school_fee_id: "",
        academic_year_no: "",
        registration_date: moment().toDate(),
        isPaid: "",
      });
    }
  }, [isCreateNewStudentRegistrationSuccess]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateNewStudentRegistrationDrawer}
      onClose={() => handleCloseCreateNewStudentRegistrationDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleCreateNewStudentRegistration)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມຂໍ້ມູນການລົງທະບຽນນັກຮຽນໃໝ່
                  </Typography>
                  <IconButton
                    onClick={() =>
                      handleCloseCreateNewStudentRegistrationDrawer()
                    }
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="registration_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໄອດີລົງທະບຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateNewStudentRegistrationLoading}
                    />
                  )}
                />
                {errors &&
                  errors.registration_id &&
                  errors.registration_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນໄອດີຂອງການລົງທະບຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລະຫັດນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateNewStudentRegistrationLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student_id &&
                  errors.student_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລະຫັດນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                {isGetAcademicYearsLoading && <Skeleton />}
                {isGetAcademicYearsSuccess &&
                  academic_years.academicYears.totalAcademicYears > 0 && (
                    <Controller
                      control={control}
                      name={`academic_year_no`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={academic_years.academicYears.academicYear}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => option.academic_year_no}
                          onChange={(
                            event: any,
                            newValue: AcademicYear | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {"ປີການສຶກສາທີ່ "}
                              {option.academic_year} {"ສົກຮຽນທີ່ "}
                              {option.academic_year_no}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ສົກຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.academic_year_no &&
                  errors.academic_year_no.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນສົກຮຽນ"}
                    </Typography>
                  )}
                {isGetAcademicYearsSuccess &&
                  academic_years.academicYears.totalAcademicYears <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຂໍ້ມູນສົກຮຽນກ່ອນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                {isGetSchoolFeesLoading && <Skeleton />}
                {isGetSchoolFeesSuccess &&
                  schoolFees.school_fees.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`school_fee_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={schoolFees.school_fees.schoolFees}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) =>
                            `ປີການສຶກສາ ${option.academic_year_no.academic_year_no} ຊັ້ນຮຽນ ${option.level_id.level_name}`
                          }
                          onChange={(
                            event: any,
                            newValue: SchoolFee | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {"ປີການສຶກສາທີ່ "}
                              {option.academic_year_no.academic_year_no}{" "}
                              {"ຊັ້ນຮຽນທີ່ "}
                              {option.level_id.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຄ່າທຳນຽມ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.school_fee_id &&
                  errors.school_fee_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຄ່າທຳນຽມ"}
                    </Typography>
                  )}
                {isGetSchoolFeesSuccess &&
                  schoolFees.school_fees.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຂໍ້ມູນຄ່າທຳນຽມກ່ອນ"}
                    </Typography>
                  )}
              </Grid>
              {/* <Grid item xs={12}>
                {isGetClassLevelsLoading && <Skeleton />}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`level_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={classLevels.classLevels.classLevels}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => option.level_name}
                          onChange={(
                            event: any,
                            newValue: ClassLevel | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {option.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຊັ້ນຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.level_id &&
                  errors.level_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຂອງຊັ້ນຣຽນ"}
                    </Typography>
                  )}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຊັ້ນຮຽນ"}
                    </Typography>
                  )}
              </Grid> */}

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="registration_date"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DesktopDatePicker
                        {...field}
                        label="ວັນທີ່ມາລົງທະບຽນຮຽນ"
                        inputFormat="DD/MM/YYYY"
                        onChange={(e) => handleChangeRegistration_date(e)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="filled"
                            size="small"
                            fullWidth
                          />
                        )}
                      />
                    </LocalizationProvider>
                  )}
                />

                {errors &&
                  errors.registration_date &&
                  errors.registration_date.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name={`isPaid`}
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      options={isPaid}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => option.title}
                      onChange={(
                        event: any,
                        newValue: { title: string; isPaid: string } | null
                      ) => {
                        field.onChange(newValue?.isPaid);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{
                            "& > img": { mr: 2, flexShrink: 0 },
                          }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ຈ່າຍເງິນ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                            autoComplete: "disabled",
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.isPaid &&
                  errors.isPaid.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນການຈ່າຍເງິນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateNewStudentRegistrationLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateNewStudentRegistrationForm;

import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateNewStudentRegistrationDrawer: () => void;
}

const RegistrationNewStudentPageHeaderText = ({handleOpenCreateNewStudentRegistrationDrawer}: Props) => {
    return (
        <Box>
          <Stack
            direction={'row'}
            spacing={2}
          >
            <Typography
              variant='h5'
            >
              ຂໍ້ມູນການລົງທະບຽນ
            </Typography>
            <Button
              variant='contained'
              onClick={() => handleOpenCreateNewStudentRegistrationDrawer()}
            >
              <Add /> ເພີ່ມຂໍ້ມູນການລົງທະບຽນ
            </Button>
          </Stack>
        </Box>
      )
}

export default RegistrationNewStudentPageHeaderText
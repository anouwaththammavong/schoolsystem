import * as React from 'react';
import ProfileCard from './ProfileCard';
import { Box } from '@mui/material';
import { Stack } from '@mui/system';
import ChangePasswordForm from './ChangePasswordForm';
import { useProfile } from '../../../hooks/useProfile';

export interface OpenItemMenu {
  isOpen: boolean;
  itemId: string;
  anchorEl: null | HTMLElement;
}

const ProfilePage = () => {
  const [isOpenChangePasswordDrawer, setIsOpenChangePasswordDrawer] = React.useState(false);

  const {
    profileState,
    changePassword
  } = useProfile();

  const { isChangePasswordSuccess } = profileState;

  const handleOpenChangePasswordDrawer = () => {
    setIsOpenChangePasswordDrawer(true);
  }

  const handleCloseChangePasswordDrawer = () => {
    setIsOpenChangePasswordDrawer(false);
  }

  React.useEffect(() => {
    handleCloseChangePasswordDrawer();
  }, [isChangePasswordSuccess])

  return (
    <Box>
      <Stack
        spacing={3}
      >
        <ChangePasswordForm
          changePassword={changePassword}
          handleCloseChangePasswordDrawer={handleCloseChangePasswordDrawer}
          isOpenChangePasswordDrawer={isOpenChangePasswordDrawer}
          profileState={profileState}
        />
        <ProfileCard
          profileState={profileState}
          handleOpenChangePasswordDrawer={handleOpenChangePasswordDrawer}
        />
      </Stack>
    </Box>
  );
};

export default ProfilePage;

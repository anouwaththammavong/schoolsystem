import * as React from 'react';
import { Avatar, Button, Card, CardContent, Stack, Typography } from '@mui/material';
import profileIcon from '../../../../assets/images/profile-icon.png'
import { ProfileState } from '../../../../domain/profile';

interface Props {
    profileState: ProfileState;
    handleOpenChangePasswordDrawer: () => void;
}

const ProfileCard: React.FC<Props> = ({
    profileState,
    handleOpenChangePasswordDrawer
}) => {

    const { profile } = profileState;

    return (
        <Card>
            <CardContent>
                <Stack
                    alignItems={'center'}
                    spacing={3}
                >
                    <Avatar
                        src={profileIcon}
                        sx={{ width: 200, height: 200 }}
                    />
                    <Stack
                        alignItems={'center'}
                    >
                        <Typography
                            variant='h5'
                            fontWeight={'bold'}
                        >
                            {profile?.email}
                        </Typography>
                        <Typography
                            variant='body1'
                        >
                            {profile?.role}
                        </Typography>
                    </Stack>
                    <Button
                        variant='contained'
                        sx={{ paddingInline: 10 }}
                        onClick={() => handleOpenChangePasswordDrawer()}
                    >
                        ປ່ຽນລະຫັດຜ່ານ
                    </Button>
                </Stack>
            </CardContent>
        </Card>
    );
};

export default ProfileCard;

import * as React from 'react';
import { Box, Button, Drawer, Grid, IconButton, TextField, Typography, useMediaQuery, useTheme } from '@mui/material';
import { Close } from '@mui/icons-material';
import { Stack } from '@mui/system';
import { Controller, useForm } from 'react-hook-form';
import { IChangePasswordHold, ProfileState } from '../../../../domain/profile';

interface Props {
    profileState: ProfileState;
    isOpenChangePasswordDrawer: boolean;
    handleCloseChangePasswordDrawer: () => void;
    changePassword: (changePasswordHold: IChangePasswordHold) => void;
}

const ChangePasswordForm: React.FC<Props> = ({ profileState, isOpenChangePasswordDrawer, handleCloseChangePasswordDrawer, changePassword }) => {
    const theme = useTheme();
    const breakpointSm = useMediaQuery(theme.breakpoints.down('sm'));
    const breakpointMd = useMediaQuery(theme.breakpoints.down('md'));
    const breakpointLg = useMediaQuery(theme.breakpoints.down('lg'));

    const { isChangePasswordLoading } = profileState;

    const {
        handleSubmit,
        control,
        formState: { errors }
    } = useForm();

    const handleChangePassword = (data: any) => {
        changePassword({ ...data });
    }

    return (
        <Drawer
            anchor={'right'}
            open={isOpenChangePasswordDrawer}
            onClose={() => handleCloseChangePasswordDrawer()}
        >
            <form
                style={{ width: '100%' }}
                onSubmit={handleSubmit(handleChangePassword)}
            >
                <Box
                    width={breakpointSm ? '100%' : (breakpointMd ? 600 : (breakpointLg ? 900 : 1200))}
                >
                    <Stack
                        spacing={3}
                        padding={3}
                    >
                        <Grid
                            container
                            spacing={3}
                            alignItems='center'
                            justifyContent='center'
                        >
                            <Grid
                                item
                                xs={12}
                            >
                                <Stack
                                    direction={'row'}
                                    justifyContent='space-between'
                                >
                                    <Typography
                                        variant={'h5'}
                                        component='div'
                                    >
                                        ປ່ຽນແປງລະຫັດຜ່ານ
                                    </Typography>
                                    <IconButton
                                        onClick={() => handleCloseChangePasswordDrawer()}
                                    >
                                        <Close />
                                    </IconButton>
                                </Stack>
                            </Grid>
                            <Grid
                                item
                                xs={12}
                            >
                                <Controller
                                    control={control}
                                    name='oldPassword'
                                    rules={{
                                        required: true,
                                        maxLength: 40,
                                        minLength: 4,
                                    }}
                                    render={({ field }) =>
                                        <TextField
                                            {...field}
                                            size='small'
                                            type='password'
                                            label={'ລະຫັດຜ່ານເກົ່າ'}
                                            fullWidth
                                            variant='filled'
                                            disabled={isChangePasswordLoading}
                                        />
                                    }
                                />
                                {
                                    errors.oldPassword && errors.oldPassword.type === 'required' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ
                                    </Typography>
                                }

                                {
                                    errors.oldPassword && errors.oldPassword.type === 'maxLength' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ບໍ່ສາມາດປ້ອນຂໍ້ມູນເກີນກວ່າ 40 ໂຕອັກສອນ
                                    </Typography>
                                }

                                {
                                    errors.oldPassword && errors.oldPassword.type === 'minLength' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ບໍ່ສາມາດປ້ອນຂໍ້ມູນໜ້ອຍກວ່າ 4 ໂຕອັກສອນ
                                    </Typography>
                                }
                            </Grid>
                            <Grid
                                item
                                xs={12}
                            >
                                <Controller
                                    control={control}
                                    name='newPassword'
                                    rules={{
                                        required: true,
                                        maxLength: 40,
                                        minLength: 4,
                                    }}
                                    render={({ field }) =>
                                        <TextField
                                            {...field}
                                            size='small'
                                            type='password'
                                            label={'ລະຫັດຜ່ານໃໝ່'}
                                            fullWidth
                                            variant='filled'
                                            disabled={isChangePasswordLoading}
                                        />
                                    }
                                />
                                {
                                    errors.newPassword && errors.newPassword.type === 'required' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ
                                    </Typography>
                                }

                                {
                                    errors.newPassword && errors.newPassword.type === 'maxLength' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ບໍ່ສາມາດປ້ອນຂໍ້ມູນເກີນກວ່າ 40 ໂຕອັກສອນ
                                    </Typography>
                                }

                                {
                                    errors.newPassword && errors.newPassword.type === 'minLength' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ບໍ່ສາມາດປ້ອນຂໍ້ມູນໜ້ອຍກວ່າ 4 ໂຕອັກສອນ
                                    </Typography>
                                }
                            </Grid>
                            <Grid
                                item
                                xs={12}
                            >
                                <Button
                                    type='submit'
                                    variant='contained'
                                    sx={{ paddingInline: 5 }}
                                    disabled={isChangePasswordLoading}
                                >
                                    ບັນທຶກ
                                </Button>
                            </Grid>
                        </Grid>
                    </Stack>
                </Box>
            </form>
        </Drawer>
    );
};

export default ChangePasswordForm;

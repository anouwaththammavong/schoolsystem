import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateClassRoomDrawer: () => void;
}

const ClassRoomPageHeaderText = ({handleOpenCreateClassRoomDrawer}: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນຫ້ອງຮຽນ
        </Typography>
        <Button
          variant='contained'
          onClick={() => handleOpenCreateClassRoomDrawer()}
        >
          <Add /> ເພີ່ມຫ້ອງຮຽນ
        </Button>
      </Stack>
    </Box>
  )
}

export default ClassRoomPageHeaderText
import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./ClassRoomPage";
import { ClassRoomState } from "../../../../domain/ClassRoom";

type Props = {
  classRoomState: ClassRoomState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateClassRoomDrawer: () => void;
  handleOpenDeleteClassRoomAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const ClassRoomTable = ({
  classRoomState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateClassRoomDrawer,
  handleOpenDeleteClassRoomAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [classRoomTable, setClassRoomTable] = React.useState<Array<any>>([]);

  const {
    classRooms,
    isGetClassRoomsLoading,
    isGetClassRoomsSuccess,
    getClassRoomsError,
  } = classRoomState;

  React.useEffect(() => {
    setClassRoomTable(
      classRooms.classrooms.classRooms.map((room) => {
        return {
          ໄອດີຂອງຫ້ອງຮຽນ: <Typography>{room.room_id}</Typography>,
          ຫ້ອງຮຽນ: <Typography>{room.room_name}</Typography>,
          ຊັ້ນຮຽນ: <Typography>{room.level_id.level_name}</Typography>,
          "": (
            <>
              <IconButton
                onClick={(e) => handleOpenItemMenu(e, room._id, room.room_name)}
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen && openItemMenu.class_room_id === room._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateClassRoomDrawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem onClick={() => handleOpenDeleteClassRoomAlertSweet()}>
                  ລົບຂໍ້ມູນ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [classRooms, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetClassRoomsLoading}
          isLoadSuccess={isGetClassRoomsSuccess}
          isLoadError={getClassRoomsError.isError}
          data={classRoomTable}
          all={classRooms.classrooms.totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default ClassRoomTable;
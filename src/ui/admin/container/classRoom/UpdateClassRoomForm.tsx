import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Skeleton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import {
  ClassRoom,
  ClassRoomState,
  UpdateClassRoom,
} from "../../../../domain/ClassRoom";
import { ClassLevel, ClassLevelState } from "../../../../domain/ClassLevel";

type Props = {
  classRoomState: ClassRoomState;
  classLevelState: ClassLevelState;
  isOpenUpdateClassRoomDrawer: boolean;
  handleCloseUpdateClassRoomDrawer: () => void;
  updateClassRoom: (_id: string, classRoom: UpdateClassRoom) => void;
  classRoomHold: ClassRoom | undefined;
};

const UpdateClassRoomForm = ({
  classRoomState,
  classLevelState,
  isOpenUpdateClassRoomDrawer,
  handleCloseUpdateClassRoomDrawer,
  updateClassRoom,
  classRoomHold,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isUpdateClassRoomLoading } = classRoomState;

  const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
    classLevelState;

  const {
    handleSubmit,
    control,
    setValue,
    trigger,
    getValues,
    formState: { errors },
  } = useForm<UpdateClassRoom>();

  const handleUpdateClassRoom = (data: UpdateClassRoom) => {
    if (classRoomHold) {
      updateClassRoom(classRoomHold?._id, {
        room_id: data.room_id,
        room_name: data.room_name,
        level_id: data.level_id,
      });
    }
  };

  React.useEffect(() => {
    if (classRoomHold) {
      setValue("room_id", classRoomHold.room_id);
      setValue("room_name", classRoomHold.room_name);
      setValue("level_id", classRoomHold.level_id._id);
    }
  }, [classRoomHold]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateClassRoomDrawer}
      onClose={() => handleCloseUpdateClassRoomDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleUpdateClassRoom)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ແກ້ໄຂຫ້ອງຮຽນ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseUpdateClassRoomDrawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="room_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໄອດີຫ້ອງຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateClassRoomLoading}
                    />
                  )}
                />
                {errors &&
                  errors.room_id &&
                  errors.room_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນໄອດີຂອງຫ້ອງຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="room_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ຂອງຫ້ອງຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateClassRoomLoading}
                    />
                  )}
                />
                {errors &&
                  errors.room_name &&
                  errors.room_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຊື່ຂອງຫ້ອງຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                {isGetClassLevelsLoading && <Skeleton />}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`level_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={classLevels.classLevels.classLevels}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => option.level_name}
                          onChange={(
                            event: any,
                            newValue: ClassLevel | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {option.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຊັ້ນຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                          defaultValue={classLevels.classLevels.classLevels.find(
                            (o) => o._id === classRoomHold?.level_id._id
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.level_id &&
                  errors.level_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຂອງຊັ້ນຣຽນ"}
                    </Typography>
                  )}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຊັ້ນຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateClassRoomLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateClassRoomForm;

import * as React from "react";
import { useForm } from "react-hook-form";
import { ClassRoom } from "../../../../domain/ClassRoom";
import { useClassRoom } from "../../../hooks/useClassRoom";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import ClassRoomPageHeaderText from "./ClassRoomPageHeaderText";
import { useClassLevel } from "../../../hooks/useClassLevel";
import CreateClassRoomForm from "./CreateClassRoomForm";
import ClassRoomTable from "./ClassRoomTable";
import UpdateClassRoomForm from "./UpdateClassRoomForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  class_room_id: string;
  anchorEl: null | HTMLElement;
}

const ClassRoomPage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateClassRoomDrawer, setIsOpenCreateClassRoomDrawer] =
    React.useState(false);
  const [isOpenUpdateClassRoomDrawer, setIsOpenUpdateClassRoomDrawer] =
    React.useState(false);
  const [classRoomHold, setClassRoomHold] = React.useState<
    ClassRoom | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    class_room_id: "",
    anchorEl: null,
  });

  const {
    classRoomState,
    getClassRooms,
    createClassRoom,
    createClassRoomReset,
    updateClassRoom,
    updateClassRoomReset,
    deleteClassRoom,
    deleteClassRoomReset,
  } = useClassRoom();

  const { getClassLevels, classLevelState } = useClassLevel();

  const {
    classRooms,
    isCreateClassRoomSuccess,
    createClassRoomError,
    isUpdateClassRoomSuccess,
    updateClassRoomError,
    isDeleteClassRoomSuccess,
    deleteClassRoomError,
  } = classRoomState;

  const handleOpenCreateClassRoomDrawer = () => {
    setIsOpenCreateClassRoomDrawer(true);
  };

  const handleCloseCreateClassRoomDrawer = () => {
    setIsOpenCreateClassRoomDrawer(false);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    class_room_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      class_room_id: class_room_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      class_room_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateClassRoomDrawer = () => {
    const classRoom = classRooms.classrooms.classRooms.filter(
      (room) => room._id === openItemMenu.class_room_id
    )[0];
    if (classRoom) {
      setClassRoomHold(classRoom);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateClassRoomDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        class_room_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateClassRoomDrawer = () => {
    setIsOpenUpdateClassRoomDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    getClassRooms(rowPerPage, newPage * rowPerPage);
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    getClassRooms(parseInt(event.target.value, 10), 0);
  };

  const handleOpenDeleteClassRoomAlertSweet = () => {
    const class_room = openItemMenu.class_room_id;
    if (class_room) {
      setOpenItemMenu({
        isOpen: false,
        class_room_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງຫ້ອງຮຽນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && class_room) {
        deleteClassRoom(class_room);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateClassRoomSuccess) {
      handleCloseCreateClassRoomDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມຫ້ອງຮຽນສຳເລັດ!",
        icon: "success",
      });
      createClassRoomReset();
      getClassRooms(defaultLimit, 0);
    } else if (createClassRoomError.isError) {
      handleCloseCreateClassRoomDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createClassRoomError.message,
        icon: "error",
      });
      createClassRoomReset();
      getClassRooms(defaultLimit, 0);
    }
  }, [isCreateClassRoomSuccess, createClassRoomError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateClassRoomSuccess) {
      handleCloseUpdateClassRoomDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນຂອງຫ້ອງຮຽນນີ້ສຳເລັດ!",
        icon: "success",
      });
      updateClassRoomReset();
      getClassRooms(defaultLimit, 0);
    } else if (updateClassRoomError.isError) {
      handleCloseUpdateClassRoomDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateClassRoomError.message,
        icon: "error",
      });
      updateClassRoomReset();
      getClassRooms(defaultLimit, 0);
    }
  }, [isUpdateClassRoomSuccess, updateClassRoomError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteClassRoomSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງຫ້ອງຮຽນສຳເລັດ!",
        icon: "success",
      });
      deleteClassRoomReset();
      getClassRooms(defaultLimit, 0);
    } else if (deleteClassRoomError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteClassRoomReset();
      getClassRooms(defaultLimit, 0);
    }
  }, [isDeleteClassRoomSuccess, deleteClassRoomError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getClassLevels(0, 0);
      getClassRooms(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
        <ClassRoomPageHeaderText
          handleOpenCreateClassRoomDrawer={handleOpenCreateClassRoomDrawer}
        />
        <CreateClassRoomForm
          classRoomState={classRoomState}
          classLevelState={classLevelState}
          createClassRoom={createClassRoom}
          handleCloseCreateClassRoomDrawer={handleCloseCreateClassRoomDrawer}
          isOpenCreateClassRoomDrawer={isOpenCreateClassRoomDrawer}
        />
        {classRoomHold && (
          <UpdateClassRoomForm
            classRoomState={classRoomState}
            classLevelState={classLevelState}
            isOpenUpdateClassRoomDrawer={isOpenUpdateClassRoomDrawer}
            handleCloseUpdateClassRoomDrawer={handleCloseUpdateClassRoomDrawer}
            updateClassRoom={updateClassRoom}
            classRoomHold={classRoomHold}
          />
        )}
        <ClassRoomTable
          classRoomState={classRoomState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateClassRoomDrawer={handleOpenUpdateClassRoomDrawer}
          handleOpenDeleteClassRoomAlertSweet={
            handleOpenDeleteClassRoomAlertSweet
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default ClassRoomPage;

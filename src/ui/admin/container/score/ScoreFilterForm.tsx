import * as React from "react";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { StudentState } from "../../../../domain/Student";
import { ScoreState } from "../../../../domain/Scores";
import { ClassRoomState } from "../../../../domain/ClassRoom";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import ReactToPrint from "react-to-print";
import { Print } from "@mui/icons-material";
import ScoreReport from "./report/ScoreReport";

type Props = {
  control: Control<{
    room_id: string;
    month: Date;
    generation: string;
    term: string;
  }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleChangeClassRoom: (room_id: string) => void;
  handleChangeMonth: (month: Date) => void;
  handleChangeGeneration: (generation: string) => void;
  handleChangeTerm: (term: string) => void;
  scoreState: ScoreState;
  studentState: StudentState;
  classRoomState: ClassRoomState;
};

const ScoreFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleChangeClassRoom,
  handleChangeMonth,
  handleChangeGeneration,
  handleChangeTerm,
  scoreState,
  studentState,
  classRoomState,
}: Props) => {
  const componentRef = React.useRef(null);
  const { isGetScoresLoading, isGetScoresSuccess } = scoreState;
  const { students, isGetStudentsLoading, isGetStudentsSuccess } = studentState;

  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;

  const term1AndTerm2: { title: string }[] = [
    {
      title: "ພາກຮຽນ1",
    },
    {
      title: "ພາກຮຽນ2",
    },
  ];
  return (
    <Box>
      {isGetStudentsLoading ||
        isGetClassRoomsLoading ||
        (isGetScoresLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        ))}
      {isGetStudentsSuccess && isGetClassRoomsSuccess && isGetScoresSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="room_id"
                defaultValue={classRooms.classrooms.classRooms[0]._id}
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ຫ້ອງຮຽນທີ່</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeClassRoom(e.target.value)
                      }
                    >
                      {classRooms.classrooms.classRooms.map((val, key) => (
                        <MenuItem key={key} value={val._id}>
                          {val.room_name} {"ຊັ້ນຮຽນ"}
                          {val.level_id.level_name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="generation"
                defaultValue={students.students.uniqueGenerations[0].generation}
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ນັກຮຽນລຸ້ນທີ່</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeGeneration(e.target.value)
                      }
                    >
                      {students.students.uniqueGenerations.map((val, key) => (
                        <MenuItem key={key} value={val.generation}>
                          {val.generation}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="month"
                rules={{
                  required: true,
                }}
                defaultValue={new Date()}
                render={({ field }) => (
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DesktopDatePicker
                      {...field}
                      label="ວັນທີ"
                      inputFormat="DD/MM/YYYY"
                      value={field.value ? dayjs(field.value) : null}
                      onChange={(date) => {
                        if (date) {
                          const formattedDate = date.toDate(); // Convert dayjs object to native Date object
                          field.onChange(formattedDate); // Update the form state with the native Date object
                          handleChangeMonth(formattedDate); // Call your custom handler with the Date object
                        }
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="filled"
                          size="small"
                          fullWidth
                        />
                      )}
                    />
                  </LocalizationProvider>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="term"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ພາກຮຽນທີ່</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) => {
                        handleChangeTerm(e.target.value);
                      }}
                    >
                      {term1AndTerm2.map((val, key) => (
                        <MenuItem key={key} value={val.title}>
                          {val.title}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetScoresLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetScoresLoading && (
                        <CircularProgress
                          color="inherit"
                          size={"2rem"}
                          sx={{ marginLeft: 1 }}
                        />
                      )}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນອາຈານ"
                pageStyle="print"
              />
              <Box display={"none"}>
                <ScoreReport scoreState={scoreState} ref={componentRef} />
              </Box>
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default ScoreFilterForm;

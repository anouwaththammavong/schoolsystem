import * as React from "react";
import { useScore } from "../../../hooks/useScore";
import Swal from "sweetalert2";
import { Score } from "../../../../domain/Scores";
import { useForm } from "react-hook-form";
import { useStudent } from "../../../hooks/useStudent";
import { useClassRoom } from "../../../hooks/useClassRoom";
import { useSubjectV2 } from "../../../hooks/useSubjectV2";
import { useTeacher } from "../../../hooks/useTeacher";
import { useClassLevel } from "../../../hooks/useClassLevel";

export interface OpenItemMenu {
  isOpen: boolean;
  score_id: string;
  anchorEl: null | HTMLElement;
}

type Props = {
  defaultLimit: number;
};

const UseHookScorePage = ({ defaultLimit }: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm<{
    room_id: string;
    month: Date;
    generation: string;
    term: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateScoreDrawer, setIsOpenCreateScoreDrawer] =
    React.useState(false);
  const [isOpenUpdateScoreDrawer, setIsOpenUpdateScoreDrawer] =
    React.useState(false);
  const [isOpenScoreDetailsDialog, setIsOpenScoreDetailsDialog] =
    React.useState(false);
  const [scoreHold, setScoreHold] = React.useState<Score | undefined>();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    score_id: "",
    anchorEl: null,
  });

  const {
    scoreState,
    getScores,
    createScore,
    createScoreReset,
    updateScore,
    updateScoreReset,
    deleteScore,
    deleteScoreReset,
  } = useScore();

  const { getStudents, studentState } = useStudent();

  const { getClassRooms, classRoomState } = useClassRoom();

  const { getClassLevels, classLevelState } = useClassLevel();

  const { getSubjectV2s, subjectV2State } = useSubjectV2();

  const { getTeachers, teacherState } = useTeacher();

  const {
    scores,
    isCreateScoreSuccess,
    createScoreError,
    isUpdateScoreSuccess,
    updateScoreError,
    isDeleteScoreSuccess,
    deleteScoreError,
  } = scoreState;

  const { students, isGetStudentsSuccess } = studentState;

  const { classRooms, isGetClassRoomsSuccess } = classRoomState;

  const handleChangeClassRoom = (room_id: string) => {
    setValue("room_id", room_id);
    handleSearch();
  };

  const handleChangeMonth = (month: Date) => {
    setValue("month", month);
    handleSearch();
  };

  const handleChangeGeneration = (generation: string) => {
    setValue("generation", generation);
    handleSearch();
  };

  const handleChangeTerm = (term: string) => {
    setValue("term", term);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getScores(
      rowPerPage,
      0,
      data.room_id,
      data.month,
      data.generation,
      data.term
    );
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    score_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      score_id: score_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      score_id: "",
      anchorEl: null,
    });
  };

  const handleOpenCreateScoreDrawer = () => {
    setIsOpenCreateScoreDrawer(true);
  };

  const handleCloseCreateScoreDrawer = () => {
    setIsOpenCreateScoreDrawer(false);
  };

  const handleOpenUpdateScoreDrawer = () => {
    const score = scores.scores.scores.filter(
      (score) => score._id === openItemMenu.score_id
    );
    if (score) {
      setScoreHold(score[0]);
      setIsOpenUpdateScoreDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        score_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateScoreDrawer = () => {
    setIsOpenUpdateScoreDrawer(false);
  };

  const handleOpenScoreDetailsDialog = () => {
    const score = scores.scores.scores.filter(
      (score) => score._id === openItemMenu.score_id
    );
    if (score) {
      setScoreHold(score[0]);
      setIsOpenScoreDetailsDialog(true);
      setOpenItemMenu({
        isOpen: false,
        score_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseScoreDetailsDialog = () => {
    setIsOpenScoreDetailsDialog(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getScores(
      rowPerPage,
      newPage * rowPerPage,
      data.room_id,
      data.month,
      data.generation,
      data.term
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getScores(
      parseInt(event.target.value, 10),
      0,
      data.room_id,
      data.month,
      data.generation,
      data.term
    );
  };

  const handleOpenDeleteScoreAlertSweet = () => {
    const absence = openItemMenu.score_id;
    if (absence) {
      setOpenItemMenu({
        isOpen: false,
        score_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງຄະແນນສອບເສັງຂອງນັກຮຽນຜູ້ນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && absence) {
        deleteScore(absence);
      }
    });
  };

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getClassRooms(0, 0);
      getStudents(0, 0);
      getSubjectV2s(0, 0);
      getTeachers(0, 0);
      getClassLevels(0, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  React.useEffect(() => {
    if (isGetClassRoomsSuccess && isGetStudentsSuccess) {
      getScores(
        defaultLimit,
        0,
        classRooms.classrooms.classRooms[0]?._id,
        new Date(),
        students.students.uniqueGenerations[0]?.generation
      );
    }
  }, [isGetClassRoomsSuccess, isGetStudentsSuccess]);

  React.useEffect(() => {
    if (isCreateScoreSuccess) {
      const data = getValues();
      handleCloseCreateScoreDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມຄະແນນສຳເລັດ!",
        icon: "success",
      });
      createScoreReset();
      getScores(
        defaultLimit,
        0,
        data.room_id,
        data.month,
        data.generation,
        data.term
      );
    } else if (createScoreError.isError) {
      const data = getValues();
      handleCloseCreateScoreDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createScoreError.message,
        icon: "error",
      });
      createScoreReset();
      getScores(
        defaultLimit,
        0,
        data.room_id,
        data.month,
        data.generation,
        data.term
      );
    }
  }, [isCreateScoreSuccess, createScoreError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateScoreSuccess) {
      const data = getValues();
      handleCloseUpdateScoreDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນຄະແນນສອບເສັງຂອງນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      updateScoreReset();
      getScores(
        defaultLimit,
        0,
        data.room_id,
        data.month,
        data.generation,
        data.term
      );
    } else if (updateScoreError.isError) {
      const data = getValues();
      handleCloseUpdateScoreDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateScoreError.message,
        icon: "error",
      });
      updateScoreReset();
      getScores(
        defaultLimit,
        0,
        data.room_id,
        data.month,
        data.generation,
        data.term
      );
    }
  }, [isUpdateScoreSuccess, updateScoreError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteScoreSuccess) {
      const data = getValues();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງຄະແນນສອບເສັງຂອງນັກຮຽນຜູ້ນີ້ສຳເລັດ!",
        icon: "success",
      });
      deleteScoreReset();
      getScores(
        defaultLimit,
        0,
        data.room_id,
        data.month,
        data.generation,
        data.term
      );
    } else if (deleteScoreError.isError) {
      const data = getValues();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteScoreReset();
      getScores(
        defaultLimit,
        0,
        data.room_id,
        data.month,
        data.generation,
        data.term
      );
    }
  }, [isDeleteScoreSuccess, deleteScoreError.isError, defaultLimit]);

  return {
    control,
    handleSubmit,
    scoreHold,
    isOpenCreateScoreDrawer,
    isOpenUpdateScoreDrawer,
    isOpenScoreDetailsDialog,
    page,
    rowPerPage,
    setPage,
    setRowPerPage,
    openItemMenu,
    scoreState,
    studentState,
    classRoomState,
    classLevelState,
    subjectV2State,
    teacherState,
    createScore,
    updateScore,
    handleChangeClassRoom,
    handleChangeMonth,
    handleChangeGeneration,
    handleChangeTerm,
    handleSearch,
    handleOpenItemMenu,
    handleCloseItemMenu,
    handleOpenCreateScoreDrawer,
    handleCloseCreateScoreDrawer,
    handleOpenUpdateScoreDrawer,
    handleCloseUpdateScoreDrawer,
    handleOpenScoreDetailsDialog,
    handleCloseScoreDetailsDialog,
    handleOpenDeleteScoreAlertSweet,
    handleChangeTablePage,
    handleChangeRowsPerTablePage,
  };
};

export default UseHookScorePage;

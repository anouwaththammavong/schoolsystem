import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./UseHookScorePage";
import { ScoreState } from "../../../../domain/Scores";

type Props = {
  scoreState: ScoreState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    score_id: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateScoreDrawer: () => void;
  handleOpenDeleteScoreAlertSweet: () => void;
  handleOpenScoreDetailsDialog: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const ScoreTable = ({
  scoreState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateScoreDrawer,
  handleOpenDeleteScoreAlertSweet,
  handleOpenScoreDetailsDialog,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [scoreTable, setScoreTable] = React.useState<Array<any>>([]);

  const { scores, isGetScoresLoading, isGetScoresSuccess, getScoresError } =
    scoreState;

  React.useEffect(() => {
    setScoreTable(
      scores.scores.scores
        .map((score, index) => {
          return {
            ລະຫັດນັກຮຽນ: <Typography>{score.student.student_id}</Typography>,
            ຊື່ແລະນາມສະກຸນ: (
              <Typography>
                {score.student.name} {score.student.last_name}
              </Typography>
            ),
            ຄະແນນເດືອນ: (
              <Typography>{moment(score.month).format("MM")}</Typography>
            ),
            ພາກຮຽນ: <Typography>{score.term}</Typography>,
            ຄະແນນລວມທັງໝົດ: <Typography>{score.total_point}</Typography>,
            ອັນດັບທີ່: <Typography>{score.rank}</Typography>,
            "": (
              <>
                <IconButton
                  onClick={(e) =>
                    handleOpenItemMenu(e, score._id, score.student.name)
                  }
                >
                  <MoreVert />
                </IconButton>
                <Menu
                  keepMounted
                  open={
                    openItemMenu.isOpen && openItemMenu.score_id === score._id
                  }
                  anchorEl={openItemMenu.anchorEl}
                  onClose={handleCloseItemMenu}
                  PaperProps={{
                    style: {
                      width: "20ch",
                    },
                  }}
                >
                  <MenuItem onClick={() => handleOpenUpdateScoreDrawer()}>
                    ແກ້ໄຂຂໍ້ມູນ
                  </MenuItem>
                  <MenuItem onClick={() => handleOpenDeleteScoreAlertSweet()}>
                    ລົບຂໍ້ມູນ
                  </MenuItem>
                  <MenuItem onClick={() => handleOpenScoreDetailsDialog()}>
                    ລາຍລະອຽດຄະແນນເສັງ
                  </MenuItem>
                </Menu>
              </>
            ),
          };
        })
        .flat()
    );
  }, [scores, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetScoresLoading}
          isLoadSuccess={isGetScoresSuccess}
          isLoadError={getScoresError.isError}
          data={scoreTable}
          all={scores.scores.totalScore}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default ScoreTable;

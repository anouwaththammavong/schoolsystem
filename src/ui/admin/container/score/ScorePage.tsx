import * as React from "react";
import { Box, Stack } from "@mui/material";
import UseHookScorePage from "./UseHookScorePage";
import ScorePageHeaderText from "./ScorePageHeaderText";
import ScoreFilterForm from "./ScoreFilterForm";
import ScoreTable from "./ScoreTable";
import CreateScoreForm from "./CreateScoreForm";
import UpdateScoreForm from "./UpdateScoreForm";
import ScoreDetailsDialog from "./ScoreDetailsDialog";

const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

const ScorePage = (props: Props) => {
  const {
    control,
    handleSubmit,
    scoreHold,
    isOpenCreateScoreDrawer,
    isOpenUpdateScoreDrawer,
    isOpenScoreDetailsDialog,
    page,
    rowPerPage,
    setPage,
    setRowPerPage,
    openItemMenu,
    scoreState,
    studentState,
    classRoomState,
    classLevelState,
    subjectV2State,
    teacherState,
    createScore,
    updateScore,
    handleChangeClassRoom,
    handleChangeMonth,
    handleChangeGeneration,
    handleChangeTerm,
    handleSearch,
    handleOpenItemMenu,
    handleCloseItemMenu,
    handleOpenCreateScoreDrawer,
    handleCloseCreateScoreDrawer,
    handleOpenUpdateScoreDrawer,
    handleCloseUpdateScoreDrawer,
    handleOpenScoreDetailsDialog,
    handleCloseScoreDetailsDialog,
    handleOpenDeleteScoreAlertSweet,
    handleChangeTablePage,
    handleChangeRowsPerTablePage,
  } = UseHookScorePage({ defaultLimit });

  return (
    <Box>
      <Stack spacing={3}>
        <ScorePageHeaderText
          handleOpenCreateScoreDrawer={handleOpenCreateScoreDrawer}
        />

        <ScoreFilterForm
          control={control}
          handleChangeClassRoom={handleChangeClassRoom}
          handleChangeMonth={handleChangeMonth}
          handleChangeGeneration={handleChangeGeneration}
          handleChangeTerm={handleChangeTerm}
          handleSearch={handleSearch}
          handleSubmit={handleSubmit}
          scoreState={scoreState}
          studentState={studentState}
          classRoomState={classRoomState}
        />

        <CreateScoreForm
          teacherState={teacherState}
          studentState={studentState}
          classRoomState={classRoomState}
          classLevelState={classLevelState}
          subjectV2State={subjectV2State}
          scoreState={scoreState}
          handleCloseCreateScoreDrawer={handleCloseCreateScoreDrawer}
          isOpenCreateScoreDrawer={isOpenCreateScoreDrawer}
          createScore={createScore}
        />

        {scoreHold && (
          <UpdateScoreForm
            scoreHold={scoreHold}
            handleCloseUpdateScoreDrawer={handleCloseUpdateScoreDrawer}
            isOpenUpdateScoreDrawer={isOpenUpdateScoreDrawer}
            teacherState={teacherState}
            studentState={studentState}
            classRoomState={classRoomState}
            classLevelState={classLevelState}
            subjectV2State={subjectV2State}
            scoreState={scoreState}
            updateScore={updateScore}
          />
        )}

        {scoreHold && (
          <ScoreDetailsDialog
            scoreHold={scoreHold}
            isOpenScoreDetailsDialog={isOpenScoreDetailsDialog}
            handleCloseScoreDetailsDialog={handleCloseScoreDetailsDialog}
          />
        )}

        <ScoreTable
          scoreState={scoreState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateScoreDrawer={handleOpenUpdateScoreDrawer}
          handleOpenScoreDetailsDialog={handleOpenScoreDetailsDialog}
          handleOpenDeleteScoreAlertSweet={handleOpenDeleteScoreAlertSweet}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default ScorePage;

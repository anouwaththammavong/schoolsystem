import * as React from "react";
import { Score } from "../../../../domain/Scores";
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
} from "@mui/material";
import moment from "moment";

type Props = {
  scoreHold: Score | undefined;
  isOpenScoreDetailsDialog: boolean;
  handleCloseScoreDetailsDialog: () => void;
};

const ScoreDetailsDialog = ({
  scoreHold,
  isOpenScoreDetailsDialog,
  handleCloseScoreDetailsDialog,
}: Props) => {
  return (
    <Dialog
      open={isOpenScoreDetailsDialog}
      onClose={handleCloseScoreDetailsDialog}
      scroll={"paper"}
      aria-labelledby="scroll-dialog-title"
      aria-describedby="scroll-dialog-description"
    >
      <DialogTitle id="scroll-dialog-title" textAlign={"center"}>
        ລາຍລະອຽດຄະແນນເສັງ
      </DialogTitle>
      <DialogContent dividers>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ລະຫັດນັກຮຽນ:
            </DialogContentText>
          </Grid>
          <Grid item xs={6} textAlign={"end"}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {scoreHold?.student.student_id}
            </DialogContentText>
          </Grid>
          <Grid item xs={6}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ຊື່ ແລະ ນາມສະກຸນ:
            </DialogContentText>
          </Grid>
          <Grid item xs={6} textAlign={"end"}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {scoreHold?.student.name} {scoreHold?.student.last_name}
            </DialogContentText>
          </Grid>

          <Grid item xs={6}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ຫ້ອງຮຽນ:
            </DialogContentText>
          </Grid>
          <Grid item xs={6} textAlign={"end"}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {scoreHold?.room_id.room_name}
            </DialogContentText>
          </Grid>

          <Grid item xs={6}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ຊັ້ນຮຽນ:
            </DialogContentText>
          </Grid>
          <Grid item xs={6} textAlign={"end"}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {scoreHold?.level_id.level_name}
            </DialogContentText>
          </Grid>

          <Grid item xs={6}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ເດືອນ:
            </DialogContentText>
          </Grid>
          <Grid item xs={6} textAlign={"end"}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {moment(scoreHold?.month).format("MM")}
            </DialogContentText>
          </Grid>

          {scoreHold?.term !== "ບໍ່ແມ່ນ" && (
            <Grid item xs={12}>
              <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
                {scoreHold?.term}
              </DialogContentText>
            </Grid>
          )}
        </Grid>
      </DialogContent>
      <DialogContent dividers>
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ວິຊາ
            </DialogContentText>
          </Grid>
          <Grid item xs={4}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ຄະແນນ
            </DialogContentText>
          </Grid>
          <Grid item xs={4}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ອາຈານ
            </DialogContentText>
          </Grid>
          {scoreHold?.subjects.map((score) => (
            <>
              <Grid item xs={4}>
                <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
                  {score.title.subject_name}
                </DialogContentText>
              </Grid>
              <Grid item xs={4}>
                <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
                  {score.point}
                </DialogContentText>
              </Grid>
              <Grid item xs={4}>
                <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
                  {score.teacher.name} {score.teacher.last_name}
                </DialogContentText>
              </Grid>
            </>
          ))}
        </Grid>
      </DialogContent>
      <DialogContent dividers>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ຄະແນນລວມທັງໝົດ:
            </DialogContentText>
          </Grid>
          <Grid item xs={6} textAlign={"end"}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {scoreHold?.total_point}
            </DialogContentText>
          </Grid>
          <Grid item xs={6}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              ອັນດັບທີ່:
            </DialogContentText>
          </Grid>
          <Grid item xs={6} textAlign={"end"}>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {scoreHold?.rank}
            </DialogContentText>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
};

export default ScoreDetailsDialog;

import * as React from "react";
import { Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";
import { ScoreState } from "../../../../../domain/Scores";

type Props = {
  scoreState: ScoreState;
};

const ScoreReport = React.forwardRef<HTMLDivElement, Props>(
  ({ scoreState }, ref) => {
    const [scoreTable, setScoreTable] = React.useState<Array<any>>([]);

    const { scores, isGetScoresLoading, isGetScoresSuccess, getScoresError } =
      scoreState;

    React.useEffect(() => {
      setScoreTable(
        scores.scores.scores
          .map((score, index) => {
            return {
              ລະຫັດນັກຮຽນ: <Typography>{score.student.student_id}</Typography>,
              ຊື່ແລະນາມສະກຸນ: (
                <Typography>
                  {score.student.name} {score.student.last_name}
                </Typography>
              ),
              ຄະແນນເດືອນ: (
                <Typography>{moment(score.month).format("MM")}</Typography>
              ),
              ພາກຮຽນ: <Typography>{score.term}</Typography>,
              ຄະແນນລວມທັງໝົດ: <Typography>{score.total_point}</Typography>,
              ອັນດັບທີ່: <Typography>{score.rank}</Typography>,
            };
          })
          .flat()
      );
    }, [scores]);
    return (
      <div ref={ref} style={{ marginTop: "20px" }}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"}>
          ລາຍງານຂໍ້ມູນຄະແນນເສັງ {scores.scores.scores[0]?.room_id.room_name}{" "}
          ຊັ້ນຮຽນທີ່ {scores.scores.scores[0]?.level_id.level_name}{" "}
          {scores.scores.scores[0]?.term === "ບໍ່ແມ່ນ"
            ? `ປະຈຳເດືອນ${moment(scores.scores.scores[0]?.month).format("MM")}`
            : scores.scores.scores[0]?.term}{" "}
        </Typography>

        <TableComponentForRePort
          isLoading={isGetScoresLoading}
          isLoadSuccess={isGetScoresSuccess}
          isLoadError={getScoresError.isError}
          data={scoreTable}
          all={scores.scores.totalScore}
        />
      </div>
    );
  }
);

export default ScoreReport;

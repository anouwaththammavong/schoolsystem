import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Paper,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Add, Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import moment from "moment";
import { ClassRoom, ClassRoomState } from "../../../../domain/ClassRoom";
import { ClassLevel, ClassLevelState } from "../../../../domain/ClassLevel";
import { StudentState, UniqueGeneration } from "../../../../domain/Student";
import { SubjectV2, SubjectV2State } from "../../../../domain/SubjectV2";
import { UpdateScore, ScoreState, Score } from "../../../../domain/Scores";
import EnhancedTableHead, {
  HeadCell,
} from "../../components/EnhancedTableHead";
import { TeacherElement, TeacherState } from "../../../../domain/Teacher";

type Props = {
  teacherState: TeacherState;
  studentState: StudentState;
  classRoomState: ClassRoomState;
  classLevelState: ClassLevelState;
  subjectV2State: SubjectV2State;
  scoreState: ScoreState;
  isOpenUpdateScoreDrawer: boolean;
  handleCloseUpdateScoreDrawer: () => void;
  updateScore: (_id: string, score: UpdateScore) => void;
  scoreHold: Score | undefined;
};

const headCells: HeadCell[] = [
  { id: "title", label: "ວິຊາ" },
  { id: "point", label: "ຄະແນນ" },
  { id: "teacher", label: "ອາຈານ" },
];

const UpdateScoreForm = ({
  teacherState,
  studentState,
  classRoomState,
  classLevelState,
  subjectV2State,
  scoreState,
  isOpenUpdateScoreDrawer,
  handleCloseUpdateScoreDrawer,
  updateScore,
  scoreHold,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const [isEmptyItems, setIsEmptyItems] = React.useState(false);

  const term = [
    {
      title: "ພາກຮຽນ1",
    },
    {
      title: "ພາກຮຽນ2",
    },
    {
      title: "ບໍ່ແມ່ນ",
    },
  ];

  const { subjectV2s, isGetSubjectV2sLoading, isGetSubjectV2sSuccess } =
    subjectV2State;

  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;

  const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
    classLevelState;

  const { students, isGetStudentsLoading, isGetStudentsSuccess } = studentState;

  const { teachers, isGetTeachersLoading, isGetTeachersSuccess } = teacherState;

  const { isUpdateScoreLoading } = scoreState;

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm<UpdateScore>();

  const { fields, append, remove } = useFieldArray({
    control,
    name: "subjects",
  });

  const handleChangeMonth = (month: any) => {
    if (month) {
      setValue("month", month);
    }
  };

  const handleUpdateScore = (data: UpdateScore) => {
    if (data.subjects.length >= 1 && scoreHold) {
      updateScore(scoreHold._id, {
        student: data.student,
        room_id: data.room_id,
        level_id: data.level_id,
        generation: data.generation,
        month: data.month,
        term: data.term,
        subjects: data.subjects,
        rank: data.rank,
      });
    } else {
      setIsEmptyItems(true);
    }
  };

  React.useEffect(() => {
    const subjects =
      scoreHold?.subjects?.map((item) => ({
        title: item.title._id,
        point: item.point,
        teacher: item.teacher._id,
      })) ?? [];
    if (scoreHold) {
      setValue("student", scoreHold.student.student_id);
      setValue("room_id", scoreHold.room_id._id);
      setValue("level_id", scoreHold.level_id._id);
      setValue("generation", scoreHold.generation);
      setValue("month", scoreHold.month);
      setValue("term", scoreHold.term);
      setValue("subjects", subjects);
      setValue("rank", String(scoreHold.rank));
    }
  }, [scoreHold, isOpenUpdateScoreDrawer]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateScoreDrawer}
      onClose={() => handleCloseUpdateScoreDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleUpdateScore)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມຄະແນນນັກຮຽໜ
                  </Typography>
                  <IconButton onClick={() => handleCloseUpdateScoreDrawer()}>
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລະຫັດນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateScoreLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student &&
                  errors.student.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລະຫັດນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                {isGetClassRoomsLoading && <Skeleton />}
                {isGetClassRoomsSuccess &&
                  classRooms.classrooms.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`room_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          disabled={isUpdateScoreLoading}
                          id="combo-box-demo"
                          options={classRooms.classrooms.classRooms}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) =>
                            `${option.room_name} ຊັ້ນຮຽນ ${option.level_id.level_name}`
                          }
                          onChange={(
                            event: any,
                            newValue: ClassRoom | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {option.room_name} ຊັ້ນຮຽນ
                              {option.level_id.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຫ້ອງຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                          defaultValue={classRooms.classrooms.classRooms.find(
                            (o) => o._id === scoreHold?.room_id._id
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.room_id &&
                  errors.room_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຂອງຫ້ອງຮຽນ"}
                    </Typography>
                  )}
                {isGetClassRoomsSuccess &&
                  classRooms.classrooms.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຫ້ອງຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                {isGetClassLevelsLoading && <Skeleton />}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`level_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          disabled={isUpdateScoreLoading}
                          options={classLevels.classLevels.classLevels}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => option.level_name}
                          onChange={(
                            event: any,
                            newValue: ClassLevel | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {option.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຊັ້ນຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                          defaultValue={classLevels.classLevels.classLevels.find(
                            (o) => o._id === scoreHold?.level_id._id
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.level_id &&
                  errors.level_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຂອງຊັ້ນຣຽນ"}
                    </Typography>
                  )}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຊັ້ນຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                {isGetStudentsLoading && <Skeleton />}
                {isGetStudentsSuccess && students.students.totalCount > 0 && (
                  <Controller
                    control={control}
                    name={`generation`}
                    rules={{
                      required: true,
                    }}
                    render={({ field }) => (
                      <Autocomplete
                        id="combo-box-demo"
                        disabled={isUpdateScoreLoading}
                        options={students.students.uniqueGenerations}
                        autoComplete
                        fullWidth
                        getOptionLabel={(option) => option.generation}
                        onChange={(
                          event: any,
                          newValue: UniqueGeneration | null
                        ) => {
                          field.onChange(newValue?.generation);
                        }}
                        renderOption={(props, option) => (
                          <Box
                            component="li"
                            sx={{
                              "& > img": { mr: 2, flexShrink: 0 },
                            }}
                            {...props}
                          >
                            {option.generation}
                          </Box>
                        )}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label="ລຸ້ນທີ່"
                            variant="filled"
                            inputProps={{
                              ...params.inputProps,
                              autoComplete: "disabled",
                            }}
                            fullWidth
                          />
                        )}
                        defaultValue={students.students.uniqueGenerations.find(
                          (o) => o.generation === scoreHold?.generation
                        )}
                      />
                    )}
                  />
                )}
                {errors &&
                  errors.generation &&
                  errors.generation.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລຸ້ນທີ່"}
                    </Typography>
                  )}
                {isGetStudentsSuccess &&
                  students.students.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຂໍ້ມູນນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              {/* date of birth */}
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="month"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DesktopDatePicker
                        {...field}
                        label="ເດືອນ"
                        inputFormat="DD/MM/YYYY"
                        onChange={(e) => handleChangeMonth(e)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="filled"
                            size="small"
                            fullWidth
                          />
                        )}
                      />
                    </LocalizationProvider>
                  )}
                />

                {errors && errors.month && errors.month.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                  </Typography>
                )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="term"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isUpdateScoreLoading}
                      options={term}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ພາກຮຽນທີ່"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                      defaultValue={term.find(
                        (o) => o.title === scoreHold?.term
                      )}
                    />
                  )}
                />
                {errors && errors.term && errors.term.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາບອກວ່າແມ່ນເສັງພາກຮຽນບໍ"}
                  </Typography>
                )}
              </Grid>

              <Grid item xs={12}>
                <Button
                  variant="contained"
                  sx={{ paddingInline: 3 }}
                  onClick={() => {
                    setIsEmptyItems(false);
                    append({ title: "", point: 0, teacher: "" });
                  }}
                  disabled={isUpdateScoreLoading}
                >
                  <Add /> ເພີ່ມຄະແນນເສັງ
                </Button>
              </Grid>

              <Grid item xs={12}>
                <TableContainer component={Paper}>
                  <Table aria-label="simple table">
                    <EnhancedTableHead headCells={headCells} />
                    <TableBody>
                      {fields.map(({ id }, index) => {
                        return (
                          <TableRow key={id}>
                            <TableCell width={"33%"}>
                              {isGetSubjectV2sLoading && <Skeleton />}
                              {isGetSubjectV2sSuccess &&
                              subjectV2s.subjectV2s.subjectV2s.length > 0 ? (
                                <Controller
                                  control={control}
                                  name={`subjects.${index}.title`}
                                  rules={{
                                    required: true,
                                  }}
                                  render={({ field }) => (
                                    <Autocomplete
                                      id="combo-box-demo"
                                      options={subjectV2s.subjectV2s.subjectV2s}
                                      autoComplete
                                      fullWidth
                                      getOptionLabel={(option) =>
                                        option.subject_name
                                      }
                                      onChange={(
                                        event: any,
                                        newValue: SubjectV2 | null
                                      ) => {
                                        field.onChange(newValue?._id);
                                      }}
                                      renderOption={(props, option) => (
                                        <Box
                                          component="li"
                                          sx={{
                                            "& > img": { mr: 2, flexShrink: 0 },
                                          }}
                                          {...props}
                                        >
                                          {option.subject_name}
                                        </Box>
                                      )}
                                      renderInput={(params) => (
                                        <TextField
                                          {...params}
                                          label="ວິຊາ"
                                          variant="filled"
                                          inputProps={{
                                            ...params.inputProps,
                                            autoComplete: "disabled",
                                          }}
                                          fullWidth
                                        />
                                      )}
                                      defaultValue={subjectV2s.subjectV2s.subjectV2s.find(
                                        (o) => o._id === field.value
                                      )}
                                    />
                                  )}
                                />
                              ) : (
                                <Typography
                                  variant="subtitle2"
                                  color="error"
                                  marginTop={1}
                                >
                                  {"ກະລຸນາໄປເພີ່ມວິຊາທີ່ຮຽນກ່ອນ"}
                                </Typography>
                              )}
                              {errors &&
                                errors.subjects &&
                                errors.subjects?.[index] &&
                                errors.subjects?.[index]?.title?.type ===
                                  "required" && (
                                  <Typography
                                    variant="subtitle2"
                                    color="error"
                                    marginTop={1}
                                  >
                                    {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                                  </Typography>
                                )}
                            </TableCell>

                            <TableCell>
                              <Controller
                                control={control}
                                name={`subjects.${index}.point`}
                                defaultValue={1}
                                rules={{
                                  required: true,
                                  min: 0,
                                  max: 10,
                                }}
                                render={({ field: { onChange, value } }) => (
                                  <TextField
                                    size="small"
                                    type="number"
                                    label={`ຄະແນນ`}
                                    fullWidth
                                    variant="filled"
                                    value={value}
                                    onChange={({ target: { value } }) => {
                                      onChange(Number(value));
                                    }}
                                  />
                                )}
                              />

                              {errors &&
                                errors.subjects &&
                                errors.subjects?.[index] &&
                                errors.subjects?.[index]?.point?.type ===
                                  "required" && (
                                  <Typography
                                    variant="subtitle2"
                                    color="error"
                                    marginTop={1}
                                  >
                                    {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                                  </Typography>
                                )}

                              {errors &&
                                errors.subjects &&
                                errors.subjects?.[index] &&
                                errors.subjects?.[index]?.point?.type ===
                                  "min" && (
                                  <Typography
                                    variant="subtitle2"
                                    color="error"
                                    marginTop={1}
                                  >
                                    {"ຈຳນວນບໍ່ສາມາດຕ່ຳກວ່າ 0"}
                                  </Typography>
                                )}

                              {errors &&
                                errors.subjects &&
                                errors.subjects?.[index] &&
                                errors.subjects?.[index]?.point?.type ===
                                  "max" && (
                                  <Typography
                                    variant="subtitle2"
                                    color="error"
                                    marginTop={1}
                                  >
                                    {"ຈຳນວນບໍ່ສາມາດສູງກວ່າ 10"}
                                  </Typography>
                                )}
                            </TableCell>

                            <TableCell width={"30%"}>
                              {isGetTeachersLoading && <Skeleton />}
                              {isGetTeachersSuccess &&
                              teachers.teachers.totalTeachers > 0 ? (
                                <Controller
                                  control={control}
                                  name={`subjects.${index}.teacher`}
                                  rules={{
                                    required: true,
                                  }}
                                  render={({ field }) => (
                                    <Autocomplete
                                      id="combo-box-demo"
                                      options={teachers.teachers.teachers}
                                      autoComplete
                                      fullWidth
                                      getOptionLabel={(option) => option.name}
                                      onChange={(
                                        event: any,
                                        newValue: TeacherElement | null
                                      ) => {
                                        field.onChange(newValue?._id);
                                      }}
                                      renderOption={(props, option) => (
                                        <Box
                                          component="li"
                                          sx={{
                                            "& > img": { mr: 2, flexShrink: 0 },
                                          }}
                                          {...props}
                                        >
                                          {option.name}
                                        </Box>
                                      )}
                                      renderInput={(params) => (
                                        <TextField
                                          {...params}
                                          label="ອາຈານ"
                                          variant="filled"
                                          inputProps={{
                                            ...params.inputProps,
                                            autoComplete: "disabled",
                                          }}
                                          fullWidth
                                        />
                                      )}
                                      defaultValue={teachers.teachers.teachers.find(
                                        (o) => o._id === field.value
                                      )}
                                    />
                                  )}
                                />
                              ) : (
                                <Typography
                                  variant="subtitle2"
                                  color="error"
                                  marginTop={1}
                                >
                                  {"ກະລຸນາໄປເພີ່ມຂໍ້ມູນອາຈານກ່ອນ"}
                                </Typography>
                              )}
                              {errors &&
                                errors.subjects &&
                                errors.subjects?.[index] &&
                                errors.subjects?.[index]?.teacher?.type ===
                                  "required" && (
                                  <Typography
                                    variant="subtitle2"
                                    color="error"
                                    marginTop={1}
                                  >
                                    {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                                  </Typography>
                                )}
                            </TableCell>

                            <TableCell>
                              <IconButton
                                onClick={() => {
                                  remove(index);
                                }}
                              >
                                <Close />
                              </IconButton>
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </TableContainer>
                {isEmptyItems && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາເພີ່ມວິຊາທີ່ອາຈານສອນ"}
                  </Typography>
                )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="rank"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ອັນດັບທີ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateScoreLoading}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateScoreLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateScoreForm;

import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
  handleOpenCreateScoreDrawer: () => void;
}

const ScorePageHeaderText = ({handleOpenCreateScoreDrawer}: Props) => {
    return (
        <Box>
          <Stack
            direction={'row'}
            spacing={2}
          >
            <Typography
              variant='h5'
            >
              ຂໍ້ມູນຄະແນນເສັງແລະອັນດັບທີ່
            </Typography>
            <Button
          variant='contained'
          onClick={() => handleOpenCreateScoreDrawer()}
        >
          <Add /> ເພີ່ມຂໍ້ມູນຄະແນນເສັງ
        </Button>
          </Stack>
        </Box>)
}

export default ScorePageHeaderText
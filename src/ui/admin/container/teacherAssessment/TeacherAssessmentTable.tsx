import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./UseHookTeacherAssessmentPage";
import { TeacherAssessmentState } from "../../../../domain/TeacherAssessments";

type Props = {
  teacherAssessmentState: TeacherAssessmentState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    assessment_id: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateTeacherAssessmentDrawer: () => void;
  handleOpenDeleteTeacherAssessmentAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const TeacherAssessmentTable = ({
  teacherAssessmentState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateTeacherAssessmentDrawer,
  handleOpenDeleteTeacherAssessmentAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [teacherAssessmentTable, setTeacherAssessmentTable] = React.useState<
    Array<any>
  >([]);

  const {
    teacherAssessments,
    isGetTeacherAssessmentsLoading,
    isGetTeacherAssessmentsSuccess,
    getTeacherAssessmentsError,
  } = teacherAssessmentState;

  React.useEffect(() => {
    setTeacherAssessmentTable(
      teacherAssessments.teacherAssessments.teacher_assessments
        .map((teacherAssessment, index) => {
          return {
            ລະຫັດອາຈານ: (
              <Typography>{teacherAssessment.teacher.teacher_id}</Typography>
            ),
            ຊື່ແລະນາມສະກຸນ: (
              <Typography>
                {teacherAssessment.teacher.name}{" "}
                {teacherAssessment.teacher.last_name}
              </Typography>
            ),
            ພຶດຕິກຳ: <Typography>{teacherAssessment.behavior}</Typography>,
            ການມາສອນ: (
              <Typography>{teacherAssessment.attendance_class}</Typography>
            ),
            ການສອນ: (
              <Typography>{teacherAssessment.assess_teaching}</Typography>
            ),
            ໝາຍເຫດ: <Typography>{teacherAssessment.note}</Typography>,
            ປະເມີນວັນທີ: (
              <Typography>
                {moment(teacherAssessment.assessment_date).format("DD/MM/YYYY")}
              </Typography>
            ),
            "": (
              <>
                <IconButton
                  onClick={(e) =>
                    handleOpenItemMenu(
                      e,
                      teacherAssessment._id,
                      teacherAssessment.teacher.name
                    )
                  }
                >
                  <MoreVert />
                </IconButton>
                <Menu
                  keepMounted
                  open={
                    openItemMenu.isOpen &&
                    openItemMenu.assessment_id === teacherAssessment._id
                  }
                  anchorEl={openItemMenu.anchorEl}
                  onClose={handleCloseItemMenu}
                  PaperProps={{
                    style: {
                      width: "20ch",
                    },
                  }}
                >
                  <MenuItem
                    onClick={() => handleOpenUpdateTeacherAssessmentDrawer()}
                  >
                    ແກ້ໄຂຂໍ້ມູນ
                  </MenuItem>
                  <MenuItem
                    onClick={() =>
                      handleOpenDeleteTeacherAssessmentAlertSweet()
                    }
                  >
                    ລົບຂໍ້ມູນ
                  </MenuItem>
                </Menu>
              </>
            ),
          };
        })
        .flat()
    );
  }, [teacherAssessments, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetTeacherAssessmentsLoading}
          isLoadSuccess={isGetTeacherAssessmentsSuccess}
          isLoadError={getTeacherAssessmentsError.isError}
          data={teacherAssessmentTable}
          all={teacherAssessments.teacherAssessments.totalAssessment}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default TeacherAssessmentTable;

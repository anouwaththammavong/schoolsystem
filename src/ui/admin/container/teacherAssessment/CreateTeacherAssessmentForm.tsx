import * as React from "react";

import { useTheme } from "@mui/system";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Paper,
  Skeleton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import { Add, Close, Save } from "@mui/icons-material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import {
  CreateTeacherAssessment,
  TeacherAssessmentState,
} from "../../../../domain/TeacherAssessments";
import moment from "moment";

type Props = {
  teacherAssessmentState: TeacherAssessmentState;
  isOpenCreateTeacherAssessmentDrawer: boolean;
  handleCloseCreateTeacherAssessmentDrawer: () => void;
  createTeacherAssessment: (teacherAssessment: CreateTeacherAssessment) => void;
};

const CreateTeacherAssessmentForm = ({
  teacherAssessmentState,
  isOpenCreateTeacherAssessmentDrawer,
  handleCloseCreateTeacherAssessmentDrawer,
  createTeacherAssessment,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isCreateTeacherAssessmentLoading, isCreateTeacherAssessmentSuccess } =
    teacherAssessmentState;

  const {
    handleSubmit,
    setValue,
    control,
    getValues,
    reset,
    formState: { errors },
  } = useForm<CreateTeacherAssessment>();

  const handleChangeAssessmentDate = (assessment_date: any) => {
    if (assessment_date) {
      setValue("assessment_date", assessment_date);
    }
  };

  const handleCreateTeacherAssessment = (data: CreateTeacherAssessment) => {
    if (data) {
      createTeacherAssessment({
        teacher: data.teacher,
        assess_teaching: data.assess_teaching,
        attendance_class: data.attendance_class,
        behavior: data.behavior,
        assessment_date: data.assessment_date,
        note: data.note,
      });
    }
  };

  React.useEffect(() => {
    if (isCreateTeacherAssessmentSuccess) {
      reset({
        teacher: "",
        assess_teaching: "",
        attendance_class: "",
        behavior: "",
        assessment_date: moment().toDate(),
        note: "",
      });
    }
  }, [isCreateTeacherAssessmentSuccess]);
  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateTeacherAssessmentDrawer}
      onClose={() => handleCloseCreateTeacherAssessmentDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleCreateTeacherAssessment)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ປະເມິນອາຈານ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseCreateTeacherAssessmentDrawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="teacher"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລະຫັດອາຈານ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherAssessmentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.teacher &&
                  errors.teacher.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລະຫັດອາຈານ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="behavior"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isCreateTeacherAssessmentLoading}
                      options={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ພຶດຕິກຳຂອງອາຈານ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.behavior &&
                  errors.behavior.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປະເມີນພຶດຕິກຳຂອງອາຈານ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="attendance_class"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isCreateTeacherAssessmentLoading}
                      options={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ການມາສອນຂອງອາຈານ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.attendance_class &&
                  errors.attendance_class.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປະເມີນການມາສອນຂອງອາຈານ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="assess_teaching"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isCreateTeacherAssessmentLoading}
                      options={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ການສອນຂອງອາຈານ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.assess_teaching &&
                  errors.assess_teaching.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປະເມີນການສອນຂອງອາຈານ"}
                    </Typography>
                  )}
              </Grid>

              {/* date of birth */}
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="assessment_date"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DesktopDatePicker
                        {...field}
                        label="ວັນທີປະເມີນ"
                        inputFormat="DD/MM/YYYY"
                        onChange={(e) => handleChangeAssessmentDate(e)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="filled"
                            size="small"
                            fullWidth
                          />
                        )}
                      />
                    </LocalizationProvider>
                  )}
                />

                {errors &&
                  errors.assessment_date &&
                  errors.assessment_date.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="note"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໝາຍເຫດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherAssessmentLoading}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateTeacherAssessmentLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateTeacherAssessmentForm;

import * as React from "react";
import { StudentState } from "../../../../../domain/Student";
import { Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";
import { TeacherAssessmentState } from "../../../../../domain/TeacherAssessments";

type Props = {
  teacherAssessmentState: TeacherAssessmentState;
};

const TeacherAssessmentAssessmentReport = React.forwardRef<HTMLDivElement, Props>(
  ({ teacherAssessmentState }, ref) => {
    const [teacherAssessmentTable, setTeacherAssessmentTable] = React.useState<
    Array<any>
  >([]);

  const {
    teacherAssessments,
    isGetTeacherAssessmentsLoading,
    isGetTeacherAssessmentsSuccess,
    getTeacherAssessmentsError,
  } = teacherAssessmentState;

  React.useEffect(() => {
    setTeacherAssessmentTable(
      teacherAssessments.teacherAssessments.teacher_assessments
        .map((teacherAssessment, index) => {
          return {
            ລະຫັດອາຈານ: (
              <Typography>{teacherAssessment.teacher.teacher_id}</Typography>
            ),
            ຊື່ແລະນາມສະກຸນ: (
              <Typography>
                {teacherAssessment.teacher.name}{" "}
                {teacherAssessment.teacher.last_name}
              </Typography>
            ),
            ພຶດຕິກຳ: <Typography>{teacherAssessment.behavior}</Typography>,
            ການມາສອນ: (
              <Typography>{teacherAssessment.attendance_class}</Typography>
            ),
            ການສອນ: (
              <Typography>{teacherAssessment.assess_teaching}</Typography>
            ),
            ໝາຍເຫດ: <Typography>{teacherAssessment.note}</Typography>,
            ປະເມີນວັນທີ: (
              <Typography>
                {moment(teacherAssessment.assessment_date).format("DD/MM/YYYY")}
              </Typography>
            )
          };
        })
        .flat()
    );
  }, [teacherAssessments]);
    return (
        <div ref={ref} style={{marginTop:"20px"}}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"} >ລາຍງານຂໍ້ມູນການປະເມີນອາຈານ</Typography>
        <TableComponentForRePort
          isLoading={isGetTeacherAssessmentsLoading}
          isLoadSuccess={isGetTeacherAssessmentsSuccess}
          isLoadError={getTeacherAssessmentsError.isError}
          data={teacherAssessmentTable}
          all={teacherAssessments.teacherAssessments.totalAssessment}
        />
      </div>
    );
  }
);

export default TeacherAssessmentAssessmentReport;

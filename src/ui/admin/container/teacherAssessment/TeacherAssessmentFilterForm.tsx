import * as React from "react";
import { Print, Search } from "@mui/icons-material";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
  useForm,
} from "react-hook-form";
import { TeacherAssessmentState } from "../../../../domain/TeacherAssessments";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import ReactToPrint from "react-to-print";
import TeacherAssessmentAssessmentReport from "./report/TeacherAssessmentReport";

type Props = {
  control: Control<
    {
      teacher_id: string;
      assessment_date: Date;
    },
    any
  >;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleBlurSearch: (no: string) => void;
  handleChangeAssessmentDate: (assessment_date: Date) => void;
  teacherAssessmentState: TeacherAssessmentState;
};

const TeacherAssessmentFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleBlurSearch,
  handleChangeAssessmentDate,
  teacherAssessmentState,
}: Props) => {
    const componentRef = React.useRef(null);
  const { isGetTeacherAssessmentsLoading, isGetTeacherAssessmentsSuccess } =
    teacherAssessmentState;
  return (
    <Box>
      {isGetTeacherAssessmentsLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
        </Paper>
      )}
      {isGetTeacherAssessmentsSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
            <Grid item sm={12} xs={12} md={4} lg={4} xl={4}>
              <Controller
                control={control}
                name="teacher_id"
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="ຄົ້ນຫາ"
                    size="small"
                    fullWidth
                    onBlur={(e) => handleBlurSearch(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <Search />
                        </InputAdornment>
                      ),
                    }}
                    variant="filled"
                  />
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="assessment_date"
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DesktopDatePicker
                      {...field}
                      label="ວັນທີ"
                      inputFormat="DD/MM/YYYY"
                      value={field.value ? dayjs(field.value) : null}
                      onChange={(date) => {
                        if (date) {
                          const formattedDate = date.toDate(); // Convert dayjs object to native Date object
                          field.onChange(formattedDate); // Update the form state with the native Date object
                          handleChangeAssessmentDate(formattedDate); // Call your custom handler with the Date object
                        }
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="filled"
                          size="small"
                          fullWidth
                        />
                      )}
                    />
                  </LocalizationProvider>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetTeacherAssessmentsLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetTeacherAssessmentsLoading && (
                        <CircularProgress
                          color="inherit"
                          size={"2rem"}
                          sx={{ marginLeft: 1 }}
                        />
                      )}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນອາຈານ"
                pageStyle="print"
              />
              <Box display={"none"}>
              <TeacherAssessmentAssessmentReport teacherAssessmentState={teacherAssessmentState}  ref={componentRef} />
              </Box>
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default TeacherAssessmentFilterForm;

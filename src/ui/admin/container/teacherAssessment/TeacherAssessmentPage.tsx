import * as React from "react";
import { Box, Stack } from "@mui/material";
import UseHookTeacherAssessmentPage from "./UseHookTeacherAssessmentPage";
import TeacherAssessmentPageHeaderText from "./TeacherAssessmentPageHeaderTex";
import TeacherAssessmentFilterForm from "./TeacherAssessmentFilterForm";
import TeacherAssessmentTable from "./TeacherAssessmentTable";
import CreateTeacherAssessmentForm from "./CreateTeacherAssessmentForm";
import UpdateTeacherAssessmentForm from "./UpdateTeacherAssessmentForm";

const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

const TeacherAssessmentPage = (props: Props) => {
  const {
    control,
    handleSubmit,
    teacherAssessmentHold,
    isOpenCreateTeacherAssessmentDrawer,
    isOpenUpdateTeacherAssessmentDrawer,
    page,
    rowPerPage,
    setPage,
    setRowPerPage,
    openItemMenu,
    teacherAssessmentState,
    createTeacherAssessment,
    updateTeacherAssessment,
    handleBlurSearch,
    handleChangeAssessmentDate,
    handleSearch,
    handleOpenItemMenu,
    handleCloseItemMenu,
    handleOpenCreateTeacherAssessmentDrawer,
    handleCloseCreateTeacherAssessmentDrawer,
    handleOpenUpdateTeacherAssessmentDrawer,
    handleCloseUpdateTeacherAssessmentDrawer,
    handleOpenDeleteTeacherAssessmentAlertSweet,
    handleChangeTablePage,
    handleChangeRowsPerTablePage,
  } = UseHookTeacherAssessmentPage({ defaultLimit });

  return (
    <Box>
      <Stack spacing={3}>
        <TeacherAssessmentPageHeaderText
          handleOpenCreateTeacherAssessmentDrawer={
            handleOpenCreateTeacherAssessmentDrawer
          }
        />
        <TeacherAssessmentFilterForm
          control={control}
          handleBlurSearch={handleBlurSearch}
          handleChangeAssessmentDate={handleChangeAssessmentDate}
          handleSearch={handleSearch}
          handleSubmit={handleSubmit}
          teacherAssessmentState={teacherAssessmentState}
        />
        <CreateTeacherAssessmentForm
          teacherAssessmentState={teacherAssessmentState}
          handleCloseCreateTeacherAssessmentDrawer={
            handleCloseCreateTeacherAssessmentDrawer
          }
          isOpenCreateTeacherAssessmentDrawer={
            isOpenCreateTeacherAssessmentDrawer
          }
          createTeacherAssessment={createTeacherAssessment}
        />
        
      {teacherAssessmentHold && (
        <UpdateTeacherAssessmentForm
          teacherAssessmentHold={teacherAssessmentHold}
          handleCloseUpdateTeacherAssessmentDrawer={
            handleCloseUpdateTeacherAssessmentDrawer
          }
          isOpenUpdateTeacherAssessmentDrawer={
            isOpenUpdateTeacherAssessmentDrawer
          }
          teacherAssessmentState={teacherAssessmentState}
          updateTeacherAssessment={updateTeacherAssessment}
        />
      )}
       
        <TeacherAssessmentTable
          teacherAssessmentState={teacherAssessmentState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateTeacherAssessmentDrawer={
            handleOpenUpdateTeacherAssessmentDrawer
          }
          handleOpenDeleteTeacherAssessmentAlertSweet={handleOpenDeleteTeacherAssessmentAlertSweet}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default TeacherAssessmentPage;

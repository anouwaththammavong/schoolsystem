import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system'
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateTeacherAssessmentDrawer: () => void;
}

const TeacherAssessmentPageHeaderTex = ({handleOpenCreateTeacherAssessmentDrawer}: Props) => {
    return (
        <Box>
          <Stack
            direction={'row'}
            spacing={2}
          >
            <Typography
              variant='h5'
            >
              ຂໍ້ມູນການປະເມີນອາຈານ
            </Typography>
            <Button
          variant='contained'
          onClick={() => handleOpenCreateTeacherAssessmentDrawer()}
        >
          <Add /> ເພີ່ມຂໍ້ມູນການປະເມີນອາຈານ
        </Button>
          </Stack>
        </Box>
      )
}

export default TeacherAssessmentPageHeaderTex
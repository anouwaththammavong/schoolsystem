import * as React from "react";
import { useTeacherAssessment } from "../../../hooks/useTeacherAssessment";
import Swal from "sweetalert2";
import { TeacherAssessment } from "../../../../domain/TeacherAssessments";
import { useForm } from "react-hook-form";

export interface OpenItemMenu {
  isOpen: boolean;
  assessment_id: string;
  anchorEl: null | HTMLElement;
}

type Props = {
  defaultLimit: number;
};

const UseHookTeacherAssessmentPage = ({ defaultLimit }: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm<{
    teacher_id: string;
    assessment_date: Date;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [
    isOpenCreateTeacherAssessmentDrawer,
    setIsOpenCreateTeacherAssessmentDrawer,
  ] = React.useState(false);
  const [
    isOpenUpdateTeacherAssessmentDrawer,
    setIsOpenUpdateTeacherAssessmentDrawer,
  ] = React.useState(false);
  const [teacherAssessmentHold, setTeacherAssessmentHold] = React.useState<
    TeacherAssessment | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    assessment_id: "",
    anchorEl: null,
  });

  const {
    teacherAssessmentState,
    getTeacherAssessment,
    createTeacherAssessment,
    createTeacherAssessmentReset,
    updateTeacherAssessment,
    updateTeacherAssessmentReset,
    deleteTeacherAssessment,
    deleteTeacherAssessmentReset
  } = useTeacherAssessment();

  const {
    teacherAssessments,
    isCreateTeacherAssessmentSuccess,
    createTeacherAssessmentError,
    isUpdateTeacherAssessmentSuccess,
    updateTeacherAssessmentError,
    isDeleteTeacherAssessmentSuccess,
    deleteTeacherAssessmentError
  } = teacherAssessmentState;

  const handleBlurSearch = (teacher_id: string) => {
    setValue("teacher_id", teacher_id);
    handleSearch();
  };

  const handleChangeAssessmentDate = (assessment_date: Date) => {
    setValue("assessment_date", assessment_date);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getTeacherAssessment(rowPerPage, 0, data.teacher_id, data.assessment_date);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    assessment_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      assessment_id: assessment_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      assessment_id: "",
      anchorEl: null,
    });
  };

  const handleOpenCreateTeacherAssessmentDrawer = () => {
    setIsOpenCreateTeacherAssessmentDrawer(true);
  };

  const handleCloseCreateTeacherAssessmentDrawer = () => {
    setIsOpenCreateTeacherAssessmentDrawer(false);
  };

  const handleOpenUpdateTeacherAssessmentDrawer = () => {
    const assessment =
      teacherAssessments.teacherAssessments.teacher_assessments.filter(
        (assessment) => assessment._id === openItemMenu.assessment_id
      );
    if (assessment) {
      console.log(assessment);
      setTeacherAssessmentHold(assessment[0]);
      setIsOpenUpdateTeacherAssessmentDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        assessment_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateTeacherAssessmentDrawer = () => {
    setIsOpenUpdateTeacherAssessmentDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getTeacherAssessment(
      rowPerPage,
      newPage * rowPerPage,
      data.teacher_id,
      data.assessment_date
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getTeacherAssessment(
      parseInt(event.target.value, 10),
      0,
      data.teacher_id,
      data.assessment_date
    );
  };

  const handleOpenAlertSweetWarningIfThisTeacherHasNotBeenAssessmentYet =
    () => {
      Swal.fire({
        title: "ຂໍ້ມູນອາຈານຜູ້ນີ້ຍັງບໍ່ທັນໄດ້ມີການປະເມີນເທື່ອ",
        text: "ທ່ານຕ້ອງປະເມີນອາຈານຜູ້ນີ້ກ່ອນຈຶ່ງຈະສາມາດແກ້ໄຂຂໍ້ມູນໄດ້",
        icon: "warning",
      });
    };

    const handleOpenDeleteTeacherAssessmentAlertSweet = () => {
      const absence = openItemMenu.assessment_id;
      if (absence) {
        setOpenItemMenu({
          isOpen: false,
          assessment_id: "",
          anchorEl: null,
        });
      }
      Swal.fire({
        title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງການປະເມີນອາຈານຜູ້ນີ້ບໍ?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "ຕ້ອງການ",
        cancelButtonText: `ບໍ່ຕ້ອງການ`,
        confirmButtonColor: "#22bb33",
        cancelButtonColor: "#d33",
      }).then((result) => {
        if (result.isConfirmed && absence) {
          deleteTeacherAssessment(absence);
        }
      });
    };

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getTeacherAssessment(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  React.useEffect(() => {
    if (isCreateTeacherAssessmentSuccess) {
      const data = getValues();
      handleCloseCreateTeacherAssessmentDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ປະເມີນອາຈານສຳເລັດ!",
        icon: "success",
      });
      createTeacherAssessmentReset();
      getTeacherAssessment(
        defaultLimit,
        0,
        data.teacher_id,
        data.assessment_date
      );
    } else if (createTeacherAssessmentError.isError) {
      const data = getValues();
      handleCloseCreateTeacherAssessmentDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createTeacherAssessmentError.message,
        icon: "error",
      });
      createTeacherAssessmentReset();
      getTeacherAssessment(
        defaultLimit,
        0,
        data.teacher_id,
        data.assessment_date
      );
    }
  }, [
    isCreateTeacherAssessmentSuccess,
    createTeacherAssessmentError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isUpdateTeacherAssessmentSuccess) {
      const data = getValues();
      handleCloseUpdateTeacherAssessmentDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນການປະເມີນອາຈານສຳເລັດ!",
        icon: "success",
      });
      updateTeacherAssessmentReset();
      getTeacherAssessment(
        defaultLimit,
        0,
        data.teacher_id,
        data.assessment_date
      );
    } else if (updateTeacherAssessmentError.isError) {
      const data = getValues();
      handleCloseUpdateTeacherAssessmentDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateTeacherAssessmentError.message,
        icon: "error",
      });
      updateTeacherAssessmentReset();
      getTeacherAssessment(
        defaultLimit,
        0,
        data.teacher_id,
        data.assessment_date
      );
    }
  }, [
    isUpdateTeacherAssessmentSuccess,
    updateTeacherAssessmentError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isDeleteTeacherAssessmentSuccess) {
      const data = getValues();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງການປະເມີນອາຈານສຳເລັດ!",
        icon: "success",
      });
      deleteTeacherAssessmentReset();
      getTeacherAssessment(
        defaultLimit,
        0,
        data.teacher_id,
        data.assessment_date
      );
    } else if (deleteTeacherAssessmentError.isError) {
      const data = getValues();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteTeacherAssessmentReset();
      getTeacherAssessment(
        defaultLimit,
        0,
        data.teacher_id,
        data.assessment_date
      );
    }
  }, [
    isDeleteTeacherAssessmentSuccess,
    deleteTeacherAssessmentError.isError,
    defaultLimit,
  ]);

  return {
    control,
    handleSubmit,
    teacherAssessmentHold,
    isOpenCreateTeacherAssessmentDrawer,
    isOpenUpdateTeacherAssessmentDrawer,
    page,
    rowPerPage,
    setPage,
    setRowPerPage,
    openItemMenu,
    teacherAssessmentState,
    createTeacherAssessment,
    updateTeacherAssessment,
    handleBlurSearch,
    handleChangeAssessmentDate,
    handleSearch,
    handleOpenItemMenu,
    handleCloseItemMenu,
    handleOpenCreateTeacherAssessmentDrawer,
    handleCloseCreateTeacherAssessmentDrawer,
    handleOpenUpdateTeacherAssessmentDrawer,
    handleCloseUpdateTeacherAssessmentDrawer,
    handleOpenDeleteTeacherAssessmentAlertSweet,
    handleChangeTablePage,
    handleChangeRowsPerTablePage,
  };
};

export default UseHookTeacherAssessmentPage;

import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import {
  ClassLevel,
  ClassLevelState,
  UpdateClassLevelForm as UpdateClassLevelFormData,
} from "../../../../domain/ClassLevel";

type Props = {
  classLevelState: ClassLevelState;
  isOpenUpdateClassLevelDrawer: boolean;
  handleCloseUpdateClassLevelDrawer: () => void;
  updateClassLevel: (_id: string, classLevel: UpdateClassLevelFormData) => void;
  classLevelHold: ClassLevel | undefined;
};

const UpdateClassLevelForm = ({
  classLevelState,
  isOpenUpdateClassLevelDrawer,
  handleCloseUpdateClassLevelDrawer,
  updateClassLevel,
  classLevelHold,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isUpdateClassLevelLoading } = classLevelState;

  const {
    handleSubmit,
    control,
    setValue,
    trigger,
    getValues,
    formState: { errors },
  } = useForm<UpdateClassLevelFormData>();

  const handleUpdateClassLevel = (data: UpdateClassLevelFormData) => {
    if (classLevelHold) {
      updateClassLevel(classLevelHold?._id, {
        level_id: data.level_id,
        level_name: data.level_name,
      });
    }
  };

  React.useEffect(() => {
    if (classLevelHold) {
      setValue("level_id", classLevelHold.level_id);
      setValue("level_name", classLevelHold.level_name);
    }
  }, [classLevelHold]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateClassLevelDrawer}
      onClose={() => handleCloseUpdateClassLevelDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleUpdateClassLevel)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ແກ້ໄຂຂໍ້ມູນຂອງຊັ້ນຮຽນ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseUpdateClassLevelDrawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="level_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໄອດີຊັ້ນຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateClassLevelLoading}
                    />
                  )}
                />
                {errors &&
                  errors.level_id &&
                  errors.level_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນໄອດີຂອງຊັ້ນຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="level_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ຂອງຊັ້ນຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateClassLevelLoading}
                    />
                  )}
                />
                {errors &&
                  errors.level_name &&
                  errors.level_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຊື່ຂອງຊັ້ນຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateClassLevelLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateClassLevelForm;

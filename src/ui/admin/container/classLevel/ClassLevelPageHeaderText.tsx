import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateClassLevelDrawer: () => void;
}

const ClassLevelPageHeaderText = ({handleOpenCreateClassLevelDrawer}: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນຊັ້ນຮຽນ
        </Typography>
        <Button
          variant='contained'
          onClick={() => handleOpenCreateClassLevelDrawer()}
        >
          <Add /> ເພີ່ມຊັ້ນຮຽນ
        </Button>
      </Stack>
    </Box>
  )
}

export default ClassLevelPageHeaderText
import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./ClassLevelPage";
import { ClassLevelState } from "../../../../domain/ClassLevel";

type Props = {
  classLevelState: ClassLevelState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateClassLevelDrawer: () => void;
  handleOpenDeleteClassLevelAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const ClassLevelTable = ({
  classLevelState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateClassLevelDrawer,
  handleOpenDeleteClassLevelAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [classLevelTable, setClassLevelTable] = React.useState<Array<any>>([]);

  const {
    classLevels,
    isGetClassLevelsLoading,
    isGetClassLevelsSuccess,
    getClassLevelsError,
  } = classLevelState;

  React.useEffect(() => {
    setClassLevelTable(
      classLevels.classLevels.classLevels.map((classLevel) => {
        return {
          ໄອດີຂອງຊັ້ນຮຽນ: <Typography>{classLevel.level_id}</Typography>,
          ຊື່ຂອງຊັ້ນຮຽນ: <Typography>{classLevel.level_name}</Typography>,
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(e, classLevel._id, classLevel.level_name)
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen &&
                  openItemMenu.class_level_id === classLevel._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateClassLevelDrawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem
                  onClick={() => handleOpenDeleteClassLevelAlertSweet()}
                >
                  ລົບຂໍ້ມູນ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [classLevels, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetClassLevelsLoading}
          isLoadSuccess={isGetClassLevelsSuccess}
          isLoadError={getClassLevelsError.isError}
          data={classLevelTable}
          all={classLevels.classLevels.totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default ClassLevelTable;

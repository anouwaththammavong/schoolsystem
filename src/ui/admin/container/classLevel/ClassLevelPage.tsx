import * as React from "react";
import { useForm } from "react-hook-form";
import { ClassLevel } from "../../../../domain/ClassLevel";
import { useClassLevel } from "../../../hooks/useClassLevel";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import ClassLevelPageHeaderText from "./ClassLevelPageHeaderText";
import CreateClassLevelForm from "./CreateClassLevelForm";
import ClassLevelTable from "./ClassLevelTable";
import UpdateClassLevelForm from "./UpdateClassLevelForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  class_level_id: string;
  anchorEl: null | HTMLElement;
}

const ClassLevelPage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateClassLevelDrawer, setIsOpenCreateClassLevelDrawer] =
    React.useState(false);
  const [isOpenUpdateClassLevelDrawer, setIsOpenUpdateClassLevelDrawer] =
    React.useState(false);
  const [classLevelHold, setClassLevelHold] = React.useState<
    ClassLevel | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    class_level_id: "",
    anchorEl: null,
  });

  const {
    classLevelState,
    getClassLevels,
    createClassLevel,
    createClassLevelReset,
    updateClassLevel,
    updateClassLevelReset,
    deleteClassLevel,
    deleteClassLevelReset,
  } = useClassLevel();

  const {
    classLevels,
    isCreateClassLevelSuccess,
    createClassLevelError,
    isUpdateClassLevelSuccess,
    updateClassLevelError,
    isDeleteClassLevelSuccess,
    deleteClassLevelError,
  } = classLevelState;

  const handleOpenCreateClassLevelDrawer = () => {
    setIsOpenCreateClassLevelDrawer(true);
  };

  const handleCloseCreateClassLevelDrawer = () => {
    setIsOpenCreateClassLevelDrawer(false);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    class_level_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      class_level_id: class_level_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      class_level_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateClassLevelDrawer = () => {
    const classLevel = classLevels.classLevels.classLevels.filter(
      (level) => level._id === openItemMenu.class_level_id
    )[0];
    if (classLevel) {
      setClassLevelHold(classLevel);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateClassLevelDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        class_level_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateClassLevelDrawer = () => {
    setIsOpenUpdateClassLevelDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    getClassLevels(rowPerPage, newPage * rowPerPage);
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    getClassLevels(parseInt(event.target.value, 10), 0);
  };

  const handleOpenDeleteClassLevelAlertSweet = () => {
    const class_level = openItemMenu.class_level_id;
    if (class_level) {
      setOpenItemMenu({
        isOpen: false,
        class_level_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງຊັ້ນຮຽນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && class_level) {
        deleteClassLevel(class_level);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateClassLevelSuccess) {
      handleCloseCreateClassLevelDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມຊັ້ນຮຽນສຳເລັດ!",
        icon: "success",
      });
      createClassLevelReset();
      getClassLevels(defaultLimit, 0);
    } else if (createClassLevelError.isError) {
      handleCloseCreateClassLevelDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createClassLevelError.message,
        icon: "error",
      });
      createClassLevelReset();
      getClassLevels(defaultLimit, 0);
    }
  }, [isCreateClassLevelSuccess, createClassLevelError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateClassLevelSuccess) {
      handleCloseUpdateClassLevelDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນຂອງຊັ້ນຮຽນນີ້ສຳເລັດ!",
        icon: "success",
      });
      updateClassLevelReset();
      getClassLevels(defaultLimit, 0);
    } else if (updateClassLevelError.isError) {
      handleCloseUpdateClassLevelDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateClassLevelError.message,
        icon: "error",
      });
      updateClassLevelReset();
      getClassLevels(defaultLimit, 0);
    }
  }, [isUpdateClassLevelSuccess, updateClassLevelError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteClassLevelSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງຊັ້ນຮຽນສຳເລັດ!",
        icon: "success",
      });
      deleteClassLevelReset();
      getClassLevels(defaultLimit, 0);
    } else if (deleteClassLevelError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteClassLevelReset();
      getClassLevels(defaultLimit, 0);
    }
  }, [isDeleteClassLevelSuccess, deleteClassLevelError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getClassLevels(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
        <ClassLevelPageHeaderText
          handleOpenCreateClassLevelDrawer={handleOpenCreateClassLevelDrawer}
        />
        <CreateClassLevelForm
          classLevelState={classLevelState}
          createClassLevel={createClassLevel}
          handleCloseCreateClassLevelDrawer={handleCloseCreateClassLevelDrawer}
          isOpenCreateClassLevelDrawer={isOpenCreateClassLevelDrawer}
        />
        {classLevelHold && (
          <UpdateClassLevelForm
            classLevelState={classLevelState}
            handleCloseUpdateClassLevelDrawer={
              handleCloseUpdateClassLevelDrawer
            }
            isOpenUpdateClassLevelDrawer={isOpenUpdateClassLevelDrawer}
            classLevelHold={classLevelHold}
            updateClassLevel={updateClassLevel}
          />
        )}
        <ClassLevelTable
          classLevelState={classLevelState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateClassLevelDrawer={handleOpenUpdateClassLevelDrawer}
          handleOpenDeleteClassLevelAlertSweet={
            handleOpenDeleteClassLevelAlertSweet
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default ClassLevelPage;

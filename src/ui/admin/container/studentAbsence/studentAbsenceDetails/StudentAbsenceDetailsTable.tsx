import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./StudentAbsenceDetailsPage";
import { StudentAbsenceState } from "../../../../../domain/StudentAbsence";
import moment from "moment";

type Props = {
  studentAbsenceState: StudentAbsenceState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    absence_id: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateStudentAbsenceDrawer: () => void;
  handleOpenDeleteStudentAbsenceAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const StudentAbsenceDetailsTable = ({
  studentAbsenceState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateStudentAbsenceDrawer,
  handleOpenDeleteStudentAbsenceAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [studentAbsenceDetailsTable, setStudentAbsenceDetailsTable] =
    React.useState<Array<any>>([]);

  const {
    studentIdAbsence,
    isGetStudentIdAbsencesLoading,
    isGetStudentIdAbsencesSuccess,
    getStudentIdAbsencesError,
  } = studentAbsenceState;

  React.useEffect(() => {
    setStudentAbsenceDetailsTable(
      studentIdAbsence.studentAbsence.absences.map((student_absence) => {
        return {
          ວັນທີຂາດຮຽນ: (
            <Typography>
              {moment(student_absence.absence_date).format("DD/MM/YYYY")}
            </Typography>
          ),
          ຂາດໝົດມື້ຫຼືບໍ: (
            <Typography>
              {student_absence.absence_allday}
            </Typography>
          ),
          ມີເຫດຜົນບໍ: (
            <Typography>
              {student_absence.be_reasonable}
            </Typography>
          ),
          ຂາດເທີມໃດ: <Typography>ເທີມ {student_absence.term}</Typography>,
          ເຫດຜົນຂອງການຂາດ: (
            <Typography>
              {student_absence.note ? student_absence.note : "ບໍ່ໄດ້ບອກເຫດຜົນ"}
            </Typography>
          ),
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(
                    e,
                    student_absence._id,
                    student_absence.student.name
                  )
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen &&
                  openItemMenu.absence_id === student_absence._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem
                  onClick={() => handleOpenUpdateStudentAbsenceDrawer()}
                >
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem
                  onClick={() => handleOpenDeleteStudentAbsenceAlertSweet()}
                >
                  ລົບຂໍ້ມູນ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [studentIdAbsence, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetStudentIdAbsencesLoading}
          isLoadSuccess={isGetStudentIdAbsencesSuccess}
          isLoadError={getStudentIdAbsencesError.isError}
          data={studentAbsenceDetailsTable}
          all={studentIdAbsence.studentAbsence.totalAbsences}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  )
};

export default StudentAbsenceDetailsTable;

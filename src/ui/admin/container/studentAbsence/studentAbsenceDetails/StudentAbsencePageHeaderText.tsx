import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateStudentAbsenceDrawer: () => void;
}

const StudentAbsencePageHeaderText = ({handleOpenCreateStudentAbsenceDrawer}: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ລາຍລະອຽດຂອງການຂາດຮຽນຂອງນັກຮຽນ
        </Typography>
        <Button
          variant='contained'
          onClick={() => handleOpenCreateStudentAbsenceDrawer()}
        >
          <Add /> ໝາຍຂາດ
        </Button>
      </Stack>
    </Box>
  )
}

export default StudentAbsencePageHeaderText
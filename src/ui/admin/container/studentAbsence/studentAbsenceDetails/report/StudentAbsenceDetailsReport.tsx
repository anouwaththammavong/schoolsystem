import * as React from "react";
import { Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../../components/ui-tool/TableComponentForReport";
import { StudentAbsenceState } from "../../../../../../domain/StudentAbsence";

type Props = {
  studentAbsenceState: StudentAbsenceState;
};

const StudentAbsenceDetailsReport = React.forwardRef<HTMLDivElement, Props>(
  ({ studentAbsenceState }, ref) => {
    const [studentAbsenceDetailsTable, setStudentAbsenceDetailsTable] =
      React.useState<Array<any>>([]);

    const {
      studentIdAbsence,
      isGetStudentIdAbsencesLoading,
      isGetStudentIdAbsencesSuccess,
      getStudentIdAbsencesError,
    } = studentAbsenceState;

    React.useEffect(() => {
      setStudentAbsenceDetailsTable(
        studentIdAbsence.studentAbsence.absences.map((student_absence) => {
          return {
            ວັນທີຂາດຮຽນ: (
              <Typography>
                {moment(student_absence.absence_date).format("DD/MM/YYYY")}
              </Typography>
            ),
            ຂາດໝົດມື້ຫຼືບໍ: (
              <Typography>{student_absence.absence_allday}</Typography>
            ),
            ມີເຫດຜົນບໍ: (
              <Typography>{student_absence.be_reasonable}</Typography>
            ),
            ຂາດເທີມໃດ: <Typography>ເທີມ {student_absence.term}</Typography>,
            ເຫດຜົນຂອງການຂາດ: (
              <Typography>
                {student_absence.note
                  ? student_absence.note
                  : "ບໍ່ໄດ້ບອກເຫດຜົນ"}
              </Typography>
            ),
          };
        })
      );
    }, [studentIdAbsence]);

    return (
      <div ref={ref} style={{ marginTop: "20px" }}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"}>
          ລາຍລະອຽດຂອງການຂາດຮຽນຂອງ{" "}
          {studentIdAbsence.studentAbsence.absences[0]?.student.gender === "ຊາຍ"
            ? "ທ້າວ"
            : "ນາງ"}
            {studentIdAbsence.studentAbsence.absences[0]?.student.name} {studentIdAbsence.studentAbsence.absences[0]?.student.last_name}
            {" "}ຊັ້ນຮຽນ {studentIdAbsence.studentAbsence.absences[0]?.room_id.level_id.level_name}
        </Typography>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"}>
          ລວມມື້ຂາດຮຽນທັງໝົດ{" "}
          {studentIdAbsence.studentAbsence.totalAbsences}{" "}ມື້
          {" "}ຂາດຮຽນແບບມີເຫດຜົນ
            {studentIdAbsence.studentAbsence.totalBe_reasonable}{" "}ມື້
            {" "}ຂາດຮຽນແບບບໍ່ມີເຫດຜົນ {studentIdAbsence.studentAbsence.totalNotBe_reasonable}{" "}ມື້
        </Typography>
        <TableComponentForRePort
          isLoading={isGetStudentIdAbsencesLoading}
          isLoadSuccess={isGetStudentIdAbsencesSuccess}
          isLoadError={getStudentIdAbsencesError.isError}
          data={studentAbsenceDetailsTable}
          all={studentIdAbsence.studentAbsence.totalAbsences}
        />
      </div>
    );
  }
);

export default StudentAbsenceDetailsReport;

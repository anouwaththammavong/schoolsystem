import * as React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Stack } from "@mui/material";
import { useForm } from "react-hook-form";
import { Absence } from "../../../../../domain/StudentAbsence";
import { useStudentAbsence } from "../../../../hooks/useStudentAbsence";
import Swal from "sweetalert2";
import StudentAbsencePageHeaderText from "./StudentAbsencePageHeaderText";
import StudentAbsenceDetailsFilterForm from "./StudentAbsenceDetailsFilterForm";
import StudentAbsenceDetailsTable from "./StudentAbsenceDetailsTable";
import CreateStudentAbsenceDetailsForm from "./CreateStudentAbsenceDetailsForm";
import UpdateStudentAbsenceDetailsForm from "./UpdateStudentAbsenceDetailsForm";
import { useClassRoom } from "../../../../hooks/useClassRoom";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

export interface OpenItemMenu {
  isOpen: boolean;
  absence_id: string;
  anchorEl: null | HTMLElement;
}

type Props = {};

const StudentAbsenceDetailsPage = (props: Props) => {
  const { student_id, room_id } = useParams();
  const navigate = useNavigate();
  const { handleSubmit, control, setValue, getValues } = useForm<{
    absence_date: Date;
    term: string;
    be_reasonable: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [
    isOpenCreateStudentAbsenceDrawer,
    setIsOpenCreateStudentAbsenceDrawer,
  ] = React.useState(false);
  const [
    isOpenUpdateStudentAbsenceDrawer,
    setIsOpenUpdateStudentAbsenceDrawer,
  ] = React.useState(false);
  const [studentAbsenceHold, setStudentAbsenceHold] = React.useState<
    Absence | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    absence_id: "",
    anchorEl: null,
  });

  const {
    studentAbsenceState,
    getStudentIdAbsences,
    createStudentAbsence,
    createStudentAbsenceReset,
    updateStudentAbsence,
    updateStudentAbsenceReset,
    deleteStudentAbsence,
    deleteStudentAbsenceReset,
  } = useStudentAbsence();

  const { getClassRooms, classRoomState } = useClassRoom();

  const {
    studentIdAbsence,
    isGetStudentIdAbsencesSuccess,
    isCreateStudentAbsenceSuccess,
    createStudentAbsenceError,
    isUpdateStudentAbsenceSuccess,
    updateStudentAbsenceError,
    isDeleteStudentAbsenceSuccess,
    deleteStudentAbsenceError,
  } = studentAbsenceState;

  const handleChangeClass_room = (room_id: string) => {
      const encodedClassRoom = encodeURIComponent(room_id);
      navigate(
        `/absence_student/student/${student_id}/room_id/${encodedClassRoom}/studentAbsenceDetails`
      );
  };

  const handleChangeAbsence_date = (absence_date: Date) => {
    setValue("absence_date", absence_date);
    handleSearch();
  };

  const handleChangeTerm = (term: string) => {
    setValue("term", term);
    handleSearch();
  };

  const handleChangeBe_reasonable = (be_reasonable: string) => {
    setValue("be_reasonable", be_reasonable);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    if (student_id && room_id) {
      getStudentIdAbsences(
        rowPerPage,
        0,
        student_id,
        room_id,
        data.absence_date,
        data.term,
        data.be_reasonable
      );
    }
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    absence_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      absence_id: absence_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      absence_id: "",
      anchorEl: null,
    });
  };

  const handleOpenCreateStudentAbsenceDrawer = () => {
    setIsOpenCreateStudentAbsenceDrawer(true);
  };

  const handleCloseCreateStudentAbsenceDrawer = () => {
    setIsOpenCreateStudentAbsenceDrawer(false);
  };

  const handleOpenUpdateStudentAbsenceDrawer = () => {
    const absences = studentIdAbsence.studentAbsence.absences.filter(
      (absence) => absence._id === openItemMenu.absence_id
    )[0];
    if (absences) {
      setStudentAbsenceHold(absences);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateStudentAbsenceDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        absence_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateStudentAbsenceDrawer = () => {
    setIsOpenUpdateStudentAbsenceDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    if (student_id && room_id) {
      getStudentIdAbsences(
        rowPerPage,
        newPage * rowPerPage,
        student_id,
        room_id,
        data.absence_date,
        data.term,
        data.be_reasonable
      );
    }
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    if (student_id && room_id) {
      getStudentIdAbsences(
        parseInt(event.target.value, 10),
        0,
        student_id,
        room_id,
        data.absence_date,
        data.term,
        data.be_reasonable
      );
    }
  };

  const handleOpenDeleteStudentAbsenceAlertSweet = () => {
    const absence = openItemMenu.absence_id;
    if (absence) {
      setOpenItemMenu({
        isOpen: false,
        absence_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງນັກຮຽນຜູ້ນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && absence) {
        deleteStudentAbsence(absence);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateStudentAbsenceSuccess && student_id && room_id) {
      handleCloseCreateStudentAbsenceDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ໝາຍຂາດນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      createStudentAbsenceReset();
      getStudentIdAbsences(defaultLimit, 0, student_id, room_id);
    } else if (createStudentAbsenceError.isError && student_id && room_id) {
      handleCloseCreateStudentAbsenceDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createStudentAbsenceError.message,
        icon: "error",
      });
      createStudentAbsenceReset();
      getStudentIdAbsences(defaultLimit, 0, student_id, room_id);
    }
  }, [
    isCreateStudentAbsenceSuccess,
    createStudentAbsenceError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isUpdateStudentAbsenceSuccess && student_id && room_id) {
      handleCloseUpdateStudentAbsenceDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      updateStudentAbsenceReset();
      getStudentIdAbsences(defaultLimit, 0, student_id, room_id);
    } else if (updateStudentAbsenceError.isError && student_id && room_id) {
      handleCloseUpdateStudentAbsenceDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateStudentAbsenceError.message,
        icon: "error",
      });
      updateStudentAbsenceReset();
      getStudentIdAbsences(defaultLimit, 0, student_id, room_id);
    }
  }, [
    isUpdateStudentAbsenceSuccess,
    updateStudentAbsenceError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isDeleteStudentAbsenceSuccess && student_id && room_id) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງການໝາຍຂາດນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      deleteStudentAbsenceReset();
      getStudentIdAbsences(defaultLimit, 0, student_id, room_id);
    } else if (deleteStudentAbsenceError.isError && student_id && room_id) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteStudentAbsenceReset();
      getStudentIdAbsences(defaultLimit, 0, student_id, room_id);
    }
  }, [
    isDeleteStudentAbsenceSuccess,
    deleteStudentAbsenceError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp && student_id && room_id) {
      getStudentIdAbsences(defaultLimit, 0, student_id, room_id);
      getClassRooms(0,0)
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit, student_id, room_id]);

  return (
    <Box>
      <Stack spacing={3}>
        {isGetStudentIdAbsencesSuccess && (
          <StudentAbsencePageHeaderText
            handleOpenCreateStudentAbsenceDrawer={
              handleOpenCreateStudentAbsenceDrawer
            }
          />
        )}
        <StudentAbsenceDetailsFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleSearch={handleSearch}
          handleChangeClass_room={handleChangeClass_room}
          handleChangeAbsence_date={handleChangeAbsence_date}
          handleChangeTerm={handleChangeTerm}
          handleChangeBe_reasonable={handleChangeBe_reasonable}
          studentAbsenceState={studentAbsenceState}
          classRoomState={classRoomState}
        />
        <CreateStudentAbsenceDetailsForm
          studentAbsenceState={studentAbsenceState}
          student_id={student_id}
          room_id={room_id}
          createStudentAbsence={createStudentAbsence}
          isOpenCreateStudentAbsenceDrawer={isOpenCreateStudentAbsenceDrawer}
          handleCloseCreateStudentAbsenceDrawer={
            handleCloseCreateStudentAbsenceDrawer
          }
        />
        {studentAbsenceHold && (
          <UpdateStudentAbsenceDetailsForm
            studentAbsenceState={studentAbsenceState}
            handleCloseUpdateStudentAbsenceDrawer={
              handleCloseUpdateStudentAbsenceDrawer
            }
            isOpenUpdateStudentAbsenceDrawer={isOpenUpdateStudentAbsenceDrawer}
            studentAbsenceHold={studentAbsenceHold}
            updateStudentAbsence={updateStudentAbsence}
          />
        )}
        <StudentAbsenceDetailsTable
          studentAbsenceState={studentAbsenceState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateStudentAbsenceDrawer={
            handleOpenUpdateStudentAbsenceDrawer
          }
          handleOpenDeleteStudentAbsenceAlertSweet={
            handleOpenDeleteStudentAbsenceAlertSweet
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default StudentAbsenceDetailsPage;

import * as React from "react";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { StudentAbsenceState } from "../../../../../domain/StudentAbsence";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import ReactToPrint from "react-to-print";
import { Print } from "@mui/icons-material";
import StudentAbsenceDetailsReport from "./report/StudentAbsenceDetailsReport";
import { ClassRoomState } from "../../../../../domain/ClassRoom";

type Props = {
  control: Control<{
    absence_date: Date;
    term: string;
    be_reasonable: string;
  }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleChangeClass_room: (room_id: string) => void;
  handleChangeAbsence_date: (absence_date: Date) => void;
  handleChangeTerm: (term: string) => void;
  handleChangeBe_reasonable: (be_reasonable: string) => void;
  studentAbsenceState: StudentAbsenceState;
  classRoomState: ClassRoomState;
};

const StudentAbsenceDetailsFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleChangeClass_room,
  handleChangeAbsence_date,
  handleChangeTerm,
  handleChangeBe_reasonable,
  studentAbsenceState,
  classRoomState,
}: Props) => {
  const componentRef = React.useRef(null);
  const {
    isGetStudentIdAbsencesLoading,
    isGetStudentIdAbsencesSuccess,
    studentIdAbsence,
  } = studentAbsenceState;

  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;
  return (
    <Box>
      {isGetStudentIdAbsencesLoading ||
        (isGetClassRoomsLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        ))}
      {isGetStudentIdAbsencesSuccess && isGetClassRoomsSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
            <Grid
              item
              sm={12}
              xs={12}
              md={12}
              lg={12}
              xl={12}
              direction={"row"}
            >
              <Stack direction={"row"} gap={5}>
                <Typography variant="h6" color={"#00a10d"}>
                  ຂາດຮຽນແບບມີເຫດຜົນ{" "}
                  {studentIdAbsence.studentAbsence.totalBe_reasonable} ມື້
                </Typography>
                <Typography variant="h6" color={"#d80000"}>
                  ຂາດຮຽນແບບບໍ່ມີເຫດຜົນ{" "}
                  {studentIdAbsence.studentAbsence.totalNotBe_reasonable} ມື້
                </Typography>
              </Stack>
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <FormControl fullWidth variant="filled" sx={{ minWidth: 120 }}>
                <InputLabel>ເລືອກ ຫ້ອງຮຽນທີ່</InputLabel>
                <Select
                  size="small"
                  onChange={(e: SelectChangeEvent) =>
                    handleChangeClass_room(e.target.value)
                  }
                >
                  {classRooms.classrooms.classRooms.map((val, key) => (
                    <MenuItem key={key} value={val._id}>
                      {val.room_name} {"ຊັ້ນຮຽນ"}
                      {val.level_id.level_name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="absence_date"
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DesktopDatePicker
                      {...field}
                      label="ວັນທີ"
                      inputFormat="DD/MM/YYYY"
                      value={field.value ? dayjs(field.value) : null}
                      onChange={(date) => {
                        if (date) {
                          const formattedDate = date.toDate(); // Convert dayjs object to native Date object
                          field.onChange(formattedDate); // Update the form state with the native Date object
                          handleChangeAbsence_date(formattedDate); // Call your custom handler with the Date object
                        }
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="filled"
                          size="small"
                          fullWidth
                        />
                      )}
                    />
                  </LocalizationProvider>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="term"
                // defaultValue="1"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ເທີມທີ່ຂາດຮຽນ</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeTerm(e.target.value)
                      }
                    >
                      <MenuItem value={"1"}>1</MenuItem>
                      <MenuItem value={"2"}>2</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="be_reasonable"
                // defaultValue="1"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ມີເຫດຜົນບໍໃນການຂາດຮຽນ</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeBe_reasonable(e.target.value)
                      }
                    >
                      <MenuItem value={"ມີເຫດຜົນ"}>ມີເຫດຜົນ</MenuItem>
                      <MenuItem value={"ບໍ່ມີເຫດຜົນ"}>ບໍ່ມີເຫດຜົນ</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={2} xl={3}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetStudentIdAbsencesLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetStudentIdAbsencesLoading && (
                        <CircularProgress
                          color="inherit"
                          size={"2rem"}
                          sx={{ marginLeft: 1 }}
                        />
                      )}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນນັກຮຽນ"
                pageStyle="print"
              />
              <Box display={"none"}>
                <StudentAbsenceDetailsReport
                  studentAbsenceState={studentAbsenceState}
                  ref={componentRef}
                />
              </Box>
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default StudentAbsenceDetailsFilterForm;

import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./StudentAbsencePage";
import { StudentAbsenceState } from "../../../../domain/StudentAbsence";

type Props = {
  studentAbsenceState: StudentAbsenceState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    class_level: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenCreateStudentAbsenceDrawer: () => void;
  handleOpenDetailsStudentAbsence: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const StudentAbsenceTable = ({
  studentAbsenceState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenCreateStudentAbsenceDrawer,
  handleOpenDetailsStudentAbsence,
  page,
  rowsPerPage,
  handleChangeTablePage,
  handleChangeRowsPerTablePage,
}: Props) => {
  const [studentAbsenceTable, setStudentAbsenceTable] = React.useState<
    Array<any>
  >([]);
  const {
    studentAbsence,
    isGetAllStudentAbsencesLoading,
    isGetAllStudentAbsencesSuccess,
    getAllStudentAbsencesError,
  } = studentAbsenceState;

  React.useEffect(() => {
    setStudentAbsenceTable(
      studentAbsence.studentAbsence.studentAbsence.studentAbsence
        .map((studentAbsence, index) => {
          // Check if the studentAbsence array is empty
          if (studentAbsence.studentAbsence.length === 0) {
            return {
              ລຳດັບ: <Typography>{studentAbsence.student_number}</Typography>,
              ຊື່ແລະນາມສະກຸນ: (
                <Typography>
                  {studentAbsence.name} {studentAbsence.last_name}
                </Typography>
              ),
              ລະຫັດນັກຮຽນ: <Typography>{studentAbsence.student_id}</Typography>,
              ເທີມ1: <Typography>ບໍ່ມີຂໍ້ມູນ</Typography>,
              ເທີມ2: <Typography>ບໍ່ມີຂໍ້ມູນ</Typography>,
              ລວມມື້ຂາດຮຽນທັງໝົດ: <Typography>ບໍ່ມີຂໍ້ມູນ</Typography>,
              "": (
                <>
                  <IconButton
                    onClick={(e) =>
                      handleOpenItemMenu(
                        e,
                        studentAbsence._id,
                        studentAbsence.level_id.level_name,
                        studentAbsence.name
                      )
                    }
                  >
                    <MoreVert />
                  </IconButton>
                  <Menu
                    keepMounted
                    open={
                      openItemMenu.isOpen &&
                      openItemMenu.student_id === studentAbsence._id
                    }
                    anchorEl={openItemMenu.anchorEl}
                    onClose={handleCloseItemMenu}
                    PaperProps={{
                      style: {
                        width: "20ch",
                      },
                    }}
                  >
                    <MenuItem
                      onClick={() => handleOpenCreateStudentAbsenceDrawer()}
                    >
                      ໝາຍຂາດ
                    </MenuItem>
                    <MenuItem onClick={() => handleOpenDetailsStudentAbsence()}>
                      ລາຍລະອຽດຂອງການຂາດຮຽນ
                    </MenuItem>
                  </Menu>
                </>
              ),
            };
          } else {
            // Map over the studentAbsence array if it is not empty
            return studentAbsence.studentAbsence.map((absence, innerIndex) => {
              return {
                ລຳດັບ: <Typography>{studentAbsence.student_number}</Typography>,
                ຊື່ແລະນາມສະກຸນ: (
                  <Typography>
                    {studentAbsence.name} {studentAbsence.last_name}
                  </Typography>
                ),
                ລະຫັດນັກຮຽນ: (
                  <Typography>{studentAbsence.student_id}</Typography>
                ),
                ເທີມ1: (
                  <Typography>
                    {absence.student_absence_term1
                      ? absence.student_absence_term1
                      : "ບໍ່ມີຂໍ້ມູນ"}{" "}
                    ມື້
                  </Typography>
                ),
                ເທີມ2: (
                  <Typography>
                    {absence.student_absence_term2
                      ? absence.student_absence_term2
                      : "ບໍ່ມີຂໍ້ມູນ"}{" "}
                    ມື້
                  </Typography>
                ),
                ລວມມື້ຂາດຮຽນທັງໝົດ: (
                  <Typography>
                    {absence.student_sum_all_term1_and_term2
                      ? absence.student_sum_all_term1_and_term2
                      : "ບໍ່ມີຂໍ້ມູນ"}{" "}
                    ມື້
                  </Typography>
                ),
                "": (
                  <>
                    <IconButton
                      onClick={(e) =>
                        handleOpenItemMenu(
                          e,
                          studentAbsence._id,
                          studentAbsence.level_id.level_name,
                          studentAbsence.name
                        )
                      }
                    >
                      <MoreVert />
                    </IconButton>
                    <Menu
                      keepMounted
                      open={
                        openItemMenu.isOpen &&
                        openItemMenu.student_id === studentAbsence._id
                      }
                      anchorEl={openItemMenu.anchorEl}
                      onClose={handleCloseItemMenu}
                      PaperProps={{
                        style: {
                          width: "20ch",
                        },
                      }}
                    >
                      <MenuItem
                        onClick={() => handleOpenCreateStudentAbsenceDrawer()}
                      >
                        ໝາຍຂາດ
                      </MenuItem>
                      <MenuItem
                        onClick={() => handleOpenDetailsStudentAbsence()}
                      >
                        ລາຍລະອຽດການຂາດຮຽນ
                      </MenuItem>
                    </Menu>
                  </>
                ),
              };
            });
          }
        })
        .flat()
    );
  }, [studentAbsence, openItemMenu]);
  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetAllStudentAbsencesLoading}
          isLoadSuccess={isGetAllStudentAbsencesSuccess}
          isLoadError={getAllStudentAbsencesError.isError}
          data={studentAbsenceTable}
          all={studentAbsence.studentAbsence.studentAbsence.totalStudentsAbsent}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default StudentAbsenceTable;

import * as React from "react";
import { Box, Stack } from "@mui/material";
import { useForm } from "react-hook-form";
import { TentacledStudentAbsence } from "../../../../domain/StudentAbsence";
import { useStudentAbsence } from "../../../hooks/useStudentAbsence";
import { useStudent } from "../../../hooks/useStudent";
import StudentAbsencePageHeaderText from "./StudentAbsencePageHeaderText";
import StudentAbsenceFilterForm from "./StudentAbsenceFilterForm";
import StudentAbsenceTable from "./StudentAbsenceTable";
import Swal from "sweetalert2";
import CreateStudent_AbsenceForm from "./CreateStudentAbsenceForm";
import { useNavigate } from "react-router-dom";
import { useClassRoom } from "../../../hooks/useClassRoom";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  student_id: string;
  room_id: string;
  anchorEl: null | HTMLElement;
}

const StudentAbsencePage = (props: Props) => {
  const navigate = useNavigate();
  const { handleSubmit, control, setValue, getValues } = useForm<{
    room_id: string;
    generation: string;
    student_id: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [
    isOpenCreateStudentAbsenceDrawer,
    setIsOpenCreateStudentAbsenceDrawer,
  ] = React.useState(false);
  const [studentAbsenceHold, setStudentAbsenceHold] = React.useState<
    TentacledStudentAbsence | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    student_id: "",
    room_id: "",
    anchorEl: null,
  });

  const {
    studentAbsenceState,
    getAllStudentAbsences,
    createStudentAbsence,
    createStudentAbsenceReset,
  } = useStudentAbsence();

  const { getStudents, studentState } = useStudent();

  const { getClassRooms, classRoomState } = useClassRoom();

  const {
    studentAbsence,
    isCreateStudentAbsenceSuccess,
    createStudentAbsenceError,
  } = studentAbsenceState;

  const { classRooms, isGetClassRoomsSuccess } = classRoomState;

  const handleChangeClassRoom = (room_id: string) => {
    setValue("room_id", room_id);
    handleSearch();
  };

  const handleChangeGeneration = (generation: string) => {
    setValue("generation", generation);
    handleSearch();
  };

  const handleBlurSearch = (teacher_id: string) => {
    setValue("student_id", teacher_id);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getAllStudentAbsences(
      rowPerPage,
      0,
      data.room_id,
      data.generation,
      data.student_id
    );
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    student_id: string,
    room_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      student_id: student_id,
      room_id: room_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      student_id: "",
      room_id: "",
      anchorEl: null,
    });
  };

  const handleOpenCreateStudentAbsenceDrawer = () => {
    const stud =
      studentAbsence.studentAbsence.studentAbsence.studentAbsence.filter(
        (stud) => stud._id === openItemMenu.student_id
      )[0];
    if (stud) {
      setStudentAbsenceHold(stud);
      setIsOpenCreateStudentAbsenceDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        student_id: "",
        room_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseCreateStudentAbsenceDrawer = () => {
    setIsOpenCreateStudentAbsenceDrawer(false);
  };

  const handleOpenDetailsStudentAbsence = () => {
    const data = getValues();
    const encodedStudentId = encodeURIComponent(openItemMenu.student_id);
    const encodedClassLevel = encodeURIComponent(data.room_id);
    navigate(
      `/absence_student/student/${encodedStudentId}/room_id/${encodedClassLevel}/studentAbsenceDetails`
    );
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getAllStudentAbsences(
      rowPerPage,
      newPage * rowPerPage,
      data.room_id,
      data.generation,
      data.student_id
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getAllStudentAbsences(
      parseInt(event.target.value, 10),
      0,
      data.room_id,
      data.generation,
      data.student_id
    );
  };

  React.useEffect(() => {
    if (isCreateStudentAbsenceSuccess) {
      const data = getValues();
      handleCloseCreateStudentAbsenceDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ໝາຍຂາດນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      createStudentAbsenceReset();
      getAllStudentAbsences(
        defaultLimit,
        0,
        data.room_id,
        data.generation,
        data.student_id
      );
    } else if (createStudentAbsenceError.isError) {
      const data = getValues();
      handleCloseCreateStudentAbsenceDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createStudentAbsenceError.message,
        icon: "error",
      });
      createStudentAbsenceReset();
      getAllStudentAbsences(
        defaultLimit,
        0,
        data.room_id,
        data.generation,
        data.student_id
      );
    }
  }, [
    isCreateStudentAbsenceSuccess,
    createStudentAbsenceError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      // Ensure classRooms is defined and not empty before accessing properties
      getClassRooms(0, 0);
      getStudents(0, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp]);

  React.useEffect(() => {
    if (isGetClassRoomsSuccess) {
      getAllStudentAbsences(
        defaultLimit,
        0,
        classRooms.classrooms.classRooms[0]._id,
        "1"
      );
      setValue("room_id", classRooms.classrooms.classRooms[0]._id);
    }
  }, [isGetClassRoomsSuccess]);

  return (
    <Box>
      <Stack spacing={3}>
        <StudentAbsencePageHeaderText />
        <StudentAbsenceFilterForm
          control={control}
          handleBlurSearch={handleBlurSearch}
          handleChangeClassRoom={handleChangeClassRoom}
          handleChangeGeneration={handleChangeGeneration}
          handleSearch={handleSearch}
          handleSubmit={handleSubmit}
          studentAbsenceState={studentAbsenceState}
          studentState={studentState}
          classRoomState={classRoomState}
          generation={getValues("generation")}
        />
        {studentAbsenceHold && (
          <CreateStudent_AbsenceForm
            createStudentAbsence={createStudentAbsence}
            handleCloseCreateStudentAbsenceDrawer={
              handleCloseCreateStudentAbsenceDrawer
            }
            isOpenCreateStudentAbsenceDrawer={isOpenCreateStudentAbsenceDrawer}
            studentAbsenceHold={studentAbsenceHold}
            studentAbsenceState={studentAbsenceState}
            room_id={getValues("room_id")}
          />
        )}
        <StudentAbsenceTable
          studentAbsenceState={studentAbsenceState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenCreateStudentAbsenceDrawer={
            handleOpenCreateStudentAbsenceDrawer
          }
          handleOpenDetailsStudentAbsence={handleOpenDetailsStudentAbsence}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default StudentAbsencePage;

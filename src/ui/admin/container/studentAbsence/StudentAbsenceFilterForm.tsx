import * as React from "react";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { RoomID, StudentAbsenceState } from "../../../../domain/StudentAbsence";
import { StudentState } from "../../../../domain/Student";
import { Print, Search } from "@mui/icons-material";
import ReactToPrint from "react-to-print";
import StudentAbsenceReport from "./report/StudentAbsenceReport";
import { ClassRoomState } from "../../../../domain/ClassRoom";

type Props = {
  control: Control<{
    room_id: string;
    generation: string;
    student_id: string;
  }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleBlurSearch: (no: string) => void;
  handleChangeGeneration: (generation: string) => void;
  handleChangeClassRoom: (room_id: string) => void;
  studentAbsenceState: StudentAbsenceState;
  classRoomState: ClassRoomState;
  studentState: StudentState;
  generation: string;
};

const StudentAbsenceFilterForm = ({
  control,
  handleBlurSearch,
  handleChangeClassRoom,
  handleChangeGeneration,
  handleSearch,
  handleSubmit,
  studentAbsenceState,
  studentState,
  generation,
  classRoomState,
}: Props) => {
  const componentRef = React.useRef(null);
  const { isGetAllStudentAbsencesLoading, isGetAllStudentAbsencesSuccess } =
    studentAbsenceState;

  const { students, isGetStudentsLoading, isGetStudentsSuccess } = studentState;

  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;

  return (
    <Box>
      {isGetStudentsLoading ||
        isGetClassRoomsLoading ||
        (isGetAllStudentAbsencesLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        ))}
      {isGetStudentsSuccess &&
        isGetAllStudentAbsencesSuccess &&
        isGetClassRoomsSuccess && (
          <form onSubmit={handleSubmit(handleSearch)}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12} md={4} lg={4} xl={4}>
                <Controller
                  control={control}
                  name="student_id"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      label="ຄົ້ນຫາ"
                      size="small"
                      fullWidth
                      onBlur={(e) => handleBlurSearch(e.target.value)}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="start">
                            <Search />
                          </InputAdornment>
                        ),
                      }}
                      variant="filled"
                    />
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="room_id"
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ຫ້ອງຮຽນທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeClassRoom(e.target.value)
                        }
                      >
                        {classRooms.classrooms.classRooms.map((val, key) => (
                          <MenuItem key={key} value={val._id}>
                            {val.room_name} {"ຊັ້ນຮຽນ"}
                            {val.level_id.level_name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="generation"
                  defaultValue={
                    students.students.uniqueGenerations[0].generation
                  }
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ນັກຮຽນລຸ້ນທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeGeneration(e.target.value)
                        }
                      >
                        {students.students.uniqueGenerations.map((val, key) => (
                          <MenuItem key={key} value={val.generation}>
                            {val.generation}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={2} xl={3}>
                <ReactToPrint
                  trigger={() => {
                    return (
                      <Button
                        variant="contained"
                        // onClick={() => handleGeneratePdf()}
                        disabled={
                          isGetStudentsLoading || isGetAllStudentAbsencesLoading
                        }
                        size="large"
                      >
                        <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                        {isGetStudentsLoading && (
                          <CircularProgress
                            color="inherit"
                            size={"2rem"}
                            sx={{ marginLeft: 1 }}
                          />
                        )}
                      </Button>
                    );
                  }}
                  content={() => componentRef.current}
                  documentTitle="ລາຍງານຂໍ້ມູນນັກຮຽນ"
                  pageStyle="print"
                />
                {
                  <Box display={"none"}>
                    <StudentAbsenceReport
                      studentAbsenceState={studentAbsenceState}
                      ref={componentRef}
                      generation={generation}
                    />
                  </Box>
                }
              </Grid>
            </Grid>
          </form>
        )}
    </Box>
  );
};

export default StudentAbsenceFilterForm;

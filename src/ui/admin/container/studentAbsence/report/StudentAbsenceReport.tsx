import React from "react";
import { Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";
import { StudentAbsenceState } from "../../../../../domain/StudentAbsence";

type Props = {
  studentAbsenceState: StudentAbsenceState;
  generation: string;
};

const StudentAbsenceReport = React.forwardRef<HTMLDivElement, Props>(
  ({ studentAbsenceState, generation }, ref) => {
    const [studentAbsenceTable, setStudentAbsenceTable] = React.useState<
      Array<any>
    >([]);
    const {
      studentAbsence,
      isGetAllStudentAbsencesLoading,
      isGetAllStudentAbsencesSuccess,
      getAllStudentAbsencesError,
    } = studentAbsenceState;

    React.useEffect(() => {
      setStudentAbsenceTable(
        studentAbsence.studentAbsence.studentAbsence.studentAbsence
          .map((studentAbsence, index) => {
            // Check if the studentAbsence array is empty
            if (studentAbsence.studentAbsence.length === 0) {
              return {
                ລຳດັບ: <Typography>{studentAbsence.student_number}</Typography>,
                ຊື່ແລະນາມສະກຸນ: (
                  <Typography>
                    {studentAbsence.name} {studentAbsence.last_name}
                  </Typography>
                ),
                ລະຫັດນັກຮຽນ: (
                  <Typography>{studentAbsence.student_id}</Typography>
                ),
                ເທີມ1: <Typography>ບໍ່ມີຂໍ້ມູນ</Typography>,
                ເທີມ2: <Typography>ບໍ່ມີຂໍ້ມູນ</Typography>,
                ລວມມື້ຂາດຮຽນທັງໝົດ: (
                  <Typography textAlign={"center"}>ບໍ່ມີຂໍ້ມູນ</Typography>
                ),
              };
            } else {
              // Map over the studentAbsence array if it is not empty
              return studentAbsence.studentAbsence.map(
                (absence, innerIndex) => {
                  return {
                    ລຳດັບ: (
                      <Typography>{studentAbsence.student_number}</Typography>
                    ),
                    ຊື່ແລະນາມສະກຸນ: (
                      <Typography>
                        {studentAbsence.name} {studentAbsence.last_name}
                      </Typography>
                    ),
                    ລະຫັດນັກຮຽນ: (
                      <Typography>{studentAbsence.student_id}</Typography>
                    ),
                    ເທີມ1: (
                      <Typography>
                        {absence.student_absence_term1
                          ? absence.student_absence_term1
                          : "ບໍ່ມີຂໍ້ມູນ"}{" "}
                        ມື້
                      </Typography>
                    ),
                    ເທີມ2: (
                      <Typography>
                        {absence.student_absence_term2
                          ? absence.student_absence_term2
                          : "ບໍ່ມີຂໍ້ມູນ"}{" "}
                        ມື້
                      </Typography>
                    ),
                    ລວມມື້ຂາດຮຽນທັງໝົດ: (
                      <Typography textAlign={"center"}>
                        {absence.student_sum_all_term1_and_term2
                          ? absence.student_sum_all_term1_and_term2
                          : "ບໍ່ມີຂໍ້ມູນ"}{" "}
                        ມື້
                      </Typography>
                    ),
                  };
                }
              );
            }
          })
          .flat()
      );
    }, [studentAbsence]);
    return (
      studentAbsence.studentAbsence.studentAbsence.studentAbsence[0]?.room_id
        .room_name && (
        <div ref={ref} style={{ marginTop: "20px" }}>
          <Typography textAlign={"center"} variant="h5" marginBottom={"20px"}>
            ລາຍງານຂໍ້ມູນການຂາດຮຽນຂອງນັກຮຽນ{" "}
            {
              studentAbsence.studentAbsence.studentAbsence.studentAbsence[0]
                ?.room_id.room_name
            }{" "}
            ຊັ້ນຮຽນທີ່{" "}
            {
              studentAbsence.studentAbsence.studentAbsence.studentAbsence[0]
                ?.level_id.level_name
            }{" "}
            ລຸ້ນທີ່{" "}
            {
              studentAbsence.studentAbsence.studentAbsence.studentAbsence[0]
                ?.generation
            }
          </Typography>
          <TableComponentForRePort
            isLoading={isGetAllStudentAbsencesLoading}
            isLoadSuccess={isGetAllStudentAbsencesSuccess}
            isLoadError={getAllStudentAbsencesError.isError}
            data={studentAbsenceTable}
            all={
              studentAbsence.studentAbsence.studentAbsence.totalStudentsAbsent
            }
          />
        </div>
      )
    );
  }
);

export default StudentAbsenceReport;

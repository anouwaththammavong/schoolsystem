import * as React from "react";
import { useTheme } from "@mui/system";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Paper,
  Skeleton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import { Add, Close, Save } from "@mui/icons-material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import {
  CreateStudentAbsenceForm,
  StudentAbsenceState,
  TentacledStudentAbsence,
} from "../../../../domain/StudentAbsence";

type Props = {
  studentAbsenceState: StudentAbsenceState;
  isOpenCreateStudentAbsenceDrawer: boolean;
  handleCloseCreateStudentAbsenceDrawer: () => void;
  createStudentAbsence: (studentAbsence: CreateStudentAbsenceForm) => void;
  studentAbsenceHold: TentacledStudentAbsence | undefined;
  room_id: string;
};

const CreateStudent_AbsenceForm = ({
  studentAbsenceState,
  isOpenCreateStudentAbsenceDrawer,
  handleCloseCreateStudentAbsenceDrawer,
  createStudentAbsence,
  studentAbsenceHold,
  room_id
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isCreateStudentAbsenceLoading } = studentAbsenceState;

  const {
    handleSubmit,
    setValue,
    control,
    getValues,
    formState: { errors },
  } = useForm<CreateStudentAbsenceForm>();

  const handleChangeAbsence_date = (absence_date: any) => {
    if (absence_date) {
      setValue("absence_date", absence_date);
    }
  };

  const handleUpdateSubject = (data: CreateStudentAbsenceForm) => {
    if (data && studentAbsenceHold) {
      createStudentAbsence({
        student: studentAbsenceHold._id,
        room_id: room_id,
        absence_allday: data.absence_allday,
        absence_date: dayjs(data.absence_date, "DD/MM/YYYY").format("YYYY-MM-DD"),
        be_reasonable: data.be_reasonable,
        term: data.term,
        note: data.note,
      });
    }
  };

  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateStudentAbsenceDrawer}
      onClose={() => handleCloseCreateStudentAbsenceDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleUpdateSubject)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ໝາຍຂາດ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseCreateStudentAbsenceDrawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="be_reasonable"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      disabled={isCreateStudentAbsenceLoading}
                      id="combo-box-demo"
                      options={[
                        { title: "ມີເຫດຜົນ" },
                        { title: "ບໍ່ມີເຫດຜົນ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ມີເຫດຜົນບໍ?"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.be_reasonable &&
                  errors.be_reasonable.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນວ່າມີເຫດຜົນ ຫຼື ບໍ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="absence_allday"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isCreateStudentAbsenceLoading}
                      options={[
                        { title: "ຂາດໝົດມື້" },
                        { title: "ຂາດບາງຊົ່ວໂມງ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ຂາດໝົດມື້ບໍ?"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.absence_allday &&
                  errors.absence_allday.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນວ່ານັກຮຽນໄດ້ຂາດໝົດມື້ ຫຼື ບໍ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="absence_date"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DesktopDatePicker
                        {...field}
                        label="ວັນທີ"
                        inputFormat="DD/MM/YYYY"
                        disabled={isCreateStudentAbsenceLoading}
                        onChange={(e) => handleChangeAbsence_date(e)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="filled"
                            size="small"
                            fullWidth
                          />
                        )}
                      />
                    </LocalizationProvider>
                  )}
                />

                {errors &&
                  errors.absence_date &&
                  errors.absence_date.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນວັນທີຂາດຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="term"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      disabled={isCreateStudentAbsenceLoading}
                      id="combo-box-demo"
                      options={[
                        { term: "1", title: "ເທີມ1" },
                        { term: "2", title: "ເທີມ2" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: { term: string; title: string } | null
                      ) => {
                        field.onChange(newValue?.term);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ເທີມ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors && errors.term && errors.term.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຂໍ້ມູນເທີມທີ່ຂາດຮຽນ"}
                  </Typography>
                )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="note"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໝາຍເຫດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateStudentAbsenceLoading}
                    />
                  )}
                />
              </Grid>

              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateStudentAbsenceLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateStudent_AbsenceForm;

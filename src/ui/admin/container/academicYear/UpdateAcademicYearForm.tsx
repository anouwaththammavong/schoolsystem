import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import {
    AcademicYearState,
    AcademicYear,
    UpdateAcademicYear,
  } from "../../../../domain/AcademicYears";


type Props = {
    academicYearState: AcademicYearState;
  isOpenUpdateAcademicYearDrawer: boolean;
  handleCloseUpdateAcademicYearDrawer: () => void;
  updateAcademicYear: (_id: string, academicYear: UpdateAcademicYear) => void;
  academicYearHold: AcademicYear | undefined;
}

const UpdateAcademicYearForm = ({academicYearState,
    isOpenUpdateAcademicYearDrawer,
    handleCloseUpdateAcademicYearDrawer,
    updateAcademicYear,
    academicYearHold,}: Props) => {
        const theme = useTheme();
        const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
        const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
        const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

        const { isUpdateAcademicYearLoading } = academicYearState

        const {
            handleSubmit,
            control,
            setValue,
            trigger,
            getValues,
            formState: { errors },
          } = useForm<UpdateAcademicYear>();
        
          const handleUpdateAcademicYear = (data: UpdateAcademicYear) => {
            if (academicYearHold) {
              updateAcademicYear(academicYearHold?._id, {
                academic_year: data.academic_year,
                academic_year_no: data.academic_year_no,
              });
            }
          };
        
          React.useEffect(() => {
            if (academicYearHold) {
              setValue("academic_year", academicYearHold.academic_year);
              setValue("academic_year_no", academicYearHold.academic_year_no);
            }
          }, [academicYearHold]);
          return (
            <Drawer
              anchor={"right"}
              open={isOpenUpdateAcademicYearDrawer}
              onClose={() => handleCloseUpdateAcademicYearDrawer()}
            >
              <form
                style={{ width: "100%" }}
                onSubmit={handleSubmit(handleUpdateAcademicYear)}
              >
                <Box
                  width={
                    breakpointSm
                      ? "100%"
                      : breakpointMd
                      ? 600
                      : breakpointLg
                      ? 900
                      : 1200
                  }
                >
                  <Stack spacing={3} padding={3}>
                    <Grid
                      container
                      spacing={3}
                      alignItems="center"
                      justifyContent="center"
                    >
                      <Grid item xs={12}>
                        <Stack direction={"row"} justifyContent="space-between">
                          <Typography variant={"h5"} component="div">
                            ເພີ່ມສົກຮຽນ
                          </Typography>
                          <IconButton
                            onClick={() => handleCloseUpdateAcademicYearDrawer()}
                          >
                            <Close />
                          </IconButton>
                        </Stack>
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          control={control}
                          name="academic_year"
                          rules={{
                            required: true,
                          }}
                          render={({ field }) => (
                            <TextField
                              {...field}
                              size="small"
                              type="text"
                              label={"ປີການສຶກສາ"}
                              fullWidth
                              variant="filled"
                              multiline
                              disabled={isUpdateAcademicYearLoading}
                            />
                          )}
                        />
                        {errors &&
                          errors.academic_year &&
                          errors.academic_year.type === "required" && (
                            <Typography variant="subtitle2" color="error" marginTop={1}>
                              {"ກະລຸນາປ້ອນປີການສຶກສາ"}
                            </Typography>
                          )}
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          control={control}
                          name="academic_year_no"
                          rules={{
                            required: true,
                          }}
                          render={({ field }) => (
                            <TextField
                              {...field}
                              size="small"
                              type="text"
                              label={"ສົກຮຽນທີ່"}
                              fullWidth
                              variant="filled"
                              multiline
                              disabled={isUpdateAcademicYearLoading}
                            />
                          )}
                        />
                        {errors &&
                          errors.academic_year_no &&
                          errors.academic_year_no.type === "required" && (
                            <Typography variant="subtitle2" color="error" marginTop={1}>
                              {"ກະລຸນາປ້ອນສົກຮຽນທີ່"}
                            </Typography>
                          )}
                      </Grid>
                      <Grid item xs={12}>
                        <Divider />
                      </Grid>
                      <Grid item xs={12}>
                        <Button
                          type="submit"
                          variant="contained"
                          sx={{ paddingInline: 5 }}
                          disabled={isUpdateAcademicYearLoading}
                        >
                          <Save /> ບັນທຶກ
                        </Button>
                      </Grid>
                    </Grid>
                  </Stack>
                </Box>
              </form>
            </Drawer>
          );
}

export default UpdateAcademicYearForm
import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import {
  AcademicYearState,
  CreateAcademicYear,
} from "../../../../domain/AcademicYears";

type Props = {
  academicYearState: AcademicYearState;
  isOpenCreateAcademicYearDrawer: boolean;
  handleCloseCreateAcademicYearDrawer: () => void;
  createAcademicYear: (academicYear: CreateAcademicYear) => void;
};

const CreateAcademicYearForm = ({
  academicYearState,
  isOpenCreateAcademicYearDrawer,
  handleCloseCreateAcademicYearDrawer,
  createAcademicYear,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isCreateAcademicYearLoading, isCreateAcademicYearSuccess } =
    academicYearState;

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm<CreateAcademicYear>();

  const handleCreateAcademicYear = (data: CreateAcademicYear) => {
    createAcademicYear({
      academic_year: data.academic_year,
      academic_year_no: data.academic_year_no,
    });
  };

  React.useEffect(() => {
    if (isCreateAcademicYearSuccess) {
      reset({
        academic_year: "",
        academic_year_no: "",
      });
    }
  }, [isCreateAcademicYearSuccess]);
  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateAcademicYearDrawer}
      onClose={() => handleCloseCreateAcademicYearDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleCreateAcademicYear)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມສົກຮຽນ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseCreateAcademicYearDrawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="academic_year"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ປີການສຶກສາ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateAcademicYearLoading}
                    />
                  )}
                />
                {errors &&
                  errors.academic_year &&
                  errors.academic_year.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນປີການສຶກສາ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="academic_year_no"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ສົກຮຽນທີ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateAcademicYearLoading}
                    />
                  )}
                />
                {errors &&
                  errors.academic_year_no &&
                  errors.academic_year_no.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນສົກຮຽນທີ່"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateAcademicYearLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateAcademicYearForm;

import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateAcademicYearDrawer: () => void;
}

const AcademicYearPageHeaderText = ({handleOpenCreateAcademicYearDrawer}: Props) => {
    return (
        <Box>
          <Stack
            direction={'row'}
            spacing={2}
          >
            <Typography
              variant='h5'
            >
              ຂໍ້ມູນສົກຮຽນ
            </Typography>
            <Button
              variant='contained'
              onClick={() => handleOpenCreateAcademicYearDrawer()}
            >
              <Add /> ເພີ່ມສົກຮຽນ
            </Button>
          </Stack>
        </Box>
      )
}

export default AcademicYearPageHeaderText
import * as React from "react";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import { AcademicYear } from "../../../../domain/AcademicYears";
import { useAcademicYear } from "../../../hooks/useAcademicYear";
import AcademicYearPageHeaderText from "./AcademicYearPageHeaderText";
import AcademicYearTable from "./AcademicYearTable";
import CreateAcademicYearForm from "./CreateAcademicYearForm";
import UpdateAcademicYearForm from "./UpdateAcademicYearForm";
// import AcademicYearPageHeaderText from "./AcademicYearPageHeaderText";
// import CreateAcademicYearForm from "./CreateAcademicYearForm";
// import AcademicYearTable from "./AcademicYearTable";
// import UpdateAcademicYearForm from "./UpdateAcademicYearForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  academic_year_id: string;
  anchorEl: null | HTMLElement;
}

const AcademicYearPage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateAcademicYearDrawer, setIsOpenCreateAcademicYearDrawer] =
    React.useState(false);
  const [isOpenUpdateAcademicYearDrawer, setIsOpenUpdateAcademicYearDrawer] =
    React.useState(false);
  const [academicYearHold, setAcademicYearHold] = React.useState<
    AcademicYear | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    academic_year_id: "",
    anchorEl: null,
  });

  const {
    academicYearState,
    getAcademicYears,
    createAcademicYear,
    createAcademicYearReset,
    updateAcademicYear,
    updateAcademicYearReset,
    deleteAcademicYear,
    deleteAcademicYearReset,
  } = useAcademicYear();

  const {
    academic_years,
    isCreateAcademicYearSuccess,
    createAcademicYearError,
    isUpdateAcademicYearSuccess,
    updateAcademicYearError,
    isDeleteAcademicYearSuccess,
    deleteAcademicYearError,
  } = academicYearState;

  const handleOpenCreateAcademicYearDrawer = () => {
    setIsOpenCreateAcademicYearDrawer(true);
  };

  const handleCloseCreateAcademicYearDrawer = () => {
    setIsOpenCreateAcademicYearDrawer(false);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    academic_year_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      academic_year_id: academic_year_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      academic_year_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateAcademicYearDrawer = () => {
    const academic_year = academic_years.academicYears.academicYear.filter(
      (academic) => academic._id === openItemMenu.academic_year_id
    )[0];
    if (academic_year) {
      setAcademicYearHold(academic_year);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateAcademicYearDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        academic_year_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateAcademicYearDrawer = () => {
    setIsOpenUpdateAcademicYearDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    getAcademicYears(rowPerPage, newPage * rowPerPage);
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    getAcademicYears(parseInt(event.target.value, 10), 0);
  };

  const handleOpenDeleteAcademicYearAlertSweet = () => {
    const academic_year = openItemMenu.academic_year_id;
    if (academic_year) {
      setOpenItemMenu({
        isOpen: false,
        academic_year_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງສົກຮຽນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && academic_year) {
        deleteAcademicYear(academic_year);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateAcademicYearSuccess) {
      handleCloseCreateAcademicYearDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມສົກຮຽນສຳເລັດ!",
        icon: "success",
      });
      createAcademicYearReset();
      getAcademicYears(defaultLimit, 0);
    } else if (createAcademicYearError.isError) {
      handleCloseCreateAcademicYearDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createAcademicYearError.message,
        icon: "error",
      });
      createAcademicYearReset();
      getAcademicYears(defaultLimit, 0);
    }
  }, [isCreateAcademicYearSuccess, createAcademicYearError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateAcademicYearSuccess) {
      handleCloseUpdateAcademicYearDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນຂອງສົກຮຽນນີ້ສຳເລັດ!",
        icon: "success",
      });
      updateAcademicYearReset();
      getAcademicYears(defaultLimit, 0);
    } else if (updateAcademicYearError.isError) {
      handleCloseUpdateAcademicYearDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateAcademicYearError.message,
        icon: "error",
      });
      updateAcademicYearReset();
      getAcademicYears(defaultLimit, 0);
    }
  }, [isUpdateAcademicYearSuccess, updateAcademicYearError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteAcademicYearSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງສົກຮຽນສຳເລັດ!",
        icon: "success",
      });
      deleteAcademicYearReset();
      getAcademicYears(defaultLimit, 0);
    } else if (deleteAcademicYearError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteAcademicYearReset();
      getAcademicYears(defaultLimit, 0);
    }
  }, [isDeleteAcademicYearSuccess, deleteAcademicYearError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getAcademicYears(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
        <AcademicYearPageHeaderText
          handleOpenCreateAcademicYearDrawer={handleOpenCreateAcademicYearDrawer}
        />
        <CreateAcademicYearForm
          academicYearState={academicYearState}
          createAcademicYear={createAcademicYear}
          handleCloseCreateAcademicYearDrawer={handleCloseCreateAcademicYearDrawer}
          isOpenCreateAcademicYearDrawer={isOpenCreateAcademicYearDrawer}
        />
        {academicYearHold && (
          <UpdateAcademicYearForm
            academicYearState={academicYearState}
            handleCloseUpdateAcademicYearDrawer={
              handleCloseUpdateAcademicYearDrawer
            }
            isOpenUpdateAcademicYearDrawer={isOpenUpdateAcademicYearDrawer}
            academicYearHold={academicYearHold}
            updateAcademicYear={updateAcademicYear}
          />
        )}
        <AcademicYearTable
          academicYearState={academicYearState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateAcademicYearDrawer={handleOpenUpdateAcademicYearDrawer}
          handleOpenDeleteAcademicYearAlertSweet={
            handleOpenDeleteAcademicYearAlertSweet
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default AcademicYearPage;

import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./AcademicYearPage";
import { AcademicYearState } from "../../../../domain/AcademicYears";

type Props = {
  academicYearState: AcademicYearState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateAcademicYearDrawer: () => void;
  handleOpenDeleteAcademicYearAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const AcademicYearTable = ({
  academicYearState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateAcademicYearDrawer,
  handleOpenDeleteAcademicYearAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [academicYearTable, setAcademicYearTable] = React.useState<Array<any>>(
    []
  );

  const {
    academic_years,
    isGetAcademicYearsLoading,
    isGetAcademicYearsSuccess,
    getAcademicYearsError,
  } = academicYearState;

  React.useEffect(() => {
    setAcademicYearTable(
      academic_years.academicYears.academicYear.map((academic) => {
        return {
          ປີການສຶກສາ: <Typography>{academic.academic_year}</Typography>,
          ສົກຮຽນທີ່: <Typography>{academic.academic_year_no}</Typography>,
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(e, academic._id, academic.academic_year_no)
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen &&
                  openItemMenu.academic_year_id === academic._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateAcademicYearDrawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem
                  onClick={() => handleOpenDeleteAcademicYearAlertSweet()}
                >
                  ລົບຂໍ້ມູນ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [academic_years, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetAcademicYearsLoading}
          isLoadSuccess={isGetAcademicYearsSuccess}
          isLoadError={getAcademicYearsError.isError}
          data={academicYearTable}
          all={academic_years.academicYears.totalAcademicYears}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default AcademicYearTable;

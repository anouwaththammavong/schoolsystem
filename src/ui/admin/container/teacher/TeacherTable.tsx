import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./TeacherPage";
import { TeacherState } from "../../../../domain/Teacher";

type Props = {
  teacherState: TeacherState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateTeacherDrawer: () => void;
  handleOpenDeleteTeacherAlertSweet: () => void;
  handleOpenDetailsTeacherAbsence: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const TeacherTable = ({
  teacherState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateTeacherDrawer,
  handleOpenDeleteTeacherAlertSweet,
  handleOpenDetailsTeacherAbsence,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [teacherTable, setTeacherTable] = React.useState<Array<any>>([]);

  const {
    teachers,
    isGetTeachersLoading,
    isGetTeachersSuccess,
    getTeachersError,
  } = teacherState;

  React.useEffect(() => {
    setTeacherTable(
      teachers.teachers.teachers.map((teacher) => {
        return {
          ລະຫັດອາຈານ: <Typography>{teacher.teacher_id}</Typography>,
          ຊື່: <Typography>{teacher.name}</Typography>,
          ນາມສະກຸນ: <Typography>{teacher.last_name}</Typography>,
          ວັນເດືອນປີເກີດ: (
            <Typography>
              {moment(teacher.date_of_birth).format("DD/MM/YYYY")}
            </Typography>
          ),
          ອາຍຸ: <Typography>{teacher.age}</Typography>,
          ເພດ: <Typography>{teacher.gender}</Typography>,
          ລະດັບການສຶກສາ: <Typography>{teacher.degree}</Typography>,
          ຄູປະຈຳຫ້ອງ: (
            <Typography>
              {teacher.class_teacher?.room_name ? `${teacher.class_teacher.room_name} ຊັ້ນຮຽນ ${teacher.class_teacher.level_id.level_name}` : "ບໍ່ມີ"}
            </Typography>
          ),
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(e, teacher._id, teacher.name)
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen && openItemMenu.teacher_id === teacher._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateTeacherDrawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem onClick={() => handleOpenDeleteTeacherAlertSweet()}>
                  ລົບຂໍ້ມູນ
                </MenuItem>
                <MenuItem onClick={() => handleOpenDetailsTeacherAbsence()}>
                  ລາຍລະອຽດຂອງການຂາດ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [teachers, openItemMenu]);
  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetTeachersLoading}
          isLoadSuccess={isGetTeachersSuccess}
          isLoadError={getTeachersError.isError}
          data={teacherTable}
          all={teachers.teachers.totalTeachers}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default TeacherTable;

import React from "react";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import { TeacherAbsenceState } from "../../../../../domain/TeacherAbsence";
import ReactToPrint from "react-to-print";
import { Print } from "@mui/icons-material";
import TeacherAbsenceDetailsReport from "./report/TeacherAbsenceDetailsReport";

type Props = {
  control: Control<{
    absence_date: Date;
    be_reasonable: string;
  }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleChangeAbsence_date: (absence_date: Date) => void;
  handleChangeBe_reasonable: (be_reasonable: string) => void;
  teacherAbsenceState: TeacherAbsenceState;
};

const TeacherAbsenceDetailsFilterForm = ({
  control,
  handleChangeAbsence_date,
  handleChangeBe_reasonable,
  handleSearch,
  handleSubmit,
  teacherAbsenceState,
}: Props) => {
  const componentRef = React.useRef(null);
  const {
    isGetTeacherAbsencesLoading,
    isGetTeacherAbsencesSuccess,
    teacherAbsences,
  } = teacherAbsenceState;
  return (
    <Box>
      {isGetTeacherAbsencesLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
        </Paper>
      )}
      {isGetTeacherAbsencesSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
          <Grid
              item
              sm={12}
              xs={12}
              md={12}
              lg={12}
              xl={12}
              direction={"row"}
            >
              <Stack direction={"row"} gap={5}>
                <Typography variant="h6" color={"#00a10d"}>
                  ຂາດການສອນແບບມີເຫດຜົນ{" "}
                  {teacherAbsences.teacherAbsence.totalBe_reasonable} ມື້
                </Typography>
                <Typography variant="h6" color={"#d80000"}>
                ຂາດການສອນແບບບໍ່ມີເຫດຜົນ{" "}
                  {teacherAbsences.teacherAbsence.totalNotBe_reasonable} ມື້
                </Typography>
              </Stack>
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="absence_date"
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DesktopDatePicker
                      {...field}
                      label="ວັນທີ"
                      inputFormat="DD/MM/YYYY"
                      value={field.value ? dayjs(field.value) : null}
                      onChange={(date) => {
                        if (date) {
                          const formattedDate = date.toDate(); // Convert dayjs object to native Date object
                          field.onChange(formattedDate); // Update the form state with the native Date object
                          handleChangeAbsence_date(formattedDate); // Call your custom handler with the Date object
                        }
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="filled"
                          size="small"
                          fullWidth
                        />
                      )}
                    />
                  </LocalizationProvider>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="be_reasonable"
                // defaultValue="1"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ມີເຫດຜົນບໍໃນການຂາດຮຽນ</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeBe_reasonable(e.target.value)
                      }
                    >
                      <MenuItem value={"ມີເຫດຜົນ"}>ມີເຫດຜົນ</MenuItem>
                      <MenuItem value={"ບໍ່ມີເຫດຜົນ"}>ບໍ່ມີເຫດຜົນ</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetTeacherAbsencesLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetTeacherAbsencesLoading && (
                        <CircularProgress
                          color="inherit"
                          size={"2rem"}
                          sx={{ marginLeft: 1 }}
                        />
                      )}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນການຂາດຂອງອາຈານ"
                pageStyle="print"
              />
              <Box display={"none"}>
                <TeacherAbsenceDetailsReport
                  teacherAbsenceState={teacherAbsenceState}
                  ref={componentRef}
                />
              </Box>
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default TeacherAbsenceDetailsFilterForm;

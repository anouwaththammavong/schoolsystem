import * as React from "react";
import { useParams } from "react-router-dom";
import { Box, Stack } from "@mui/material";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { Absence } from "../../../../../domain/TeacherAbsence";
import { useTeacherAbsence } from "../../../../hooks/useTeacherAbsence";
import TeacherAbsencePageHeaderText from "./TeacherAbsencePageHeaderText";
import TeacherAbsenceDetailsFilterForm from "./TeacherAbsenceDetailsFilterForm";
import TeacherAbsenceDetailsTable from "./TeacherAbsenceDetailsTable";
import CreateTeacherAbsenceDetailsForm from "./CreateTeacherAbsenceDetailsForm";
import UpdateTeacherAbsenceDetailsForm from "./UpdateTeacherAbsenceDetailsForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

export interface OpenItemMenu {
  isOpen: boolean;
  absence_id: string;
  anchorEl: null | HTMLElement;
}

type Props = {};

const TeacherAbsenceDetailsPage = (props: Props) => {
  const { teacher_id } = useParams();
  const { handleSubmit, control, setValue, getValues } = useForm<{
    absence_date: Date;
    be_reasonable: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [
    isOpenCreateTeacherAbsenceDrawer,
    setIsOpenCreateTeacherAbsenceDrawer,
  ] = React.useState(false);
  const [
    isOpenUpdateTeacherAbsenceDrawer,
    setIsOpenUpdateTeacherAbsenceDrawer,
  ] = React.useState(false);
  const [teacherAbsenceHold, setTeacherAbsenceHold] = React.useState<
    Absence | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    absence_id: "",
    anchorEl: null,
  });

  const {
    teacherAbsenceState,
    getTeacherAbsence,
    createTeacherAbsence,
    createTeacherAbsenceReset,
    updateTeacherAbsence,
    updateTeacherAbsenceReset,
    deleteTeacherAbsence,
    deleteTeacherAbsenceReset,
  } = useTeacherAbsence();

  const {
    teacherAbsences,
    isGetTeacherAbsencesSuccess,
    isCreateTeacherAbsenceSuccess,
    createTeacherAbsenceError,
    isUpdateTeacherAbsenceSuccess,
    updateTeacherAbsenceError,
    isDeleteTeacherAbsenceSuccess,
    deleteTeacherAbsenceError,
  } = teacherAbsenceState;

  const handleChangeAbsence_date = (absence_date: Date) => {
    setValue("absence_date", absence_date);
    handleSearch();
  };

  const handleChangeBe_reasonable = (be_reasonable: string) => {
    setValue("be_reasonable", be_reasonable);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    if (teacher_id) {
      getTeacherAbsence(rowPerPage, 0, teacher_id, data.absence_date, data.be_reasonable);
    }
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    absence_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      absence_id: absence_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      absence_id: "",
      anchorEl: null,
    });
  };

  const handleOpenCreateTeacherAbsenceDrawer = () => {
    setIsOpenCreateTeacherAbsenceDrawer(true);
  };

  const handleCloseCreateTeacherAbsenceDrawer = () => {
    setIsOpenCreateTeacherAbsenceDrawer(false);
  };

  const handleOpenUpdateTeacherAbsenceDrawer = () => {
    const absences = teacherAbsences.teacherAbsence.absences.filter(
      (absence) => absence._id === openItemMenu.absence_id
    )[0];
    if (absences) {
      setTeacherAbsenceHold(absences);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateTeacherAbsenceDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        absence_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateTeacherAbsenceDrawer = () => {
    setIsOpenUpdateTeacherAbsenceDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    if (teacher_id) {
      getTeacherAbsence(
        rowPerPage,
        newPage * rowPerPage,
        teacher_id,
        data.absence_date,
        data.be_reasonable
      );
    }
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    if (teacher_id) {
      getTeacherAbsence(
        parseInt(event.target.value, 10),
        0,
        teacher_id,
        data.absence_date,
        data.be_reasonable
      );
    }
  };

  const handleOpenDeleteTeacherAbsenceAlertSweet = () => {
    const absence = openItemMenu.absence_id;
    if (absence) {
      setOpenItemMenu({
        isOpen: false,
        absence_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && absence) {
        deleteTeacherAbsence(absence);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateTeacherAbsenceSuccess && teacher_id) {
      handleCloseCreateTeacherAbsenceDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ໝາຍຂາດອາຈານສຳເລັດ!",
        icon: "success",
      });
      createTeacherAbsenceReset();
      getTeacherAbsence(defaultLimit, 0, teacher_id);
    } else if (createTeacherAbsenceError.isError && teacher_id) {
      handleCloseCreateTeacherAbsenceDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createTeacherAbsenceError.message,
        icon: "error",
      });
      createTeacherAbsenceReset();
      getTeacherAbsence(defaultLimit, 0, teacher_id);
    }
  }, [
    isCreateTeacherAbsenceSuccess,
    createTeacherAbsenceError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isUpdateTeacherAbsenceSuccess && teacher_id) {
      handleCloseUpdateTeacherAbsenceDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນອາຈານສຳເລັດ!",
        icon: "success",
      });
      updateTeacherAbsenceReset();
      getTeacherAbsence(defaultLimit, 0, teacher_id);
    } else if (updateTeacherAbsenceError.isError && teacher_id) {
      handleCloseUpdateTeacherAbsenceDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateTeacherAbsenceError.message,
        icon: "error",
      });
      updateTeacherAbsenceReset();
      getTeacherAbsence(defaultLimit, 0, teacher_id);
    }
  }, [
    isUpdateTeacherAbsenceSuccess,
    updateTeacherAbsenceError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isDeleteTeacherAbsenceSuccess && teacher_id) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງການໝາຍຂາດອາຈານສຳເລັດ!",
        icon: "success",
      });
      deleteTeacherAbsenceReset();
      getTeacherAbsence(defaultLimit, 0, teacher_id);
    } else if (deleteTeacherAbsenceError.isError && teacher_id) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteTeacherAbsenceReset();
      getTeacherAbsence(defaultLimit, 0, teacher_id);
    }
  }, [
    isDeleteTeacherAbsenceSuccess,
    deleteTeacherAbsenceError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp && teacher_id) {
      getTeacherAbsence(defaultLimit, 0, teacher_id);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit, teacher_id]);

  return (
    <Box>
      <Stack spacing={3}>
        {isGetTeacherAbsencesSuccess && (
          <TeacherAbsencePageHeaderText
            handleOpenCreateTeacherAbsenceDrawer={
              handleOpenCreateTeacherAbsenceDrawer
            }
          />
        )}
        <TeacherAbsenceDetailsFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleSearch={handleSearch}
          handleChangeAbsence_date={handleChangeAbsence_date}
          handleChangeBe_reasonable={handleChangeBe_reasonable}
          teacherAbsenceState={teacherAbsenceState}
        />
        <CreateTeacherAbsenceDetailsForm
          teacherAbsenceState={teacherAbsenceState}
          teacher_id={teacher_id}
          createTeacherAbsence={createTeacherAbsence}
          isOpenCreateTeacherAbsenceDrawer={isOpenCreateTeacherAbsenceDrawer}
          handleCloseCreateTeacherAbsenceDrawer={
            handleCloseCreateTeacherAbsenceDrawer
          }
        />
        {teacherAbsenceHold && (
          <UpdateTeacherAbsenceDetailsForm
            teacherAbsenceHold={teacherAbsenceHold}
            handleCloseUpdateTeacherAbsenceDrawer={
              handleCloseUpdateTeacherAbsenceDrawer
            }
            isOpenUpdateTeacherAbsenceDrawer={isOpenUpdateTeacherAbsenceDrawer}
            teacherAbsenceState={teacherAbsenceState}
            updateTeacherAbsence={updateTeacherAbsence}
          />
        )}
        <TeacherAbsenceDetailsTable
          teacherAbsenceState={teacherAbsenceState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateTeacherAbsenceDrawer={
            handleOpenUpdateTeacherAbsenceDrawer
          }
          handleOpenDeleteTeacherAbsenceAlertSweet={
            handleOpenDeleteTeacherAbsenceAlertSweet
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default TeacherAbsenceDetailsPage;

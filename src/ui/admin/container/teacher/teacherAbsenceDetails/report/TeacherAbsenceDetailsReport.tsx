import * as React from "react";
import { TeacherAbsenceState } from "../../../../../../domain/TeacherAbsence";
import { Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../../components/ui-tool/TableComponentForReport";

type Props = {
  teacherAbsenceState: TeacherAbsenceState;
};

const TeacherAbsenceDetailsReport = React.forwardRef<HTMLDivElement, Props>(
  ({ teacherAbsenceState }, ref) => {
    const [teacherAbsenceDetailsTable, setTeacherAbsenceDetailsTable] =
      React.useState<Array<any>>([]);

    const {
      teacherAbsences,
      isGetTeacherAbsencesLoading,
      isGetTeacherAbsencesSuccess,
      getTeacherAbsencesError,
    } = teacherAbsenceState;

    React.useEffect(() => {
      setTeacherAbsenceDetailsTable(
        teacherAbsences.teacherAbsence.absences.map((teacher_absence) => {
          return {
            ວັນທີຂາດການສອນ: (
              <Typography>
                {moment(teacher_absence.absence_date).format("DD/MM/YYYY")}
              </Typography>
            ),
            ຂາດໝົດມື້ຫຼືບໍ: (
              <Typography>{teacher_absence.absence_allday}</Typography>
            ),
            ມີເຫດຜົນຂອງການຂາດບໍ: (
              <Typography>{teacher_absence.be_reasonable}</Typography>
            ),
            ເຫດຜົນຂອງການຂາດ: (
              <Typography>
                {teacher_absence.note
                  ? teacher_absence.note
                  : "ບໍ່ໄດ້ບອກເຫດຜົນ"}
              </Typography>
            ),
          };
        })
      );
    }, [teacherAbsences]);
    return (
      <div ref={ref} style={{margin:"0 10px 0 10px" }}>
        <Typography textAlign={"center"} variant="h5">
          ລາຍງານຂໍ້ມູນລາຍລະອຽດວັນຂາດຂອງ ອາຈານ{" "}
          {teacherAbsences.teacherAbsence.absences[0]?.teacher.name}{" "}
          {teacherAbsences.teacherAbsence.absences[0]?.teacher.last_name}
        </Typography>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"}>
          ລວມມື້ຂາດສອນທັງໝົດ{" "}
          {teacherAbsences.teacherAbsence.totalAbsences}{" "}ມື້
          {" "}ຂາດສອນແບບມີເຫດຜົນ
            {teacherAbsences.teacherAbsence.totalBe_reasonable}{" "}ມື້
            {" "}ຂາດສອນແບບບໍ່ມີເຫດຜົນ {teacherAbsences.teacherAbsence.totalNotBe_reasonable}{" "}ມື້
        </Typography>
        <TableComponentForRePort
          isLoading={isGetTeacherAbsencesLoading}
          isLoadSuccess={isGetTeacherAbsencesSuccess}
          isLoadError={getTeacherAbsencesError.isError}
          data={teacherAbsenceDetailsTable}
          all={teacherAbsences.teacherAbsence.totalAbsences}
        />
      </div>
    );
  }
);

export default TeacherAbsenceDetailsReport;

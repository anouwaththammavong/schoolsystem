import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateTeacherAbsenceDrawer: () => void;
}

const TeacherAbsencePageHeaderText = ({handleOpenCreateTeacherAbsenceDrawer}: Props) => {
    return (
        <Box>
          <Stack
            direction={'row'}
            spacing={2}
          >
            <Typography
              variant='h5'
            >
              ລາຍລະອຽດຂອງການຂາດຮຽນຂອງອາຈານ
            </Typography>
            <Button
              variant='contained'
              onClick={() => handleOpenCreateTeacherAbsenceDrawer()}
            >
              <Add /> ໝາຍຂາດ
            </Button>
          </Stack>
        </Box>
      )
}

export default TeacherAbsencePageHeaderText
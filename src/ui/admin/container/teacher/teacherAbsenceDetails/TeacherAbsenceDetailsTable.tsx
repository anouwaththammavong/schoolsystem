import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./TeacherAbsenceDetailsPage";
import { TeacherAbsenceState } from "../../../../../domain/TeacherAbsence";
import moment from "moment";

type Props = {
  teacherAbsenceState: TeacherAbsenceState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    absence_id: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateTeacherAbsenceDrawer: () => void;
  handleOpenDeleteTeacherAbsenceAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const TeacherAbsenceDetailsTable = ({
  teacherAbsenceState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateTeacherAbsenceDrawer,
  handleOpenDeleteTeacherAbsenceAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [teacherAbsenceDetailsTable, setTeacherAbsenceDetailsTable] =
    React.useState<Array<any>>([]);

  const {
    teacherAbsences,
    isGetTeacherAbsencesLoading,
    isGetTeacherAbsencesSuccess,
    getTeacherAbsencesError,
  } = teacherAbsenceState;

  React.useEffect(() => {
    setTeacherAbsenceDetailsTable(
      teacherAbsences.teacherAbsence.absences.map((teacher_absence) => {
        return {
          ວັນທີຂາດການສອນ: (
            <Typography>
              {moment(teacher_absence.absence_date).format("DD/MM/YYYY")}
            </Typography>
          ),
          ຂາດໝົດມື້ຫຼືບໍ: (
            <Typography>{teacher_absence.absence_allday}</Typography>
          ),
          ມີເຫດຜົນຂອງການຂາດບໍ: (
            <Typography>{teacher_absence.be_reasonable}</Typography>
          ),
          ເຫດຜົນຂອງການຂາດ: (
            <Typography>
              {teacher_absence.note
                ? teacher_absence.note
                : "ບໍ່ໄດ້ບອກເຫດຜົນ"}
            </Typography>
          ),
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(
                    e,
                    teacher_absence._id,
                    teacher_absence.teacher._id
                  )
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen &&
                  openItemMenu.absence_id === teacher_absence._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem
                  onClick={() => handleOpenUpdateTeacherAbsenceDrawer()}
                >
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem
                  onClick={() => handleOpenDeleteTeacherAbsenceAlertSweet()}
                >
                  ລົບຂໍ້ມູນ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [teacherAbsences, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetTeacherAbsencesLoading}
          isLoadSuccess={isGetTeacherAbsencesSuccess}
          isLoadError={getTeacherAbsencesError.isError}
          data={teacherAbsenceDetailsTable}
          all={teacherAbsences.teacherAbsence.totalAbsences}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default TeacherAbsenceDetailsTable;

import { Box, Stack } from "@mui/material";
import React from "react";
import TeacherPageHeaderText from "./TeacherPageHeaderText";
import { useForm } from "react-hook-form";
import { TeacherElement } from "../../../../domain/Teacher";
import { useTeacher } from "../../../hooks/useTeacher";
import TeacherFilterForm from "./TeacherFilterForm";
import TeacherTable from "./TeacherTable";
import Swal from "sweetalert2";
import CreateTeacherForm from "./CreateTeacherForm";
import UpdateTeacherForm from "./UpdateTeacherForm";
import { useNavigate } from "react-router-dom";
import { useSubjectV2 } from "../../../hooks/useSubjectV2";
import { useClassRoom } from "../../../hooks/useClassRoom";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  teacher_id: string;
  anchorEl: null | HTMLElement;
}

const TeacherPage = (props: Props) => {
  const navigate = useNavigate();
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateTeacherDrawer, setIsOpenCreateTeacherDrawer] =
    React.useState(false);
  const [isOpenUpdateTeacherDrawer, setIsOpenUpdateTeacherDrawer] =
    React.useState(false);
  const [teacherHold, setTeacherHold] = React.useState<
    TeacherElement | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    teacher_id: "",
    anchorEl: null,
  });

  const {
    teacherState,
    getTeachers,
    createTeacher,
    createTeacherReset,
    updateTeacher,
    updateTeacherReset,
    deleteTeacher,
    deleteTeacherReset,
  } = useTeacher();

  const { getSubjectV2s, subjectV2State } = useSubjectV2();
  const { getClassRooms, classRoomState } = useClassRoom();

  const {
    teachers,
    isCreateTeacherSuccess,
    CreateTeacherError,
    isUpdateTeacherSuccess,
    updateTeacherError,
    isDeleteTeacherSuccess,
    deleteTeacherError,
  } = teacherState;

  const handleOpenCreateTeacherDrawer = () => {
    setIsOpenCreateTeacherDrawer(true);
  };

  const handleCloseCreateTeacherDrawer = () => {
    setIsOpenCreateTeacherDrawer(false);
  };

  const handleBlurSearch = (teacher_id: string) => {
    setValue("teacher_id", teacher_id);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getTeachers(rowPerPage, 0, data.teacher_id);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    teacher_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      teacher_id: teacher_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      teacher_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateTeacherDrawer = () => {
    const teacher = teachers.teachers.teachers.filter(
      (teacher) => teacher._id === openItemMenu.teacher_id
    )[0];
    if (teacher) {
      setTeacherHold(teacher);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateTeacherDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        teacher_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateTeacherDrawer = () => {
    setIsOpenUpdateTeacherDrawer(false);
  };

  const handleOpenDetailsTeacherAbsence = () => {
    const encodedTeacherId = encodeURIComponent(openItemMenu.teacher_id);
    navigate(`/absence_teacher/teacher/${encodedTeacherId}/teacherAbsenceDetails`);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    if (data.teacher_id) {
      getTeachers(rowPerPage, newPage * rowPerPage, data.teacher_id);
    } else {
      getTeachers(rowPerPage, newPage * rowPerPage);
    }
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    if (data.teacher_id) {
      getTeachers(parseInt(event.target.value, 10), 0, data.teacher_id);
    } else {
      getTeachers(parseInt(event.target.value, 10), 0);
    }
  };

  const handleOpenDeleteTeacherAlertSweet = () => {
    const teacher = openItemMenu.teacher_id;
    if (teacher) {
      setOpenItemMenu({
        isOpen: false,
        teacher_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງອາຈານຜູ້ນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && teacher) {
        deleteTeacher(teacher);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateTeacherSuccess) {
      handleCloseCreateTeacherDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມຂໍ້ມູນອາຈານສຳເລັດ!",
        icon: "success",
      });
      createTeacherReset();
      getTeachers(defaultLimit, 0);
    } else if (CreateTeacherError.isError) {
      handleCloseCreateTeacherDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: CreateTeacherError.message,
        icon: "error",
      });
      createTeacherReset();
      getTeachers(defaultLimit, 0);
    }
  }, [isCreateTeacherSuccess, CreateTeacherError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateTeacherSuccess) {
      handleCloseUpdateTeacherDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນອາຈານສຳເລັດ!",
        icon: "success",
      });
      updateTeacherReset();
      getTeachers(defaultLimit, 0);
    } else if (updateTeacherError.isError) {
      handleCloseUpdateTeacherDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateTeacherError.message,
        icon: "error",
      });
      updateTeacherReset();
      getTeachers(defaultLimit, 0);
    }
  }, [isUpdateTeacherSuccess, updateTeacherError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteTeacherSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນອາຈານສຳເລັດ!",
        icon: "success",
      });
      deleteTeacherReset();
      getTeachers(defaultLimit, 0);
    } else if (deleteTeacherError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteTeacherReset();
      getTeachers(defaultLimit, 0);
    }
  }, [isDeleteTeacherSuccess, deleteTeacherError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getSubjectV2s(0,0);
      getClassRooms(0,0)
      getTeachers(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp]);

  return (
    <Box>
      <Stack spacing={3}>
        <TeacherPageHeaderText
          handleOpenCreateTeacherDrawer={handleOpenCreateTeacherDrawer}
        />
        <TeacherFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleBlurSearch={handleBlurSearch}
          handleSearch={handleSearch}
          teacherState={teacherState}
        />
        <CreateTeacherForm createTeacher={createTeacher}
          teacherState={teacherState}
          subjectV2State={subjectV2State}
          classRoomState={classRoomState}
          handleCloseCreateTeacherDrawer={handleCloseCreateTeacherDrawer}
          isOpenCreateTeacherDrawer={isOpenCreateTeacherDrawer} />
          {teacherHold && (
          <UpdateTeacherForm
            teacherHold={teacherHold}
            subjectV2State={subjectV2State}
          classRoomState={classRoomState}
            isOpenUpdateTeacherDrawer={isOpenUpdateTeacherDrawer}
            handleCloseUpdateTeacherDrawer={handleCloseUpdateTeacherDrawer}
            teacherState={teacherState}
            updateTeacher={updateTeacher}
          />
        )}
        <TeacherTable
          teacherState={teacherState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateTeacherDrawer={handleOpenUpdateTeacherDrawer}
          handleOpenDeleteTeacherAlertSweet={handleOpenDeleteTeacherAlertSweet}
          handleOpenDetailsTeacherAbsence={handleOpenDetailsTeacherAbsence}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default TeacherPage;

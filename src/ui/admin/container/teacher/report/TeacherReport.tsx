import * as React from "react";
import { StudentState } from "../../../../../domain/Student";
import { Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";
import { TeacherState } from "../../../../../domain/Teacher";

type Props = {
  teacherState: TeacherState;
};

const TeacherReport = React.forwardRef<HTMLDivElement, Props>(
  ({ teacherState }, ref) => {
    const [teacherTable, setTeacherTable] = React.useState<Array<any>>([]);

  const {
    teachers,
    isGetTeachersLoading,
    isGetTeachersSuccess,
    getTeachersError,
  } = teacherState;

  React.useEffect(() => {
    setTeacherTable(
      teachers.teachers.teachers.map((teacher) => {
        return {
          ລະຫັດອາຈານ: <Typography>{teacher.teacher_id}</Typography>,
          ຊື່: <Typography>{teacher.name}</Typography>,
          ນາມສະກຸນ: <Typography>{teacher.last_name}</Typography>,
          ວັນເດືອນປີເກີດ: (
            <Typography>
              {moment(teacher.date_of_birth).format("DD/MM/YYYY")}
            </Typography>
          ),
          ອາຍຸ: <Typography>{teacher.age}</Typography>,
          ເພດ: <Typography>{teacher.gender}</Typography>,
          ລະດັບການສຶກສາ: <Typography>{teacher.degree}</Typography>,
          ຄູປະຈຳຫ້ອງ: (
            <Typography>
              {teacher.class_teacher?.room_name ? `${teacher.class_teacher.room_name} ຊັ້ນຮຽນ ${teacher.class_teacher.level_id.level_name}` : "ບໍ່ມີ"}
            </Typography>
          ),
        };
      })
    );
  }, [teachers]);
    return (
        <div ref={ref} style={{marginTop:"20px"}}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"} >ລາຍງານຂໍ້ມູນອາຈານ</Typography>
        <TableComponentForRePort
          isLoading={isGetTeachersLoading}
          isLoadSuccess={isGetTeachersSuccess}
          isLoadError={getTeachersError.isError}
          data={teacherTable}
          all={teachers.teachers.totalTeachers}
        />
      </div>
    );
  }
);

export default TeacherReport;

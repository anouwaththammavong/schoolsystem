import * as React from "react";
import { Print, Search } from "@mui/icons-material";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
  useForm,
} from "react-hook-form";
import ReactToPrint from "react-to-print";
import { TeacherState } from "../../../../domain/Teacher";
import TeacherReport from "./report/TeacherReport";

type Props = {
  control: Control<FieldValues, any>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleBlurSearch: (no: string) => void;
  teacherState: TeacherState;
};

const TeacherFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleBlurSearch,
  teacherState,
}: Props) => {
  const componentRef = React.useRef(null);
  const { isGetTeachersLoading, isGetTeachersSuccess } = teacherState;
  return (
    <Box>
      {isGetTeachersLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
        </Paper>
      )}
      {isGetTeachersSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
            <Grid item sm={12} xs={12} md={4} lg={4} xl={4}>
              <Controller
                control={control}
                name="teacher_id"
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="ຄົ້ນຫາ"
                    size="small"
                    fullWidth
                    onBlur={(e) => handleBlurSearch(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <Search />
                        </InputAdornment>
                      ),
                    }}
                    variant="filled"
                  />
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetTeachersLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetTeachersLoading && (
                        <CircularProgress
                          color="inherit"
                          size={"2rem"}
                          sx={{ marginLeft: 1 }}
                        />
                      )}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນອາຈານ"
                pageStyle="print"
              />
              <Box display={"none"}>
              <TeacherReport teacherState={teacherState}  ref={componentRef} />
              </Box>
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default TeacherFilterForm;

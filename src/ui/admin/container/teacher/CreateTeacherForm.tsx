import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Paper,
  Skeleton,
  Table,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Add, Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import EnhancedTableHead, {
  HeadCell,
} from "../../components/EnhancedTableHead";
import moment from "moment";
import { CreateTeacher, TeacherState } from "../../../../domain/Teacher";
import { SubjectV2, SubjectV2State } from "../../../../domain/SubjectV2";
import { ClassRoom, ClassRoomState } from "../../../../domain/ClassRoom";

type Props = {
  teacherState: TeacherState;
  subjectV2State: SubjectV2State;
  classRoomState: ClassRoomState;
  isOpenCreateTeacherDrawer: boolean;
  handleCloseCreateTeacherDrawer: () => void;
  createTeacher: (teacher: CreateTeacher) => void;
};

const headCells: HeadCell[] = [
  { id: "title", label: "ວິຊາທີ່ສອນ" },
  { id: "action", label: "" },
];

const CreateTeacherForm = ({
  teacherState,
  subjectV2State,
  classRoomState,
  isOpenCreateTeacherDrawer,
  handleCloseCreateTeacherDrawer,
  createTeacher,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const [isEmptyItems, setIsEmptyItems] = React.useState(false);

  const degree = [
    {
      title: "ປະລິນຍາຕີ",
    },
    {
      title: "ປະລິນຍາໂທ",
    },
    {
      title: "ປະລິນຍາເອກ",
    },
  ];

  const { subjectV2s, isGetSubjectV2sLoading, isGetSubjectV2sSuccess } =
    subjectV2State;

  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;

  const { isCreateTeacherLoading, isCreateTeacherSuccess } = teacherState;

  const {
    handleSubmit,
    control,
    setValue,
    trigger,
    getValues,
    reset,
    formState: { errors },
  } = useForm<CreateTeacher>();

  const { fields, append, remove } = useFieldArray({
    control,
    name: "subjects",
  });

  const handleChangeDate_of_birth = (date_of_birth: any) => {
    if (date_of_birth) {
      setValue("date_of_birth", date_of_birth);
    }
  };

  const handleCreateTeacher = (data: CreateTeacher) => {
    if (data.subjects.length >= 1) {
      createTeacher({
        teacher_id: data.teacher_id,
        name: data.name,
        last_name: data.last_name,
        age: data.age,
        teacher_tel: data.teacher_tel,
        date_of_birth: moment(data.date_of_birth).format("YYYY-MM-DD"),
        teacher_village: data.teacher_village,
        teacher_district: data.teacher_district,
        teacher_province: data.teacher_province,
        nationality: data.nationality,
        graduated_from_institute: data.graduated_from_institute,
        gender: data.gender,
        degree: data.degree,
        subjects: data.subjects,
        ...(data.class_teacher && { class_teacher: data.class_teacher }),
      });
    } else {
      setIsEmptyItems(true);
    }
  };

  React.useEffect(() => {
    if (isCreateTeacherSuccess) {
      reset({
        teacher_id: "",
        name: "",
        last_name: "",
        age: "",
        teacher_tel: "",
        date_of_birth: moment().format(),
        teacher_village: "",
        teacher_district: "",
        teacher_province: "",
        nationality: "",
        graduated_from_institute: "",
        gender: "",
        degree: "",
        class_teacher: "",
      });
      remove()
    }
  }, [isCreateTeacherSuccess]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateTeacherDrawer}
      onClose={() => handleCloseCreateTeacherDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleCreateTeacher)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມຂໍ້ມູນອາຈານ
                  </Typography>
                  <IconButton onClick={() => handleCloseCreateTeacherDrawer()}>
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="teacher_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລະຫັດອາຈານ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.teacher_id &&
                  errors.teacher_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລະຫັດອາຈານ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ອາຈານ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors && errors.name && errors.name.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຊື່ອາຈານ"}
                  </Typography>
                )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="last_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ນາມສະກຸນອາຈານ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.last_name &&
                  errors.last_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນນາມສະກຸນອາຈານ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="age"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ອາຍຸ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors && errors.age && errors.age.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນອາຍຸຂອງອາຈານ"}
                  </Typography>
                )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="teacher_tel"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ເບີໂທ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.teacher_tel &&
                  errors.teacher_tel.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນເບີໂທ"}
                    </Typography>
                  )}
              </Grid>

              {/* date of birth */}
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="date_of_birth"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DesktopDatePicker
                        {...field}
                        label="ວັນທີ"
                        inputFormat="DD/MM/YYYY"
                        onChange={(e) => handleChangeDate_of_birth(e)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="filled"
                            size="small"
                            fullWidth
                          />
                        )}
                      />
                    </LocalizationProvider>
                  )}
                />

                {errors &&
                  errors.date_of_birth &&
                  errors.date_of_birth.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="teacher_village"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ບ້ານຢູ່ອາໃສ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.teacher_village &&
                  errors.teacher_village.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນບ້ານຢູ່ອາໃສ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="teacher_district"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ເມື່ອງ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.teacher_district &&
                  errors.teacher_district.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນເມື່ອງ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="teacher_province"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ແຂວງ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.teacher_province &&
                  errors.teacher_province.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນແຂວງ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="nationality"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ສັນຊາດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.nationality &&
                  errors.nationality.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນສັນຊາດ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="graduated_from_institute"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຈົບຈາກສະຖາບັນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateTeacherLoading}
                    />
                  )}
                />
                {errors &&
                  errors.graduated_from_institute &&
                  errors.graduated_from_institute.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນສະຖາບັນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="gender"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      options={[{ gender: "ຊາຍ" }, { gender: "ຍິງ" }]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.gender}`}
                      onChange={(
                        event: any,
                        newValue: { gender: string } | null
                      ) => {
                        field.onChange(newValue?.gender);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.gender}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ເພດ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.gender &&
                  errors.gender.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນເພດຂອງອາຈານ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="degree"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      options={degree}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: { title: string } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ລະດັບການສຶກສາ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.degree &&
                  errors.degree.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນລະດັບການສຶກສາຂອງອາຈານ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Button
                  variant="contained"
                  sx={{ paddingInline: 3 }}
                  onClick={() => {
                    setIsEmptyItems(false);
                    append({ title: "" });
                  }}
                  disabled={isCreateTeacherLoading}
                >
                  <Add /> ເພີ່ມຂໍ້ມູນວິຊາທີ່ສອນ
                </Button>
              </Grid>

              <Grid item xs={12}>
                <TableContainer component={Paper}>
                  <Table aria-label="simple table">
                    <EnhancedTableHead headCells={headCells} />
                    {fields.map(({ id }, index) => {
                      return (
                        <TableRow key={id}>
                          <TableCell width={"60%"}>
                            {isGetSubjectV2sLoading && <Skeleton />}
                            {isGetSubjectV2sSuccess &&
                            subjectV2s.subjectV2s.subjectV2s.length > 0 ? (
                              <Controller
                                control={control}
                                name={`subjects.${index}.title`}
                                rules={{
                                  required: true,
                                }}
                                render={({ field }) => (
                                  <Autocomplete
                                    id="combo-box-demo"
                                    options={subjectV2s.subjectV2s.subjectV2s}
                                    autoComplete
                                    fullWidth
                                    getOptionLabel={(option) =>
                                      option.subject_name
                                    }
                                    onChange={(
                                      event: any,
                                      newValue: SubjectV2 | null
                                    ) => {
                                      field.onChange(newValue?._id);
                                    }}
                                    renderOption={(props, option) => (
                                      <Box
                                        component="li"
                                        sx={{
                                          "& > img": { mr: 2, flexShrink: 0 },
                                        }}
                                        {...props}
                                      >
                                        {option.subject_name}
                                      </Box>
                                    )}
                                    renderInput={(params) => (
                                      <TextField
                                        {...params}
                                        label="ວິຊາ"
                                        variant="filled"
                                        inputProps={{
                                          ...params.inputProps,
                                          autoComplete: "disabled",
                                        }}
                                        fullWidth
                                      />
                                    )}
                                  />
                                )}
                              />
                            ) : (
                              <Typography
                                variant="subtitle2"
                                color="error"
                                marginTop={1}
                              >
                                {"ກະລຸນາໄປເພີ່ມວິຊາທີ່ຮຽນກ່ອນ"}
                              </Typography>
                            )}
                            {errors &&
                              errors.subjects &&
                              errors.subjects?.[index] &&
                              errors.subjects?.[index]?.title?.type ===
                                "required" && (
                                <Typography
                                  variant="subtitle2"
                                  color="error"
                                  marginTop={1}
                                >
                                  {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                                </Typography>
                              )}
                          </TableCell>
                          <TableCell>
                            <IconButton
                              onClick={() => {
                                remove(index);
                              }}
                            >
                              <Close />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </Table>
                </TableContainer>
                {isEmptyItems && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາເພີ່ມວິຊາທີ່ອາຈານສອນ"}
                  </Typography>
                )}
              </Grid>

              <Grid item xs={12}>
                {isGetClassRoomsLoading && <Skeleton />}
                {isGetClassRoomsSuccess &&
                  classRooms.classrooms.totalCount > 0 && (
                    <Controller
                      control={control}
                      name="class_teacher"
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={classRooms.classrooms.classRooms}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => `${option.room_name} ຊັ້ນຮຽນ ${option.level_id.level_name}`}
                          onChange={(
                            event: any,
                            newValue: ClassRoom | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                              {...props}
                            >
                              {option.room_name}
                              {" "}
                              {"ຊັ້ນຮຽນ "}
                              {option.level_id.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ເປັນຄູປະຈຳຫ້ອງ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                              }}
                              fullWidth
                            />
                          )}
                        />
                      )}
                    />
                  )}
                {isGetClassRoomsSuccess &&
                  classRooms.classrooms.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຫ້ອງຮຽນກ່ອນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateTeacherLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateTeacherForm;

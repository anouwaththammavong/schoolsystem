import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateTeacherDrawer: () => void;
}

const TeacherPageHeaderText = ({handleOpenCreateTeacherDrawer}: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນອາຈານ
        </Typography>
        <Button
          variant='contained'
          onClick={() => handleOpenCreateTeacherDrawer()}
        >
          <Add /> ເພີ່ມລາຍການ
        </Button>
      </Stack>
    </Box>
  )
}

export default TeacherPageHeaderText
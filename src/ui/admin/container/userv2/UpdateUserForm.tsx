import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import { UserState, UpdateUser, UserElement } from "../../../../domain/user";

type Props = {
  userState: UserState;
  isOpenUpdateUserDrawer: boolean;
  handleCloseUpdateUserDrawer: () => void;
  updateUser: (_id: string, user: UpdateUser) => void;
  userHold: UserElement | undefined;
};

const UpdateUserForm = ({
  userState,
  isOpenUpdateUserDrawer,
  handleCloseUpdateUserDrawer,
  updateUser,
  userHold,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const userRole = [
    {
      title: "ADMIN",
    },
    {
      title: "TEACHER",
    },
  ];

  const { isUpdateUserLoading, isUpdateUserSuccess } = userState;

  const {
    handleSubmit,
    control,
    setValue,
    trigger,
    getValues,
    reset,
    formState: { errors },
  } = useForm<UpdateUser>();

  const handleUpdateUser = (data: UpdateUser) => {
    if (userHold) {
      updateUser(userHold?._id, {
        username: data.username,
        password: data.password,
        role: data.role,
      });
    }
  };

  React.useEffect(() => {
    reset({
      password: "",
    });
  }, [isUpdateUserSuccess]);

  React.useEffect(() => {
    if (userHold) {
      setValue("username", userHold.username);
      setValue("role", userHold.role);
    }
  }, [userHold]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateUserDrawer}
      onClose={() => handleCloseUpdateUserDrawer()}
    >
      <form style={{ width: "100%" }} onSubmit={handleSubmit(handleUpdateUser)}>
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ແກ້ໄຂຂໍ້ມູນຜູ້ໃຊ້
                  </Typography>
                  <IconButton onClick={() => handleCloseUpdateUserDrawer()}>
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="username"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ຜູ້ໃຊ້"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateUserLoading}
                    />
                  )}
                />
                {errors &&
                  errors.username &&
                  errors.username.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="password"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="password"
                      InputProps={{
                        type: "password",
                      }}
                      label={"ລະຫັດຜ່ານ"}
                      fullWidth
                      variant="filled"
                      // multiline
                      disabled={isUpdateUserLoading}
                    />
                  )}
                />
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="role"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isUpdateUserLoading}
                      options={userRole}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="role"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                      defaultValue={userRole.find(
                        (o) => o.title === userHold?.role
                      )}
                    />
                  )}
                />
                {errors && errors.role && errors.role.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຂໍ້ມູນ"}
                  </Typography>
                )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateUserLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateUserForm;

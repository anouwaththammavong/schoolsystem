import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import { UserState, ICreateUser } from "../../../../domain/user";

type Props = {
  userState: UserState;
  isOpenCreateUserDrawer: boolean;
  handleCloseCreateUserDrawer: () => void;
  createUser: (user: ICreateUser) => void;
};

const CreateUserForm = ({
  userState,
  isOpenCreateUserDrawer,
  handleCloseCreateUserDrawer,
  createUser,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const userRole = [
    {
      title: "ADMIN",
    },
    {
      title: "TEACHER",
    },
  ];

  const { isCreateUserLoading, isCreateUserSuccess } = userState;

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm<ICreateUser>();

  const handleCreateUser = (data: ICreateUser) => {
    createUser({
      username: data.username,
      password: data.password,
      role: data.role,
    });
  };

  React.useEffect(() => {
    if (isCreateUserSuccess) {
      reset({
        username: "",
        password: "",
        role: "",
      });
    }
  }, [isCreateUserSuccess]);
  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateUserDrawer}
      onClose={() => handleCloseCreateUserDrawer()}
    >
      <form style={{ width: "100%" }} onSubmit={handleSubmit(handleCreateUser)}>
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມຂໍ້ມູນຜູ້ໃຊ້
                  </Typography>
                  <IconButton onClick={() => handleCloseCreateUserDrawer()}>
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="username"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ຜູ້ໃຊ້"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateUserLoading}
                    />
                  )}
                />
                {errors &&
                  errors.username &&
                  errors.username.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="password"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="password"
                      InputProps={{
                        type: "password",
                      }}
                      label={"ລະຫັດຜ່ານ"}
                      fullWidth
                      variant="filled"
                      // multiline
                      disabled={isCreateUserLoading}
                    />
                  )}
                />
                {errors &&
                  errors.password &&
                  errors.password.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="role"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isCreateUserLoading}
                      options={userRole}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="role"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                    />
                  )}
                />
                {errors && errors.role && errors.role.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຂໍ້ມູນ"}
                  </Typography>
                )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateUserLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateUserForm;

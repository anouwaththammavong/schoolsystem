import * as React from "react";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import { User, UserElement } from "../../../../domain/user";
import { useUser } from "../../../hooks/useUser";
import UserPageHeaderText from "./UserPageHeaderText";
import UserTable from "./UserTable";
import CreateUserForm from "./CreateUserForm";
import UpdateUserForm from "./UpdateUserForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  user_id: string;
  anchorEl: null | HTMLElement;
}

const UserPage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateUserDrawer, setIsOpenCreateUserDrawer] =
    React.useState(false);
  const [isOpenUpdateUserDrawer, setIsOpenUpdateUserDrawer] =
    React.useState(false);
  const [userHold, setUserHold] = React.useState<
    UserElement | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    user_id: "",
    anchorEl: null,
  });

  const {
    userState,
    getUsers,
    createUser,
    createUserReset,
    updateUser,
    updateUserReset,
  } = useUser();

  const {
    users,
    isCreateUserSuccess,
    createUserError,
    isUpdateUserSuccess,
    updateUserError,
  } = userState;

  const handleOpenCreateUserDrawer = () => {
    setIsOpenCreateUserDrawer(true);
  };

  const handleCloseCreateUserDrawer = () => {
    setIsOpenCreateUserDrawer(false);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    user_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      user_id: user_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      user_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateUserDrawer = () => {
    const user = users.users.filter(
      (academic) => academic._id === openItemMenu.user_id
    )[0];
    if (user) {
      setUserHold(user);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateUserDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        user_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateUserDrawer = () => {
    setIsOpenUpdateUserDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    getUsers(rowPerPage, newPage * rowPerPage);
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    getUsers(parseInt(event.target.value, 10), 0);
  };

  // const handleOpenDeleteUserAlertSweet = () => {
  //   const user = openItemMenu.user_id;
  //   if (user) {
  //     setOpenItemMenu({
  //       isOpen: false,
  //       user_id: "",
  //       anchorEl: null,
  //     });
  //   }
  //   Swal.fire({
  //     title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງຂໍ້ມູນຜູ້ໃຊ້ນີ້ບໍ?",
  //     icon: "warning",
  //     showCancelButton: true,
  //     confirmButtonText: "ຕ້ອງການ",
  //     cancelButtonText: `ບໍ່ຕ້ອງການ`,
  //     confirmButtonColor: "#22bb33",
  //     cancelButtonColor: "#d33",
  //   }).then((result) => {
  //     if (result.isConfirmed && user) {
  //       deleteUser(user);
  //     }
  //   });
  // };

  React.useEffect(() => {
    if (isCreateUserSuccess) {
      handleCloseCreateUserDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມຂໍ້ມູນຜູ້ໃຊ້ສຳເລັດ!",
        icon: "success",
      });
      createUserReset();
      getUsers(defaultLimit, 0);
    } else if (createUserError.isError) {
      handleCloseCreateUserDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createUserError.message,
        icon: "error",
      });
      createUserReset();
      getUsers(defaultLimit, 0);
    }
  }, [isCreateUserSuccess, createUserError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateUserSuccess) {
      handleCloseUpdateUserDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນຂອງຂໍ້ມູນຜູ້ໃຊ້ນີ້ສຳເລັດ!",
        icon: "success",
      });
      updateUserReset();
      getUsers(defaultLimit, 0);
    } else if (updateUserError.isError) {
      handleCloseUpdateUserDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateUserError.message,
        icon: "error",
      });
      updateUserReset();
      getUsers(defaultLimit, 0);
    }
  }, [isUpdateUserSuccess, updateUserError.isError, defaultLimit]);

  // React.useEffect(() => {
  //   if (isDeleteUserSuccess) {
  //     Swal.fire({
  //       title: "ສຳເລັດ!",
  //       text: "ລົບຂໍ້ມູນຂອງຂໍ້ມູນຜູ້ໃຊ້ສຳເລັດ!",
  //       icon: "success",
  //     });
  //     deleteUserReset();
  //     getUsers(defaultLimit, 0);
  //   } else if (deleteUserError.isError) {
  //     Swal.fire({
  //       title: "ເກີດຂໍ້ຜິດພາດ!",
  //       text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
  //       icon: "error",
  //     });
  //     deleteUserReset();
  //     getUsers(defaultLimit, 0);
  //   }
  // }, [isDeleteUserSuccess, deleteUserError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getUsers(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
         <UserPageHeaderText
          handleOpenCreateUserDrawer={handleOpenCreateUserDrawer}
        />
        
        <CreateUserForm
          userState={userState}
          createUser={createUser}
          handleCloseCreateUserDrawer={handleCloseCreateUserDrawer}
          isOpenCreateUserDrawer={isOpenCreateUserDrawer}
        />
        
        {userHold && (
          <UpdateUserForm
            userState={userState}
            handleCloseUpdateUserDrawer={
              handleCloseUpdateUserDrawer
            }
            isOpenUpdateUserDrawer={isOpenUpdateUserDrawer}
            userHold={userHold}
            updateUser={updateUser}
          />
        )}
          
        <UserTable
          userState={userState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateUserDrawer={handleOpenUpdateUserDrawer}
          // handleOpenDeleteUserAlertSweet={
          //   handleOpenDeleteUserAlertSweet
          // }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        /> 
      </Stack>
    </Box>
  );
};

export default UserPage;

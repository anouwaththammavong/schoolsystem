import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateUserDrawer: () => void;
}

const UserPageHeaderText = ({handleOpenCreateUserDrawer}: Props) => {
    return (
        <Box>
          <Stack
            direction={'row'}
            spacing={2}
          >
            <Typography
              variant='h5'
            >
              ຂໍ້ມູນຜູ້ໃຊ້
            </Typography>
            <Button
              variant='contained'
              onClick={() => handleOpenCreateUserDrawer()}
            >
              <Add /> ເພີ່ມຜູ້ໃຊ້
            </Button>
          </Stack>
        </Box>
      )
}

export default UserPageHeaderText
import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./UserPage";
import { UserState } from "../../../../domain/user";

type Props = {
  userState: UserState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateUserDrawer: () => void;
  // handleOpenDeleteUserAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const UserTable = ({
  userState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateUserDrawer,
  // handleOpenDeleteUserAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [userTable, setUserTable] = React.useState<Array<any>>(
    []
  );

  const {
    users,
    isGetUsersLoading,
    isGetUsersSuccess,
    getUsersError,
  } = userState;

  React.useEffect(() => {
    setUserTable(
      users.users.map((user) => {
        return {
          username: <Typography>{user.username}</Typography>,
          userRole: <Typography>{user.role}</Typography>,
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(e, user._id, user.username)
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen &&
                  openItemMenu.user_id === user._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateUserDrawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                {/* <MenuItem
                  onClick={() => handleOpenDeleteUserAlertSweet()}
                >
                  ລົບຂໍ້ມູນ
                </MenuItem> */}
              </Menu>
            </>
          ),
        };
      })
    );
  }, [users, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetUsersLoading}
          isLoadSuccess={isGetUsersSuccess}
          isLoadError={getUsersError.isError}
          data={userTable}
          all={users.totalUser}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default UserTable;

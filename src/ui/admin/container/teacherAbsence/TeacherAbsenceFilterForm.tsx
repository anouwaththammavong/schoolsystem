import * as React from "react";
import { Print, Search } from "@mui/icons-material";
import {
  Box,
  Grid,
  InputAdornment,
  Paper,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { TeacherState } from "../../../../domain/Teacher";

type Props = {
  control: Control<FieldValues, any>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleBlurSearch: (no: string) => void;
  teacherState: TeacherState;
};

const TeacherAbsenceFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleBlurSearch,
  teacherState,
}: Props) => {
  const componentRef = React.useRef(null);
  const { isGetTeachersLoading, isGetTeachersSuccess } = teacherState;
  return (
    <Box>
      {isGetTeachersLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
        </Paper>
      )}
      {isGetTeachersSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
            <Grid item sm={12} xs={12} md={4} lg={4} xl={4}>
              <Controller
                control={control}
                name="teacher_id"
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="ຄົ້ນຫາ"
                    size="small"
                    fullWidth
                    onBlur={(e) => handleBlurSearch(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <Search />
                        </InputAdornment>
                      ),
                    }}
                    variant="filled"
                  />
                )}
              />
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default TeacherAbsenceFilterForm;

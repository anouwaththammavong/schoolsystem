import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
}

const TeacherAbsencePageHeaderText = ({}: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນອາຈານ
        </Typography>
      </Stack>
    </Box>
  )
}

export default TeacherAbsencePageHeaderText
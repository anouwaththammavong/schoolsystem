import { Box, Stack } from "@mui/material";
import React from "react";
import { useForm } from "react-hook-form";
import { TeacherElement } from "../../../../domain/Teacher";
import { useTeacher } from "../../../hooks/useTeacher";
import TeacherFilterForm from "./TeacherAbsenceFilterForm";
import TeacherTable from "./TeacherAbsenceTable";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";
import { useSubjectV2 } from "../../../hooks/useSubjectV2";
import { useClassRoom } from "../../../hooks/useClassRoom";
import TeacherAbsencePageHeaderText from "./TeacherAbsencePageHeaderText";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  teacher_id: string;
  anchorEl: null | HTMLElement;
}

const TeacherAbsencePage = (props: Props) => {
  const navigate = useNavigate();
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateTeacherDrawer, setIsOpenCreateTeacherDrawer] =
    React.useState(false);
  const [isOpenUpdateTeacherDrawer, setIsOpenUpdateTeacherDrawer] =
    React.useState(false);
  const [teacherHold, setTeacherHold] = React.useState<
    TeacherElement | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    teacher_id: "",
    anchorEl: null,
  });

  const {
    teacherState,
    getTeachers,
    createTeacher,
    createTeacherReset,
    updateTeacher,
    updateTeacherReset,
    deleteTeacher,
    deleteTeacherReset,
  } = useTeacher();

  const { getSubjectV2s, subjectV2State } = useSubjectV2();
  const { getClassRooms, classRoomState } = useClassRoom();

  const {
    teachers,
    isCreateTeacherSuccess,
    CreateTeacherError,
    isUpdateTeacherSuccess,
    updateTeacherError,
    isDeleteTeacherSuccess,
    deleteTeacherError,
  } = teacherState;

  const handleOpenCreateTeacherDrawer = () => {
    setIsOpenCreateTeacherDrawer(true);
  };

  const handleCloseCreateTeacherDrawer = () => {
    setIsOpenCreateTeacherDrawer(false);
  };

  const handleBlurSearch = (teacher_id: string) => {
    setValue("teacher_id", teacher_id);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getTeachers(rowPerPage, 0, data.teacher_id);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    teacher_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      teacher_id: teacher_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      teacher_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateTeacherDrawer = () => {
    const teacher = teachers.teachers.teachers.filter(
      (teacher) => teacher._id === openItemMenu.teacher_id
    )[0];
    if (teacher) {
      setTeacherHold(teacher);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateTeacherDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        teacher_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateTeacherDrawer = () => {
    setIsOpenUpdateTeacherDrawer(false);
  };

  const handleOpenDetailsTeacherAbsence = () => {
    const encodedTeacherId = encodeURIComponent(openItemMenu.teacher_id);
    navigate(`/absence_teacher/teacher/${encodedTeacherId}/teacherAbsenceDetails`);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    if (data.teacher_id) {
      getTeachers(rowPerPage, newPage * rowPerPage, data.teacher_id);
    } else {
      getTeachers(rowPerPage, newPage * rowPerPage);
    }
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    if (data.teacher_id) {
      getTeachers(parseInt(event.target.value, 10), 0, data.teacher_id);
    } else {
      getTeachers(parseInt(event.target.value, 10), 0);
    }
  };

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getSubjectV2s(0,0);
      getClassRooms(0,0)
      getTeachers(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp]);

  return (
    <Box>
      <Stack spacing={3}>
        <TeacherAbsencePageHeaderText
        />
        <TeacherFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleBlurSearch={handleBlurSearch}
          handleSearch={handleSearch}
          teacherState={teacherState}
        />
        <TeacherTable
          teacherState={teacherState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenDetailsTeacherAbsence={handleOpenDetailsTeacherAbsence}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default TeacherAbsencePage;

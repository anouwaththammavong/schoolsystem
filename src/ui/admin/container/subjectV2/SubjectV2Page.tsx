import * as React from "react";
import { useForm } from "react-hook-form";
import { SubjectV2 } from "../../../../domain/SubjectV2";
import { useSubjectV2 } from "../../../hooks/useSubjectV2";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import SubjectV2PageHeaderText from "./SubjectV2PageHeaderText";
import CreateSubjectV2Form from "./CreateSubjectV2Form";
import SubjectV2Table from "./SubjectV2Table";
import UpdateSubjectV2Form from "./UpdateSubjectV2Form";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  subject_v2_id: string;
  anchorEl: null | HTMLElement;
}

const SubjectV2Page = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateSubjectV2Drawer, setIsOpenCreateSubjectV2Drawer] =
    React.useState(false);
  const [isOpenUpdateSubjectV2Drawer, setIsOpenUpdateSubjectV2Drawer] =
    React.useState(false);
  const [subjectV2Hold, setSubjectV2Hold] = React.useState<
    SubjectV2 | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    subject_v2_id: "",
    anchorEl: null,
  });

  const {
    subjectV2State,
    getSubjectV2s,
    createSubjectV2,
    createSubjectV2Reset,
    updateSubjectV2,
    updateSubjectV2Reset,
    deleteSubjectV2,
    deleteSubjectV2Reset,
  } = useSubjectV2();

  const {
    subjectV2s,
    isCreateSubjectV2Success,
    createSubjectV2Error,
    isUpdateSubjectV2Success,
    updateSubjectV2Error,
    isDeleteSubjectV2Success,
    deleteSubjectV2Error,
  } = subjectV2State;

  const handleOpenCreateSubjectV2Drawer = () => {
    setIsOpenCreateSubjectV2Drawer(true);
  };

  const handleCloseCreateSubjectV2Drawer = () => {
    setIsOpenCreateSubjectV2Drawer(false);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    subject_v2_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      subject_v2_id: subject_v2_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      subject_v2_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateSubjectV2Drawer = () => {
    const subjectV2 = subjectV2s.subjectV2s.subjectV2s.filter(
      (subject) => subject._id === openItemMenu.subject_v2_id
    )[0];
    if (subjectV2) {
      setSubjectV2Hold(subjectV2);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateSubjectV2Drawer(true);
      setOpenItemMenu({
        isOpen: false,
        subject_v2_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateSubjectV2Drawer = () => {
    setIsOpenUpdateSubjectV2Drawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    getSubjectV2s(rowPerPage, newPage * rowPerPage);
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    getSubjectV2s(parseInt(event.target.value, 10), 0);
  };

  const handleOpenDeleteSubjectV2AlertSweet = () => {
    const subject_v2 = openItemMenu.subject_v2_id;
    if (subject_v2) {
      setOpenItemMenu({
        isOpen: false,
        subject_v2_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງວິຊາຮຽນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && subject_v2) {
        deleteSubjectV2(subject_v2);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateSubjectV2Success) {
      handleCloseCreateSubjectV2Drawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມວິຊາຮຽນສຳເລັດ!",
        icon: "success",
      });
      createSubjectV2Reset();
      getSubjectV2s(defaultLimit, 0);
    } else if (createSubjectV2Error.isError) {
      handleCloseCreateSubjectV2Drawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createSubjectV2Error.message,
        icon: "error",
      });
      createSubjectV2Reset();
      getSubjectV2s(defaultLimit, 0);
    }
  }, [isCreateSubjectV2Success, createSubjectV2Error.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateSubjectV2Success) {
      handleCloseUpdateSubjectV2Drawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນຂອງວິຊາຮຽນນີ້ສຳເລັດ!",
        icon: "success",
      });
      updateSubjectV2Reset();
      getSubjectV2s(defaultLimit, 0);
    } else if (updateSubjectV2Error.isError) {
      handleCloseUpdateSubjectV2Drawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateSubjectV2Error.message,
        icon: "error",
      });
      updateSubjectV2Reset();
      getSubjectV2s(defaultLimit, 0);
    }
  }, [isUpdateSubjectV2Success, updateSubjectV2Error.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteSubjectV2Success) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງວິຊາຮຽນນີ້ສຳເລັດ!",
        icon: "success",
      });
      deleteSubjectV2Reset();
      getSubjectV2s(defaultLimit, 0);
    } else if (deleteSubjectV2Error.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteSubjectV2Reset();
      getSubjectV2s(defaultLimit, 0);
    }
  }, [isDeleteSubjectV2Success, deleteSubjectV2Error.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getSubjectV2s(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
        <SubjectV2PageHeaderText
          handleOpenCreateSubjectV2Drawer={handleOpenCreateSubjectV2Drawer}
        />
        <CreateSubjectV2Form
          subjectV2State={subjectV2State}
          createSubjectV2={createSubjectV2}
          handleCloseCreateSubjectV2Drawer={handleCloseCreateSubjectV2Drawer}
          isOpenCreateSubjectV2Drawer={isOpenCreateSubjectV2Drawer}
        />
        {subjectV2Hold && (
          <UpdateSubjectV2Form
            subjectV2State={subjectV2State}
            handleCloseUpdateSubjectV2Drawer={
              handleCloseUpdateSubjectV2Drawer
            }
            isOpenUpdateSubjectV2Drawer={isOpenUpdateSubjectV2Drawer}
            subjectV2Hold={subjectV2Hold}
            updateSubjectV2={updateSubjectV2}
          />
        )}
        <SubjectV2Table
          subjectV2State={subjectV2State}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateSubjectV2Drawer={handleOpenUpdateSubjectV2Drawer}
          handleOpenDeleteSubjectV2AlertSweet={
            handleOpenDeleteSubjectV2AlertSweet
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default SubjectV2Page;

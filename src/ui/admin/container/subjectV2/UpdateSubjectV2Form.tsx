import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import {
  SubjectV2,
  SubjectV2State,
  UpdateSubjectV2Form as UpdateSubjectV2FormData,
} from "../../../../domain/SubjectV2";

type Props = {
  subjectV2State: SubjectV2State;
  isOpenUpdateSubjectV2Drawer: boolean;
  handleCloseUpdateSubjectV2Drawer: () => void;
  updateSubjectV2: (_id: string, subjectV2: UpdateSubjectV2FormData) => void;
  subjectV2Hold: SubjectV2 | undefined;
};

const UpdateSubjectV2Form = ({
  subjectV2State,
  isOpenUpdateSubjectV2Drawer,
  handleCloseUpdateSubjectV2Drawer,
  updateSubjectV2,
  subjectV2Hold,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isUpdateSubjectV2Loading } = subjectV2State;

  const {
    handleSubmit,
    control,
    setValue,
    trigger,
    getValues,
    formState: { errors },
  } = useForm<UpdateSubjectV2FormData>();

  const handleUpdateSubjectV2 = (data: UpdateSubjectV2FormData) => {
    if (subjectV2Hold) {
      updateSubjectV2(subjectV2Hold?._id, {
        subject_id: data.subject_id,
        subject_name: data.subject_name,
      });
    }
  };

  React.useEffect(() => {
    if (subjectV2Hold) {
      setValue("subject_id", subjectV2Hold.subject_id);
      setValue("subject_name", subjectV2Hold.subject_name);
    }
  }, [subjectV2Hold]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateSubjectV2Drawer}
      onClose={() => handleCloseUpdateSubjectV2Drawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleUpdateSubjectV2)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ແກ້ໄຂຂໍ້ມູນຂອງວິຊາທີ່ຮຽນ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseUpdateSubjectV2Drawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="subject_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໄອດີວິຊາທີ່ຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectV2Loading}
                    />
                  )}
                />
                {errors &&
                  errors.subject_id &&
                  errors.subject_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນໄອດີຂອງວິຊາທີ່ຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="subject_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ຂອງວິຊາທີ່ຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectV2Loading}
                    />
                  )}
                />
                {errors &&
                  errors.subject_name &&
                  errors.subject_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຊື່ຂອງວິຊາທີ່ຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateSubjectV2Loading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateSubjectV2Form;

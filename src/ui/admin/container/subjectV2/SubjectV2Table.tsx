import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./SubjectV2Page";
import { SubjectV2State } from "../../../../domain/SubjectV2";

type Props = {
  subjectV2State: SubjectV2State;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateSubjectV2Drawer: () => void;
  handleOpenDeleteSubjectV2AlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const SubjectV2Table = ({
  subjectV2State,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateSubjectV2Drawer,
  handleOpenDeleteSubjectV2AlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [subjectV2Table, setSubjectV2Table] = React.useState<Array<any>>([]);

  const {
    subjectV2s,
    isGetSubjectV2sLoading,
    isGetSubjectV2sSuccess,
    getSubjectV2sError,
  } = subjectV2State;

  React.useEffect(() => {
    setSubjectV2Table(
      subjectV2s.subjectV2s.subjectV2s.map((subjectV2) => {
        return {
          ໄອດີຂອງວິຊາທີ່ຮຽນ: <Typography>{subjectV2.subject_id}</Typography>,
          ຊື່ຂອງວິຊາທີ່ຮຽນ: <Typography>{subjectV2.subject_name}</Typography>,
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(e, subjectV2._id, subjectV2.subject_name)
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen &&
                  openItemMenu.subject_v2_id === subjectV2._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateSubjectV2Drawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem
                  onClick={() => handleOpenDeleteSubjectV2AlertSweet()}
                >
                  ລົບຂໍ້ມູນ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [subjectV2s, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetSubjectV2sLoading}
          isLoadSuccess={isGetSubjectV2sSuccess}
          isLoadError={getSubjectV2sError.isError}
          data={subjectV2Table}
          all={subjectV2s.subjectV2s.totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default SubjectV2Table;

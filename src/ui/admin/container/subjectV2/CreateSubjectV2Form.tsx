import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import {
  SubjectV2State,
  CreateSubjectV2Form as CreateSubjectV2FormData,
} from "../../../../domain/SubjectV2";

type Props = {
  subjectV2State: SubjectV2State;
  isOpenCreateSubjectV2Drawer: boolean;
  handleCloseCreateSubjectV2Drawer: () => void;
  createSubjectV2: (subjectV2: CreateSubjectV2FormData) => void;
};

const CreateSubjectV2Form = ({
  subjectV2State,
  isOpenCreateSubjectV2Drawer,
  handleCloseCreateSubjectV2Drawer,
  createSubjectV2,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isCreateSubjectV2Loading, isCreateSubjectV2Success } =
    subjectV2State;

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm<CreateSubjectV2FormData>();

  const handleCreateSubjectV2 = (data: CreateSubjectV2FormData) => {
    createSubjectV2({ subject_id: data.subject_id, subject_name: data.subject_name });
  };

  React.useEffect(() => {
    if (isCreateSubjectV2Success) {
      reset({
        subject_id: "",
        subject_name: "",
      });
    }
  }, [isCreateSubjectV2Success]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateSubjectV2Drawer}
      onClose={() => handleCloseCreateSubjectV2Drawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleCreateSubjectV2)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມວິຊາທີ່ຮຽນ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseCreateSubjectV2Drawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="subject_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໄອດີວິຊາທີ່ຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateSubjectV2Loading}
                    />
                  )}
                />
                {errors &&
                  errors.subject_id &&
                  errors.subject_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນໄອດີຂອງວິຊາທີ່ຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="subject_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ຂອງວິຊາທີ່ຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateSubjectV2Loading}
                    />
                  )}
                />
                {errors &&
                  errors.subject_name &&
                  errors.subject_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຊື່ຂອງວິຊາທີ່ຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateSubjectV2Loading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateSubjectV2Form;

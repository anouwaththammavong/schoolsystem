import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateSubjectV2Drawer: () => void;
}

const SubjectV2PageHeaderText = ({handleOpenCreateSubjectV2Drawer}: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນວິຊາຮຽນ
        </Typography>
        <Button
          variant='contained'
          onClick={() => handleOpenCreateSubjectV2Drawer()}
        >
          <Add /> ເພີ່ມວິຊາຮຽນ
        </Button>
      </Stack>
    </Box>
  )
}

export default SubjectV2PageHeaderText
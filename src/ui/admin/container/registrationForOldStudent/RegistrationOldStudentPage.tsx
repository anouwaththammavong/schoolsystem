import * as React from "react";
import { useForm } from "react-hook-form";
import { useAcademicYear } from "../../../hooks/useAcademicYear";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import RegistrationOldStudentPageHeaderText from "./RegistrationOldStudentPageHeaderText";
import { useClassRoom } from "../../../hooks/useClassRoom";
import { Registration } from "../../../../domain/Registrations";
import { useRegistration } from "../../../hooks/useRegistration";
import RegistrationFilterForm from "./RegistrationFilterForm";
import CreateOldStudentRegistrationForm from "./CreateOldStudentRegistrationForm";
import RegistrationTable from "./RegistrationTable";
import { useSchoolFee } from "../../../hooks/useSchoolFee";
import { useClassLevel } from "../../../hooks/useClassLevel";
import UpdateRegistrationForm from "./UpdateRegistrationForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  registration_id: string;
  anchorEl: null | HTMLElement;
}

const RegistrationOldStudentPage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm<{
    student_id: string;
    academic_year_no: string;
    room_id: string;
    isPaid: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [
    isOpenCreateOldStudentRegistrationDrawer,
    setIsOpenCreateOldStudentRegistrationDrawer,
  ] = React.useState(false);
  const [isOpenUpdateRegistrationDrawer, setIsOpenUpdateRegistrationDrawer] =
    React.useState(false);
  const [registrationHold, setRegistrationHold] = React.useState<
    Registration | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    registration_id: "",
    anchorEl: null,
  });

  const {
    registrationState,
    getRegistrations,
    createOldStudentRegistration,
    createOldStudentRegistrationReset,
    updateRegistration,
    updateRegistrationReset,
    deleteRegistration,
    deleteRegistrationReset,
  } = useRegistration();

  const { getAcademicYears, academicYearState } = useAcademicYear();

  const { getClassRooms, classRoomState } = useClassRoom();

  const { getClassLevels, classLevelState } = useClassLevel();

  const { getSchoolFees, schoolFeeState } = useSchoolFee();

  const {
    registrations,
    isCreateOldStudentRegistrationSuccess,
    createOldStudentRegistrationError,
    isUpdateRegistrationSuccess,
    updateRegistrationError,
    isDeleteRegistrationSuccess,
    deleteRegistrationError,
  } = registrationState;

  const handleOpenCreateOldStudentRegistrationDrawer = () => {
    setIsOpenCreateOldStudentRegistrationDrawer(true);
  };

  const handleCloseCreateOldStudentRegistrationDrawer = () => {
    setIsOpenCreateOldStudentRegistrationDrawer(false);
  };

  const handleBlurSearch = (student_id: string) => {
    setValue("student_id", student_id);
    handleSearch();
  };

  const handleChangeAcademicYear = (academic_year_no: string) => {
    setValue("academic_year_no", academic_year_no);
    handleSearch();
  };

  const handleChangeClassRoom = (room_id: string) => {
    setValue("room_id", room_id);
    handleSearch();
  };

  const handleChangeIsPaid = (isPaid: string) => {
    setValue("isPaid", isPaid);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getRegistrations(
      rowPerPage,
      0,
      false,
      data.student_id,
      data.academic_year_no,
      data.room_id,
      data.isPaid
    );
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    registration_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      registration_id: registration_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      registration_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateRegistrationDrawer = () => {
    const registration = registrations.registrations.registrations.filter(
      (regis) => regis._id === openItemMenu.registration_id
    )[0];
    if (registration) {
      setRegistrationHold(registration);
      setIsOpenUpdateRegistrationDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        registration_id: "",
        anchorEl: null,
      });
    }
  };

  const handleOpenRegistrationOldStudentBillReport = () => {
    const registration = registrations.registrations.registrations.filter(
      (regis) => regis._id === openItemMenu.registration_id
    )[0];
    if (registration) {
      setRegistrationHold(registration);
      setOpenItemMenu({
        isOpen: false,
        registration_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateRegistrationDrawer = () => {
    setIsOpenUpdateRegistrationDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getRegistrations(
      rowPerPage,
      newPage * rowPerPage,
      false,
      data.student_id,
      data.academic_year_no,
      data.room_id,
      data.isPaid
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getRegistrations(
      parseInt(event.target.value, 10),
      0,
      false,
      data.student_id,
      data.academic_year_no,
      data.room_id,
      data.isPaid
    );
  };

  const handleOpenDeleteRegistrationAlertSweet = () => {
    const registration = openItemMenu.registration_id;
    if (registration) {
      setOpenItemMenu({
        isOpen: false,
        registration_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງການລົງທະບຽນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && registration) {
        deleteRegistration(registration);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateOldStudentRegistrationSuccess) {
      handleCloseCreateOldStudentRegistrationDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົງທະບຽນສຳເລັດ!",
        icon: "success",
      });
      createOldStudentRegistrationReset();
      getRegistrations(defaultLimit, 0, false);
    } else if (createOldStudentRegistrationError.isError) {
      handleCloseCreateOldStudentRegistrationDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createOldStudentRegistrationError.message,
        icon: "error",
      });
      createOldStudentRegistrationReset();
      getRegistrations(defaultLimit, 0, false);
    }
  }, [
    isCreateOldStudentRegistrationSuccess,
    createOldStudentRegistrationError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isUpdateRegistrationSuccess) {
      handleCloseUpdateRegistrationDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂການລົງທະບຽນສຳເລັດ!",
        icon: "success",
      });
      updateRegistrationReset();
      getRegistrations(defaultLimit, 0, false);
    } else if (updateRegistrationError.isError) {
      handleCloseUpdateRegistrationDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateRegistrationError.message,
        icon: "error",
      });
      updateRegistrationReset();
      getRegistrations(defaultLimit, 0, false);
    }
  }, [
    isUpdateRegistrationSuccess,
    updateRegistrationError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isDeleteRegistrationSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບການລົງທະບຽນສຳເລັດ!",
        icon: "success",
      });
      deleteRegistrationReset();
      getRegistrations(defaultLimit, 0, false);
    } else if (deleteRegistrationError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteRegistrationReset();
      getRegistrations(defaultLimit, 0, false);
    }
  }, [
    isDeleteRegistrationSuccess,
    deleteRegistrationError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getClassRooms(0, 0);
      getClassLevels(0, 0);
      getAcademicYears(0, 0);
      getSchoolFees(0, 0);
      getRegistrations(defaultLimit, 0, false);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);
  return (
    <Box>
      <Stack spacing={3}>
        <RegistrationOldStudentPageHeaderText
          handleOpenCreateOldStudentRegistrationDrawer={
            handleOpenCreateOldStudentRegistrationDrawer
          }
        />
        <RegistrationFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleSearch={handleSearch}
          handleBlurSearch={handleBlurSearch}
          handleChangeClassRoom={handleChangeClassRoom}
          handleChangeAcademicYear={handleChangeAcademicYear}
          handleChangeIsPaid={handleChangeIsPaid}
          registrationState={registrationState}
          classRoomState={classRoomState}
          academicYearState={academicYearState}
        />
        <CreateOldStudentRegistrationForm
          registrationState={registrationState}
          schoolFeeState={schoolFeeState}
          classRoomState={classRoomState}
          classLevelState={classLevelState}
          academicYearState={academicYearState}
          isOpenCreateOldStudentRegistrationDrawer={
            isOpenCreateOldStudentRegistrationDrawer
          }
          handleCloseCreateOldStudentRegistrationDrawer={
            handleCloseCreateOldStudentRegistrationDrawer
          }
          createOldStudentRegistration={createOldStudentRegistration}
        />

        {registrationHold && (
          <UpdateRegistrationForm
            registrationState={registrationState}
            schoolFeeState={schoolFeeState}
            classRoomState={classRoomState}
            classLevelState={classLevelState}
            academicYearState={academicYearState}
            isOpenUpdateRegistrationDrawer={isOpenUpdateRegistrationDrawer}
            handleCloseUpdateRegistrationDrawer={
              handleCloseUpdateRegistrationDrawer
            }
            updateRegistration={updateRegistration}
            registrationHold={registrationHold}
          />
        )}
        <RegistrationTable
          registrationState={registrationState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateRegistrationDrawer={
            handleOpenUpdateRegistrationDrawer
          }
          handleOpenDeleteRegistrationAlertSweet={
            handleOpenDeleteRegistrationAlertSweet
          }
          handleOpenRegistrationOldStudentBillReport={
            handleOpenRegistrationOldStudentBillReport
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
          registrationHold={registrationHold}
          isOpenUpdateRegistrationDrawer={isOpenUpdateRegistrationDrawer}
        />
      </Stack>
    </Box>
  );
};

export default RegistrationOldStudentPage;

import * as React from "react";
import {
  Box,
  Button,
  CircularProgress,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./RegistrationOldStudentPage";
import {
  Registration,
  RegistrationState,
} from "../../../../domain/Registrations";
import { formatMoney } from "../../../../application/service/Utils";
import RegistrationOldStudentBillReport from "./report/RegistrationOldStudentBillReport";
import ReactToPrint from "react-to-print";

type Props = {
  registrationState: RegistrationState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateRegistrationDrawer: () => void;
  handleOpenDeleteRegistrationAlertSweet: () => void;
  handleOpenRegistrationOldStudentBillReport: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
  registrationHold: Registration | undefined;
  isOpenUpdateRegistrationDrawer: boolean;
};

const RegistrationTable = ({
  registrationState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateRegistrationDrawer,
  handleOpenDeleteRegistrationAlertSweet,
  handleOpenRegistrationOldStudentBillReport,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
  registrationHold,
  isOpenUpdateRegistrationDrawer,
}: Props) => {
  const componentRef = React.useRef<HTMLDivElement>(null);
  const reactToPrintRef = React.useRef<ReactToPrint>(null);
  const [registrationTable, setRegistrationTable] = React.useState<Array<any>>(
    []
  );

  const {
    registrations,
    isGetRegistrationsLoading,
    isGetRegistrationsSuccess,
    getRegistrationsError,
  } = registrationState;

  React.useEffect(() => {
    if (
      registrationHold &&
      reactToPrintRef.current &&
      !isOpenUpdateRegistrationDrawer
    ) {
      setTimeout(() => {
        reactToPrintRef.current!.handlePrint();
      }, 0);
    }
  }, [registrationHold]);

  React.useEffect(() => {
    setRegistrationTable(
      registrations.registrations.registrations.map((regis) => {
        return {
          ໄອດີຂອງການລົງທະບຽນ: <Typography>{regis.registration_id}</Typography>,
          ໄອດີຂອງນັກຮຽນ: <Typography>{regis.student_id.student_id}</Typography>,
          "ຊື່ ແລະ ນາມສະກຸນ": (
            <Typography>
              {regis.student_id.name} {regis.student_id.last_name}
            </Typography>
          ),
          ລົງທະບຽນຫ້ອງຮຽນ: <Typography>{regis.room_id.room_name}</Typography>,
          ລົງທະບຽນຊັ້ນຮຽນທີ່: (
            <Typography>{regis.level_id.level_name}</Typography>
          ),
          ປີການສຶກສາທີ່: (
            <Typography>{regis.academic_year_no.academic_year}</Typography>
          ),
          ວັນທີ່ມາລົງທະບຽນຮຽນ: (
            <Typography>
              {moment(regis.registration_date).format("DD/MM/YYYY")}
            </Typography>
          ),
          ລວມເງິນທັງໝົດຂອງການລົງທະບຽນ: (
            <Typography>
              {formatMoney(regis.school_fee_id.total_fee)} ກີບ
            </Typography>
          ),
          ຈ່າຍເງິນແລ້ວບໍ: (
            <Typography>
              {regis.isPaid === "isPaid" ? "ຈ່າຍແລ້ວ" : "ບໍ່ທັນຈ່າຍ"}
            </Typography>
          ),
          "": (
            <>
              <IconButton
                onClick={(e) =>
                  handleOpenItemMenu(
                    e,
                    regis._id,
                    regis.academic_year_no.academic_year
                  )
                }
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen &&
                  openItemMenu.registration_id === regis._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateRegistrationDrawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem
                  onClick={() => handleOpenDeleteRegistrationAlertSweet()}
                >
                  ລົບຂໍ້ມູນ
                </MenuItem>
                <MenuItem
                  onClick={() => handleOpenRegistrationOldStudentBillReport()}
                >
                  <ReactToPrint
                    ref={reactToPrintRef}
                    trigger={() => (
                      <Button
                        variant="text"
                        disabled={isGetRegistrationsLoading}
                        size="large"
                        sx={{
                          padding: "0",
                          margin: "0",
                          border: 0,
                          minWidth: "0",
                          color: "#0953A0",
                        }}
                      >
                        ປິ້ນໃບບິວ
                        {isGetRegistrationsLoading && (
                          <CircularProgress
                            color="inherit"
                            size={"2rem"}
                            sx={{ marginLeft: 1 }}
                          />
                        )}
                      </Button>
                    )}
                    content={() => componentRef.current}
                    pageStyle="print"
                  />
                  <Box display="none">
                    <RegistrationOldStudentBillReport
                      registrationState={registrationState}
                      ref={componentRef}
                      registrationHold={registrationHold}
                    />
                  </Box>
                </MenuItem>
              </Menu>
            </>
          ),
        };
      })
    );
  }, [registrations, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetRegistrationsLoading}
          isLoadSuccess={isGetRegistrationsSuccess}
          isLoadError={getRegistrationsError.isError}
          data={registrationTable}
          all={registrations.registrations.totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default RegistrationTable;

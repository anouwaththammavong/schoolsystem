import * as React from "react";
import { useForm } from "react-hook-form";
import { useAcademicYear } from "../../../hooks/useAcademicYear";
import Swal from "sweetalert2";
import { Box, Stack } from "@mui/material";
import SchoolFeePageHeaderText from "./SchoolFeePageHeaderText";
import { useClassLevel } from "../../../hooks/useClassLevel";
import { SchoolFee } from "../../../../domain/SchoolFees";
import { useSchoolFee } from "../../../hooks/useSchoolFee";
import SchoolFeeFilterForm from "./SchoolFeeFilterForm";
import CreateSchoolFeeForm from "./CreateSchoolFeeForm";
import SchoolFeeTable from "./SchoolFeeTable";
import UpdateSchoolFeeForm from "./UpdateSchoolFeeForm";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  school_fee_id: string;
  anchorEl: null | HTMLElement;
}

export type SchoolFeeHold = {
  _id: string;
  school_fee_id: string;
  academic_year_no: string;
  level_id: string;
  book_fee: number;
  uniform_fee: number;
  total_fee: number;
  term_fee: number;
};

const SchoolFeePage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm<{
    level_id: string;
    academic_year_no: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateSchoolFeeDrawer, setIsOpenCreateSchoolFeeDrawer] =
    React.useState(false);
  const [isOpenUpdateSchoolFeeDrawer, setIsOpenUpdateSchoolFeeDrawer] =
    React.useState(false);
  const [schoolFeeHold, setSchoolFeeHold] = React.useState<
    SchoolFee | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    school_fee_id: "",
    anchorEl: null,
  });

  const {
    schoolFeeState,
    getSchoolFees,
    createSchoolFee,
    createSchoolFeeReset,
    updateSchoolFee,
    updateSchoolFeeReset,
    deleteSchoolFee,
    deleteSchoolFeeReset,
  } = useSchoolFee();

  const { getAcademicYears, academicYearState } = useAcademicYear();

  const { getClassLevels, classLevelState } = useClassLevel();

  const {
    schoolFees,
    isCreateSchoolFeeSuccess,
    createSchoolFeeError,
    isUpdateSchoolFeeSuccess,
    updateSchoolFeeError,
    isDeleteSchoolFeeSuccess,
    deleteSchoolFeeError,
  } = schoolFeeState;

  const handleOpenCreateSchoolFeeDrawer = () => {
    setIsOpenCreateSchoolFeeDrawer(true);
  };

  const handleCloseCreateSchoolFeeDrawer = () => {
    setIsOpenCreateSchoolFeeDrawer(false);
  };

  const handleChangeClassLevel = (level_id: string) => {
    setValue("level_id", level_id);
    handleSearch();
  };

  const handleChangeAcademicYear = (academic_year_no: string) => {
    setValue("academic_year_no", academic_year_no);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getSchoolFees(rowPerPage, 0, data.level_id, data.academic_year_no);
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    school_fee_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      school_fee_id: school_fee_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      school_fee_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateSchoolFeeDrawer = () => {
    const school_fee = schoolFees.school_fees.schoolFees.filter(
      (fee) => fee._id === openItemMenu.school_fee_id
    )[0];
    if (school_fee) {
      setSchoolFeeHold(school_fee);
      //   setSchoolFeeHold({
      //     _id: school_fee._id,
      //     school_fee_id: school_fee.school_fee_id,
      //     academic_year_no: school_fee.academic_year_no._id,
      //     level_id: school_fee.level_id._id,
      //     book_fee: school_fee.book_fee,
      //     uniform_fee: school_fee.uniform_fee,
      //     term_fee: school_fee.term_fee,
      //     total_fee: school_fee.total_fee})
      //   getPickUpItems(prod.id);
      setIsOpenUpdateSchoolFeeDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        school_fee_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateSchoolFeeDrawer = () => {
    setIsOpenUpdateSchoolFeeDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getSchoolFees(
      rowPerPage,
      newPage * rowPerPage,
      data.level_id,
      data.academic_year_no
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getSchoolFees(
      parseInt(event.target.value, 10),
      0,
      data.level_id,
      data.academic_year_no
    );
  };

  const handleOpenDeleteSchoolFeeAlertSweet = () => {
    const school_fee = openItemMenu.school_fee_id;
    if (school_fee) {
      setOpenItemMenu({
        isOpen: false,
        school_fee_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງຂໍ້ມູນຄ່າທຳນຽມນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && school_fee) {
        deleteSchoolFee(school_fee);
      }
    });
  };

  React.useEffect(() => {
    if (isCreateSchoolFeeSuccess) {
      handleCloseCreateSchoolFeeDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມຂໍ້ມູນຄ່າທຳນຽມສຳເລັດ!",
        icon: "success",
      });
      createSchoolFeeReset();
      getSchoolFees(defaultLimit, 0);
    } else if (createSchoolFeeError.isError) {
      handleCloseCreateSchoolFeeDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: createSchoolFeeError.message,
        icon: "error",
      });
      createSchoolFeeReset();
      getSchoolFees(defaultLimit, 0);
    }
  }, [isCreateSchoolFeeSuccess, createSchoolFeeError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateSchoolFeeSuccess) {
      handleCloseUpdateSchoolFeeDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນຄ່າທຳນຽມສຳເລັດ!",
        icon: "success",
      });
      updateSchoolFeeReset();
      getSchoolFees(defaultLimit, 0);
    } else if (updateSchoolFeeError.isError) {
      handleCloseUpdateSchoolFeeDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateSchoolFeeError.message,
        icon: "error",
      });
      updateSchoolFeeReset();
      getSchoolFees(defaultLimit, 0);
    }
  }, [isUpdateSchoolFeeSuccess, updateSchoolFeeError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteSchoolFeeSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຄ່າທຳນຽມສຳເລັດ!",
        icon: "success",
      });
      deleteSchoolFeeReset();
      getSchoolFees(defaultLimit, 0);
    } else if (deleteSchoolFeeError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteSchoolFeeReset();
      getSchoolFees(defaultLimit, 0);
    }
  }, [isDeleteSchoolFeeSuccess, deleteSchoolFeeError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getClassLevels(0, 0);
      getAcademicYears(0, 0);
      getSchoolFees(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
        <SchoolFeePageHeaderText
          handleOpenCreateSchoolFeeDrawer={handleOpenCreateSchoolFeeDrawer}
        />
        <SchoolFeeFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleSearch={handleSearch}
          handleChangeClassLevel={handleChangeClassLevel}
          handleChangeAcademicYear={handleChangeAcademicYear}
          schoolFeeState={schoolFeeState}
          classLevelState={classLevelState}
          academicYearState={academicYearState}
        />
        <CreateSchoolFeeForm
          schoolFeeState={schoolFeeState}
          classLevelState={classLevelState}
          academicYearState={academicYearState}
          isOpenCreateSchoolFeeDrawer={isOpenCreateSchoolFeeDrawer}
          handleCloseCreateSchoolFeeDrawer={handleCloseCreateSchoolFeeDrawer}
          createSchoolFee={createSchoolFee}
        />
        {schoolFeeHold && (
          <UpdateSchoolFeeForm
            schoolFeeState={schoolFeeState}
            classLevelState={classLevelState}
            academicYearState={academicYearState}
            isOpenUpdateSchoolFeeDrawer={isOpenUpdateSchoolFeeDrawer}
            handleCloseUpdateSchoolFeeDrawer={handleCloseUpdateSchoolFeeDrawer}
            updateSchoolFee={updateSchoolFee}
            schoolFeeHold={schoolFeeHold}
          />
        )}
        <SchoolFeeTable
          schoolFeeState={schoolFeeState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateSchoolFeeDrawer={handleOpenUpdateSchoolFeeDrawer}
          handleOpenDeleteSchoolFeeAlertSweet={
            handleOpenDeleteSchoolFeeAlertSweet
          }
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default SchoolFeePage;

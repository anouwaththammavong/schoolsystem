import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./SchoolFeePage";
import { SchoolFeeState } from "../../../../domain/SchoolFees";
import { formatMoney } from "../../../../application/service/Utils";

type Props = {
  schoolFeeState: SchoolFeeState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateSchoolFeeDrawer: () => void;
  handleOpenDeleteSchoolFeeAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const SchoolFeeTable = ({
  schoolFeeState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateSchoolFeeDrawer,
  handleOpenDeleteSchoolFeeAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
    const [schoolFeeTable, setSchoolFeeTable] = React.useState<Array<any>>([]);

    const { schoolFees, isGetSchoolFeesLoading, isGetSchoolFeesSuccess, getSchoolFeesError } = schoolFeeState;

    React.useEffect(() => {
        setSchoolFeeTable(
            schoolFees.school_fees.schoolFees.map((fee) => {
            return {
              ໄອດີຂອງຄ່າທຳນຽມ: <Typography>{fee.school_fee_id}</Typography>,
              ປີການສຶກສາທີ່: <Typography>{fee.academic_year_no.academic_year}</Typography>,
              ຊັ້ນຮຽນ: <Typography>{fee.level_id.level_name}</Typography>,
              ຄ່າປຶ້ມ: <Typography>{formatMoney(fee.book_fee)} ກີບ</Typography>,
              ຄ່າເຄື່ອງແບບ: <Typography>{formatMoney(fee.uniform_fee)} ກີບ</Typography>,
              ຄ່າຮຽນ: <Typography>{formatMoney(fee.term_fee)} ກີບ</Typography>,
              ລວມເງິນທັງໝົດ: <Typography>{formatMoney(fee.total_fee)} ກີບ</Typography>,
              "": (
                <>
                  <IconButton
                    onClick={(e) => handleOpenItemMenu(e, fee._id, fee.academic_year_no.academic_year)}
                  >
                    <MoreVert />
                  </IconButton>
                  <Menu
                    keepMounted
                    open={
                      openItemMenu.isOpen && openItemMenu.school_fee_id === fee._id
                    }
                    anchorEl={openItemMenu.anchorEl}
                    onClose={handleCloseItemMenu}
                    PaperProps={{
                      style: {
                        width: "20ch",
                      },
                    }}
                  >
                    <MenuItem onClick={() => handleOpenUpdateSchoolFeeDrawer()}>
                      ແກ້ໄຂຂໍ້ມູນ
                    </MenuItem>
                    <MenuItem onClick={() => handleOpenDeleteSchoolFeeAlertSweet()}>
                      ລົບຂໍ້ມູນ
                    </MenuItem>
                  </Menu>
                </>
              ),
            };
          })
        );
      }, [schoolFees, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetSchoolFeesLoading}
          isLoadSuccess={isGetSchoolFeesSuccess}
          isLoadError={getSchoolFeesError.isError}
          data={schoolFeeTable}
          all={schoolFees.school_fees.totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  )
};

export default SchoolFeeTable;

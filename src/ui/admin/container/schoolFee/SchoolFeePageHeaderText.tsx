import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateSchoolFeeDrawer: () => void;
}

const SchoolFeePageHeaderText = ({handleOpenCreateSchoolFeeDrawer}: Props) => {
    return (
        <Box>
          <Stack
            direction={'row'}
            spacing={2}
          >
            <Typography
              variant='h5'
            >
              ຂໍ້ມູນຄ່າທຳນຽມ
            </Typography>
            <Button
              variant='contained'
              onClick={() => handleOpenCreateSchoolFeeDrawer()}
            >
              <Add /> ເພີ່ມຂໍ້ມູນຄ່າທຳນຽມ
            </Button>
          </Stack>
        </Box>
      )
}

export default SchoolFeePageHeaderText
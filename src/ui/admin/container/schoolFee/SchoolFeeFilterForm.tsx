import * as React from "react";
import {
  Box,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { SchoolFeeState } from "../../../../domain/SchoolFees";
import { ClassLevelState } from "../../../../domain/ClassLevel";
import { AcademicYearState } from "../../../../domain/AcademicYears";

type Props = {
  control: Control<{ level_id: string; academic_year_no: string }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleChangeClassLevel: (level_id: string) => void;
  handleChangeAcademicYear: (academic_year_no: string) => void;
  schoolFeeState: SchoolFeeState;
  classLevelState: ClassLevelState;
  academicYearState: AcademicYearState;
};

const SchoolFeeFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleChangeClassLevel,
  handleChangeAcademicYear,
  schoolFeeState,
  classLevelState,
  academicYearState,
}: Props) => {
  const { isGetSchoolFeesLoading, isGetSchoolFeesSuccess } =
    schoolFeeState;
  const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
    classLevelState;
  const {
    academic_years,
    isGetAcademicYearsLoading,
    isGetAcademicYearsSuccess,
  } = academicYearState;

  return (
    <Box>
      {isGetSchoolFeesLoading ||
        isGetClassLevelsLoading || isGetAcademicYearsLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        )}
      {isGetSchoolFeesSuccess &&
        isGetClassLevelsSuccess &&
        (isGetAcademicYearsSuccess && (
          <form onSubmit={handleSubmit(handleSearch)}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="level_id"
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ຊັ້ນຮຽນທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeClassLevel(e.target.value)
                        }
                      >
                        {classLevels.classLevels.classLevels.map((val, key) => (
                          <MenuItem key={key} value={val._id}>
                            {val.level_name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="academic_year_no"
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ປີການສຶກສາທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                            handleChangeAcademicYear(e.target.value)
                        }
                      >
                        {academic_years.academicYears.academicYear.map((val, key) => (
                          <MenuItem key={key} value={val._id}>
                            {val.academic_year}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
            </Grid>
          </form>
        ))}
    </Box>
  );
};

export default SchoolFeeFilterForm;

import * as React from "react";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Skeleton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm, useWatch } from "react-hook-form";
import { SchoolFeeState, CreateSchoolFee } from "../../../../domain/SchoolFees";
import { ClassLevel, ClassLevelState } from "../../../../domain/ClassLevel";
import {
  AcademicYear,
  AcademicYearState,
} from "../../../../domain/AcademicYears";
import { NumericFormat } from "react-number-format";

type Props = {
  schoolFeeState: SchoolFeeState;
  classLevelState: ClassLevelState;
  academicYearState: AcademicYearState;
  isOpenCreateSchoolFeeDrawer: boolean;
  handleCloseCreateSchoolFeeDrawer: () => void;
  createSchoolFee: (schoolFee: CreateSchoolFee) => void;
};

const CreateSchoolFeeForm = ({
  schoolFeeState,
  classLevelState,
  academicYearState,
  isOpenCreateSchoolFeeDrawer,
  handleCloseCreateSchoolFeeDrawer,
  createSchoolFee,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isCreateSchoolFeeLoading, isCreateSchoolFeeSuccess } = schoolFeeState;

  const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
    classLevelState;

  const {
    academic_years,
    isGetAcademicYearsLoading,
    isGetAcademicYearsSuccess,
  } = academicYearState;

  const {
    handleSubmit,
    control,
    setValue,
    getValues,
    reset,
    formState: { errors },
  } = useForm<CreateSchoolFee>();

   // Watch the fields
   const bookFee = useWatch({
    control,
    name: 'book_fee',
  });
  const uniformFee = useWatch({
    control,
    name: 'uniform_fee',
  });
  const termFee = useWatch({
    control,
    name: 'term_fee',
  });

  const handleCreateSchoolFee = (data: CreateSchoolFee) => {
    createSchoolFee({
      school_fee_id: data.school_fee_id,
      academic_year_no: data.academic_year_no,
      level_id: data.level_id,
      book_fee: data.book_fee,
      uniform_fee: data.uniform_fee,
      term_fee: data.term_fee,
      total_fee: data.total_fee,
    });
  };

  React.useEffect(() => {
    const book = Number(bookFee) || 0;
    const uniform = Number(uniformFee) || 0;
    const term = Number(termFee) || 0;
    const total = book + uniform + term;

    setValue('total_fee', total, {
      shouldValidate: true,
      shouldDirty: true,
    });
  }, [bookFee, uniformFee, termFee, setValue]);

  React.useEffect(() => {
    if (isCreateSchoolFeeSuccess) {
      reset({
        school_fee_id: "",
        academic_year_no: "",
        level_id: "",
        book_fee: 0,
        uniform_fee: 0,
        term_fee: 0,
        total_fee: 0,
      });
    }
  }, [isCreateSchoolFeeSuccess]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenCreateSchoolFeeDrawer}
      onClose={() => handleCloseCreateSchoolFeeDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleCreateSchoolFee)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມຂໍ້ມູນຄ່າທຳນຽມ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseCreateSchoolFeeDrawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="school_fee_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໄອດີຄ່າທຳນຽມ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isCreateSchoolFeeLoading}
                    />
                  )}
                />
                {errors &&
                  errors.school_fee_id &&
                  errors.school_fee_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນໄອດີຂອງຄ່າທຳນຽມ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                {isGetAcademicYearsLoading && <Skeleton />}
                {isGetAcademicYearsSuccess &&
                  academic_years.academicYears.totalAcademicYears > 0 && (
                    <Controller
                      control={control}
                      name={`academic_year_no`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={academic_years.academicYears.academicYear}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => option.academic_year_no}
                          onChange={(
                            event: any,
                            newValue: AcademicYear | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {"ປີການສຶກສາທີ່ "}
                              {option.academic_year} {"ສົກຮຽນທີ່ "}
                              {option.academic_year_no}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ສົກຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.academic_year_no &&
                  errors.academic_year_no.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນສົກຮຽນ"}
                    </Typography>
                  )}
                {isGetAcademicYearsSuccess &&
                  academic_years.academicYears.totalAcademicYears <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຂໍ້ມູນສົກຮຽນກ່ອນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                {isGetClassLevelsLoading && <Skeleton />}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`level_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={classLevels.classLevels.classLevels}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => option.level_name}
                          onChange={(
                            event: any,
                            newValue: ClassLevel | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {option.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຊັ້ນຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.level_id &&
                  errors.level_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຂອງຊັ້ນຣຽນ"}
                    </Typography>
                  )}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຊັ້ນຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="book_fee"
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, value } }) => (
                    <NumericFormat
                      customInput={TextField}
                      size="small"
                      label={`ຄ່າປຶ້ມ`}
                      fullWidth
                      thousandSeparator={true}
                      allowNegative={false}
                      prefix={""}
                      variant="filled"
                      value={value}
                      onChange={({ target: { value } }) => {
                        onChange(value.replace(/\D/g, ''));
                      }}
                    />
                  )}
                />
                {errors &&
                  errors.book_fee &&
                  errors.book_fee.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄ່າປຶ້ມ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="uniform_fee"
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, value } }) => (
                    <NumericFormat
                      customInput={TextField}
                      size="small"
                      label={`ຄ່າເຄື່ອງແບບ`}
                      fullWidth
                      thousandSeparator={true}
                      allowNegative={false}
                      prefix={""}
                      variant="filled"
                      value={value}
                      onChange={({ target: { value } }) => {
                        onChange(value.replace(/\D/g, ''));
                      }}
                    />
                  )}
                />
                {errors &&
                  errors.uniform_fee &&
                  errors.uniform_fee.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄ່າເຄື່ອງແບບ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="term_fee"
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, value } }) => (
                    <NumericFormat
                      customInput={TextField}
                      size="small"
                      label={`ຄ່າຮຽນ`}
                      fullWidth
                      thousandSeparator={true}
                      allowNegative={false}
                      prefix={""}
                      variant="filled"
                      value={value}
                      onChange={({ target: { value } }) => {
                        onChange(value.replace(/\D/g, ''));
                      }}
                    />
                  )}
                />
                {errors &&
                  errors.term_fee &&
                  errors.term_fee.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄ່າຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="total_fee"
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, value } }) => (
                    <NumericFormat
                      customInput={TextField}
                      size="small"
                      label={`ລວມເປັນເງິນທັງໝົດ`}
                      fullWidth
                      thousandSeparator={true}
                      allowNegative={false}
                      prefix={""}
                      variant="filled"
                      value={value}
                      onChange={({ target: { value } }) => {
                        onChange(value.replace(/\D/g, ''));
                      }}
                    />
                  )}
                />
                {errors &&
                  errors.total_fee &&
                  errors.total_fee.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລວມເປັນເງິນທັງໝົດ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isCreateSchoolFeeLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default CreateSchoolFeeForm;

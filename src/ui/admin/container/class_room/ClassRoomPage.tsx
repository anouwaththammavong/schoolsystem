import * as React from "react";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;
import { Box, Stack } from "@mui/material";
import Swal from "sweetalert2";
import { useForm } from "react-hook-form";
import { SubjectsSubject } from "../../../../domain/Subject";
import { useSubject } from "../../../hooks/useSubject";
import ClassRoomPageHeaderText from "./ClassRoomPageHeaderText";
import { useStudent } from "../../../hooks/useStudent";
import ClassRoomFilterForm from "./ClassRoomFilterForm";
import ClassRoomTable from "./ClassRoomTable";
import UpdateClassRoomForm from "./UpdateClassRoomForm";

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  subject_id: string;
  anchorEl: null | HTMLElement;
}

const ClassRoom = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm<{
    class_level: string;
    class_room: string;
    term: string;
    generation: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenUpdateSubjectDrawer, setIsOpenUpdateSubjectDrawer] =
    React.useState(false);
  const [subjectHold, setSubjectHold] = React.useState<
    SubjectsSubject | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);
  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    subject_id: "",
    anchorEl: null,
  });

  const {
    subjectState,
    getSubjects,
    updateSubject,
    updateSubjectReset,
    deleteSubject,
    deleteSubjectReset,
  } = useSubject();

  const { getStudents, studentState } = useStudent();

  const {
    subjects,
    isUpdateSubjectSuccess,
    updateSubjectError,
    isDeleteSubjectSuccess,
    deleteSubjectError,
  } = subjectState;

  const handleChangeClass_level = (class_level: string) => {
    setValue("class_level", class_level);
    handleSearch();
  };

  const handleChangeClass_room = (class_room: string) => {
    setValue("class_room", class_room);
    handleSearch();
  };

  const handleChangeTerm = (term: string) => {
    setValue("term", term);
    handleSearch();
  };

  const handleChangeGeneration = (generation: string) => {
    setValue("generation", generation);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getSubjects(
      rowPerPage,
      0,
      data.class_level,
      data.class_room,
      data.term,
      data.generation
    );
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    subject_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      subject_id: subject_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      subject_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateSubjectDrawer = () => {
    const subject = subjects.subjects.subjects.filter(
      (subject) => subject.subjects[0]._id === openItemMenu.subject_id
    )[0];
    if (subject) {
      setSubjectHold(subject);
      setIsOpenUpdateSubjectDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        subject_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateSubjectDrawer = () => {
    setIsOpenUpdateSubjectDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getSubjects(
      rowPerPage,
      newPage * rowPerPage,
      data.class_level,
      data.class_room,
      data.term,
      data.generation
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getSubjects(
      parseInt(event.target.value, 10),
      0,
      data.class_level,
      data.class_room,
      data.term,
      data.generation
    );
  };

  const handleOpenDeleteSubjectAlertSweet = () => {
    const subject = openItemMenu.subject_id;
    if (subject) {
      setOpenItemMenu({
        isOpen: false,
        subject_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && subject) {
        deleteSubject(subject);
      }
    });
  };

  React.useEffect(() => {
    if (isUpdateSubjectSuccess) {
      const data = getValues();
      handleCloseUpdateSubjectDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນສຳເລັດ!",
        icon: "success",
      });
      updateSubjectReset();
      getSubjects(
        defaultLimit,
        0,
        data.class_level,
        data.class_room,
        data.term,
        data.generation
      );
    } else if (updateSubjectError.isError) {
      const data = getValues();
      handleCloseUpdateSubjectDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການແກ້ໄຂຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      updateSubjectReset();
      getSubjects(
        defaultLimit,
        0,
        data.class_level,
        data.class_room,
        data.term,
        data.generation
      );
    }
  }, [isUpdateSubjectSuccess, updateSubjectError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteSubjectSuccess) {
      const data = getValues();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນອາຈານສຳເລັດ!",
        icon: "success",
      });
      deleteSubjectReset();
      getSubjects(
        defaultLimit,
        0,
        data.class_level,
        data.class_room,
        data.term,
        data.generation
      );
    } else if (deleteSubjectError.isError) {
      const data = getValues();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteSubjectReset();
      getSubjects(
        defaultLimit,
        0,
        data.class_level,
        data.class_room,
        data.term,
        data.generation
      );
    }
  }, [isDeleteSubjectSuccess, deleteSubjectError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getSubjects(defaultLimit, 0, "1/1", "1", "1", "1");
      getStudents(defaultLimit, 0);
      setValue("class_level", "1/1");
      setValue("term", "1");
      setValue("generation", "1");
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);

  return (
    <Box>
      <Stack spacing={3}>
        <ClassRoomPageHeaderText />
        <ClassRoomFilterForm
          studentState={studentState}
          subjectState={subjectState}
          handleSubmit={handleSubmit}
          control={control}
          handleChangeClass_level={handleChangeClass_level}
          handleChangeClass_room={handleChangeClass_room}
          handleChangeGeneration={handleChangeGeneration}
          handleChangeTerm={handleChangeTerm}
          handleSearch={handleSearch}
          class_level={getValues("class_level")}
          generation={getValues("generation")}
          term={getValues("term")}
        />
        {subjectHold && (
          <UpdateClassRoomForm
            subjectHold={subjectHold}
            isOpenUpdateSubjectDrawer={isOpenUpdateSubjectDrawer}
            handleCloseUpdateSubjectDrawer={handleCloseUpdateSubjectDrawer}
            subjectState={subjectState}
            updateSubject={updateSubject}
          />
        )}
        <ClassRoomTable
          subjectState={subjectState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateSubjectDrawer={handleOpenUpdateSubjectDrawer}
          handleOpenDeleteSubjectAlertSweet={handleOpenDeleteSubjectAlertSweet}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default ClassRoom;

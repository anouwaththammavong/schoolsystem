import * as React from "react";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { SubjectState } from "../../../../domain/Subject";
import { StudentState } from "../../../../domain/Student";
import ReactToPrint from "react-to-print";
import { Print } from "@mui/icons-material";
import ClassRoomReport from "./report/ClassRoomReport";

type Props = {
  control: Control<{
    class_level: string;
    class_room: string;
    term: string;
    generation: string;
  }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleChangeGeneration: (generation: string) => void;
  handleChangeTerm: (term: string) => void;
  handleChangeClass_room: (class_room: string) => void;
  handleChangeClass_level: (class_level: string) => void;
  subjectState: SubjectState;
  studentState: StudentState;
  class_level: string;
  term: string;
  generation: string;
};

const ClassRoomFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleChangeClass_level,
  handleChangeClass_room,
  handleChangeGeneration,
  handleChangeTerm,
  subjectState,
  studentState,
  class_level,
  term,
  generation,
}: Props) => {
  const componentRef = React.useRef(null);
  const { isGetSubjectsLoading, isGetSubjectsSuccess } = subjectState;
  const { students, isGetStudentsLoading, isGetStudentsSuccess } = studentState;
  const class_level_class_room: { class_level: string; class_room: string }[] =
    [
      {
        class_level: "1/1",
        class_room: "1",
      },
      {
        class_level: "1/2",
        class_room: "1",
      },
      {
        class_level: "2/1",
        class_room: "2",
      },
      {
        class_level: "2/2",
        class_room: "2",
      },
      {
        class_level: "3/1",
        class_room: "3",
      },
      {
        class_level: "3/2",
        class_room: "3",
      },
      {
        class_level: "4/1",
        class_room: "4",
      },
      {
        class_level: "4/2",
        class_room: "4",
      },
      {
        class_level: "5/1",
        class_room: "5",
      },
      {
        class_level: "5/2",
        class_room: "5",
      },
      {
        class_level: "6/1",
        class_room: "6",
      },
      {
        class_level: "6/2",
        class_room: "6",
      },
      {
        class_level: "7/1",
        class_room: "7",
      },
      {
        class_level: "7/2",
        class_room: "7",
      },
    ];
  return (
    <Box>
      {isGetStudentsLoading ||
        (isGetSubjectsLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        ))}
      {isGetStudentsSuccess && isGetSubjectsSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="class_level"
                defaultValue="1/1"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ຊັ້ນຮຽນ</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) => {
                        const selectedClassLevel = e.target.value;
                        // Find the corresponding class_room for the selected class_level
                        const selectedClassRoom = class_level_class_room.find(
                          (item) => item.class_level === selectedClassLevel
                        )?.class_room;
                        handleChangeClass_room(selectedClassRoom || "");
                        handleChangeClass_level(selectedClassLevel);
                      }}
                    >
                      {class_level_class_room.map((val, key) => (
                        <MenuItem key={key} value={val.class_level}>
                          {val.class_level}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="term"
                defaultValue="1"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ເທີມຮຽນ</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeTerm(e.target.value)
                      }
                    >
                      <MenuItem value={"1"}>1</MenuItem>
                      <MenuItem value={"2"}>2</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="generation"
                defaultValue={students.students.uniqueGenerations[0].generation}
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ນັກຮຽນລຸ້ນທີ່</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeGeneration(e.target.value)
                      }
                    >
                      {students.students.uniqueGenerations.map((val, key) => (
                        <MenuItem key={key} value={val.generation}>
                          {val.generation}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={3} lg={2} xl={2}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetStudentsLoading || isGetSubjectsLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetStudentsLoading ||
                        (isGetSubjectsLoading && (
                          <CircularProgress
                            color="inherit"
                            size={"2rem"}
                            sx={{ marginLeft: 1 }}
                          />
                        ))}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນຄະແນນນັກຮຽນ"
                pageStyle="print"
              />
              <Box display={"none"}>
                <ClassRoomReport
                  subjectState={subjectState}
                  ref={componentRef}
                  class_level={class_level}
                  generation={generation}
                  term={term}
                />
              </Box>
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default ClassRoomFilterForm;

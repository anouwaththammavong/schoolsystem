import * as React from "react";
import { SubjectState } from "../../../../../domain/Subject";
import { Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";

type Props = {
  subjectState: SubjectState;
  class_level:string;
  term:string;
  generation:string
};

const ClassRoomReport = React.forwardRef<HTMLDivElement, Props>(
  ({ subjectState,class_level,generation,term }, ref) => {
    const [subjectTable, setSubjectTable] = React.useState<Array<any>>([]);

    const {
      subjects,
      isGetSubjectsLoading,
      isGetSubjectsSuccess,
      getSubjectsError,
    } = subjectState;

    React.useEffect(() => {
      setSubjectTable(
        subjects.subjects.subjects
          .map((subject, index) => {
            return subject.subjects.map((subjectDetail, innerIndex) => {
              return {
                ລຳດັບ: <Typography>{subject.student_number}</Typography>,
                ຊື່ແລະນາມສະກຸນ: (
                  <Typography>
                    {subject.name} {subject.last_name}
                  </Typography>
                ),
                ພາສາລາວ: <Typography>{subjectDetail.laos}</Typography>,
                ພາສາອັງກິດ: <Typography>{subjectDetail.english}</Typography>,
                ພູມສາດ: <Typography>{subjectDetail.geography}</Typography>,
                ລາວສຶກສາ: <Typography>{subjectDetail.social}</Typography>,
                ພາລະສຶກສາ: (
                  <Typography>{subjectDetail.physical_education}</Typography>
                ),
                ປະຫວັດສາດ: <Typography>{subjectDetail.history}</Typography>,
                ຄະນິດສາດ: <Typography>{subjectDetail.math}</Typography>,
                ເຄມີ: <Typography>{subjectDetail.chemical}</Typography>,
                ຟີຊິກ: <Typography>{subjectDetail.physics}</Typography>,
                ຊີວະ: <Typography>{subjectDetail.biology}</Typography>,
                ຄະແນນລວມ: <Typography>{subjectDetail.total_score}</Typography>,
                ຜົນການຮຽນ: (
                  <Typography>{subjectDetail.academic_results}</Typography>
                ),
                ອັນດັບທີ່: <Typography>{subjectDetail.rank}</Typography>,
              };
            });
          })
          .flat()
      );
    }, [subjects]);
    return (
      <div ref={ref} style={{ marginTop: "20px" }}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"}>
          ລາຍງານຄະແນນຂອງນັກຮຽນ ຊັ້ນຮຽນ {class_level} ພາກຮຽນທີ່ {term} ລຸ້ນທີ່ {generation}
        </Typography>
        <TableComponentForRePort
          isLoading={isGetSubjectsLoading}
          isLoadSuccess={isGetSubjectsSuccess}
          isLoadError={getSubjectsError.isError}
          data={subjectTable}
          all={subjects.subjects.totalStudents}
        />
      </div>
    );
  }
);

export default ClassRoomReport;

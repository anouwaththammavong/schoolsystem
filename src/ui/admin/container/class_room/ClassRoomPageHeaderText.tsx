import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';

type Props = {}

const ClassRoomPageHeaderText = (props: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນນັກຮຽນທີ່ຢູ່ໃນຊັ້ນຮຽນນີ້
        </Typography>
      </Stack>
    </Box>
  )
}

export default ClassRoomPageHeaderText
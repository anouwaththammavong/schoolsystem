import * as React from "react";
import { useTheme } from "@mui/system";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Paper,
  Skeleton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import EnhancedTableHead, {
  HeadCell,
} from "../../components/EnhancedTableHead";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import { Add, Close, Save } from "@mui/icons-material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import moment from "moment";
import {
  SubjectState,
  SubjectsSubject,
  UpdateSubjectForm,
} from "../../../../domain/Subject";

type Props = {
  subjectState: SubjectState;
  isOpenUpdateSubjectDrawer: boolean;
  handleCloseUpdateSubjectDrawer: () => void;
  updateSubject: (_id: string, subject: UpdateSubjectForm) => void;
  subjectHold: SubjectsSubject | undefined;
};

const UpdateClassRoomForm = ({
  subjectState,
  subjectHold,
  handleCloseUpdateSubjectDrawer,
  isOpenUpdateSubjectDrawer,
  updateSubject,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isUpdateSubjectLoading } = subjectState;

  const {
    handleSubmit,
    setValue,
    control,
    getValues,
    formState: { errors },
  } = useForm<UpdateSubjectForm>();

  const handleUpdateSubject = (data: UpdateSubjectForm) => {
    if (data && subjectHold) {
      updateSubject(subjectHold?.subjects[0]._id, {
        laos: data.laos,
        english: data.english,
        geography: data.geography,
        social: data.social,
        physical_education: data.physical_education,
        history: data.history,
        math: data.math,
        chemical: data.chemical,
        physics: data.physics,
        biology: data.biology,
        rank: data.rank,
      });
    }
  };

  React.useEffect(() => {
    if (subjectHold) {
      setValue("laos", subjectHold.subjects[0].laos);
      setValue("english", subjectHold.subjects[0].english);
      setValue("geography", subjectHold.subjects[0].geography);
      setValue("social", subjectHold.subjects[0].social);
      setValue(
        "physical_education",
        subjectHold.subjects[0].physical_education
      );
      setValue("history", subjectHold.subjects[0].history);
      setValue("math", subjectHold.subjects[0].math);
      setValue("chemical", subjectHold.subjects[0].chemical);
      setValue("physics", subjectHold.subjects[0].physics);
      setValue("biology", subjectHold.subjects[0].biology);
      setValue("rank", Number(subjectHold.subjects[0].rank) ? Number(subjectHold.subjects[0].rank) : 0);
    }
  }, [subjectHold]);
  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateSubjectDrawer}
      onClose={() => handleCloseUpdateSubjectDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleUpdateSubject)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ແກ້ໄຂຄະແນນຂອງນັກຮຽນ
                  </Typography>
                  <IconButton onClick={() => handleCloseUpdateSubjectDrawer()}>
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="laos"
                  rules={{
                    required: true,
                    max: 10,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ພາສາລາວ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors && errors.laos && errors.laos.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຄະແນນ"}
                  </Typography>
                )}
                {errors &&
                  errors.laos &&
                  errors.laos.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="english"
                  rules={{
                    required: true,
                    max: 10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ພາສາອັງກິດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.english &&
                  errors.english.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                  {errors &&
                  errors.english &&
                  errors.english.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="geography"
                  rules={{
                    required: true,
                    max: 10,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ພູມສາດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.geography &&
                  errors.geography.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                  {errors &&
                  errors.geography &&
                  errors.geography.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="social"
                  rules={{
                    required: true,
                    max: 10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລາວສຶກສາ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.social &&
                  errors.social.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                   {errors &&
                  errors.social &&
                  errors.social.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="physical_education"
                  rules={{
                    required: true,
                    max:10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ພາລະສຶກສາ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.physical_education &&
                  errors.physical_education.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                  {errors &&
                  errors.physical_education &&
                  errors.physical_education.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="history"
                  rules={{
                    required: true,
                    max: 10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ປະຫວັດສາດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.history &&
                  errors.history.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                  {errors &&
                  errors.history &&
                  errors.history.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="math"
                  rules={{
                    required: true,
                    max: 10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຄະນິດສາດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors && errors.math && errors.math.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຄະແນນ"}
                  </Typography>
                )}
                {errors &&
                  errors.math &&
                  errors.math.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="chemical"
                  rules={{
                    required: true,
                    max: 10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ເຄມີ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.chemical &&
                  errors.chemical.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                  {errors &&
                  errors.chemical &&
                  errors.chemical.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="physics"
                  rules={{
                    required: true,
                    max: 10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຟີຊິກ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.physics &&
                  errors.physics.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                  {errors &&
                  errors.physics &&
                  errors.physics.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="biology"
                  rules={{
                    required: true,
                    max: 10
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊີວະ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
                {errors &&
                  errors.biology &&
                  errors.biology.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຄະແນນ"}
                    </Typography>
                  )}
                  {errors &&
                  errors.biology &&
                  errors.biology.type === "max" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ຄະແນນບໍ່ຄວນເກີນ 10"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="rank"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ອັນດັບທີ່	"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateSubjectLoading}
                    />
                  )}
                />
              </Grid>

              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateSubjectLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateClassRoomForm;

import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./ClassRoomPage";
import { SubjectState } from "../../../../domain/Subject";

type Props = {
  subjectState: SubjectState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateSubjectDrawer: () => void;
  handleOpenDeleteSubjectAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const ClassRoomTable = ({
  subjectState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateSubjectDrawer,
  handleOpenDeleteSubjectAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [subjectTable, setSubjectTable] = React.useState<Array<any>>([]);

  const {
    subjects,
    isGetSubjectsLoading,
    isGetSubjectsSuccess,
    getSubjectsError,
  } = subjectState;

  React.useEffect(() => {
    setSubjectTable(
      subjects.subjects.subjects
        .map((subject, index) => {
          return subject.subjects.map((subjectDetail, innerIndex) => {
            return {
              ລຳດັບ: <Typography>{subject.student_number}</Typography>,
              ຊື່ແລະນາມສະກຸນ: (
                <Typography>
                  {subject.name} {subject.last_name}
                </Typography>
              ),
              ພາສາລາວ: <Typography>{subjectDetail.laos}</Typography>,
              ພາສາອັງກິດ: <Typography>{subjectDetail.english}</Typography>,
              ພູມສາດ: <Typography>{subjectDetail.geography}</Typography>,
              ລາວສຶກສາ: <Typography>{subjectDetail.social}</Typography>,
              ພາລະສຶກສາ: (
                <Typography>{subjectDetail.physical_education}</Typography>
              ),
              ປະຫວັດສາດ: <Typography>{subjectDetail.history}</Typography>,
              ຄະນິດສາດ: <Typography>{subjectDetail.math}</Typography>,
              ເຄມີ: <Typography>{subjectDetail.chemical}</Typography>,
              ຟີຊິກ: <Typography>{subjectDetail.physics}</Typography>,
              ຊີວະ: <Typography>{subjectDetail.biology}</Typography>,
              ຄະແນນລວມ: <Typography>{subjectDetail.total_score}</Typography>,
              ຜົນການຮຽນ: (
                <Typography>{subjectDetail.academic_results}</Typography>
              ),
              ອັນດັບທີ່: <Typography>{subjectDetail.rank}</Typography>,
              "": (
                <>
                  <IconButton
                    onClick={(e) =>
                      handleOpenItemMenu(e, subjectDetail._id, subject.name)
                    }
                  >
                    <MoreVert />
                  </IconButton>
                  <Menu
                    keepMounted
                    open={
                      openItemMenu.isOpen &&
                      openItemMenu.subject_id === subjectDetail._id
                    }
                    anchorEl={openItemMenu.anchorEl}
                    onClose={handleCloseItemMenu}
                    PaperProps={{
                      style: {
                        width: "20ch",
                      },
                    }}
                  >
                    <MenuItem onClick={() => handleOpenUpdateSubjectDrawer()}>
                      ແກ້ໄຂຂໍ້ມູນ
                    </MenuItem>
                    <MenuItem
                      onClick={() => handleOpenDeleteSubjectAlertSweet()}
                    >
                      ລົບຂໍ້ມູນ
                    </MenuItem>
                  </Menu>
                </>
              ),
            };
          });
        })
        .flat()
    );
  }, [subjects, openItemMenu]);

  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetSubjectsLoading}
          isLoadSuccess={isGetSubjectsSuccess}
          isLoadError={getSubjectsError.isError}
          data={subjectTable}
          all={subjects.subjects.totalStudents}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default ClassRoomTable;

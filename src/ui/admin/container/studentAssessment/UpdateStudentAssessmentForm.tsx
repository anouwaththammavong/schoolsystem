import * as React from "react";
import { useTheme } from "@mui/system";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Paper,
  Skeleton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import { Add, Close, Save } from "@mui/icons-material";
import {
  StudentAssessmentState,
  StudentAssessment,
  UpdateStudentAssessmentForm as UpdateStudentAssessmentFormType,
} from "../../../../domain/StudentAssessments";

type Props = {
  studentAssessmentState: StudentAssessmentState;
  isOpenUpdateStudentAssessmentDrawer: boolean;
  handleCloseUpdateStudentAssessmentDrawer: () => void;
  updateStudentAssessment: (
    _id: string,
    studentAssessment: UpdateStudentAssessmentFormType
  ) => void;
  studentAssessmentHold: StudentAssessment | undefined;
};

const UpdateStudentAssessmentForm = ({
  studentAssessmentState,
  isOpenUpdateStudentAssessmentDrawer,
  handleCloseUpdateStudentAssessmentDrawer,
  updateStudentAssessment,
  studentAssessmentHold,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isUpdateStudentAssessmentLoading } = studentAssessmentState;

  const {
    handleSubmit,
    setValue,
    control,
    getValues,
    formState: { errors },
  } = useForm<UpdateStudentAssessmentFormType>();

  const handleUpdateStudentAssessment = (
    data: UpdateStudentAssessmentFormType
  ) => {
    if (data && studentAssessmentHold) {
      updateStudentAssessment(studentAssessmentHold?._id, {
        assess_learning: data.assess_learning,
        attendance_class: data.attendance_class,
        behavior: data.behavior,
        note: data.note,
      });
    }
  };

  React.useEffect(() => {
    if(studentAssessmentHold) {
        setValue("assess_learning", studentAssessmentHold.assess_learning);
        setValue("attendance_class", studentAssessmentHold.attendance_class);
        setValue("behavior", studentAssessmentHold.behavior);
        setValue("note", studentAssessmentHold.note)
    }
  },[studentAssessmentHold])

  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateStudentAssessmentDrawer}
      onClose={() => handleCloseUpdateStudentAssessmentDrawer()}
    >
      <form
        style={{ width: "100%" }}
        onSubmit={handleSubmit(handleUpdateStudentAssessment)}
      >
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ປະເມີນນັກຮຽນ
                  </Typography>
                  <IconButton
                    onClick={() => handleCloseUpdateStudentAssessmentDrawer()}
                  >
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="behavior"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isUpdateStudentAssessmentLoading}
                      options={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ພຶດຕິກຳຂອງນັກຮຽນ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                      defaultValue={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ].find(
                        (o) =>
                          o.title ===
                          studentAssessmentHold?.behavior
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.behavior &&
                  errors.behavior.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປະເມີນພຶດຕິກຳຂອງນັກຮຽນຂ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="attendance_class"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isUpdateStudentAssessmentLoading}
                      options={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ການມາຮຽນຂອງນັກຮຽນ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                      defaultValue={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ].find(
                        (o) =>
                          o.title ===
                          studentAssessmentHold?.attendance_class
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.attendance_class &&
                  errors.attendance_class.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປະເມີນການມາຮຽນຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="assess_learning"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      disabled={isUpdateStudentAssessmentLoading}
                      options={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.title}`}
                      onChange={(
                        event: any,
                        newValue: {
                          title: string;
                        } | null
                      ) => {
                        field.onChange(newValue?.title);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.title}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ການຮຽນຂອງນັກຮຽນ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                      defaultValue={[
                        { title: "ອ່ອນຫຼາຍ" },
                        { title: "ອ່ອນ" },
                        { title: "ປານກາງ" },
                        { title: "ດີ" },
                        { title: "ດີຫຼາຍ" },
                      ].find(
                        (o) =>
                          o.title ===
                          studentAssessmentHold?.assess_learning
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.assess_learning &&
                  errors.assess_learning.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປະເມີນການຮຽນຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="note"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໝາຍເຫດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentAssessmentLoading}
                    />
                  )}
                />
              </Grid>

              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateStudentAssessmentLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateStudentAssessmentForm;

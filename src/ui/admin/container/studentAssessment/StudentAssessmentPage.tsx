import * as React from "react";
import { Box, Stack } from "@mui/material";
import StudentAssessmentPageHeaderText from "./StudentAssessmentPageHeaderText";
import StudentAssessmentFilterForm from "./StudentAssessmentFilterForm";
import StudentAssessmentTable from "./StudentAssessmentTable";
import UpdateStudentAssessmentForm from "./UpdateStudentAssessmentForm";
import UseHookStudentAssessmentPage from "./UseHookStudentAssessmentPage";

const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

type Props = {};

const StudentAssessmentPage = (props: Props) => {
  
  const {
    control,
    handleSubmit,
    studentAssessmentHold,
    isOpenCreateStudentAssessmentDrawer,
    isOpenUpdateStudentAssessmentDrawer,
    page,
    rowPerPage,
    setPage,
    setRowPerPage,
    openItemMenu,
    studentAssessmentState,
    studentState,
    classLevelState,
    classRoomState,
    updateStudentAssessment,
    handleChangeClass_level,
    handleChangeClass_room,
    handleChangeGeneration,
    handleSearch,
    handleOpenItemMenu,
    handleCloseItemMenu,
    handleOpenUpdateStudentAssessmentDrawer,
    handleCloseUpdateStudentAssessmentDrawer,
    handleOpenDeleteStudentAssessmentAlertSweet,
    handleChangeTablePage,
    handleChangeRowsPerTablePage,
  } = UseHookStudentAssessmentPage({defaultLimit});

  return (
    <Box>
      <Stack spacing={3}>
        <StudentAssessmentPageHeaderText />
        <StudentAssessmentFilterForm
          control={control}
          handleChangeClass_level={handleChangeClass_level}
          handleChangeClass_room={handleChangeClass_room}
          handleChangeGeneration={handleChangeGeneration}
          handleSearch={handleSearch}
          handleSubmit={handleSubmit}
          studentAssessmentState={studentAssessmentState}
          studentState={studentState}
          classLevelState={classLevelState}
          classRoomState={classRoomState}
        />
        {studentAssessmentHold && (
          <UpdateStudentAssessmentForm
            studentAssessmentHold={studentAssessmentHold}
            handleCloseUpdateStudentAssessmentDrawer={
              handleCloseUpdateStudentAssessmentDrawer
            }
            isOpenUpdateStudentAssessmentDrawer={
              isOpenUpdateStudentAssessmentDrawer
            }
            studentAssessmentState={studentAssessmentState}
            updateStudentAssessment={updateStudentAssessment}
          />
        )}
        <StudentAssessmentTable
          studentAssessmentState={studentAssessmentState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateStudentAssessmentDrawer={
            handleOpenUpdateStudentAssessmentDrawer
          }
          handleOpenDeleteStudentAssessmentAlertSweet={handleOpenDeleteStudentAssessmentAlertSweet}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default StudentAssessmentPage;

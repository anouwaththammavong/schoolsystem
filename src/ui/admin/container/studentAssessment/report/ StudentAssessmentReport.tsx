import * as React from "react";
import { Typography } from "@mui/material";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";
import { StudentAssessmentState } from "../../../../../domain/StudentAssessments";

type Props = {
  studentAssessmentState: StudentAssessmentState;
};

const StudentAssessmentReport = React.forwardRef<HTMLDivElement, Props>(
  ({ studentAssessmentState }, ref) => {
    const [studentAssessmentTable, setStudentAssessmentTable] = React.useState<
      Array<any>
    >([]);

    const {
      studentAssessments,
      isGetStudentAssessmentsLoading,
      isGetStudentAssessmentsSuccess,
      getStudentAssessmentsError,
    } = studentAssessmentState;

    React.useEffect(() => {
      setStudentAssessmentTable(
        studentAssessments.studentAssessments.studentAssessments
          .map((studentAssessment, index) => {
            return {
              ລຳດັບ: (
                <Typography>
                  {studentAssessment.student.student_number}
                </Typography>
              ),
              ຊື່ແລະນາມສະກຸນ: (
                <Typography>
                  {studentAssessment.student.name}{" "}
                  {studentAssessment.student.last_name}
                </Typography>
              ),
              ລະຫັດນັກຮຽນ: (
                <Typography>{studentAssessment.student.student_id}</Typography>
              ),
              ພຶດຕິກຳ: <Typography>{studentAssessment.behavior}</Typography>,
              ການມາຮຽນ: (
                <Typography>{studentAssessment.attendance_class}</Typography>
              ),
              ການຮຽນ: (
                <Typography>{studentAssessment.assess_learning}</Typography>
              ),
              ໝາຍເຫດ: <Typography>{studentAssessment.note}</Typography>,
            };
          })
          .flat()
      );
    }, [studentAssessments]);

    return (
      <div ref={ref} style={{ marginTop: "20px" }}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"}>
          ຂໍ້ມູນການປະເມີນນັກຮຽນ ຊັ້ນ
          {
            studentAssessments.studentAssessments.studentAssessments[0]
              ?.levelDetails.level_name
          }{" "}
          ລຸ້ນ{" "}
          {
            studentAssessments.studentAssessments.studentAssessments[0]
              ?.generation
          }
        </Typography>
        <TableComponentForRePort
          isLoading={isGetStudentAssessmentsLoading}
          isLoadSuccess={isGetStudentAssessmentsSuccess}
          isLoadError={getStudentAssessmentsError.isError}
          data={studentAssessmentTable}
          all={studentAssessments.studentAssessments.totalStudents}
        />
      </div>
    );
  }
);

export default StudentAssessmentReport;

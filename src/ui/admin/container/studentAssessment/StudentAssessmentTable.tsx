import * as React from "react";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./UseHookStudentAssessmentPage";
import { StudentAssessmentState } from "../../../../domain/StudentAssessments";

type Props = {
  studentAssessmentState: StudentAssessmentState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    assessment_id: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateStudentAssessmentDrawer: () => void;
  handleOpenDeleteStudentAssessmentAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const StudentAssessmentTable = ({
  studentAssessmentState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateStudentAssessmentDrawer,
  handleOpenDeleteStudentAssessmentAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [studentAssessmentTable, setStudentAssessmentTable] = React.useState<
    Array<any>
  >([]);

  const { studentAssessments, isGetStudentAssessmentsLoading, isGetStudentAssessmentsSuccess, getStudentAssessmentsError } = studentAssessmentState;

  React.useEffect(() => {
    setStudentAssessmentTable(
      studentAssessments.studentAssessments.studentAssessments
        .map((studentAssessment, index) => {
            return {
              ລຳດັບ: <Typography>{studentAssessment.student.student_number}</Typography>,
              ຊື່ແລະນາມສະກຸນ: (
                <Typography>
                  {studentAssessment.student.name} {studentAssessment.student.last_name}
                </Typography>
              ),
              ລະຫັດນັກຮຽນ: <Typography>{studentAssessment.student.student_id}</Typography>,
              ພຶດຕິກຳ: <Typography>{studentAssessment.behavior}</Typography>,
              ການມາຮຽນ: <Typography>{studentAssessment.attendance_class}</Typography>,
              ການຮຽນ: <Typography>{studentAssessment.assess_learning}</Typography>,
              ໝາຍເຫດ: <Typography>{studentAssessment.note}</Typography>,
              "": (
                <>
                  <IconButton
                    onClick={(e) =>
                      handleOpenItemMenu(e, studentAssessment._id, studentAssessment.student.name)
                    }
                  >
                    <MoreVert />
                  </IconButton>
                  <Menu
                    keepMounted
                    open={
                      openItemMenu.isOpen &&
                      openItemMenu.assessment_id === studentAssessment._id
                    }
                    anchorEl={openItemMenu.anchorEl}
                    onClose={handleCloseItemMenu}
                    PaperProps={{
                      style: {
                        width: "20ch",
                      },
                    }}
                  >
                    <MenuItem onClick={() => handleOpenUpdateStudentAssessmentDrawer()}>
                      ແກ້ໄຂຂໍ້ມູນ
                    </MenuItem>
                    <MenuItem
                    onClick={() =>
                      handleOpenDeleteStudentAssessmentAlertSweet()
                    }
                  >
                    ລົບຂໍ້ມູນ
                  </MenuItem>
                  </Menu>
                </>
              ),
            };
        })
        .flat()
    );
  }, [studentAssessments, openItemMenu]);

  return (
    <Box>
    <>
      <TableComponent
        isLoading={isGetStudentAssessmentsLoading}
        isLoadSuccess={isGetStudentAssessmentsSuccess}
        isLoadError={getStudentAssessmentsError.isError}
        data={studentAssessmentTable}
        all={studentAssessments.studentAssessments.totalStudents}
        rowsPerPage={rowsPerPage}
        page={page}
        handleChangeTablePage={handleChangeTablePage}
        handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
      />
    </>
  </Box>
  );
};

export default StudentAssessmentTable;

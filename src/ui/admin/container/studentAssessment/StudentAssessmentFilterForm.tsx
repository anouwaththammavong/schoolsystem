import * as React from "react";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { StudentState } from "../../../../domain/Student";
import { StudentAssessmentState } from "../../../../domain/StudentAssessments";
import ReactToPrint from "react-to-print";
import { Print } from "@mui/icons-material";
import StudentAssessmentReport from "./report/ StudentAssessmentReport";
import { ClassLevelState } from "../../../../domain/ClassLevel";
import { ClassRoomState } from "../../../../domain/ClassRoom";

type Props = {
  control: Control<{
    level_id: string;
    room_id: string;
    generation: string;
  }>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleChangeGeneration: (generation: string) => void;
  handleChangeClass_level: (level_id: string) => void;
  handleChangeClass_room: (room_id: string) => void;
  studentState: StudentState;
  studentAssessmentState: StudentAssessmentState;
  classLevelState: ClassLevelState;
  classRoomState: ClassRoomState;
};

const StudentAssessmentFilterForm = ({
  control,
  handleChangeClass_level,
  handleChangeClass_room,
  handleChangeGeneration,
  handleSearch,
  handleSubmit,
  studentAssessmentState,
  studentState,
  classLevelState,
  classRoomState,
}: Props) => {
  const componentRef = React.useRef(null);
  const { isGetStudentAssessmentsLoading, isGetStudentAssessmentsSuccess } =
    studentAssessmentState;
  const { students, isGetStudentsLoading, isGetStudentsSuccess } = studentState;

  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;

  const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
    classLevelState;

  return (
    <Box>
      {isGetStudentsLoading ||
        isGetClassRoomsLoading ||
        isGetClassLevelsLoading ||
        (isGetStudentAssessmentsLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        ))}
      {isGetStudentsSuccess &&
        isGetStudentAssessmentsSuccess &&
        isGetClassRoomsSuccess &&
        isGetClassLevelsSuccess && (
          <form onSubmit={handleSubmit(handleSearch)}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="room_id"
                  defaultValue={
                    classRooms.classrooms.classRooms[0]._id
                  }
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ຫ້ອງຮຽນທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeClass_room(e.target.value)
                        }
                      >
                        {classRooms.classrooms.classRooms.map((val, key) => (
                          <MenuItem key={key} value={val._id}>
                            {val.room_name} {"ຊັ້ນຮຽນ"}
                            {val.level_id.level_name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="level_id"
                  defaultValue={
                    classLevels.classLevels.classLevels[0]._id
                  }
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ຊັ້ນຮຽນ</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeClass_level(e.target.value)
                        }
                      >
                        {classLevels.classLevels.classLevels.map((val, key) => (
                          <MenuItem key={key} value={val._id}>
                            {"ຊັ້ນຮຽນ"} {val.level_name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
                <Controller
                  control={control}
                  name="generation"
                  defaultValue={
                    students.students.uniqueGenerations[0].generation
                  }
                  render={({ field }) => (
                    <FormControl
                      fullWidth
                      variant="filled"
                      sx={{ minWidth: 120 }}
                    >
                      <InputLabel>ເລືອກ ນັກຮຽນລຸ້ນທີ່</InputLabel>
                      <Select
                        {...field}
                        size="small"
                        onChange={(e: SelectChangeEvent) =>
                          handleChangeGeneration(e.target.value)
                        }
                      >
                        {students.students.uniqueGenerations.map((val, key) => (
                          <MenuItem key={key} value={val.generation}>
                            {val.generation}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={
                        isGetStudentsLoading || isGetStudentAssessmentsLoading
                      }
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetStudentsLoading ||
                        (isGetStudentAssessmentsLoading && (
                          <CircularProgress
                            color="inherit"
                            size={"2rem"}
                            sx={{ marginLeft: 1 }}
                          />
                        ))}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນນັກຮຽນ"
                pageStyle="print"
              />
              <Box display={"none"}>
                <StudentAssessmentReport
                  studentAssessmentState={studentAssessmentState}
                  ref={componentRef}
                />
              </Box>
            </Grid>
            </Grid>
          </form>
        )}
    </Box>
  );
};

export default StudentAssessmentFilterForm;

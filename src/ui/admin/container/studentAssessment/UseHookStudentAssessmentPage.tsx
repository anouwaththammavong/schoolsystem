import * as React from "react";
import { useStudentAssessment } from "../../../hooks/useStudentAssessment";
import { useStudent } from "../../../hooks/useStudent";
import { useClassRoom } from "../../../hooks/useClassRoom";
import { useClassLevel } from "../../../hooks/useClassLevel";
import Swal from "sweetalert2";
import { StudentAssessment } from "../../../../domain/StudentAssessments";
import { useForm } from "react-hook-form";

export interface OpenItemMenu {
  isOpen: boolean;
  assessment_id: string;
  anchorEl: null | HTMLElement;
}

type Props = {
  defaultLimit: number;
};

const UseHookStudentAssessmentPage = ({ defaultLimit }: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm<{
    level_id: string;
    room_id: string;
    generation: string;
  }>();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [
    isOpenCreateStudentAssessmentDrawer,
    setIsOpenCreateStudentAssessmentDrawer,
  ] = React.useState(false);
  const [
    isOpenUpdateStudentAssessmentDrawer,
    setIsOpenUpdateStudentAssessmentDrawer,
  ] = React.useState(false);
  const [studentAssessmentHold, setStudentAssessmentHold] = React.useState<
    StudentAssessment | undefined
  >();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    assessment_id: "",
    anchorEl: null,
  });

  const {
    studentAssessmentState,
    getStudentAssessment,
    createStudentAssessment,
    createStudentAssessmentReset,
    updateStudentAssessment,
    updateStudentAssessmentReset,
    deleteStudentAssessment,
    deleteStudentAssessmentReset
  } = useStudentAssessment();

  const { getStudents, studentState } = useStudent();

  const { getClassRooms, classRoomState } = useClassRoom();

  const { getClassLevels, classLevelState } = useClassLevel();

  const {
    studentAssessments,
    isCreateStudentAssessmentSuccess,
    createStudentAssessmentError,
    isUpdateStudentAssessmentSuccess,
    updateStudentAssessmentError,
    isDeleteStudentAssessmentSuccess,
    deleteStudentAssessmentError
  } = studentAssessmentState;

  const { students, isGetStudentsSuccess } = studentState;

  const { classRooms, isGetClassRoomsSuccess } = classRoomState;

  const { classLevels, isGetClassLevelsSuccess } = classLevelState;

  const handleChangeClass_level = (level_id: string) => {
    setValue("level_id", level_id);
    handleSearch();
  };

  const handleChangeClass_room = (room_id: string) => {
    setValue("room_id", room_id);
    handleSearch();
  };

  const handleChangeGeneration = (generation: string) => {
    setValue("generation", generation);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getStudentAssessment(
      rowPerPage,
      0,
      data.level_id,
      data.room_id,
      data.generation
    );
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    assessment_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      assessment_id: assessment_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      assessment_id: "",
      anchorEl: null,
    });
  };

  const handleOpenCreateStudentAssessmentDrawer = () => {
    setIsOpenCreateStudentAssessmentDrawer(true);
  };

  const handleCloseCreateStudentAssessmentDrawer = () => {
    setIsOpenCreateStudentAssessmentDrawer(false);
  };

  const handleOpenUpdateStudentAssessmentDrawer = () => {
    const assessment =
      studentAssessments.studentAssessments.studentAssessments.filter(
        (assessment) =>
          assessment._id === openItemMenu.assessment_id
      );
    if (assessment) {
      console.log(assessment)
      setStudentAssessmentHold(assessment[0]);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateStudentAssessmentDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        assessment_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateStudentAssessmentDrawer = () => {
    setIsOpenUpdateStudentAssessmentDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getStudentAssessment(
      rowPerPage,
      newPage * rowPerPage,
      data.level_id,
      data.room_id,
      data.generation
    );
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getStudentAssessment(
      parseInt(event.target.value, 10),
      0,
      data.level_id,
      data.room_id,
      data.generation
    );
  };

  const handleOpenAlertSweetWarningIfThisStudentHasNotBeenAssessmentYet =
    () => {
      Swal.fire({
        title: "ຂໍ້ມູນນັກຮຽນຜູ້ນີ້ຍັງບໍ່ທັນໄດ້ມີການປະເມີນເທື່ອ",
        text: "ທ່ານຕ້ອງປະເມີນນັກຮຽນຜູ້ນີ້ກ່ອນຈຶ່ງຈະສາມາດແກ້ໄຂຂໍ້ມູນໄດ້",
        icon: "warning",
      });
    };

    const handleOpenDeleteStudentAssessmentAlertSweet = () => {
      const absence = openItemMenu.assessment_id;
      if (absence) {
        setOpenItemMenu({
          isOpen: false,
          assessment_id: "",
          anchorEl: null,
        });
      }
      Swal.fire({
        title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງການປະເມີນນັກຮຽນຜູ້ນີ້ບໍ?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "ຕ້ອງການ",
        cancelButtonText: `ບໍ່ຕ້ອງການ`,
        confirmButtonColor: "#22bb33",
        cancelButtonColor: "#d33",
      }).then((result) => {
        if (result.isConfirmed && absence) {
          deleteStudentAssessment(absence);
        }
      });
    };

    React.useEffect(() => {
      setCleanUp(true);
      if (cleanUp) {
        getClassRooms(0, 0);
        getClassLevels(0,0);
        getStudents(0, 0);
      }
      return () => {
        setCleanUp(false);
      };
    }, [cleanUp, defaultLimit]);
  
    React.useEffect(() => {
      if (isGetClassLevelsSuccess && isGetClassRoomsSuccess && isGetStudentsSuccess) {
        getStudentAssessment(
          defaultLimit,
          0,
          classLevels.classLevels.classLevels[0]?._id,
          classRooms.classrooms.classRooms[0]?._id,
          students.students.uniqueGenerations[0]?.generation
        );
      }
    }, [isGetClassLevelsSuccess, isGetClassRoomsSuccess, isGetStudentsSuccess]);

  React.useEffect(() => {
    if (isCreateStudentAssessmentSuccess) {
      const data = getValues();
      handleCloseCreateStudentAssessmentDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ປະເມີນນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      createStudentAssessmentReset();
      getStudentAssessment(
        defaultLimit,
        0,
        data.level_id,
        data.room_id,
        data.generation
      );
    } else if (createStudentAssessmentError.isError) {
      const data = getValues();
      handleCloseCreateStudentAssessmentDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການປະເມີນນັກຮຽນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      createStudentAssessmentReset();
      getStudentAssessment(
        defaultLimit,
        0,
        data.level_id,
        data.room_id,
        data.generation
      );
    }
  }, [
    isCreateStudentAssessmentSuccess,
    createStudentAssessmentError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isUpdateStudentAssessmentSuccess) {
      const data = getValues();
      handleCloseUpdateStudentAssessmentDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນການປະເມີນນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      updateStudentAssessmentReset();
      getStudentAssessment(
        defaultLimit,
        0,
        data.level_id,
        data.room_id,
        data.generation
      );
    } else if (updateStudentAssessmentError.isError) {
      const data = getValues();
      handleCloseUpdateStudentAssessmentDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການແກ້ໄຂຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      updateStudentAssessmentReset();
      getStudentAssessment(
        defaultLimit,
        0,
        data.level_id,
        data.room_id,
        data.generation
      );
    }
  }, [
    isUpdateStudentAssessmentSuccess,
    updateStudentAssessmentError.isError,
    defaultLimit,
  ]);

  React.useEffect(() => {
    if (isDeleteStudentAssessmentSuccess) {
      const data = getValues();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນຂອງການປະເມີນອາຈານສຳເລັດ!",
        icon: "success",
      });
      deleteStudentAssessmentReset();
      getStudentAssessment(
        defaultLimit,
        0,
        data.level_id,
        data.room_id,
        data.generation
      );
    } else if (deleteStudentAssessmentError.isError) {
      const data = getValues();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteStudentAssessmentReset();
      getStudentAssessment(
        defaultLimit,
        0,
        data.level_id,
        data.room_id,
        data.generation
      );
    }
  }, [
    isDeleteStudentAssessmentSuccess,
    deleteStudentAssessmentError.isError,
    defaultLimit,
  ]);

  return {
    control,
    handleSubmit,
    studentAssessmentHold,
    isOpenCreateStudentAssessmentDrawer,
    isOpenUpdateStudentAssessmentDrawer,
    page,
    rowPerPage,
    setPage,
    setRowPerPage,
    openItemMenu,
    studentAssessmentState,
    studentState,
    classLevelState,
    classRoomState,
    updateStudentAssessment,
    handleChangeClass_level,
    handleChangeClass_room,
    handleChangeGeneration,
    handleSearch,
    handleOpenItemMenu,
    handleCloseItemMenu,
    handleOpenUpdateStudentAssessmentDrawer,
    handleCloseUpdateStudentAssessmentDrawer,
    handleOpenDeleteStudentAssessmentAlertSweet,
    handleChangeTablePage,
    handleChangeRowsPerTablePage,
  };
};

export default UseHookStudentAssessmentPage;

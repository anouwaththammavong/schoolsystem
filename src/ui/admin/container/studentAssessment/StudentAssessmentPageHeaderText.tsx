import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';

type Props = {}

const StudentAssessmentPageHeaderText = (props: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນການປະເມີນນັກຮຽນ
        </Typography>
      </Stack>
    </Box>
  )
}

export default StudentAssessmentPageHeaderText
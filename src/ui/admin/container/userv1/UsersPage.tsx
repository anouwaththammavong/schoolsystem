import * as React from 'react';
import { useUser } from '../../../hooks/useUser';
import UserTable from './UserTable';
import { Box } from '@mui/material';
import { Stack } from '@mui/system';
import CreateUserForm from './CreateUserForm';
import UsersPageHeaderText from './UsersPageHeaderText';
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;
import { useSnackbar } from "notistack";

export interface OpenItemMenu {
  isOpen: boolean;
  itemId: string;
  anchorEl: null | HTMLElement;
}

const UsersPage = () => {
  const { enqueueSnackbar } = useSnackbar();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateUserDrawer, setIsOpenCreateUserDrawer] = React.useState(false);
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    itemId: '',
    anchorEl: null
  })

  const {
    userState,
    getUsers,
    createUser,
    createUserReset,
    updateUserStatus,
    updateUserStatusReset
  } = useUser();

  const { isCreateUserSuccess, createUserError, isUpdateUserStatusSuccess, updateUserStatusError } = userState;

  const handleOpenCreateUserDrawer = () => {
    setIsOpenCreateUserDrawer(true);
  }

  const handleCloseCreateUserDrawer = () => {
    setIsOpenCreateUserDrawer(false);
  }

  const handleChangeEnableStatus = (itemId: string) => {
    updateUserStatus(itemId);
  };

  const handleOpenItemMenu = (event: React.MouseEvent<HTMLButtonElement>, itemId: string, itemName: string) => {
    setOpenItemMenu({
      isOpen: true,
      itemId: itemId,
      anchorEl: event.currentTarget
    })
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      itemId: '',
      anchorEl: null
    })
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    // getUsers(rowPerPage, (newPage * rowPerPage))
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    // getUsers(parseInt(event.target.value, 10), 0)
  };

  React.useEffect(() => {
    if (isCreateUserSuccess) {
      handleCloseCreateUserDrawer()
      enqueueSnackbar("ເພີ່ມຂໍ້ມູນຜູ້ໃຊ້ສຳເລັດ", {
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      });
      createUserReset();
      getUsers();
    }else if (createUserError.isError) {
      enqueueSnackbar("ເກີດຂໍ້ຜິດພາດໃນການເພີ່ມຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ", {
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      });
      getUsers();
      createUserReset();
      
    }
  }, [isCreateUserSuccess,createUserError.isError])

  React.useEffect(() => {
    if (isUpdateUserStatusSuccess) {
      enqueueSnackbar("ແກ້ໄຂສະຖານະຜູ້ໃຊ້ສຳເລັດ", {
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      });
      updateUserStatusReset();
      getUsers();
    }else if (updateUserStatusError.isError) {
      enqueueSnackbar("ເກີດຂໍ້ຜິດພາດໃນການແກ້ໄຂຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ", {
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      });
      updateUserStatusReset();
      getUsers();
    }
  }, [isUpdateUserStatusSuccess,updateUserStatusError.isError])

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getUsers();
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp]);

  return (
    <Box>
      <Stack
        spacing={3}
      >
        <UsersPageHeaderText
          handleOpenCreateUserDrawer={handleOpenCreateUserDrawer}
        />
        <CreateUserForm
          createUser={createUser}
          handleCloseCreateUserDrawer={handleCloseCreateUserDrawer}
          isOpenCreateUserDrawer={isOpenCreateUserDrawer}
          userState={userState}
        />
        <UserTable
          userState={userState}
          openItemMenu={openItemMenu}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeEnableStatus={handleChangeEnableStatus}
          handleOpenItemMenu={handleOpenItemMenu}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default UsersPage;

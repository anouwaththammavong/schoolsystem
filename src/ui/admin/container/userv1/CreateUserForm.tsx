import * as React from 'react';
import { ICreateUser, UserState } from '../../../../domain/user';
import { Box, Button, Drawer, Grid, IconButton, TextField, Typography, useMediaQuery, useTheme } from '@mui/material';
import { Close } from '@mui/icons-material';
import { Stack } from '@mui/system';
import { Controller, useForm } from 'react-hook-form';

interface Props {
    userState: UserState;
    isOpenCreateUserDrawer: boolean;
    handleCloseCreateUserDrawer: () => void;
    createUser: (user: ICreateUser) => void;
}

const CreateUserForm: React.FC<Props> = ({ userState, isOpenCreateUserDrawer, handleCloseCreateUserDrawer, createUser }) => {
    const theme = useTheme();
    const breakpointSm = useMediaQuery(theme.breakpoints.down('sm'));
    const breakpointMd = useMediaQuery(theme.breakpoints.down('md'));
    const breakpointLg = useMediaQuery(theme.breakpoints.down('lg'));

    const { isCreateUserLoading } = userState;

    const {
        handleSubmit,
        control,
        formState: { errors }
    } = useForm();

    const handleCreateUser = (data: any) => {
        createUser({ ...data });
    }

    return (
        <Drawer
            anchor={'right'}
            open={isOpenCreateUserDrawer}
            onClose={() => handleCloseCreateUserDrawer()}
        >
            <form
                style={{ width: '100%' }}
                onSubmit={handleSubmit(handleCreateUser)}
            >
                <Box
                    width={breakpointSm ? '100%' : (breakpointMd ? 600 : (breakpointLg ? 900 : 1200))}
                >
                    <Stack
                        spacing={3}
                        padding={3}
                    >
                        <Grid
                            container
                            spacing={3}
                            alignItems='center'
                            justifyContent='center'
                        >
                            <Grid
                                item
                                xs={12}
                            >
                                <Stack
                                    direction={'row'}
                                    justifyContent='space-between'
                                >
                                    <Typography
                                        variant={'h5'}
                                        component='div'
                                    >
                                        ເພີ່ມລາຍການ User
                                    </Typography>
                                    <IconButton
                                        onClick={() => handleCloseCreateUserDrawer()}
                                    >
                                        <Close />
                                    </IconButton>
                                </Stack>
                            </Grid>
                            <Grid
                                item
                                xs={12}
                            >
                                <Controller
                                    control={control}
                                    name='username'
                                    rules={{
                                        required: true,
                                        maxLength: 40
                                    }}
                                    render={({ field }) =>
                                        <TextField
                                            {...field}
                                            size='small'
                                            type='text'
                                            label={'Username'}
                                            fullWidth
                                            variant='filled'
                                            disabled={isCreateUserLoading}
                                        />
                                    }
                                />
                                {
                                    errors.username && errors.username.type === 'required' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ
                                    </Typography>
                                }

                                {
                                    errors.username && errors.username.type === 'maxLength' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ບໍ່ສາມາດປ້ອນຂໍ້ມູນເກີນກວ່າ 40 ໂຕອັກສອນ
                                    </Typography>
                                }
                            </Grid>
                            <Grid
                                item
                                xs={12}
                            >
                                <Controller
                                    control={control}
                                    name='password'
                                    rules={{
                                        required: true,
                                        maxLength: 40,
                                        minLength: 4,
                                    }}
                                    render={({ field }) =>
                                        <TextField
                                            {...field}
                                            size='small'
                                            type='text'
                                            label={'Password'}
                                            fullWidth
                                            variant='filled'
                                            disabled={isCreateUserLoading}
                                        />
                                    }
                                />
                                {
                                    errors.username && errors.username.type === 'required' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ
                                    </Typography>
                                }

                                {
                                    errors.username && errors.username.type === 'maxLength' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ບໍ່ສາມາດປ້ອນຂໍ້ມູນເກີນກວ່າ 40 ໂຕອັກສອນ
                                    </Typography>
                                }

                                {
                                    errors.username && errors.username.type === 'minLength' &&
                                    <Typography
                                        variant='subtitle2'
                                        color='error'
                                        marginTop={1}
                                    >
                                        ບໍ່ສາມາດປ້ອນຂໍ້ມູນໜ້ອຍກວ່າ 4 ໂຕອັກສອນ
                                    </Typography>
                                }
                            </Grid>
                            <Grid
                                item
                                xs={12}
                            >
                                <Button
                                    type='submit'
                                    variant='contained'
                                    sx={{ paddingInline: 5 }}
                                    disabled={isCreateUserLoading}
                                >
                                    ບັນທຶກ
                                </Button>
                            </Grid>
                        </Grid>
                    </Stack>
                </Box>
            </form>
        </Drawer>
    );
};

export default CreateUserForm;

import * as React from 'react';
import { UserState } from '../../../../domain/user';
import { Box, IconButton, Switch, Typography } from '@mui/material';
import moment from 'moment';
import { MoreVert } from '@mui/icons-material';
import { OpenItemMenu } from './UsersPage';
import TableComponent from '../../components/ui-tool/TableComponent';

interface Props {
    userState: UserState;
    openItemMenu: OpenItemMenu;
    rowsPerPage: number,
    page: number,
    handleChangeEnableStatus: (itemId: string) => void;
    handleOpenItemMenu: (
        event: React.MouseEvent<HTMLButtonElement>,
        ticketId: string,
        ticketName: string
    ) => void;
    handleChangeTablePage: (event: unknown, newPage: number) => void;
    handleChangeRowsPerTablePage: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const UserTable: React.FC<Props> = ({
    userState,
    openItemMenu,
    rowsPerPage,
    page,
    handleChangeEnableStatus,
    handleOpenItemMenu,
    handleChangeTablePage,
    handleChangeRowsPerTablePage
}) => {

    const [userTable, setUserTable] = React.useState<Array<any>>([])

    const {
        users,
        isGetUsersLoading,
        isGetUsersSuccess,
        getUsersError,
        isUpdateUserStatusLoading,
    } = userState;

    React.useEffect(() => {
        setUserTable(
            users.data.map((user) => {
                return (
                    {
                        'ລາຍການ Users': <>
                            <Typography
                                variant={'body1'}
                                fontWeight={'bold'}
                            >
                                {user.email}
                            </Typography>
                        </>,
                        // 'ວັນທີ່ສ້າງ': moment(user.created_at).format('DD/MM/YYYY'),
                        'ສະຖານະ': <Switch
                            checked={user.status}
                            onChange={() => handleChangeEnableStatus(user.id)}
                            disabled={isUpdateUserStatusLoading}
                        />,
                        '': <>
                            <IconButton
                                onClick={(e) => handleOpenItemMenu(e, user.id, user.email)}
                            >
                                <MoreVert />
                            </IconButton>
                        </>
                    }
                )
            })
        )
    }, [users, openItemMenu, isUpdateUserStatusLoading])

    return (
        <Box>
            <TableComponent
                isLoading={isGetUsersLoading}
                isLoadSuccess={isGetUsersSuccess}
                isLoadError={getUsersError.isError}
                data={userTable}
                all={users.all}
                rowsPerPage={rowsPerPage}
                page={page}
                handleChangeTablePage={handleChangeTablePage}
                handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
            />
        </Box>
    );
};

export default UserTable;

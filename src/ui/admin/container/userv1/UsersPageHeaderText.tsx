import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

interface Props {
  handleOpenCreateUserDrawer: () => void;
}

const UsersPageHeaderText: React.FC<Props> = ({ handleOpenCreateUserDrawer }) => {

  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ລາຍການ Users
        </Typography>
        <Button
          variant='contained'
          onClick={() => handleOpenCreateUserDrawer()}
        >
          <Add /> ເພີ່ມລາຍການ
        </Button>
      </Stack>
    </Box>
  );
};

export default UsersPageHeaderText;

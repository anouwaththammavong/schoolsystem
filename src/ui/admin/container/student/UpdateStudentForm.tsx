import * as React from "react";

import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  IconButton,
  Skeleton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Close, Save } from "@mui/icons-material";
import { Stack, useTheme } from "@mui/system";
import { Controller, useForm } from "react-hook-form";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import moment from "moment";
import {
  CreateStudent,
  Student,
  StudentState,
} from "../../../../domain/Student";
import { ClassRoom, ClassRoomState } from "../../../../domain/ClassRoom";
import { ClassLevel, ClassLevelState } from "../../../../domain/ClassLevel";

type Props = {
  studentState: StudentState;
  classRoomState: ClassRoomState;
  classLevelState: ClassLevelState;
  isOpenUpdateStudentDrawer: boolean;
  handleCloseUpdateStudentDrawer: () => void;
  updateStudent: (_id: string, student: CreateStudent) => void;
  studentHold: Student | undefined;
};

const UpdateStudentForm = ({
  studentState,
  classLevelState,
  classRoomState,
  isOpenUpdateStudentDrawer,
  handleCloseUpdateStudentDrawer,
  updateStudent,
  studentHold,
}: Props) => {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.down("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.down("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.down("lg"));

  const { isUpdateStudentLoading, isUpdateStudentSuccess } = studentState;

  const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
    classLevelState;

  const { classRooms, isGetClassRoomsLoading, isGetClassRoomsSuccess } =
    classRoomState;

  const {
    handleSubmit,
    control,
    setValue,
    trigger,
    getValues,
    formState: { errors },
  } = useForm<CreateStudent>();

  const handleChangeDate_of_birth = (date_of_birth: any) => {
    if (date_of_birth) {
      setValue("date_of_birth", date_of_birth);
    }
  };

  const handleChangeSchool_entry_date = (school_entry_date: any) => {
    if (school_entry_date) {
      setValue("school_entry_date", school_entry_date);
    }
  };

  const handleUpdate = (data: CreateStudent) => {
    if (studentHold) {
      updateStudent(studentHold?._id, {
        student_id: data.student_id,
        name: data.name,
        last_name: data.last_name,
        age: data.age,
        student_tel: data.student_tel,
        date_of_birth:
          data.date_of_birth === studentHold.date_of_birth
            ? data.date_of_birth
            : new Date(data.date_of_birth),
        student_village: data.student_village,
        student_district: data.student_district,
        student_province: data.student_province,
        nationality: data.nationality,
        father_name: data.father_name,
        father_tel: data.father_tel,
        father_job: data.father_job,
        mother_name: data.mother_name,
        mother_tel: data.mother_tel,
        mother_job: data.mother_job,
        student_past_school: data.student_past_school,
        student_number: data.student_number,
        room_id: data.room_id,
        level_id: data.level_id,
        status: data.status,
        note: data.note,
        school_entry_date:
          data.school_entry_date === studentHold.school_entry_date
            ? data.school_entry_date
            : new Date(data.school_entry_date),
        gender: data.gender,
        generation: data.generation,
      });
    }
  };

  React.useEffect(() => {
    if (studentHold) {
      setValue("student_id", studentHold.student_id);
      setValue("name", studentHold.name);
      setValue("last_name", studentHold.last_name);
      setValue("age", studentHold.age);
      setValue("student_tel", studentHold.student_tel);
      setValue("date_of_birth", studentHold.date_of_birth);

      setValue("student_village", studentHold.student_village);
      setValue("student_district", studentHold.student_district);
      setValue("student_province", studentHold.student_province);
      setValue("nationality", studentHold.nationality);
      setValue("father_name", studentHold.father_name);
      setValue("father_tel", studentHold.father_tel);
      setValue("father_job", studentHold.father_job);
      setValue("mother_name", studentHold.mother_name);
      setValue("mother_tel", studentHold.mother_tel);
      setValue("mother_job", studentHold.mother_job);
      setValue("student_past_school", studentHold.student_past_school);

      setValue("student_number", studentHold.student_number);
      setValue("room_id", studentHold.room_id._id);
      setValue("level_id", studentHold.level_id._id);
      setValue("status", studentHold.status);
      setValue("note", studentHold.note);
      setValue("school_entry_date", studentHold.school_entry_date);
      setValue("gender", studentHold.gender);
      setValue("generation", studentHold.generation);
    }
  }, [studentHold]);

  return (
    <Drawer
      anchor={"right"}
      open={isOpenUpdateStudentDrawer}
      onClose={() => handleCloseUpdateStudentDrawer()}
    >
      <form style={{ width: "100%" }} onSubmit={handleSubmit(handleUpdate)}>
        <Box
          width={
            breakpointSm
              ? "100%"
              : breakpointMd
              ? 600
              : breakpointLg
              ? 900
              : 1200
          }
        >
          <Stack spacing={3} padding={3}>
            <Grid
              container
              spacing={3}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={12}>
                <Stack direction={"row"} justifyContent="space-between">
                  <Typography variant={"h5"} component="div">
                    ເພີ່ມຂໍ້ມູນນັກຮຽນ
                  </Typography>
                  <IconButton onClick={() => handleCloseUpdateStudentDrawer()}>
                    <Close />
                  </IconButton>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_id"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລະຫັດນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student_id &&
                  errors.student_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລະຫັດນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors && errors.name && errors.name.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນຊື່ນັກຮຽນ"}
                  </Typography>
                )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="last_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ນາມສະກຸນນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.last_name &&
                  errors.last_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນນາມສະກຸນນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="age"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ອາຍຸ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors && errors.age && errors.age.type === "required" && (
                  <Typography variant="subtitle2" color="error" marginTop={1}>
                    {"ກະລຸນາປ້ອນອາຍຸຂອງນັກຮຽນ"}
                  </Typography>
                )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_tel"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ເບີໂທນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student_tel &&
                  errors.student_tel.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນເບີໂທຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              {/* date of birth */}
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="date_of_birth"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DesktopDatePicker
                        {...field}
                        label="ວັນທີ"
                        inputFormat="DD/MM/YYYY"
                        onChange={(e) => handleChangeDate_of_birth(e)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="filled"
                            size="small"
                            fullWidth
                          />
                        )}
                      />
                    </LocalizationProvider>
                  )}
                />

                {errors &&
                  errors.date_of_birth &&
                  errors.date_of_birth.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_village"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ບ້ານ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student_village &&
                  errors.student_village.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນທີ່ຢູ່ບ້ານຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_district"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ເມື່ອງ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student_district &&
                  errors.student_district.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນທີ່ຢູ່ເມື່ອງຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_province"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ແຂວງ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student_province &&
                  errors.student_province.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນທີ່ຢູ່ແຂວງຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="nationality"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ສັນຊາດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.nationality &&
                  errors.nationality.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນສັນຊາດຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="father_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ພໍ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.father_name &&
                  errors.father_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຊື່ພໍ່ຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="father_tel"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ເບີໂທພໍ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.father_tel &&
                  errors.father_tel.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນເບີໂທພໍ່ຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="father_job"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ອາຊີບຂອງພໍ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.father_job &&
                  errors.father_job.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນອາຊີບຂອງພໍ່ຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="mother_name"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ຊື່ແມ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.mother_name &&
                  errors.mother_name.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຊື່ແມ່ຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="mother_tel"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ເບີໂທແມ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.mother_tel &&
                  errors.mother_tel.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນເບີໂທແມ່ຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="mother_job"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ອາຊີບຂອງແມ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.mother_job &&
                  errors.mother_job.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນອາຊີບຂອງແມ່ຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_past_school"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ໂຮງຮຽນເກົ່າ ຫຼື ໂຮງຮຽນທີ່ຍ້າຍມາ ຂອງນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="student_number"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລຳດັບນັກຮຽນ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.student_number &&
                  errors.student_number.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນລຳດັບຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                {isGetClassRoomsLoading && <Skeleton />}
                {isGetClassRoomsSuccess &&
                  classRooms.classrooms.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`room_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={classRooms.classrooms.classRooms}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => `${option.room_name} ຊັ້ນຮຽນ ${option.level_id.level_name}`}
                          onChange={(
                            event: any,
                            newValue: ClassRoom | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {option.room_name} ຊັ້ນຮຽນ
                              {option.level_id.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຫ້ອງຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                          defaultValue={classRooms.classrooms.classRooms.find(
                            (o) => o._id === studentHold?.room_id._id
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.room_id &&
                  errors.room_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຂອງຫ້ອງຮຽນ"}
                    </Typography>
                  )}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຫ້ອງຮຽນ"}
                    </Typography>
                  )}
              </Grid>
              <Grid item xs={12}>
                {isGetClassLevelsLoading && <Skeleton />}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount > 0 && (
                    <Controller
                      control={control}
                      name={`level_id`}
                      rules={{
                        required: true,
                      }}
                      render={({ field }) => (
                        <Autocomplete
                          id="combo-box-demo"
                          options={classLevels.classLevels.classLevels}
                          autoComplete
                          fullWidth
                          getOptionLabel={(option) => option.level_name}
                          onChange={(
                            event: any,
                            newValue: ClassLevel | null
                          ) => {
                            field.onChange(newValue?._id);
                          }}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{
                                "& > img": { mr: 2, flexShrink: 0 },
                              }}
                              {...props}
                            >
                              {option.level_name}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="ຊັ້ນຮຽນ"
                              variant="filled"
                              inputProps={{
                                ...params.inputProps,
                                autoComplete: "disabled",
                              }}
                              fullWidth
                            />
                          )}
                          defaultValue={classLevels.classLevels.classLevels.find(
                            (o) => o._id === studentHold?.level_id._id
                          )}
                        />
                      )}
                    />
                  )}
                {errors &&
                  errors.level_id &&
                  errors.level_id.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນຂອງຊັ້ນຣຽນ"}
                    </Typography>
                  )}
                {isGetClassLevelsSuccess &&
                  classLevels.classLevels.totalCount <= 0 && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາເພີ່ມຊັ້ນຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="status"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ສະຖານະ"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.status &&
                  errors.status.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນສະຖານະການຮຽນຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="school_entry_date"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DesktopDatePicker
                        {...field}
                        label="ວັນທີເຂົ້າໂຮງຮຽນ"
                        inputFormat="DD/MM/YYYY"
                        onChange={(e) => handleChangeSchool_entry_date(e)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="filled"
                            size="small"
                            fullWidth
                          />
                        )}
                      />
                    </LocalizationProvider>
                  )}
                />

                {errors &&
                  errors.school_entry_date &&
                  errors.school_entry_date.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບຖ້ວນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="gender"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Autocomplete
                      id="combo-box-demo"
                      options={[{ gender: "ຊາຍ" }, { gender: "ຍິງ" }]}
                      autoComplete
                      fullWidth
                      getOptionLabel={(option) => `${option.gender}`}
                      onChange={(
                        event: any,
                        newValue: { gender: string } | null
                      ) => {
                        field.onChange(newValue?.gender);
                      }}
                      renderOption={(props, option) => (
                        <Box
                          component="li"
                          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                          {...props}
                        >
                          {option.gender}
                        </Box>
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="ເພດ"
                          variant="filled"
                          inputProps={{
                            ...params.inputProps,
                          }}
                          fullWidth
                        />
                      )}
                      defaultValue={[{ gender: "ຊາຍ" }, { gender: "ຍິງ" }].find(
                        (o) => o.gender === studentHold?.gender
                      )}
                    />
                  )}
                />
                {errors &&
                  errors.gender &&
                  errors.gender.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນເພດຂອງນັກຮຽນ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="generation"
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ນັກຮຽນລຸ້ນທີ່"}
                      fullWidth
                      variant="filled"
                      multiline
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
                {errors &&
                  errors.generation &&
                  errors.generation.type === "required" && (
                    <Typography variant="subtitle2" color="error" marginTop={1}>
                      {"ກະລຸນາປ້ອນຂໍ້ມູນວ່າເປັນນັກຮຽນລຸ້ນທີ່ເທົ່າໃດ"}
                    </Typography>
                  )}
              </Grid>

              <Grid item xs={12}>
                <Controller
                  control={control}
                  name="note"
                  render={({ field }) => (
                    <TextField
                      {...field}
                      size="small"
                      type="text"
                      label={"ລາຍລະອຽດ"}
                      fullWidth
                      variant="filled"
                      multiline
                      rows={4}
                      disabled={isUpdateStudentLoading}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  sx={{ paddingInline: 5 }}
                  disabled={isUpdateStudentLoading}
                >
                  <Save /> ບັນທຶກ
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Box>
      </form>
    </Drawer>
  );
};

export default UpdateStudentForm;

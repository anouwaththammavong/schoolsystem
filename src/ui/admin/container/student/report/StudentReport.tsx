import * as React from "react";
import { StudentState } from "../../../../../domain/Student";
import {Typography } from "@mui/material";
import moment from "moment";
import TableComponentForRePort from "../../../components/ui-tool/TableComponentForReport";

type Props = {
  studentState: StudentState;
};

// const StudentReport = React.forwardRef<HTMLDivElement, Props>((props, ref) => {
//     return (
//       <div ref={ref}>StudentReport</div>
//     );
//   });

const StudentReport = React.forwardRef<HTMLDivElement, Props>(
  ({ studentState }, ref) => {
    const [studentTable, setStudentTable] = React.useState<Array<any>>([]);

    const {
      students,
      isGetStudentsLoading,
      isGetStudentsSuccess,
      getStudentsError,
    } = studentState;
    React.useEffect(() => {
      setStudentTable(
        students.students.students.map((student) => {
          return {
            ລະຫັດນັກຮຽນ: <Typography>{student.student_id}</Typography>,
            "ຊື່ ແລະ ນາມສະກຸນ": (
              <Typography>
                {student.name} {student.last_name}
              </Typography>
            ),
            ວັນເດືອນປີເກີດ: (
              <Typography>
                {moment(student.date_of_birth).format("DD/MM/YYYY")}
              </Typography>
            ),
            ຫ້ອງຮຽນ: <Typography>{student.level_id.level_name}</Typography>,
            ລຸ້ນທີ່: <Typography>{student.generation}</Typography>,
            ສະຖານະການຮຽນ: <Typography>{student.status}</Typography>,
          };
        })
      );
    }, [students]);
    return (
      <div ref={ref} style={{marginTop:"20px"}}>
        <Typography textAlign={"center"} variant="h5" marginBottom={"20px"} >ລາຍງານຂໍ້ມູນນັກຮຽນ</Typography>
        <TableComponentForRePort
          isLoading={isGetStudentsLoading}
          isLoadSuccess={isGetStudentsSuccess}
          isLoadError={getStudentsError.isError}
          data={studentTable}
          all={students.students.totalCount}
        />
      </div>
    );
  }
);

export default StudentReport;

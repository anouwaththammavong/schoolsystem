import * as React from "react";
import { useStudent } from "../../../hooks/useStudent";
import { Box, Stack } from "@mui/material";
import StudentPageHeaderText from "./StudentPageHeaderText";
import { useForm } from "react-hook-form";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;
import StudentFilterForm from "./StudentFilterForm";
import StudentTable from "./StudentTable";
import { Student } from "../../../../domain/Student";
import CreateStudentForm from "./CreateStudentForm";
import Swal from "sweetalert2";
import UpdateStudentForm from "./UpdateStudentForm";
import { useClassLevel } from "../../../hooks/useClassLevel";
import { useClassRoom } from "../../../hooks/useClassRoom";

type Props = {};

export interface OpenItemMenu {
  isOpen: boolean;
  student_id: string;
  anchorEl: null | HTMLElement;
}

const StudentPage = (props: Props) => {
  const { handleSubmit, control, setValue, getValues } = useForm();

  const [cleanUp, setCleanUp] = React.useState(false);
  const [isOpenCreateStudentDrawer, setIsOpenCreateStudentDrawer] =
    React.useState(false);
  const [isOpenUpdateStudentDrawer, setIsOpenUpdateStudentDrawer] =
    React.useState(false);
  const [studentHold, setStudentHold] = React.useState<Student | undefined>();
  const [page, setPage] = React.useState<number>(0);
  const [rowPerPage, setRowPerPage] = React.useState<number>(defaultLimit);

  const [openItemMenu, setOpenItemMenu] = React.useState<OpenItemMenu>({
    isOpen: false,
    student_id: "",
    anchorEl: null,
  });

  const {
    studentState,
    getStudents,
    createStudent,
    createStudentReset,
    updateStudent,
    updateStudentReset,
    deleteStudent,
    deleteStudentReset,
  } = useStudent();

  const { getClassRooms, classRoomState } = useClassRoom();

  const { getClassLevels, classLevelState } = useClassLevel();

  const {
    students,
    isCreateStudentSuccess,
    CreateStudentError,
    isUpdateStudentSuccess,
    updateStudentError,
    isDeleteStudentSuccess,
    deleteStudentError,
  } = studentState;

  const handleOpenCreateStudentDrawer = () => {
    setIsOpenCreateStudentDrawer(true);
  };

  const handleCloseCreateStudentDrawer = () => {
    setIsOpenCreateStudentDrawer(false);
  };

  const handleChangeGeneration = (generation: string) => {
    setValue("generation", generation);
    handleSearch();
  };

  const handleBlurSearch = (student_id: string) => {
    setValue("student_id", student_id);
    handleSearch();
  };

  const handleChangeClassLevel = (level_id: string) => {
    setValue("level_id", level_id);
    handleSearch();
  };

  const handleChangeNewStudent = (new_student: string) => {
    console.log(new_student)
    setValue("new_student", new_student);
    handleSearch();
  };

  const handleSearch = () => {
    const data = getValues();
    setPage(0);
    getStudents(
      rowPerPage,
      0,
      data.student_id,
      data.generation,
      data.level_id,
      data.new_student
    );
  };

  const handleOpenItemMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    student_id: string,
    itemName: string
  ) => {
    setOpenItemMenu({
      isOpen: true,
      student_id: student_id,
      anchorEl: event.currentTarget,
    });
  };

  const handleCloseItemMenu = () => {
    setOpenItemMenu({
      isOpen: false,
      student_id: "",
      anchorEl: null,
    });
  };

  const handleOpenUpdateStudentDrawer = () => {
    const stud = students.students.students.filter(
      (stud) => stud._id === openItemMenu.student_id
    )[0];
    if (stud) {
      setStudentHold(stud);
      //   getPickUpItems(prod.id);
      setIsOpenUpdateStudentDrawer(true);
      setOpenItemMenu({
        isOpen: false,
        student_id: "",
        anchorEl: null,
      });
    }
  };

  const handleCloseUpdateStudentDrawer = () => {
    setIsOpenUpdateStudentDrawer(false);
  };

  const handleChangeTablePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    const data = getValues();
    getStudents(
      rowPerPage,
      newPage * rowPerPage,
      data.student_id,
      data.generation,
      data.level_id,
      data.new_student
    );
  };

  const handleOpenDeleteStudentAlertSweet = () => {
    const stud = openItemMenu.student_id;
    if (stud) {
      setOpenItemMenu({
        isOpen: false,
        student_id: "",
        anchorEl: null,
      });
    }
    Swal.fire({
      title: "ທ່ານຕ້ອງການລົບຂໍ້ມູນຂອງນັກຮຽນຜູ້ນີ້ບໍ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ຕ້ອງການ",
      cancelButtonText: `ບໍ່ຕ້ອງການ`,
      confirmButtonColor: "#22bb33",
      cancelButtonColor: "#d33",
    }).then((result) => {
      if (result.isConfirmed && stud) {
        deleteStudent(stud);
      }
    });
  };

  const handleChangeRowsPerTablePage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
    const data = getValues();
    getStudents(
      parseInt(event.target.value, 10),
      0,
      data.student_id,
      data.generation,
      data.level_id,
      data.new_student
    );
  };

  React.useEffect(() => {
    if (isCreateStudentSuccess) {
      handleCloseCreateStudentDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ເພີ່ມຂໍ້ມູນນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      createStudentReset();
      getStudents(defaultLimit, 0);
    } else if (CreateStudentError.isError) {
      handleCloseCreateStudentDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: CreateStudentError.message,
        icon: "error",
      });
      createStudentReset();
      getStudents(defaultLimit, 0);
    }
  }, [isCreateStudentSuccess, CreateStudentError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isUpdateStudentSuccess) {
      handleCloseUpdateStudentDrawer();
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ແກ້ໄຂຂໍ້ມູນນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      updateStudentReset();
      getStudents(defaultLimit, 0);
    } else if (updateStudentError.isError) {
      handleCloseUpdateStudentDrawer();
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: updateStudentError.message,
        icon: "error",
      });
      updateStudentReset();
      getStudents(defaultLimit, 0);
    }
  }, [isUpdateStudentSuccess, updateStudentError.isError, defaultLimit]);

  React.useEffect(() => {
    if (isDeleteStudentSuccess) {
      Swal.fire({
        title: "ສຳເລັດ!",
        text: "ລົບຂໍ້ມູນນັກຮຽນສຳເລັດ!",
        icon: "success",
      });
      deleteStudentReset();
      getStudents(defaultLimit, 0);
    } else if (deleteStudentError.isError) {
      Swal.fire({
        title: "ເກີດຂໍ້ຜິດພາດ!",
        text: "ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນ ກະລູນາລອງໃໝ່ອີກຄັ້ງ!",
        icon: "error",
      });
      deleteStudentReset();
      getStudents(defaultLimit, 0);
    }
  }, [isDeleteStudentSuccess, deleteStudentError.isError, defaultLimit]);

  React.useEffect(() => {
    setCleanUp(true);
    if (cleanUp) {
      getClassRooms(0, 0);
      getClassLevels(0, 0);
      getStudents(defaultLimit, 0);
    }
    return () => {
      setCleanUp(false);
    };
  }, [cleanUp, defaultLimit]);
  return (
    <Box>
      <Stack spacing={3}>
        <StudentPageHeaderText
          handleOpenCreateStudentDrawer={handleOpenCreateStudentDrawer}
        />
        <StudentFilterForm
          control={control}
          handleSubmit={handleSubmit}
          handleSearch={handleSearch}
          handleBlurSearch={handleBlurSearch}
          handleChangeGeneration={handleChangeGeneration}
          studentState={studentState}
          classLevelState={classLevelState}
          handleChangeClassLevel={handleChangeClassLevel}
          handleChangeNewStudent={handleChangeNewStudent}
        />
        <CreateStudentForm
          createStudent={createStudent}
          studentState={studentState}
          classRoomState={classRoomState}
          classLevelState={classLevelState}
          handleCloseCreateStudentDrawer={handleCloseCreateStudentDrawer}
          isOpenCreateStudentDrawer={isOpenCreateStudentDrawer}
        />
        {studentHold && (
          <UpdateStudentForm
            studentState={studentState}
            classRoomState={classRoomState}
            classLevelState={classLevelState}
            handleCloseUpdateStudentDrawer={handleCloseUpdateStudentDrawer}
            isOpenUpdateStudentDrawer={isOpenUpdateStudentDrawer}
            studentHold={studentHold}
            updateStudent={updateStudent}
          />
        )}
        <StudentTable
          studentState={studentState}
          openItemMenu={openItemMenu}
          handleOpenItemMenu={handleOpenItemMenu}
          handleCloseItemMenu={handleCloseItemMenu}
          handleOpenUpdateStudentDrawer={handleOpenUpdateStudentDrawer}
          handleOpenDeleteStudentAlertSweet={handleOpenDeleteStudentAlertSweet}
          page={page}
          rowsPerPage={rowPerPage}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </Stack>
    </Box>
  );
};

export default StudentPage;

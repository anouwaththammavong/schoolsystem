import * as React from "react";
import { StudentState } from "../../../../domain/Student";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import moment from "moment";
import { MoreVert } from "@mui/icons-material";
import TableComponent from "../../components/ui-tool/TableComponent";
import { OpenItemMenu } from "./StudentPage";

type Props = {
  studentState: StudentState;
  openItemMenu: OpenItemMenu;
  handleOpenItemMenu: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
    ticketName: string
  ) => void;
  handleCloseItemMenu: () => void;
  handleOpenUpdateStudentDrawer: () => void;
  handleOpenDeleteStudentAlertSweet: () => void;
  page: number;
  rowsPerPage: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
};

const StudentTable = ({
  studentState,
  openItemMenu,
  handleOpenItemMenu,
  handleCloseItemMenu,
  handleOpenUpdateStudentDrawer,
  handleOpenDeleteStudentAlertSweet,
  page,
  rowsPerPage,
  handleChangeRowsPerTablePage,
  handleChangeTablePage,
}: Props) => {
  const [studentTable, setStudentTable] = React.useState<Array<any>>([]);

  const {
    students,
    isGetStudentsLoading,
    isGetStudentsSuccess,
    getStudentsError,
  } = studentState;

  React.useEffect(() => {
    setStudentTable(students.students.students.map((student) => {
        return {
          ລະຫັດນັກຮຽນ: <Typography>{student.student_id}</Typography>,
          ຊື່: <Typography>{student.name}</Typography>,
          ນາມສະກຸນ: <Typography>{student.last_name}</Typography>,
          ອາຍຸ:  <Typography>{student.age}</Typography>,
          ເບີໂທນັກຮຽນ:  <Typography>{student.student_tel}</Typography>,
          ວັນເດືອນປີເກີດ: (
            <Typography>
              {moment(student.date_of_birth).format("DD/MM/YYYY")}
            </Typography>
          ),
          ຫ້ອງຮຽນ: <Typography>{student.level_id.level_name}</Typography>,
          ລຸ້ນທີ່: <Typography>{student.generation}</Typography>,
          ສະຖານະການຮຽນ: <Typography>{student.status}</Typography>,
          "": (
            <>
              <IconButton
                onClick={(e) => handleOpenItemMenu(e, student._id, student.name)}
              >
                <MoreVert />
              </IconButton>
              <Menu
                keepMounted
                open={
                  openItemMenu.isOpen && openItemMenu.student_id === student._id
                }
                anchorEl={openItemMenu.anchorEl}
                onClose={handleCloseItemMenu}
                PaperProps={{
                  style: {
                    width: "20ch",
                  },
                }}
              >
                <MenuItem onClick={() => handleOpenUpdateStudentDrawer()}>
                  ແກ້ໄຂຂໍ້ມູນ
                </MenuItem>
                <MenuItem onClick={() => handleOpenDeleteStudentAlertSweet()}>
                  ລົບຂໍ້ມູນ
                </MenuItem>
              </Menu>
            </>
          ),
        };
      }))
  }, [students, openItemMenu]);
  return (
    <Box>
      <>
        <TableComponent
          isLoading={isGetStudentsLoading}
          isLoadSuccess={isGetStudentsSuccess}
          isLoadError={getStudentsError.isError}
          data={studentTable}
          all={students.students.totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangeTablePage={handleChangeTablePage}
          handleChangeRowsPerTablePage={handleChangeRowsPerTablePage}
        />
      </>
    </Box>
  );
};

export default StudentTable;

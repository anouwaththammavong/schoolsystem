import * as React from "react";
import { Print, Search } from "@mui/icons-material";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Skeleton,
  TextField,
  Typography,
} from "@mui/material";
import {
  Control,
  Controller,
  FieldValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { StudentState } from "../../../../domain/Student";
import ReactToPrint from "react-to-print";
import { ClassLevelState } from "../../../../domain/ClassLevel";
import StudentReport from "./report/StudentReport";

type Props = {
  control: Control<FieldValues, any>;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  handleSearch: () => void;
  handleChangeGeneration: (shop_id: string) => void;
  handleChangeClassLevel: (level_id: string) => void;
  handleChangeNewStudent: (new_student: string) => void;
  handleBlurSearch: (no: string) => void;
  studentState: StudentState;
  classLevelState: ClassLevelState;
};

const StudentFilterForm = ({
  control,
  handleSubmit,
  handleSearch,
  handleChangeGeneration,
  handleBlurSearch,
  studentState,
  classLevelState,
  handleChangeClassLevel,
  handleChangeNewStudent,
}: Props) => {
  const componentRef = React.useRef(null);
  const { students, isGetStudentsLoading, isGetStudentsSuccess } = studentState;
  const { classLevels, isGetClassLevelsLoading, isGetClassLevelsSuccess } =
    classLevelState;
  const new_student = [
    {
      title: "ນັກຮຽນໃໝ່",
      value: "true",
    },
    {
      title: "ນັກຮຽນເກົ່າ",
      value: "false",
    },
  ];
  return (
    <Box>
      {isGetStudentsLoading ||
        (isGetClassLevelsLoading && (
          <Paper style={{ padding: 5 }}>
            <Skeleton />
          </Paper>
        ))}
      {isGetStudentsSuccess && isGetClassLevelsSuccess && (
        <form onSubmit={handleSubmit(handleSearch)}>
          <Grid container spacing={2}>
            <Grid item sm={12} xs={12} md={3} lg={3} xl={3}>
              <Controller
                control={control}
                name="student_id"
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="ຄົ້ນຫາ"
                    size="small"
                    fullWidth
                    onBlur={(e) => handleBlurSearch(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <Search />
                        </InputAdornment>
                      ),
                    }}
                    variant="filled"
                  />
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={2.5} lg={2.5} xl={2.5}>
              <Controller
                control={control}
                name="level_id"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ຊັ້ນຮຽນທີ່</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeClassLevel(e.target.value)
                      }
                    >
                      {classLevels.classLevels.classLevels.map((val, key) => (
                        <MenuItem key={key} value={val._id}>
                          {val.level_name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={2.5} lg={2.5} xl={2.5}>
              <Controller
                control={control}
                name="new_student"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ນັກຮຽນໃໝ່ ຫຼື ເກົ່າ</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeNewStudent(e.target.value)
                      }
                    >
                      {new_student.map((val, key) => (
                        <MenuItem key={key} value={val.value}>
                          {val.title}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={2.5} lg={2.5} xl={2.5}>
              <Controller
                control={control}
                name="generation"
                render={({ field }) => (
                  <FormControl
                    fullWidth
                    variant="filled"
                    sx={{ minWidth: 120 }}
                  >
                    <InputLabel>ເລືອກ ນັກຮຽນລຸ້ນທີ່</InputLabel>
                    <Select
                      {...field}
                      size="small"
                      onChange={(e: SelectChangeEvent) =>
                        handleChangeGeneration(e.target.value)
                      }
                    >
                      {students.students.uniqueGenerations.map((val, key) => (
                        <MenuItem key={key} value={val.generation}>
                          {val.generation}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item sm={12} xs={12} md={2.5} lg={2.5} xl={2.5}>
              <ReactToPrint
                trigger={() => {
                  return (
                    <Button
                      variant="contained"
                      // onClick={() => handleGeneratePdf()}
                      disabled={isGetStudentsLoading}
                      size="large"
                    >
                      <Print sx={{ marginRight: 1 }} /> ສ້າງລາຍງານ
                      {isGetStudentsLoading && (
                        <CircularProgress
                          color="inherit"
                          size={"2rem"}
                          sx={{ marginLeft: 1 }}
                        />
                      )}
                    </Button>
                  );
                }}
                content={() => componentRef.current}
                documentTitle="ລາຍງານຂໍ້ມູນນັກຮຽນ"
                pageStyle="print"
              />
              <Box display={"none"}>
                <StudentReport studentState={studentState} ref={componentRef} />
              </Box>
            </Grid>
          </Grid>
        </form>
      )}
    </Box>
  );
};

export default StudentFilterForm;

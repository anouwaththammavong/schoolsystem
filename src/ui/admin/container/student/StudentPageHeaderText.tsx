import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { Add } from '@mui/icons-material';

type Props = {
    handleOpenCreateStudentDrawer: () => void;
}

const StudentPageHeaderText = ({handleOpenCreateStudentDrawer}: Props) => {
  return (
    <Box>
      <Stack
        direction={'row'}
        spacing={2}
      >
        <Typography
          variant='h5'
        >
          ຂໍ້ມູນນັກຮຽນ
        </Typography>
        <Button
          variant='contained'
          onClick={() => handleOpenCreateStudentDrawer()}
        >
          <Add /> ເພີ່ມຂໍ້ມູນນັກຮຽນ
        </Button>
      </Stack>
    </Box>
  )
}

export default StudentPageHeaderText
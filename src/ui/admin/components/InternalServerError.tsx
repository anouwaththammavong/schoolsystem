import { styled } from "@mui/material";
import { FC } from "react";

const Img = styled('img')({
    width: '500px',
    display: 'block',
    marginTop: '100px',
    marginInline: 'auto'
});

const InternalServerError: FC = () => {
    return (
        <>
            <Img
                src={'/images/500.jpg'}
            />
        </>
    );
};

export default InternalServerError;
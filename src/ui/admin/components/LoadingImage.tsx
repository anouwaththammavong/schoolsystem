import { styled } from "@mui/material";
import { FC } from "react";

const Img = styled('img')(({ theme }) => ({
    width: '700px',
    display: 'block',
    marginInline: 'auto',
    [theme.breakpoints.down('sm')]: {
        width: '300px',
    },
}))

const Div = styled('div')(({ theme }) => ({
    backgroundColor: '#e5eff1',
    height:'100vh',
    paddingTop:'15vh',
    [theme.breakpoints.down('xs')]: {
        paddingTop:'25vh'
    },
    [theme.breakpoints.down('sm')]: {
        paddingTop:'25vh'
    },
    [theme.breakpoints.down('md')]: {
        paddingTop:'35vh'
    },
    [theme.breakpoints.down('lg')]: {
        paddingTop:'15vh'
    },
    [theme.breakpoints.down('xl')]: {
        paddingTop:'15vh'
    }
}))

const LoadingImage: FC = () => {

    return (
        <Div>
            <Img
                src={'/images/loading.gif'}
            />
        </Div>
    );
};

export default LoadingImage;
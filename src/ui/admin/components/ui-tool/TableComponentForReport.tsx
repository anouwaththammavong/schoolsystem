import { FC } from "react";
import {
  Box,
  Card,
  Divider,
  Grid,
  Paper,
  Skeleton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableProps,
  TableRow,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import NoData from "../NoData";
import InternalServerError from "../InternalServerError";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

interface Props extends TableProps {
  isLoading: boolean;
  isLoadSuccess: boolean;
  isLoadError: boolean;
  data: any[];
  all: number;
}

const TableComponentForRePort: FC<Props> = ({
  isLoading,
  isLoadSuccess,
  isLoadError,
  data,
  all,
}) => {
  const theme = useTheme();
  const breakpointMdDown = useMediaQuery(theme.breakpoints.down("md"));
  const tableHeaders = data.length > 0 ? Object.keys(data[0]) : [];

  return (
    <>
      {isLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
        </Paper>
      )}
      {isLoadSuccess && (
        <Box>
          {!breakpointMdDown && (
            // <Paper sx={{border:"1px solid red"}}>
              <TableContainer sx={{border:"1px solid #000", marginTop:"50px"}}>
                <Table>
                  <TableHead>
                    <TableRow>
                      {tableHeaders.map((header, index) => (
                        <TableCell key={index}><Typography>
                        {header}
                      </Typography></TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data.map((item, index) => (
                      <TableRow key={index}>
                        {tableHeaders.map((header) => (
                          <TableCell key={header}>{item[header]}</TableCell>
                        ))}
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            // </Paper>
          )}
          {breakpointMdDown && (
            <Box>
              {data.length > 0 && (
                <Stack spacing={1}>
                  {data.map((item, key) => (
                    <Card key={key}>
                      <Stack
                        direction={"column"}
                        padding={2}
                        spacing={1}
                        divider={<Divider />}
                      >
                        {tableHeaders.map((header) => (
                          <Grid container>
                            <Grid xs={6}>
                              <Typography fontWeight={"bold"} variant="body2">
                                {header}:
                              </Typography>
                            </Grid>
                            <Grid xs={6}>
                              <Typography textAlign={"end"}>
                                {item[header]}
                              </Typography>
                            </Grid>
                          </Grid>
                        ))}
                      </Stack>
                    </Card>
                  ))}
                </Stack>
              )}
              {data.length === 0 && <NoData />}
            </Box>
          )}
        </Box>
      )}
      {isLoadError && (
        <>
          <InternalServerError />
        </>
      )}
    </>
  );
};

export default TableComponentForRePort;

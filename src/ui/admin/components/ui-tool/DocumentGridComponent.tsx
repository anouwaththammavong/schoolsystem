import { FC, useState } from "react";
import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Paper,
  Skeleton,
  Stack,
  TablePagination,
  TableProps,
  Typography,
  Modal,
} from "@mui/material";
import NoData from "../NoData";
import InternalServerError from "../InternalServerError";
import { Link } from "react-router-dom";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;
const endpoint = import.meta.env.VITE_API_ENDPOINT || "";
import DownloadIcon from "@mui/icons-material/Download";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import AlertDialog from "./AlertDialog";
import ModalImage from "./ModalImage";

interface Props extends TableProps {
  isLoading: boolean;
  isLoadSuccess: boolean;
  isLoadError: boolean;
  data: any[];
  all: number;
  rowsPerPage: number;
  page: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
  openDialog: boolean;
  handleClickOpenDialog: (documentId: number) => void;
  handleCloseDialog: () => void;
  DocumentId: number;
  deleteDocument: (documentIdForDelete: number) => void;
  isLoadingDelete: boolean;
}

const DocumentGridComponent: FC<Props> = ({
  isLoading,
  isLoadSuccess,
  isLoadError,
  data,
  all,
  page,
  rowsPerPage,
  handleChangeTablePage,
  handleChangeRowsPerTablePage,
  openDialog,
  handleClickOpenDialog,
  handleCloseDialog,
  DocumentId,
  deleteDocument,
  isLoadingDelete,
}) => {

  const [openModalImg, setOpenModalImg] = useState(false);
  const handleCloseModalImg = () => setOpenModalImg(false);
  const [imgModal, setImgModal] = useState("");

  const handleClickImgDocument = (srcImg:string) => {
    setImgModal(srcImg);
    setOpenModalImg(true);
  };
  
  console.log(endpoint)
  console.log("data",data)

  return (
    <>
      {isLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
        </Paper>
      )}
      {isLoadSuccess && (
        <Box>
          <AlertDialog
            openDialog={openDialog}
            handleCloseDialog={handleCloseDialog}
            DocumentId={DocumentId}
            deleteDocument={deleteDocument}
            isLoadingDelete={isLoadingDelete}
          />
          <ModalImage openModalImg={openModalImg} handleCloseModalImg={handleCloseModalImg} imgModal={imgModal} />
          <Typography variant="h5" marginBottom={"20px"}>
            ລາຍການເອກະສານ
          </Typography>
          <Grid container rowSpacing={2} spacing={2}>
            {data.map((item, index) => (
              <Grid item xl={2} lg={2} md={3} sm={4} xs={6} key={index}>
                <Card
                  sx={{
                    maxWidth: 345,
                    padding: "10px 10px",
                    position: "relative",
                    boxShadow: "0px 0px 3px 0px rgba(0,0,0,0.30)",
                  }}
                >
                  <Stack
                    direction={"row"}
                    justifyContent={"flex-end"}
                    marginBottom={"10px"}
                  >
                    <Link
                      to={`${endpoint}/${item.src}`}
                      download={"file.pdf"}
                      target="_blank"
                    >
                      <IconButton color="success" aria-label="download">
                        <DownloadIcon />
                      </IconButton>
                    </Link>
                    <IconButton
                      onClick={() => handleClickOpenDialog(item.id)}
                      color="error"
                      aria-label="delete"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Stack>
                  <CardActionArea>
                    {(item.fileType === "jpeg" ||
                      item.fileType === "jpg" ||
                      item.fileType === "png" ||
                      item.fileType === "webp") && (
                      <CardMedia
                        component="img"
                        height="120"
                        image={`${endpoint}/${item.src}`}
                        alt="green iguana"
                        sx={{ objectFit: "contain" }}
                        onClick={() => handleClickImgDocument(`${endpoint}/${item.src}`)}
                      />
                    )}
                    {item.fileType === "pdf" && item.src && (
                      <CardMedia
                        component="img"
                        height="120"
                        image={
                          "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/PDF_file_icon.svg/833px-PDF_file_icon.svg.png"
                        }
                        alt="green iguana"
                        sx={{ objectFit: "contain" }}
                      />
                    )}
                  </CardActionArea>
                  <CardContent>
                    <Typography
                      variant="body1"
                      component="div"
                      // textAlign={"center"}
                    >
                      {item.slider_name}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>

          <TablePagination
            rowsPerPageOptions={[defaultLimit, 50, 100]}
            component={"div"}
            count={all}
            rowsPerPage={rowsPerPage}
            page={page}
            labelRowsPerPage=""
            onPageChange={handleChangeTablePage}
            onRowsPerPageChange={handleChangeRowsPerTablePage}
          />
          {data.length === 0 && <NoData />}
        </Box>
      )}
      {isLoadError && (
        <>
          <InternalServerError />
        </>
      )}
    </>
  );
};

export default DocumentGridComponent;

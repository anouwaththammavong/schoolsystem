import { Box } from "@mui/material";
import * as React from "react";
import { GoogleMap, useLoadScript, MarkerF } from "@react-google-maps/api";
const apiGoogleKey =
  import.meta.env.PUBLIC_GOOGLE_MAPS_API_KEY ||
  "AIzaSyCmI4Y0u6h7l14HeA7uDq5Tg-qRldGTi54";

interface Props {
  currentPosition: { lat: number; lng: number };
  handleGetCurrentPosition: (lat: number, lng: number) => void;
  latitude?: string | number;
  longitude?: string | number;
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "50%",
  height: "50%",
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 2,
};

const GoogleMapApi: React.FC<Props> = ({
  currentPosition,
  handleGetCurrentPosition,
}) => {
    console.log(currentPosition)
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: apiGoogleKey,
    libraries: ["places"],
  });

  const onMarkerDragEnd = (e) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();
    handleGetCurrentPosition( lat, lng );
  }

  const onMarkerClick = (e) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();
    handleGetCurrentPosition( lat, lng );
  }

  React.useEffect(() => {
    if (!currentPosition.lat && !currentPosition.lng) {
      navigator.geolocation.getCurrentPosition((position) => {
        const myCurrentPosition = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        handleGetCurrentPosition(myCurrentPosition.lat, myCurrentPosition.lng);
      });
    }
  }, [currentPosition]);

  if (!isLoaded)
    return (
      <>
          <Box>Loading....</Box>
      </>
    );
  return (
      <GoogleMap
        zoom={15}
        center={currentPosition}
        mapContainerStyle={{ width: "100%", height: "90%" }}
        onClick={(e) => onMarkerClick(e)}
      >
        {
            currentPosition ? 
            (
                <MarkerF position={currentPosition} onDragEnd={(e) => onMarkerDragEnd(e)} draggable={true}/>
            )
            :
            null
        }
      </GoogleMap>
  );
};

export default GoogleMapApi;

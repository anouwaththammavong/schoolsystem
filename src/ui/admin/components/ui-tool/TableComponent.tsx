import { FC } from "react";
import {
  Box,
  Card,
  Divider,
  Grid,
  Paper,
  Skeleton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableProps,
  TableRow,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import NoData from "../NoData";
import InternalServerError from "../InternalServerError";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;

interface Props extends TableProps {
  isLoading: boolean;
  isLoadSuccess: boolean;
  isLoadError: boolean;
  data: any[];
  all: number;
  rowsPerPage: number;
  page: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
}

const TableComponent: FC<Props> = ({
  isLoading,
  isLoadSuccess,
  isLoadError,
  data,
  all,
  page,
  rowsPerPage,
  handleChangeTablePage,
  handleChangeRowsPerTablePage,
}) => {
  const theme = useTheme();
  const breakpointMdDown = useMediaQuery(theme.breakpoints.down("md"));
  const tableHeaders = data.length > 0 ? Object.keys(data[0]) : [];

  return (
    <>
      {isLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
        </Paper>
      )}
      {isLoadSuccess && (
        <Box>
          {!breakpointMdDown && (
            <Paper sx={{overflow:"hidden"}}>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {tableHeaders.map((header, index) => (
                        <TableCell key={index}>{header}</TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data.map((item, index) => (
                      <TableRow key={index}>
                        {tableHeaders.map((header) => (
                          <TableCell key={header}>{item[header]}</TableCell>
                        ))}
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[defaultLimit, 50, 100]}
                component={"div"}
                count={all}
                rowsPerPage={rowsPerPage}
                page={page}
                labelRowsPerPage=""
                onPageChange={handleChangeTablePage}
                onRowsPerPageChange={handleChangeRowsPerTablePage}
              />
              {data.length === 0 && <NoData />}
            </Paper>
          )}
          {breakpointMdDown && (
            <Box>
              {data.length > 0 && (
                <Stack spacing={1}>
                  {data.map((item, key) => (
                    <Card key={key}>
                      <Stack
                        direction={"column"}
                        padding={2}
                        spacing={1}
                        divider={<Divider />}
                      >
                        {tableHeaders.map((header) => (
                          <Grid container>
                            <Grid xs={6}>
                              <Typography fontWeight={"bold"} variant="body2">
                                {header}:
                              </Typography>
                            </Grid>
                            <Grid xs={6}>
                              <Typography textAlign={"end"}>
                                {item[header]}
                              </Typography>
                            </Grid>
                          </Grid>
                        ))}
                      </Stack>
                    </Card>
                  ))}
                </Stack>
              )}
              {data.length === 0 && <NoData />}
              <TablePagination
                rowsPerPageOptions={[defaultLimit, 50, 100]}
                component={"div"}
                count={all}
                rowsPerPage={rowsPerPage}
                labelRowsPerPage=""
                page={page}
                onPageChange={handleChangeTablePage}
                onRowsPerPageChange={handleChangeRowsPerTablePage}
              />
            </Box>
          )}
        </Box>
      )}
      {isLoadError && (
        <>
          <InternalServerError />
        </>
      )}
    </>
  );
};

export default TableComponent;

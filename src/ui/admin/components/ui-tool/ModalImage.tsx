import { Box, Modal } from "@mui/material";
import * as React from "react";

interface Props {
  openModalImg: boolean;
  handleCloseModalImg: () => void;
  imgModal: string;
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width:"auto",
  height:"auto",
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 2,
};

const ModalImage: React.FC<Props> = ({
  openModalImg,
  handleCloseModalImg,
  imgModal,
}) => {
  return (
    <Modal
      open={openModalImg}
      onClose={handleCloseModalImg}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <img src={imgModal} alt="" style={{ maxWidth: "100%", maxHeight: "880px", objectFit:"cover" }} />
      </Box>
    </Modal>
  );
};

export default ModalImage;

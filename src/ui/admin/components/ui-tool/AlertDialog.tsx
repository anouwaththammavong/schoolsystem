import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import DeleteIcon from "@mui/icons-material/Delete";
import { Box, Typography } from "@mui/material";

interface Props {
  openDialog: boolean;
  handleCloseDialog: () => void;
  DocumentId: number;
  deleteDocument: ( documentIdForDelete: number ) => void;
  isLoadingDelete: boolean;
}

const AlertDialog: React.FC<Props> = ({
  openDialog,
  handleCloseDialog,
  DocumentId,
  deleteDocument,
  isLoadingDelete
}) => {

  const handleDeleteAddStockDocument = () => {
    deleteDocument(DocumentId)
  }

  return (
    <Dialog
      open={openDialog}
      onClose={handleCloseDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      sx={{
        ".css-v8d1q-MuiPaper-root-MuiDialog-paper": {
          overflow: "visible",
        },
      }}
    >
      <DialogTitle
        id="alert-dialog-title"
        sx={{
          borderTop: "5px solid #f53649",
          position: "relative",
          paddingTop: "50px",
        }}
      >
        <Box
          sx={{
            backgroundColor: "#f53649",
            maxWidth: "34px",
            height: "34px",
            borderRadius: "50%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            color: "#fff",
            position: "absolute",
            top: "22px",
            right: "50%",
            transform: "translate(20px, -50px)",
            padding: "24px",
          }}
        >
          <DeleteIcon fontSize={"large"} />
        </Box>
        <Typography variant="h5" fontWeight={"500"}>
          {"ຜູ້ໃຊ້ຕ້ອງການທີ່ຈະລົບຂໍ້ມູນເອກະສານນີ້ບໍ?"}
        </Typography>
      </DialogTitle>
      <DialogActions>
        <Button
          size="large"
          color="warning"
          variant="outlined"
          onClick={handleCloseDialog}
          disabled={isLoadingDelete}
        >
          ຍົກເລີກ
        </Button>
        <Button
          size="large"
          variant="contained"
          color="error"
          onClick={handleDeleteAddStockDocument}
          autoFocus
          disabled={isLoadingDelete}
        >
          ຢືນຢັນ
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AlertDialog;

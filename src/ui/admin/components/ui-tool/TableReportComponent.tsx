import { FC } from 'react';
import { Box, Card, Divider, Grid, Paper, Skeleton, Stack, Table, TableBody, TableCell, TableHead, TableProps, TableRow, Typography, useMediaQuery, useTheme } from '@mui/material';
import NoData from '../NoData';
import InternalServerError from '../InternalServerError';
import { formatMoney } from '../../../../application/service/Utils';

interface Props extends TableProps {
    isLoading: boolean;
    isLoadSuccess: boolean;
    isLoadError: boolean;
    data: any[];
    grandTotal?: number;
}

const TableReportComponent: FC<Props> = ({
    isLoading,
    isLoadSuccess,
    isLoadError,
    data,
    grandTotal
}) => {
    const theme = useTheme();
    const breakpointMdDown = useMediaQuery(theme.breakpoints.down('md'));
    const tableHeaders = data.length > 0 ? Object.keys(data[0]) : [];

    return (
        <>
            {isLoading &&
                <Paper style={{ padding: 5 }}>
                    <Skeleton />
                    <Skeleton />
                    <Skeleton />
                    <Skeleton />
                    <Skeleton />
                    <Skeleton />
                    <Skeleton />
                    <Skeleton />
                </Paper>
            }
            {isLoadSuccess &&
                <Box>
                    {
                        !breakpointMdDown &&
                        <Paper>
                            {data.length > 0 &&
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            {tableHeaders.map((header, index) => (
                                                <TableCell key={index}>{header}</TableCell>
                                            ))}
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {data.map((item, index) => (
                                            <TableRow key={index}>
                                                {tableHeaders.map((header) => (
                                                    <TableCell key={header}>{item[header]}</TableCell>
                                                ))}
                                            </TableRow>
                                        ))}
                                        {grandTotal &&
                                            <TableRow>
                                                <TableCell
                                                    colSpan={tableHeaders.length - 1}
                                                    align='right'
                                                >
                                                    <Typography
                                                        variant='body1'
                                                        fontWeight={'bold'}
                                                    >
                                                        ລວມເປັນເງິນທັງໝົດ
                                                    </Typography>
                                                </TableCell>
                                                <TableCell>
                                                    <Typography
                                                        variant='body1'
                                                        fontWeight={'bold'}
                                                    >
                                                        {formatMoney(grandTotal)}
                                                    </Typography>
                                                </TableCell>
                                            </TableRow>
                                        }
                                    </TableBody>
                                </Table>
                            }
                            {data.length <= 0 &&
                                <NoData />
                            }
                        </Paper>
                    }
                    {
                        breakpointMdDown &&
                        <Box>
                            {data.length > 0 && (
                                <Stack
                                    spacing={1}
                                >
                                    {
                                        data.map((item, key) => (
                                            <Card
                                                key={key}
                                            >
                                                <Stack
                                                    direction={'column'}
                                                    padding={2}
                                                    spacing={1}
                                                    divider={<Divider />}
                                                >
                                                    {tableHeaders.map((header) => (
                                                        <Grid
                                                            container
                                                        >
                                                            <Grid
                                                                xs={6}
                                                            >
                                                                <Typography
                                                                    fontWeight={'bold'}
                                                                    variant='body2'
                                                                >
                                                                    {header}:
                                                                </Typography>
                                                            </Grid>
                                                            <Grid
                                                                xs={6}
                                                            >
                                                                <Typography
                                                                    textAlign={'end'}
                                                                >
                                                                    {item[header]}
                                                                </Typography>
                                                            </Grid>
                                                        </Grid>
                                                    ))}
                                                </Stack>
                                            </Card>
                                        ))}
                                </Stack>
                            )}
                            {data.length === 0 && <NoData />}
                        </Box>
                    }

                </Box>
            }
            {isLoadError && (
                <>
                    <InternalServerError />
                </>
            )}
        </>
    );
};

export default TableReportComponent;

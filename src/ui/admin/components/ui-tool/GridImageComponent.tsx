import { FC, useState } from "react";
import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Paper,
  Skeleton,
  Stack,
  TablePagination,
  TableProps,
  Typography,
} from "@mui/material";
import NoData from "../NoData";
import InternalServerError from "../InternalServerError";
import { Link } from "react-router-dom";
const defaultLimit = parseInt(import.meta.env.VITE_DEFAULT_LIMIT) || 20;
const endpoint = import.meta.env.VITE_API_ENDPOINT || "";
import DownloadIcon from "@mui/icons-material/Download";
import EditIcon from "@mui/icons-material/Edit";
import IconButton from "@mui/material/IconButton";
import AlertDialog from "./AlertDialog";
import ModalImage from "./ModalImage";

interface Props extends TableProps {
  title: string;
  isLoading: boolean;
  isLoadSuccess: boolean;
  isLoadError: boolean;
  data: any[];
  all: number;
  rowsPerPage: number;
  page: number;
  handleChangeTablePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerTablePage: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
  openDialog?: boolean;
  handleOpenUpdateDrawer: (
    event: React.MouseEvent<HTMLButtonElement>,
    ticketId: string,
  ) => void;
  handleCloseDialog?: () => void;
  imageId?: number;
  apiOfImg: string;
  keyImg: string;
  keyNameImg: string;
  // deleteImage: (documentIdForDelete: number) => void;
  // isLoadingDelete: boolean;
}

const GridImageComponent: FC<Props> = ({
  title,
  isLoading,
  isLoadSuccess,
  isLoadError,
  data,
  all,
  page,
  rowsPerPage,
  handleChangeTablePage,
  handleChangeRowsPerTablePage,
  handleOpenUpdateDrawer,
  apiOfImg,
  keyImg,
  keyNameImg
}) => {
  const [openModalImg, setOpenModalImg] = useState(false);
  const handleCloseModalImg = () => setOpenModalImg(false);
  const [imgModal, setImgModal] = useState("");

  const handleClickImgDocument = (srcImg: string) => {
    setImgModal(srcImg);
    setOpenModalImg(true);
  };

  const downloadFileAtURL = (url: string) => {
    fetch(url)
      .then((response) => response.blob())
      .then((blob) => {
        const blobURL = window.URL.createObjectURL(new Blob([blob]));
        const fileName: any = url.split("/").pop();
        const aTag = document.createElement("a");
        aTag.href = blobURL;
        aTag.setAttribute("download", fileName);
        document.body.appendChild(aTag);
        aTag.click();
        aTag.remove();
      });
  };

  return (
    <>
      {isLoading && (
        <Paper style={{ padding: 5 }}>
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
        </Paper>
      )}
      {isLoadSuccess && (
        <Box>
          <ModalImage
            openModalImg={openModalImg}
            handleCloseModalImg={handleCloseModalImg}
            imgModal={imgModal}
          />
          <Typography variant="h5" marginBottom={"20px"}>
            {title}
          </Typography>
          <Grid container rowSpacing={2} spacing={2}>
            {data.map((item, index) => (
              <Grid item xl={2} lg={3} md={3} sm={4} xs={6} key={index}>
                <Card
                  sx={{
                    maxWidth: 345,
                    padding: "10px 10px",
                    position: "relative",
                    boxShadow: "0px 0px 3px 0px rgba(0,0,0,0.30)",
                  }}
                >
                  <Stack
                    direction={"row"}
                    justifyContent={"flex-end"}
                    marginBottom={"10px"}
                  >
                    <Link
                      to={`#`}
                      onClick={() =>
                        downloadFileAtURL(
                          `${endpoint}/${apiOfImg}=${item[keyImg]}`
                        )
                      }
                    >
                      <IconButton color="success" aria-label="download">
                        <DownloadIcon />
                      </IconButton>
                    </Link>
                    <IconButton
                      onClick={(e) => handleOpenUpdateDrawer(e, item._id)}
                      color="primary"
                      aria-label="edit"
                    >
                      <EditIcon />
                    </IconButton>
                  </Stack>
                  <CardActionArea>
                    <CardMedia
                      component="img"
                      height="120"
                      image={`${endpoint}/${apiOfImg}=${item[keyImg]}`}
                      alt="green iguana"
                      sx={{ objectFit: "contain" }}
                      onClick={() =>
                        handleClickImgDocument(
                          `${endpoint}/${apiOfImg}=${item[keyImg]}`
                        )
                      }
                    />
                  </CardActionArea>
                  <CardContent>
                    <Typography
                      variant="body1"
                      component="div"
                      // textAlign={"center"}
                    >
                      {item[keyNameImg]}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>

          <TablePagination
            rowsPerPageOptions={[defaultLimit, 50, 100]}
            component={"div"}
            count={all}
            rowsPerPage={rowsPerPage}
            page={page}
            labelRowsPerPage=""
            onPageChange={handleChangeTablePage}
            onRowsPerPageChange={handleChangeRowsPerTablePage}
          />
          {data.length === 0 && <NoData />}
        </Box>
      )}
      {isLoadError && (
        <>
          <InternalServerError />
        </>
      )}
    </>
  );
};

export default GridImageComponent;

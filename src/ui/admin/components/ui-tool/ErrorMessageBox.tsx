import { Box, Typography } from "@mui/material";
import * as React from "react";
import CancelIcon from "@mui/icons-material/Cancel";
const ErrorMessageBox = () => {
  return (
    <>
            <Box
              sx={{
                width: "100%",
                backgroundColor: "rgb(255, 236, 240)",
                textAlign: "center",
                padding: "10px",
              }}
            >
              <CancelIcon sx={{ fontSize: "100px", color: "#ff1744" }} />
              <Typography variant="h5" color="#ff1744" marginBottom={"5px"}>
                ເກີດຂໍ້ຜິດພາດ 404
              </Typography>
              <Typography variant="body2" color="rgb(102, 27, 42)">
                ຂໍອະໄພເນື່ອງຈາກເກີດຂໍ້ຜິດພາດ
                ບໍ່ສາມາດນຳຂໍ້ມູນມາສະແດງໄດ້ເນື່ອງຈາກລະບົບ ຫຼື
                ເກີດການຂັດຄ້ອງໃນການເຊື່ອມຕໍ່ ກະລຸນາລອງໃໝ່ອີກຄັ້ງໃນພາຍຫຼັງ
              </Typography>
            </Box>
          </>
  )
}

export default ErrorMessageBox
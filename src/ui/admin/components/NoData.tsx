import { styled } from "@mui/material";
import { FC } from "react";

const Img = styled('img')({
    width: '300px',
    display: 'block',
    marginInline: 'auto'
});

const NoData: FC = () => {
    return (
        <>
            <Img
                src={'/images/no_data.jpg'}
            />
        </>
    );
};

export default NoData;
import { ElementType, FC, memo, ReactNode, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import {
  Drawer as MuiDrawer,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Collapse,
  ListItem,
  ListSubheader,
  Button,
  Divider,
  AppBar,
  AppBarProps,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import themeSetting from '../../config/theme/themeConfig';
import navigations, { Navigation } from './navigations';
import BoxScrollbar from './BoxScrollbar';
import { useProfile } from '../../hooks/useProfile';
import { IUser } from '../../../domain/profile';

const DrawerHeader = styled(AppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-start',
  paddingTop: '10px',
  paddingBottom: '10px',
}));
const NavList = styled(List, {
  shouldForwardProp: (prop) => prop !== 'item',
})<{
  component?: ElementType;
  item?: boolean;
}>(({ item }) => ({
  '& .MuiListItemButton-root': {
    paddingLeft: 10,
    paddingRight: 10,
  },
  '& .MuiListItemIcon-root': {
    minWidth: 0,
    marginRight: 20,
  },
  '& .MuiSvgIcon-root': {
    fontSize: 20,
  },
  background: 'background.paper',
  ...(item && {
    '& .MuiListItemButton-root': {
      paddingLeft: 20,
      paddingRight: 10,
    },
  }),
}));

const ItemMenu = styled(ListItem, {
  shouldForwardProp: (prop) => prop !== 'active',
})<{
  component?: ElementType;
  active?: boolean;
}>(({ active }) => ({
  color: 'rgba(0, 0, 0, 0.87)',
  paddingBlock: 0,
  ...(active && {
    backgroundColor: 'rgb(230, 244, 255)',
    borderRight:"2px solid rgb(22, 119, 255)",
    '& .MuiListItemText-root ': {
      color: '#f8c311',
    },
    '& .MuiSvgIcon-root': {
      color: 'rgb(22, 119, 255) ',
    },
    '& .MuiTypography-root': {
      color:"rgb(22, 119, 255)"
    }
  }),
}));

const Img = styled('img')({
  width: '155px',
  height:"80px",
  objectFit:"cover"
});

interface Props {
  openSidebar: boolean;
  onChangeSidebar: () => void;
  breakpoint: boolean;
}

const Sidebar: FC<Props> = ({ openSidebar, onChangeSidebar, breakpoint }) => {
  const {
    profileState
  } = useProfile();
  const { profile } = profileState;

  return (
    <MuiDrawer
      color={themeSetting.sidebar.color}
      sx={{
        width: themeSetting.sidebar.drawerWidth,
        minWidth: themeSetting.sidebar.drawerWidth,
        maxWidth: themeSetting.sidebar.drawerWidth,
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          width: themeSetting.sidebar.drawerWidth,
          boxSizing: 'border-box',
          boxShadow:"none"
        },
      }}
      PaperProps={{ elevation: 3 }}
      variant={breakpoint ? 'temporary' : 'persistent'}
      anchor='left'
      open={openSidebar}
      ModalProps={{
        keepMounted: true, // Better open performance on mobile.
      }}
      onClose={onChangeSidebar}
    >
      <DrawerHeader
        position='sticky'
        color={themeSetting.sidebar.header.color}
        elevation={themeSetting.sidebar.header.elevation}
        // sx={{ backgroundColor: '#000' }}
      >
        <Img src='/methani.png' alt='logo192' />
      </DrawerHeader>
      {/* <Divider
        sx={{ mx: 3 }}
      /> */}
      <BoxScrollbar
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        <NavList
          component='nav'
          disablePadding
          aria-labelledby='list-menu'
        >
          {profile && navigations.map((nav) => (
            <>
            <Content
              nav={nav}
              key={nav.id}
              profile={profile}
            />
            </>
          ))}
        </NavList>
      </BoxScrollbar>
    </MuiDrawer>
  );
};

interface Nav {
  nav: Navigation;
  profile: IUser;
}

const Content: FC<Nav> = ({ nav, profile }) => {
  const { pathname } = useLocation();
  const [open, setOpen] = useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <>
      <ItemMenu
        disablePadding
        active={nav.to === pathname.slice(0, nav.to?.length)}
      >
        {nav.role.includes(profile.userRole) && (
           <ListItemLink
           onClick={handleClick}
           link={nav.to}
         >
           <ListItemIcon>
             {nav.icon}
           </ListItemIcon>
           <ListItemText
             primary={nav.label}
           />
           {nav.children && (open ? <ExpandLess /> : <ExpandMore />)}
         </ListItemLink>
        )}
       
      </ItemMenu>
      {nav.children && (
        <Collapse
          in={open}
          timeout='auto'
          unmountOnExit
        >
          <NavList
            component='nav'
            aria-labelledby='list-sub-menu'
            item
          >
            {nav.children.map((children, key) => (
              <>
                {children.role.includes(profile.userRole) &&
                  <ItemMenu
                    key={key}
                    active={children.to === pathname.slice(0, children.to?.length)}
                    disablePadding
                  >
                    <ListItemLink
                      onClick={handleClick}
                      link={children.to}
                    >
                      <ListItemIcon>
                        {children.icon} { }
                      </ListItemIcon>
                      <ListItemText
                        primary={children.label}
                      />
                      {children.children && (open ? <ExpandLess /> : <ExpandMore />)}
                    </ListItemLink>
                  </ItemMenu>
                }
              </>
            ))}
          </NavList>
        </Collapse>
      )}
    </>
  );
};

interface PropsListItemLink {
  children: ReactNode;
  link?: string;
  onClick?: () => void;
}

const ListItemLink: FC<PropsListItemLink> = ({ children, onClick, link }) => {
  if (link) {
    return (
      <ListItemButton
        component={Link}
        to={link}
        sx={{ py: 1, minHeight: 32 }}
      >
        {children}
      </ListItemButton>
    );
  }
  return (
    <ListItemButton
      component={Button}
      onClick={onClick}
      sx={{ py: 1, minHeight: 32 }}
    >
      {children}
    </ListItemButton>
  );
};
export default memo(Sidebar);

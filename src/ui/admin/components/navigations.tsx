import { ReactNode } from "react";
import InventoryIcon from "@mui/icons-material/Inventory";
import {
  PeopleAlt,
  CoPresent,
  MeetingRoom,
  Settings,
  Receipt,
  FormatListNumberedRounded,
  Assessment,
  ReceiptLong,
  AttachMoney,
  School,
  Article,
  AppRegistration,
  FiberNew,
  HowToReg,
  Info
} from "@mui/icons-material";

export interface Navigation {
  id: string;
  name: string;
  label: string;
  icon?: ReactNode;
  to?: string;
  role: string[];
  children?: Navigation[];
}

const navigations: Array<Navigation> = [
  {
    id: 'report',
    name: 'report',
    icon: <Receipt />,
    role: ['ADMIN'],
    label: 'ຈັດການຂໍ້ມູນພື້ນຖານ',
    children: [
      {
        id: "student",
        name: "student",
        label: "ຂໍ້ມູນນັກຮຽນ",
        role: ["ADMIN"],
        icon: <PeopleAlt />,
        to: "/student",
      },
      {
        id: "teacher",
        name: "teacher",
        label: "ຂໍ້ມູນອາຈານ",
        role: ["ADMIN"],
        icon: <CoPresent />,
        to: "/teacher",
      },
      {
        id: "classLevel",
        name: "classLevel",
        label: "ຂໍ້ມູນຊັ້ນຮຽນ",
        role: ["ADMIN"],
        icon: <FormatListNumberedRounded />,
        to: "/classLevel",
      },
      {
        id: "classRoom",
        name: "classRoom",
        label: "ຫ້ອງຮຽນ",
        role: ["ADMIN"],
        icon: <MeetingRoom />,
        to: "/classRoom",
      },
      {
        id: "subjectV2",
        name: "subjectV2",
        label: "ຂໍ້ມູນວິຊາທີ່ຮຽນ",
        role: ["ADMIN"],
        icon: <Article />,
        to: "/subjectV2",
      },
      {
        id: "academicYear",
        name: "academicYear",
        label: "ສົກຮຽນ",
        role: ["ADMIN"],
        icon: <School />,
        to: "/academicYear",
      },
      {
        id: "schoolFee",
        name: "schoolFee",
        label: "ຂໍ້ມູນຄ່າທຳນຽມ",
        role: ["ADMIN"],
        icon: <AttachMoney />,
        to: "/schoolFee",
      },
    ]
  },
  {
    id: 'registration',
    name: 'registration',
    icon: <AppRegistration/>,
    role: ['ADMIN'],
    label: 'ການລົງທະບຽນ',
    children: [
      {
        id: "registrationNewStudent",
        name: "registrationNewStudent",
        label: "ນັກຮຽນໃໝ່",
        role: ["ADMIN"],
        icon: <FiberNew />,
        to: "/registrationNewStudent",
      },
      {
        id: "registrationOldStudent",
        name: "registrationOldStudent",
        label: "ນັກຮຽນເກົ່າ",
        role: ["ADMIN"],
        icon: <HowToReg />,
        to: "/registrationOldStudent",
      },
    ]
  },
  {
    id: 'registration',
    name: 'registration',
    icon: <Info/>,
    role: ['ADMIN', 'TEACHER'],
    label: 'ຕິດຕາມ',
    children: [
      {
        id: "absence_student",
        name: "absence_student",
        label: "ຕິດຕາມນັກຮຽນ",
        role: ["ADMIN", "TEACHER"],
        icon: <PeopleAlt />,
        to: "/absence_student",
      },
      {
        id: "teacherAbsence",
        name: "teacherAbsence",
        label: "ຕິດຕາມອາຈານ",
        role: ["ADMIN"],
        icon: <PeopleAlt />,
        to: "/teacherAbsence",
      },
    ]
  },
  {
    id: 'registration',
    name: 'registration',
    icon: <Assessment/>,
    role: ['ADMIN', 'TEACHER'],
    label: 'ປະເມີນຜົນ',
    children: [
      {
        id: "assessment_student",
        name: "assessment_student",
        label: "ປະເມີນນັກຮຽນ",
        role: ["ADMIN", "TEACHER"],
        icon: <PeopleAlt />,
        to: "/assessment_student",
      },
      {
        id: "assessment_teacher",
        name: "assessment_teacher",
        label: "ປະເມີນອາຈານ",
        role: ["ADMIN", "TEACHER"],
        icon: <CoPresent />,
        to: "/assessment_teacher",
      },
    ]
  },
  // {
  //   id: "class_room",
  //   name: "class_room",
  //   label: "ຫ້ອງຮຽນ",
  //   role: ["ADMIN", "TEACHER"],
  //   icon: <MeetingRoom />,
  //   to: "/class_room",
  // },
  {
    id: "score",
    name: "score",
    label: "ຄະແນນເສັງ",
    role: ["ADMIN", "TEACHER"],
    icon: <ReceiptLong />,
    to: "/score",
  },
  {
    id: "userv2",
    name: "userv2",
    label: "ຂໍ້ມູນຜູ້ໃຊ້",
    role: ["ADMIN"],
    icon: <Settings />,
    to: "/userv2",
  },
  // {
  //   id: "claim",
  //   name: "claim",
  //   label: "ສົ່ງເຄື່ອງຄືນ",
  //   role: ["ADMIN", "TEACHER"],
  //   icon: <LowPriority />,
  //   to: "/claim",
  // },
  // {
  //   id: "add-stock",
  //   name: "add-stock",
  //   label: "ນຳເຂົ້າເຄື່ອງ",
  //   role: ["ADMIN", "TEACHER"],
  //   icon: <Archive />,
  //   to: "/add-stocks",
  // },
  // {
  //   id: "report",
  //   name: "report",
  //   icon: <Receipt />,
  //   role: ["ADMIN", "TEACHER"],
  //   label: "ລາຍງານ",
  //   children: [
  //     {
  //       id: "product",
  //       name: "Product",
  //       label: "ລາຍງານການເບີກເຄື່ອງ",
  //       role: ["ADMIN", "TEACHER"],
  //       icon: <Unarchive />,
  //       to: "/report/product-pickUp",
  //     },
  //     {
  //       id: "product",
  //       name: "Product",
  //       label: "ລາຍງານການສົ່ງເຄື່ອງຄືນ",
  //       role: ["ADMIN", "TEACHER"],
  //       icon: <LowPriority />,
  //       to: "/report/product-claim",
  //     },
  //     {
  //       id: "product_stock_report",
  //       name: "product_stock_report",
  //       label: "ລາຍງານການນຳເຂົ້າສິນຄ້າ",
  //       role: ["ADMIN", "TEACHER"],
  //       icon: <Archive />,
  //       to: "/report/product-stock",
  //     },
  //   ],
  // },
  // {
  //   id: "setting",
  //   name: "setting",
  //   icon: <Settings />,
  //   role: ["ADMIN", "TEACHER"],
  //   label: "ຕັ້ງຄ່າ",
  //   children: [
  //     {
  //       id: "product",
  //       name: "Product",
  //       label: "ລາຍການເຄື່ອງ",
  //       role: ["ADMIN", "TEACHER"],
  //       icon: <InventoryIcon />,
  //       to: "/setting/product",
  //     },
  //     {
  //       id: "store",
  //       name: "store",
  //       label: "ສະໜາມ/ຮ້ານຄ້າ",
  //       role: ["ADMIN", "TEACHER"],
  //       icon: <Store />,
  //       to: "/setting/store",
  //     },
  //     {
  //       id: "user",
  //       name: "user",
  //       label: "Users",
  //       role: ["ADMIN"],
  //       icon: <People />,
  //       to: "/setting/users",
  //     },
  //   ],
  // },
];
export default navigations;

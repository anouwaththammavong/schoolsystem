import { styled } from "@mui/material";
import { FC } from "react";

const Img = styled('img')(({ theme }) => ({
    width: '700px',
    display: 'block',
    marginInline: 'auto',
    [theme.breakpoints.down('sm')]: {
        width: '300px',
    },
}))

const NotFoundImage: FC = () => {
    return (
        <>
            <Img
                src={'/images/404.png'}
            />
        </>
    );
};

export default NotFoundImage;
import Login from "../auth/login/Login";
import UsersPage from "../admin/container/userv1/UsersPage";
import ProfilePage from "../admin/container/profile/ProfilePage";
import StudentPage from "../admin/container/student/StudentPage";
import TeacherPage from "../admin/container/teacher/TeacherPage";
// import ClassRoomPage from "../admin/container/class_room/ClassRoomPage";
import StudentAbsencePage from "../admin/container/studentAbsence/StudentAbsencePage";
import StudentAbsenceDetailsPage from "../admin/container/studentAbsence/studentAbsenceDetails/StudentAbsenceDetailsPage";
import TeacherAbsenceDetailsPage from "../admin/container/teacher/teacherAbsenceDetails/TeacherAbsenceDetailsPage";
import StudentAssessmentPage from "../admin/container/studentAssessment/StudentAssessmentPage";
import ScorePage from "../admin/container/score/ScorePage";
import ClassLevelPage from "../admin/container/classLevel/ClassLevelPage";
import ClassRoomPage from "../admin/container/classRoom/ClassRoomPage";
import AcademicYearPage from "../admin/container/academicYear/AcademicYearPage";
import SchoolFeePage from "../admin/container/schoolFee/SchoolFeePage";
import SubjectV2Page from "../admin/container/subjectV2/SubjectV2Page";
import RegistrationNewStudentPage from "../admin/container/registrationForNewStudent/RegistrationNewStudentPage";
import RegistrationOldStudentPage from "../admin/container/registrationForOldStudent/RegistrationOldStudentPage";
import TeacherAssessmentPage from "../admin/container/teacherAssessment/TeacherAssessmentPage";
import UserPage from "../admin/container/userv2/UserPage";
import TeacherAbsencePage from "../admin/container/teacherAbsence/TeacherAbsencePage";

export type LayoutKey = "ADMIN" | "AUTH";
export interface Router {
  component: JSX.Element;
  name: string;
  id: string;
  path: string;
  index?: boolean;
  role?: string[];
}
export type Routes = {
  [key in LayoutKey]: Array<Router>;
};

const routes: Routes = {
  AUTH: [
    {
      component: <Login />,
      name: "login",
      id: "login",
      path: "/login",
    },
    {
      component: <></>,
      name: "reset password",
      id: "reset-password",
      path: "/reset-password",
    },
  ],
  ADMIN: [
    {
      component: <UsersPage />,
      name: "user",
      id: "user",
      path: "/setting/users",
      role: ["ADMIN"]
    },
    {
      component: <ProfilePage />,
      name: "profile",
      id: "profile",
      path: "/profile",
      role: ["ADMIN", "TEACHER"]
    },
    {
      component: <StudentPage />,
      name: "student",
      id: "student",
      path: "/student",
      role: ["ADMIN"]
    },
    {
      component: <TeacherPage/>,
      name: "teacher",
      id: "teacher",
      path: "/teacher",
      role: ["ADMIN"]
    },
    {
      component: <ClassLevelPage/>,
      name: "classLevel",
      id: "classLevel",
      path: "/classLevel",
      role: ["ADMIN"]
    },
    {
      component: <ClassRoomPage/>,
      name: "classRoom",
      id: "classRoom",
      path: "/classRoom",
      role: ["ADMIN"]
    },
    {
      component: <SubjectV2Page/>,
      name: "subjectV2",
      id: "subjectV2",
      path: "/subjectV2",
      role: ["ADMIN"]
    },
    {
      component: <AcademicYearPage/>,
      name: "academicYear",
      id: "academicYear",
      path: "/academicYear",
      role: ["ADMIN"]
    },
    {
      component: <SchoolFeePage/>,
      name: "schoolFee",
      id: "schoolFee",
      path: "/schoolFee",
      role: ["ADMIN"]
    },
    {
      component: <RegistrationNewStudentPage/>,
      name: "registrationNewStudent",
      id: "registrationNewStudent",
      path: "/registrationNewStudent",
      role: ["ADMIN"]
    },
    {
      component: <RegistrationOldStudentPage/>,
      name: "registrationOldStudent",
      id: "registrationOldStudent",
      path: "/registrationOldStudent",
      role: ["ADMIN"]
    },
    // {
    //   component: <ClassRoomPage/>,
    //   name: "class_room",
    //   id: "class_room",
    //   path: "/class_room",
    //   role: ["ADMIN", "TEACHER"]
    // },
    {
      component: <StudentAbsencePage/>,
      name: "absence_student",
      id: "absence_student",
      path: "/absence_student",
      role: ["ADMIN", "TEACHER"],
    },
    {
      component: <StudentAbsenceDetailsPage />,
      name: "absence_student",
      id: "absence_student",
      path: "/absence_student/student/:student_id/room_id/:room_id/studentAbsenceDetails",
      role: ["ADMIN", "TEACHER"]
    },
    {
      component: <StudentAssessmentPage/>,
      name: "assessment_student",
      id: "assessment_student",
      path: "/assessment_student",
      role: ["ADMIN", "TEACHER"],
    },
    {
      component: <TeacherAssessmentPage/>,
      name: "assessment_teacher",
      id: "assessment_teacher",
      path: "/assessment_teacher",
      role: ["ADMIN", "TEACHER"],
    },
    {
      component: <TeacherAbsencePage/>,
      name: "teacherAbsence",
      id: "teacherAbsence",
      path: "/teacherAbsence",
      role: ["ADMIN"]
    },
    {
      component: <TeacherAbsenceDetailsPage />,
      name: "absence_teacher",
      id: "absence_teacher",
      path: "/absence_teacher/teacher/:teacher_id/teacherAbsenceDetails",
      role: ["ADMIN", "TEACHER"]
    },
    {
      component: <ScorePage />,
      name: "score",
      id: "score",
      path: "/score",
      role: ["ADMIN", "TEACHER"]
    },
    {
      component: <UserPage />,
      name: "userv2",
      id: "userv2",
      path: "/userv2",
      role: ["ADMIN"]
    },
  ],
};
export default routes;

import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  CreateStudentErrorAction,
  createStudentRequestAction,
  createStudentSuccessAction,
  deleteStudentErrorAction,
  deleteStudentRequestAction,
  deleteStudentSuccessAction,
  getStudentsErrorAction,
  getStudentsRequestAction,
  getStudentsSuccessAction,
  updateStudentErrorAction,
  updateStudentRequestAction,
  updateStudentSuccessAction,
} from "./Reducer";
import {
  createStudentService,
  deleteStudentService,
  getStudentService,
  updateStudentService,
} from "../../service/Student";
import { StudentActionTypes } from "../../../domain/Student";

type ErrorMessage = {
  error: string;
}

function* getStudent({
  payload,
}: ReturnType<typeof getStudentsRequestAction>): Generator {
  try {
    const res = (yield call(getStudentService, payload)) as AxiosResponse;
    yield put(getStudentsSuccessAction({ students: res.data }));
  } catch (err) {
    yield put(getStudentsErrorAction({ message: "404" }));
  }
}

function* createStudent({
  payload,
}: ReturnType<typeof createStudentRequestAction>): Generator {
  try {
    const res = (yield call(createStudentService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createStudentSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(CreateStudentErrorAction({ message: error.response?.data.error }));
    }
  }
}

function* updateStudent({
  payload,
}: ReturnType<typeof updateStudentRequestAction>): Generator {
  try {
    const res = (yield call(updateStudentService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateStudentSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      console.log(error.response?.data.error)
      yield put(updateStudentErrorAction({ message: error.response?.data.error}));
    }
  }
}

function* deleteStudent({
  payload,
}: ReturnType<typeof deleteStudentRequestAction>): Generator {
  try {
    const res = (yield call(deleteStudentService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteStudentSuccessAction());
    }
  } catch (err) {
    yield put(deleteStudentErrorAction({ message: "404" }));
  }
}

function* watchGetStudents(): Generator {
  yield takeLatest(StudentActionTypes.GET_STUDENTS_REQUEST, getStudent);
}

function* watchCreateStudents(): Generator {
  yield takeLatest(StudentActionTypes.CREATE_STUDENTS_REQUEST, createStudent);
}

function* watchUpdateStudents(): Generator {
  yield takeLatest(StudentActionTypes.UPDATE_STUDENTS_REQUEST, updateStudent);
}

function* watchDeleteStudents(): Generator {
  yield takeLatest(StudentActionTypes.DELETE_STUDENTS_REQUEST, deleteStudent);
}

export function* studentSaga(): Generator {
  yield all([fork(watchGetStudents)]);
  yield all([fork(watchCreateStudents)]);
  yield all([fork(watchUpdateStudents)]);
  yield all([fork(watchDeleteStudents)]);
}

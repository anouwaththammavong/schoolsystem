import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateStudent,
  StudentActionTypes,
  Students,
  initialState,
} from "../../../domain/Student";

// Get
const getStudentsRequestAction = createAction<{
  limit: number;
  offset: number;
  student_id?: string;
  generation?: string;
  level_id?: string;
  new_student?: string;
}>(StudentActionTypes.GET_STUDENTS_REQUEST);
const getStudentsSuccessAction = createAction<
  { students: Students },
  StudentActionTypes.GET_STUDENTS_SUCCESS
>(StudentActionTypes.GET_STUDENTS_SUCCESS);
const getStudentsErrorAction = createAction<
  { message: string },
  StudentActionTypes.GET_STUDENTS_ERROR
>(StudentActionTypes.GET_STUDENTS_ERROR);

// Create Student
const createStudentRequestAction = createAction<
  { student: CreateStudent },
  StudentActionTypes.CREATE_STUDENTS_REQUEST
>(StudentActionTypes.CREATE_STUDENTS_REQUEST);
const createStudentSuccessAction = createAction(
  StudentActionTypes.CREATE_STUDENTS_SUCCESS
);
const CreateStudentErrorAction = createAction<
  { message: string },
  StudentActionTypes.CREATE_STUDENTS_ERROR
>(StudentActionTypes.CREATE_STUDENTS_ERROR);
const createStudentResetAction = createAction(
  StudentActionTypes.CREATE_STUDENTS_RESET
);

// Update Student
const updateStudentRequestAction = createAction<
  { _id: string; student: CreateStudent },
  StudentActionTypes.UPDATE_STUDENTS_REQUEST
>(StudentActionTypes.UPDATE_STUDENTS_REQUEST);
const updateStudentSuccessAction = createAction(
  StudentActionTypes.UPDATE_STUDENTS_SUCCESS
);
const updateStudentErrorAction = createAction<
  { message: string },
  StudentActionTypes.UPDATE_STUDENTS_ERROR
>(StudentActionTypes.UPDATE_STUDENTS_ERROR);
const updateStudentResetAction = createAction(
  StudentActionTypes.UPDATE_STUDENTS_RESET
);

// Delete Student
const deleteStudentRequestAction = createAction<
  { _id: string },
  StudentActionTypes.DELETE_STUDENTS_REQUEST
>(StudentActionTypes.DELETE_STUDENTS_REQUEST);
const deleteStudentSuccessAction = createAction(
  StudentActionTypes.DELETE_STUDENTS_SUCCESS
);
const deleteStudentErrorAction = createAction<
  { message: string },
  StudentActionTypes.DELETE_STUDENTS_ERROR
>(StudentActionTypes.DELETE_STUDENTS_ERROR);
const deleteStudentResetAction = createAction(
  StudentActionTypes.DELETE_STUDENTS_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Student
    .addCase(getStudentsRequestAction, (state) => {
      (state.students = {
        students: {
          students: [],
          totalCount: 0,
          uniqueGenerations: [],
        },
      }),
        (state.isGetStudentsLoading = true);
      state.isGetStudentsSuccess = false;
      state.getStudentsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getStudentsSuccessAction, (state, action) => {
      state.students = action.payload.students;
      state.isGetStudentsLoading = false;
      state.isGetStudentsSuccess = true;
      state.getStudentsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getStudentsErrorAction, (state, action) => {
      (state.students = {
        students: {
          students: [],
          totalCount: 0,
          uniqueGenerations: [],
        },
      }),
        (state.isGetStudentsLoading = false);
      state.isGetStudentsSuccess = false;
      state.getStudentsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create Student
    .addCase(createStudentRequestAction, (state) => {
      state.isCreateStudentLoading = true;
      state.isCreateStudentSuccess = false;
      state.CreateStudentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createStudentSuccessAction, (state) => {
      state.isCreateStudentLoading = false;
      state.isCreateStudentSuccess = true;
      state.CreateStudentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(CreateStudentErrorAction, (state, action) => {
      state.isCreateStudentLoading = false;
      state.isCreateStudentSuccess = false;
      state.CreateStudentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createStudentResetAction, (state) => {
      state.isCreateStudentLoading = false;
      state.isCreateStudentSuccess = false;
      state.CreateStudentError = {
        isError: false,
        message: undefined,
      };
    })
    // Update Student
    .addCase(updateStudentRequestAction, (state) => {
      state.isUpdateStudentLoading = true;
      state.isUpdateStudentSuccess = false;
      state.updateStudentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateStudentSuccessAction, (state) => {
      state.isUpdateStudentLoading = false;
      state.isUpdateStudentSuccess = true;
      state.updateStudentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateStudentErrorAction, (state, action) => {
      state.isUpdateStudentLoading = false;
      state.isUpdateStudentSuccess = false;
      state.updateStudentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateStudentResetAction, (state) => {
      state.isUpdateStudentLoading = false;
      state.isUpdateStudentSuccess = false;
      state.updateStudentError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete Student
    .addCase(deleteStudentRequestAction, (state) => {
      state.isDeleteStudentLoading = true;
      state.isDeleteStudentSuccess = false;
      state.deleteStudentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteStudentSuccessAction, (state) => {
      state.isDeleteStudentLoading = false;
      state.isDeleteStudentSuccess = true;
      state.deleteStudentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteStudentErrorAction, (state, action) => {
      state.isDeleteStudentLoading = false;
      state.isDeleteStudentSuccess = false;
      state.deleteStudentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteStudentResetAction, (state) => {
      state.isDeleteStudentLoading = false;
      state.isDeleteStudentSuccess = false;
      state.deleteStudentError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getStudentsRequestAction,
  getStudentsSuccessAction,
  getStudentsErrorAction,
  createStudentRequestAction,
  createStudentSuccessAction,
  CreateStudentErrorAction,
  createStudentResetAction,
  updateStudentRequestAction,
  updateStudentSuccessAction,
  updateStudentErrorAction,
  updateStudentResetAction,
  deleteStudentRequestAction,
  deleteStudentSuccessAction,
  deleteStudentErrorAction,
  deleteStudentResetAction,
};

import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateTeacherAbsenceForm,
  TeacherAbsence,
  TeacherAbsenceActionTypes,
  UpdateTeacherAbsenceForm,
  initialState,
} from "../../../domain/TeacherAbsence";

// GET TEACHER ABSENCE
const getTeacherAbsencesRequestAction = createAction<
  {
    limit: number,
    offset: number,
    teacher_id: string,
    absence_date?: Date,
    be_reasonable?: string,
  },
  TeacherAbsenceActionTypes.GET_TEACHER_ABSENCES_REQUEST
>(TeacherAbsenceActionTypes.GET_TEACHER_ABSENCES_REQUEST);
const getTeacherAbsencesSuccessAction = createAction<
  { teacherAbsences: TeacherAbsence },
  TeacherAbsenceActionTypes.GET_TEACHER_ABSENCES_SUCCESS
>(TeacherAbsenceActionTypes.GET_TEACHER_ABSENCES_SUCCESS);
const getTeacherAbsencesErrorAction = createAction<
  { message: string },
  TeacherAbsenceActionTypes.GET_TEACHER_ABSENCES_ERROR
>(TeacherAbsenceActionTypes.GET_TEACHER_ABSENCES_ERROR);

// Create TEACHER ABSENCE
const createTeacherAbsenceRequestAction = createAction<
  { teacherAbsence: CreateTeacherAbsenceForm },
  TeacherAbsenceActionTypes.CREATE_TEACHER_ABSENCE_REQUEST
>(TeacherAbsenceActionTypes.CREATE_TEACHER_ABSENCE_REQUEST);
const createTeacherAbsenceSuccessAction = createAction(
  TeacherAbsenceActionTypes.CREATE_TEACHER_ABSENCE_SUCCESS
);
const createTeacherAbsenceErrorAction = createAction<
  { message: string },
  TeacherAbsenceActionTypes.CREATE_TEACHER_ABSENCE_ERROR
>(TeacherAbsenceActionTypes.CREATE_TEACHER_ABSENCE_ERROR);
const createTeacherAbsenceResetAction = createAction(
  TeacherAbsenceActionTypes.CREATE_TEACHER_ABSENCE_RESET
);

// Update TEACHER ABSENCE
const updateTeacherAbsenceRequestAction = createAction<
  { _id: string; teacherAbsence: UpdateTeacherAbsenceForm },
  TeacherAbsenceActionTypes.UPDATE_TEACHER_ABSENCE_REQUEST
>(TeacherAbsenceActionTypes.UPDATE_TEACHER_ABSENCE_REQUEST);
const updateTeacherAbsenceSuccessAction = createAction(
  TeacherAbsenceActionTypes.UPDATE_TEACHER_ABSENCE_SUCCESS
);
const updateTeacherAbsenceErrorAction = createAction<
  { message: string },
  TeacherAbsenceActionTypes.UPDATE_TEACHER_ABSENCE_ERROR
>(TeacherAbsenceActionTypes.UPDATE_TEACHER_ABSENCE_ERROR);
const updateTeacherAbsenceResetAction = createAction(
  TeacherAbsenceActionTypes.UPDATE_TEACHER_ABSENCE_RESET
);

// Delete TEACHER ABSENCE
const deleteTeacherAbsenceRequestAction = createAction<
  { _id: string },
  TeacherAbsenceActionTypes.DELETE_TEACHER_ABSENCE_REQUEST
>(TeacherAbsenceActionTypes.DELETE_TEACHER_ABSENCE_REQUEST);
const deleteTeacherAbsenceSuccessAction = createAction(
  TeacherAbsenceActionTypes.DELETE_TEACHER_ABSENCE_SUCCESS
);
const deleteTeacherAbsenceErrorAction = createAction<
  { message: string },
  TeacherAbsenceActionTypes.DELETE_TEACHER_ABSENCE_ERROR
>(TeacherAbsenceActionTypes.DELETE_TEACHER_ABSENCE_ERROR);
const deleteTeacherAbsenceResetAction = createAction(
  TeacherAbsenceActionTypes.DELETE_TEACHER_ABSENCE_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // GET ALL TEACHER ABSENCE
    .addCase(getTeacherAbsencesRequestAction, (state) => {
      (state.teacherAbsences = {
        teacherAbsence: {
          absences: [],
          totalAbsences: 0,
          totalBe_reasonable: 0,
          totalNotBe_reasonable: 0
        },
      }),
        (state.isGetTeacherAbsencesLoading = true);
      state.isGetTeacherAbsencesSuccess = false;
      state.getTeacherAbsencesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getTeacherAbsencesSuccessAction, (state, action) => {
      state.teacherAbsences = action.payload.teacherAbsences;
      state.isGetTeacherAbsencesLoading = false;
      state.isGetTeacherAbsencesSuccess = true;
      state.getTeacherAbsencesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getTeacherAbsencesErrorAction, (state, action) => {
      (state.teacherAbsences = {
        teacherAbsence: {
          absences: [],
          totalAbsences: 0,
          totalBe_reasonable: 0,
          totalNotBe_reasonable: 0
        },
      }),
        (state.isGetTeacherAbsencesLoading = false);
      state.isGetTeacherAbsencesSuccess = false;
      state.getTeacherAbsencesError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create TEACHER ABSENCE
    .addCase(createTeacherAbsenceRequestAction, (state) => {
      state.isCreateTeacherAbsenceLoading = true;
      state.isCreateTeacherAbsenceSuccess = false;
      state.createTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createTeacherAbsenceSuccessAction, (state) => {
      state.isCreateTeacherAbsenceLoading = false;
      state.isCreateTeacherAbsenceSuccess = true;
      state.createTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createTeacherAbsenceErrorAction, (state, action) => {
      state.isCreateTeacherAbsenceLoading = false;
      state.isCreateTeacherAbsenceSuccess = false;
      state.createTeacherAbsenceError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createTeacherAbsenceResetAction, (state) => {
      state.isCreateTeacherAbsenceLoading = false;
      state.isCreateTeacherAbsenceSuccess = false;
      state.createTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    // Update TEACHER ABSENCE
    .addCase(updateTeacherAbsenceRequestAction, (state) => {
      state.isUpdateTeacherAbsenceLoading = true;
      state.isUpdateTeacherAbsenceSuccess = false;
      state.updateTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateTeacherAbsenceSuccessAction, (state) => {
      state.isUpdateTeacherAbsenceLoading = false;
      state.isUpdateTeacherAbsenceSuccess = true;
      state.updateTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateTeacherAbsenceErrorAction, (state, action) => {
      state.isUpdateTeacherAbsenceLoading = false;
      state.isUpdateTeacherAbsenceSuccess = false;
      state.updateTeacherAbsenceError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateTeacherAbsenceResetAction, (state) => {
      state.isUpdateTeacherAbsenceLoading = false;
      state.isUpdateTeacherAbsenceSuccess = false;
      state.updateTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete TEACHER ABSENCE
    .addCase(deleteTeacherAbsenceRequestAction, (state) => {
      state.isDeleteTeacherAbsenceLoading = true;
      state.isDeleteTeacherAbsenceSuccess = false;
      state.deleteTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteTeacherAbsenceSuccessAction, (state) => {
      state.isDeleteTeacherAbsenceLoading = false;
      state.isDeleteTeacherAbsenceSuccess = true;
      state.deleteTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteTeacherAbsenceErrorAction, (state, action) => {
      state.isDeleteTeacherAbsenceLoading = false;
      state.isDeleteTeacherAbsenceSuccess = false;
      state.deleteTeacherAbsenceError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteTeacherAbsenceResetAction, (state) => {
      state.isDeleteTeacherAbsenceLoading = false;
      state.isDeleteTeacherAbsenceSuccess = false;
      state.deleteTeacherAbsenceError = {
        isError: false,
        message: undefined,
      };
    });
});


export default reducer;
export {
    getTeacherAbsencesRequestAction,
    getTeacherAbsencesSuccessAction,
    getTeacherAbsencesErrorAction,
    createTeacherAbsenceRequestAction,
    createTeacherAbsenceSuccessAction,
    createTeacherAbsenceErrorAction,
    createTeacherAbsenceResetAction,
    updateTeacherAbsenceRequestAction,
    updateTeacherAbsenceSuccessAction,
    updateTeacherAbsenceErrorAction,
    updateTeacherAbsenceResetAction,
    deleteTeacherAbsenceRequestAction,
    deleteTeacherAbsenceSuccessAction,
    deleteTeacherAbsenceErrorAction,
    deleteTeacherAbsenceResetAction
}
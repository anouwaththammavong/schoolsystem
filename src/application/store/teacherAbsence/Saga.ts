import { AxiosError, AxiosResponse } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import { createTeacherAbsenceErrorAction, createTeacherAbsenceRequestAction, createTeacherAbsenceSuccessAction, deleteTeacherAbsenceErrorAction, deleteTeacherAbsenceRequestAction, deleteTeacherAbsenceSuccessAction, getTeacherAbsencesErrorAction, getTeacherAbsencesRequestAction, getTeacherAbsencesSuccessAction, updateTeacherAbsenceErrorAction, updateTeacherAbsenceRequestAction, updateTeacherAbsenceSuccessAction } from "./Reducer";
import { createTeacherAbsenceService, deleteTeacherAbsenceService, getTeacherAbsencesService, updateTeacherAbsenceService } from "../../service/TeacherAbsence";
import { TeacherAbsenceActionTypes } from "../../../domain/TeacherAbsence";

type ErrorMessage = {
  error: string;
}

function* getTeacherAbsences({
    payload,
  }: ReturnType<typeof getTeacherAbsencesRequestAction>): Generator {
    try {
      const res = (yield call(getTeacherAbsencesService, payload)) as AxiosResponse;
      yield put(getTeacherAbsencesSuccessAction({ teacherAbsences: res.data }));
    } catch (err) {
      yield put(getTeacherAbsencesErrorAction({ message: "404" }));
    }
  }

  function* createTeacherAbsence({
    payload,
  }: ReturnType<typeof createTeacherAbsenceRequestAction>): Generator {
    try {
      const res = (yield call(createTeacherAbsenceService, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(createTeacherAbsenceSuccessAction());
      }
    } catch (err) {
      const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(createTeacherAbsenceErrorAction({ message: error.response?.data.error }));
    }
    }
  }

  function* updateTeacherAbsence({
    payload,
  }: ReturnType<typeof updateTeacherAbsenceRequestAction>): Generator {
    try {
      const res = (yield call(updateTeacherAbsenceService, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(updateTeacherAbsenceSuccessAction());
      }
    } catch (err) {
      const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(updateTeacherAbsenceErrorAction({ message: error.response?.data.error }));
    }
    }
  }

  function* deleteTeacherAbsence({
    payload,
  }: ReturnType<typeof deleteTeacherAbsenceRequestAction>): Generator {
    try {
      const res = (yield call(deleteTeacherAbsenceService, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(deleteTeacherAbsenceSuccessAction());
      }
    } catch (err) {
      yield put(deleteTeacherAbsenceErrorAction({ message: "404" }));
    }
  }

  function* watchGetTeacherAbsences(): Generator {
    yield takeLatest(
      TeacherAbsenceActionTypes.GET_TEACHER_ABSENCES_REQUEST,
      getTeacherAbsences
    );
  }
  
  function* watchCreateTeacherAbsence(): Generator {
    yield takeLatest(
      TeacherAbsenceActionTypes.CREATE_TEACHER_ABSENCE_REQUEST,
      createTeacherAbsence
    );
  }
  
  function* watchUpdateTeacherAbsence(): Generator {
    yield takeLatest(
      TeacherAbsenceActionTypes.UPDATE_TEACHER_ABSENCE_REQUEST,
      updateTeacherAbsence
    );
  }
  
  function* watchDeleteTeacherAbsence(): Generator {
    yield takeLatest(
      TeacherAbsenceActionTypes.DELETE_TEACHER_ABSENCE_REQUEST,
      deleteTeacherAbsence
    );
  }

  export function* teacherAbsenceSaga(): Generator {
    yield all([fork(watchGetTeacherAbsences)]);
    yield all([fork(watchCreateTeacherAbsence)]);
    yield all([fork(watchUpdateTeacherAbsence)]);
    yield all([fork(watchDeleteTeacherAbsence)]);
  }
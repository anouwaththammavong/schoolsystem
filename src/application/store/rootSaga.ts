import { all, fork } from "redux-saga/effects";
import { userSaga } from "./user/saga";
import { profileSaga } from "./profile/saga";
import { studentSaga } from "./student/Saga";
import { teacherSaga } from "./teacher/Saga";
import { subjectSaga } from "./subject/Saga";
import { studentAbsenceSaga } from "./studentAbsence/Saga";
import { teacherAbsenceSaga } from "./teacherAbsence/Saga";
import { studentAssessmentSaga } from "./studentAssessment/Saga";
import { scoreSaga } from "./score/Saga";
import { classLevelSaga } from "./classLevel/Saga";
import { classRoomSaga } from "./classRoom/Saga";
import { academicYearSaga } from "./academicYear/Saga";
import { schoolFeeSaga } from "./schoolFee/Saga";
import { subjectV2Saga } from "./subjectV2/Saga";
import { registrationSaga } from "./registration/Saga";
import { teacherAssessmentSaga } from "./teacherAssessment/Saga";

export default function* sagas() {
  yield all([fork(userSaga)]);
  yield all([fork(profileSaga)]);
  yield all([fork(studentSaga)]);
  yield all([fork(teacherSaga)]);
  yield all([fork(subjectSaga)]);
  yield all([fork(studentAbsenceSaga)]);
  yield all([fork(teacherAbsenceSaga)]);
  yield all([fork(studentAssessmentSaga)]);
  yield all([fork(scoreSaga)]);
  yield all([fork(classLevelSaga)]);
  yield all([fork(classRoomSaga)]);
  yield all([fork(academicYearSaga)]);
  yield all([fork(schoolFeeSaga)]);
  yield all([fork(subjectV2Saga)]);
  yield all([fork(registrationSaga)]);
  yield all([fork(teacherAssessmentSaga)]);
}

import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateStudentAbsenceForm,
  StudentAbsence,
  StudentAbsenceActionTypes,
  StudentIdAbsence,
  UpdateStudentAbsenceForm,
  initialState,
} from "../../../domain/StudentAbsence";

// GET ALL STUDENT ABSENCE
const getAllStudentAbsencesRequestAction = createAction<
  {
    limit: number;
    offset: number;
    room_id: string;
    generation: string;
    student_id?: string;
  },
  StudentAbsenceActionTypes.GET_ALL_STUDENT_ABSENCES_REQUEST
>(StudentAbsenceActionTypes.GET_ALL_STUDENT_ABSENCES_REQUEST);
const getAllStudentAbsencesSuccessAction = createAction<
  { studentAbsence: StudentAbsence },
  StudentAbsenceActionTypes.GET_ALL_STUDENT_ABSENCES_SUCCESS
>(StudentAbsenceActionTypes.GET_ALL_STUDENT_ABSENCES_SUCCESS);
const getAllStudentAbsencesErrorAction = createAction<
  { message: string },
  StudentAbsenceActionTypes.GET_ALL_STUDENT_ABSENCES_ERROR
>(StudentAbsenceActionTypes.GET_ALL_STUDENT_ABSENCES_ERROR);

// GET STUDENT ID ABSENCE
const getStudentIdAbsencesRequestAction = createAction<
  {
    limit: number;
    offset: number;
    student_id: string;
    room_id: string;
    absence_date?: Date;
    term?: string;
    be_reasonable?: string;
  },
  StudentAbsenceActionTypes.GET_STUDENT_ID_ABSENCES_REQUEST
>(StudentAbsenceActionTypes.GET_STUDENT_ID_ABSENCES_REQUEST);
const getStudentIdAbsencesSuccessAction = createAction<
  { studentIdAbsence: StudentIdAbsence },
  StudentAbsenceActionTypes.GET_STUDENT_ID_ABSENCES_SUCCESS
>(StudentAbsenceActionTypes.GET_STUDENT_ID_ABSENCES_SUCCESS);
const getStudentIdAbsencesErrorAction = createAction<
  { message: string },
  StudentAbsenceActionTypes.GET_STUDENT_ID_ABSENCES_ERROR
>(StudentAbsenceActionTypes.GET_STUDENT_ID_ABSENCES_ERROR);

// Create STUDENT ABSENCE
const createStudentAbsenceRequestAction = createAction<
  { studentAbsence: CreateStudentAbsenceForm },
  StudentAbsenceActionTypes.CREATE_STUDENT_ABSENCE_REQUEST
>(StudentAbsenceActionTypes.CREATE_STUDENT_ABSENCE_REQUEST);
const createStudentAbsenceSuccessAction = createAction(
  StudentAbsenceActionTypes.CREATE_STUDENT_ABSENCE_SUCCESS
);
const createStudentAbsenceErrorAction = createAction<
  { message: string },
  StudentAbsenceActionTypes.CREATE_STUDENT_ABSENCE_ERROR
>(StudentAbsenceActionTypes.CREATE_STUDENT_ABSENCE_ERROR);
const createStudentAbsenceResetAction = createAction(
  StudentAbsenceActionTypes.CREATE_STUDENT_ABSENCE_RESET
);

// Update STUDENT ABSENCE
const updateStudentAbsenceRequestAction = createAction<
  { _id: string; studentAbsence: UpdateStudentAbsenceForm },
  StudentAbsenceActionTypes.UPDATE_STUDENT_ABSENCE_REQUEST
>(StudentAbsenceActionTypes.UPDATE_STUDENT_ABSENCE_REQUEST);
const updateStudentAbsenceSuccessAction = createAction(
  StudentAbsenceActionTypes.UPDATE_STUDENT_ABSENCE_SUCCESS
);
const updateStudentAbsenceErrorAction = createAction<
  { message: string },
  StudentAbsenceActionTypes.UPDATE_STUDENT_ABSENCE_ERROR
>(StudentAbsenceActionTypes.UPDATE_STUDENT_ABSENCE_ERROR);
const updateStudentAbsenceResetAction = createAction(
  StudentAbsenceActionTypes.UPDATE_STUDENT_ABSENCE_RESET
);

// Delete STUDENT ABSENCE
const deleteStudentAbsenceRequestAction = createAction<
  { _id: string },
  StudentAbsenceActionTypes.DELETE_STUDENT_ABSENCE_REQUEST
>(StudentAbsenceActionTypes.DELETE_STUDENT_ABSENCE_REQUEST);
const deleteStudentAbsenceSuccessAction = createAction(
  StudentAbsenceActionTypes.DELETE_STUDENT_ABSENCE_SUCCESS
);
const deleteStudentAbsenceErrorAction = createAction<
  { message: string },
  StudentAbsenceActionTypes.DELETE_STUDENT_ABSENCE_ERROR
>(StudentAbsenceActionTypes.DELETE_STUDENT_ABSENCE_ERROR);
const deleteStudentAbsenceResetAction = createAction(
  StudentAbsenceActionTypes.DELETE_STUDENT_ABSENCE_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // GET ALL STUDENT ABSENCE
    .addCase(getAllStudentAbsencesRequestAction, (state) => {
      (state.studentAbsence = {
        studentAbsence: {
          studentAbsence: { studentAbsence: [], totalStudentsAbsent: 0 },
        },
      }),
        (state.isGetAllStudentAbsencesLoading = true);
      state.isGetAllStudentAbsencesSuccess = false;
      state.getAllStudentAbsencesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getAllStudentAbsencesSuccessAction, (state, action) => {
      state.studentAbsence = action.payload.studentAbsence;
      state.isGetAllStudentAbsencesLoading = false;
      state.isGetAllStudentAbsencesSuccess = true;
      state.getAllStudentAbsencesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getAllStudentAbsencesErrorAction, (state, action) => {
      (state.studentAbsence = {
        studentAbsence: {
          studentAbsence: { studentAbsence: [], totalStudentsAbsent: 0 },
        },
      }),
        (state.isGetAllStudentAbsencesLoading = false);
      state.isGetAllStudentAbsencesSuccess = false;
      state.getAllStudentAbsencesError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // GET STUDENT ID ABSENCE
    .addCase(getStudentIdAbsencesRequestAction, (state) => {
      (state.studentIdAbsence = {
        studentAbsence: {
          absences: [],
          totalAbsences: 0,
          totalBe_reasonable: 0,
          totalNotBe_reasonable: 0,
        },
      }),
        (state.isGetStudentIdAbsencesLoading = true);
      state.isGetStudentIdAbsencesSuccess = false;
      state.getStudentIdAbsencesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getStudentIdAbsencesSuccessAction, (state, action) => {
      state.studentIdAbsence = action.payload.studentIdAbsence;
      state.isGetStudentIdAbsencesLoading = false;
      state.isGetStudentIdAbsencesSuccess = true;
      state.getStudentIdAbsencesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getStudentIdAbsencesErrorAction, (state, action) => {
      (state.studentIdAbsence = {
        studentAbsence: {
          absences: [],
          totalAbsences: 0,
          totalBe_reasonable: 0,
          totalNotBe_reasonable: 0
        },
      }),
        (state.isGetStudentIdAbsencesLoading = false);
      state.isGetStudentIdAbsencesSuccess = false;
      state.getStudentIdAbsencesError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create STUDENT ABSENCE
    .addCase(createStudentAbsenceRequestAction, (state) => {
        state.isCreateStudentAbsenceLoading = true;
        state.isCreateStudentAbsenceSuccess = false;
        state.createStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      .addCase(createStudentAbsenceSuccessAction, (state) => {
        state.isCreateStudentAbsenceLoading = false;
        state.isCreateStudentAbsenceSuccess = true;
        state.createStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      .addCase(createStudentAbsenceErrorAction, (state, action) => {
        state.isCreateStudentAbsenceLoading = false;
        state.isCreateStudentAbsenceSuccess = false;
        state.createStudentAbsenceError = {
          isError: true,
          message: action.payload.message,
        };
      })
      .addCase(createStudentAbsenceResetAction, (state) => {
        state.isCreateStudentAbsenceLoading = false;
        state.isCreateStudentAbsenceSuccess = false;
        state.createStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      // Update STUDENT ABSENCE
    .addCase(updateStudentAbsenceRequestAction, (state) => {
        state.isUpdateStudentAbsenceLoading = true;
        state.isUpdateStudentAbsenceSuccess = false;
        state.updateStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      .addCase(updateStudentAbsenceSuccessAction, (state) => {
        state.isUpdateStudentAbsenceLoading = false;
        state.isUpdateStudentAbsenceSuccess = true;
        state.updateStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      .addCase(updateStudentAbsenceErrorAction, (state, action) => {
        state.isUpdateStudentAbsenceLoading = false;
        state.isUpdateStudentAbsenceSuccess = false;
        state.updateStudentAbsenceError = {
          isError: true,
          message: action.payload.message,
        };
      })
      .addCase(updateStudentAbsenceResetAction, (state) => {
        state.isUpdateStudentAbsenceLoading = false;
        state.isUpdateStudentAbsenceSuccess = false;
        state.updateStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      // Delete STUDENT ABSENCE
    .addCase(deleteStudentAbsenceRequestAction, (state) => {
        state.isDeleteStudentAbsenceLoading = true;
        state.isDeleteStudentAbsenceSuccess = false;
        state.deleteStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      .addCase(deleteStudentAbsenceSuccessAction, (state) => {
        state.isDeleteStudentAbsenceLoading = false;
        state.isDeleteStudentAbsenceSuccess = true;
        state.deleteStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      })
      .addCase(deleteStudentAbsenceErrorAction, (state, action) => {
        state.isDeleteStudentAbsenceLoading = false;
        state.isDeleteStudentAbsenceSuccess = false;
        state.deleteStudentAbsenceError = {
          isError: true,
          message: action.payload.message,
        };
      })
      .addCase(deleteStudentAbsenceResetAction, (state) => {
        state.isDeleteStudentAbsenceLoading = false;
        state.isDeleteStudentAbsenceSuccess = false;
        state.deleteStudentAbsenceError = {
          isError: false,
          message: undefined,
        };
      });
});

export default reducer;
export {
    getAllStudentAbsencesRequestAction,
    getAllStudentAbsencesSuccessAction,
    getAllStudentAbsencesErrorAction,
    getStudentIdAbsencesRequestAction,
    getStudentIdAbsencesSuccessAction,
    getStudentIdAbsencesErrorAction,
    createStudentAbsenceRequestAction,
    createStudentAbsenceSuccessAction,
    createStudentAbsenceErrorAction,
    createStudentAbsenceResetAction,
    updateStudentAbsenceRequestAction,
    updateStudentAbsenceSuccessAction,
    updateStudentAbsenceErrorAction,
    updateStudentAbsenceResetAction,
    deleteStudentAbsenceRequestAction,
    deleteStudentAbsenceSuccessAction,
    deleteStudentAbsenceErrorAction,
    deleteStudentAbsenceResetAction
}
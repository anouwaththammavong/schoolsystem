import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createStudentAbsenceErrorAction,
  createStudentAbsenceRequestAction,
  createStudentAbsenceSuccessAction,
  deleteStudentAbsenceErrorAction,
  deleteStudentAbsenceRequestAction,
  deleteStudentAbsenceSuccessAction,
  getAllStudentAbsencesErrorAction,
  getAllStudentAbsencesRequestAction,
  getAllStudentAbsencesSuccessAction,
  getStudentIdAbsencesErrorAction,
  getStudentIdAbsencesRequestAction,
  getStudentIdAbsencesSuccessAction,
  updateStudentAbsenceErrorAction,
  updateStudentAbsenceRequestAction,
  updateStudentAbsenceSuccessAction,
} from "./Reducer";
import {
  createStudentAbsenceService,
  deleteStudentAbsenceService,
  getAllStudentAbsencesService,
  getStudentIdAbsencesService,
  updateStudentAbsenceService,
} from "../../service/StudentAbsence";
import { StudentAbsenceActionTypes } from "../../../domain/StudentAbsence";

type ErrorMessage = {
  error: string;
}

function* getAllStudentAbsences({
  payload,
}: ReturnType<typeof getAllStudentAbsencesRequestAction>): Generator {
  try {
    const res = (yield call(
      getAllStudentAbsencesService,
      payload
    )) as AxiosResponse;
    yield put(getAllStudentAbsencesSuccessAction({ studentAbsence: res.data }));
  } catch (err) {
    yield put(getAllStudentAbsencesErrorAction({ message: "404" }));
  }
}

function* getStudentIdAbsences({
  payload,
}: ReturnType<typeof getStudentIdAbsencesRequestAction>): Generator {
  try {
    const res = (yield call(
      getStudentIdAbsencesService,
      payload
    )) as AxiosResponse;
    yield put(
      getStudentIdAbsencesSuccessAction({ studentIdAbsence: res.data })
    );
  } catch (err) {
    yield put(getStudentIdAbsencesErrorAction({ message: "404" }));
  }
}

function* createStudentAbsence({
  payload,
}: ReturnType<typeof createStudentAbsenceRequestAction>): Generator {
  try {
    const res = (yield call(
      createStudentAbsenceService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(createStudentAbsenceSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(createStudentAbsenceErrorAction({ message: error.response?.data.error }));
    }
  }
}

function* updateStudentAbsence({
  payload,
}: ReturnType<typeof updateStudentAbsenceRequestAction>): Generator {
  try {
    const res = (yield call(
      updateStudentAbsenceService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateStudentAbsenceSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(updateStudentAbsenceErrorAction({ message: error.response?.data.error }));
    }
  }
}

function* deleteStudentAbsence({
  payload,
}: ReturnType<typeof deleteStudentAbsenceRequestAction>): Generator {
  try {
    const res = (yield call(
      deleteStudentAbsenceService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteStudentAbsenceSuccessAction());
    }
  } catch (err) {
    yield put(deleteStudentAbsenceErrorAction({ message: "404" }));
  }
}

function* watchGetAllStudentAbsences(): Generator {
  yield takeLatest(
    StudentAbsenceActionTypes.GET_ALL_STUDENT_ABSENCES_REQUEST,
    getAllStudentAbsences
  );
}

function* watchGetStudentIdAbsences(): Generator {
  yield takeLatest(
    StudentAbsenceActionTypes.GET_STUDENT_ID_ABSENCES_REQUEST,
    getStudentIdAbsences
  );
}

function* watchCreateStudentAbsence(): Generator {
  yield takeLatest(
    StudentAbsenceActionTypes.CREATE_STUDENT_ABSENCE_REQUEST,
    createStudentAbsence
  );
}

function* watchUpdateStudentAbsence(): Generator {
  yield takeLatest(
    StudentAbsenceActionTypes.UPDATE_STUDENT_ABSENCE_REQUEST,
    updateStudentAbsence
  );
}

function* watchDeleteStudentAbsence(): Generator {
  yield takeLatest(
    StudentAbsenceActionTypes.DELETE_STUDENT_ABSENCE_REQUEST,
    deleteStudentAbsence
  );
}

export function* studentAbsenceSaga(): Generator {
  yield all([fork(watchGetAllStudentAbsences)]);
  yield all([fork(watchGetStudentIdAbsences)]);
  yield all([fork(watchCreateStudentAbsence)]);
  yield all([fork(watchUpdateStudentAbsence)]);
  yield all([fork(watchDeleteStudentAbsence)]);
}

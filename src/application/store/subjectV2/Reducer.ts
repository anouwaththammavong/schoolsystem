import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  SubjectV2ActionTypes,
  SubjectV2s,
  CreateSubjectV2Form,
  UpdateSubjectV2Form,
  initialState,
} from "../../../domain/SubjectV2";

// Get
const getSubjectV2sRequestAction = createAction<
  {
    limit: number;
    offset: number;
  },
  SubjectV2ActionTypes.GET_SUBJECT_V2S_REQUEST
>(SubjectV2ActionTypes.GET_SUBJECT_V2S_REQUEST);
const getSubjectV2sSuccessAction = createAction<
  { subjectV2s: SubjectV2s },
  SubjectV2ActionTypes.GET_SUBJECT_V2S_SUCCESS
>(SubjectV2ActionTypes.GET_SUBJECT_V2S_SUCCESS);
const getSubjectV2sErrorAction = createAction<
  { message: string },
  SubjectV2ActionTypes.GET_SUBJECT_V2S_ERROR
>(SubjectV2ActionTypes.GET_SUBJECT_V2S_ERROR);

// Create Subject v2
const createSubjectV2RequestAction = createAction<
  { subjectV2: CreateSubjectV2Form },
  SubjectV2ActionTypes.CREATE_SUBJECT_V2_REQUEST
>(SubjectV2ActionTypes.CREATE_SUBJECT_V2_REQUEST);
const createSubjectV2SuccessAction = createAction(
  SubjectV2ActionTypes.CREATE_SUBJECT_V2_SUCCESS
);
const createSubjectV2ErrorAction = createAction<
  { message: string },
  SubjectV2ActionTypes.CREATE_SUBJECT_V2_ERROR
>(SubjectV2ActionTypes.CREATE_SUBJECT_V2_ERROR);
const createSubjectV2ResetAction = createAction(
  SubjectV2ActionTypes.CREATE_SUBJECT_V2_RESET
);

// Update Subject v2
const updateSubjectV2RequestAction = createAction<
  { _id: string; subjectV2: UpdateSubjectV2Form },
  SubjectV2ActionTypes.UPDATE_SUBJECT_V2_REQUEST
>(SubjectV2ActionTypes.UPDATE_SUBJECT_V2_REQUEST);
const updateSubjectV2SuccessAction = createAction(
  SubjectV2ActionTypes.UPDATE_SUBJECT_V2_SUCCESS
);
const updateSubjectV2ErrorAction = createAction<
  { message: string },
  SubjectV2ActionTypes.UPDATE_SUBJECT_V2_ERROR
>(SubjectV2ActionTypes.UPDATE_SUBJECT_V2_ERROR);
const updateSubjectV2ResetAction = createAction(
  SubjectV2ActionTypes.UPDATE_SUBJECT_V2_RESET
);

// Delete Subject v2
const deleteSubjectV2RequestAction = createAction<
  { _id: string },
  SubjectV2ActionTypes.DELETE_SUBJECT_V2_REQUEST
>(SubjectV2ActionTypes.DELETE_SUBJECT_V2_REQUEST);
const deleteSubjectV2SuccessAction = createAction(
  SubjectV2ActionTypes.DELETE_SUBJECT_V2_SUCCESS
);
const deleteSubjectV2ErrorAction = createAction<
  { message: string },
  SubjectV2ActionTypes.DELETE_SUBJECT_V2_ERROR
>(SubjectV2ActionTypes.DELETE_SUBJECT_V2_ERROR);
const deleteSubjectV2ResetAction = createAction(
  SubjectV2ActionTypes.DELETE_SUBJECT_V2_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Subject v2
    .addCase(getSubjectV2sRequestAction, (state) => {
      (state.subjectV2s = {
        subjectV2s: {
          subjectV2s: [],
          totalCount: 0,
        },
      }),
        (state.isGetSubjectV2sLoading = true);
      state.isGetSubjectV2sSuccess = false;
      state.getSubjectV2sError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getSubjectV2sSuccessAction, (state, action) => {
      state.subjectV2s = action.payload.subjectV2s;
      state.isGetSubjectV2sLoading = false;
      state.isGetSubjectV2sSuccess = true;
      state.getSubjectV2sError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getSubjectV2sErrorAction, (state, action) => {
      (state.subjectV2s = {
        subjectV2s: {
          subjectV2s: [],
          totalCount: 0,
        },
      }),
        (state.isGetSubjectV2sLoading = false);
      state.isGetSubjectV2sSuccess = false;
      state.getSubjectV2sError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create Subject v2
    .addCase(createSubjectV2RequestAction, (state) => {
      state.isCreateSubjectV2Loading = true;
      state.isCreateSubjectV2Success = false;
      state.createSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createSubjectV2SuccessAction, (state) => {
      state.isCreateSubjectV2Loading = false;
      state.isCreateSubjectV2Success = true;
      state.createSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createSubjectV2ErrorAction, (state, action) => {
      state.isCreateSubjectV2Loading = false;
      state.isCreateSubjectV2Success = false;
      state.createSubjectV2Error = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createSubjectV2ResetAction, (state) => {
      state.isCreateSubjectV2Loading = false;
      state.isCreateSubjectV2Success = false;
      state.createSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    // Update Subject v2
    .addCase(updateSubjectV2RequestAction, (state) => {
      state.isUpdateSubjectV2Loading = true;
      state.isUpdateSubjectV2Success = false;
      state.updateSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateSubjectV2SuccessAction, (state) => {
      state.isUpdateSubjectV2Loading = false;
      state.isUpdateSubjectV2Success = true;
      state.updateSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateSubjectV2ErrorAction, (state, action) => {
      state.isUpdateSubjectV2Loading = false;
      state.isUpdateSubjectV2Success = false;
      state.updateSubjectV2Error = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateSubjectV2ResetAction, (state) => {
      state.isUpdateSubjectV2Loading = false;
      state.isUpdateSubjectV2Success = false;
      state.updateSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    // Delete SubjectV2
    .addCase(deleteSubjectV2RequestAction, (state) => {
      state.isDeleteSubjectV2Loading = true;
      state.isDeleteSubjectV2Success = false;
      state.deleteSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteSubjectV2SuccessAction, (state) => {
      state.isDeleteSubjectV2Loading = false;
      state.isDeleteSubjectV2Success = true;
      state.deleteSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteSubjectV2ErrorAction, (state, action) => {
      state.isDeleteSubjectV2Loading = false;
      state.isDeleteSubjectV2Success = false;
      state.deleteSubjectV2Error = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteSubjectV2ResetAction, (state) => {
      state.isDeleteSubjectV2Loading = false;
      state.isDeleteSubjectV2Success = false;
      state.deleteSubjectV2Error = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getSubjectV2sRequestAction,
  getSubjectV2sSuccessAction,
  getSubjectV2sErrorAction,
  createSubjectV2RequestAction,
  createSubjectV2SuccessAction,
  createSubjectV2ErrorAction,
  createSubjectV2ResetAction,
  updateSubjectV2RequestAction,
  updateSubjectV2SuccessAction,
  updateSubjectV2ErrorAction,
  updateSubjectV2ResetAction,
  deleteSubjectV2RequestAction,
  deleteSubjectV2SuccessAction,
  deleteSubjectV2ErrorAction,
  deleteSubjectV2ResetAction,
};

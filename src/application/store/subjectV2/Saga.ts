import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createSubjectV2ErrorAction,
  createSubjectV2RequestAction,
  createSubjectV2SuccessAction,
  deleteSubjectV2ErrorAction,
  deleteSubjectV2RequestAction,
  deleteSubjectV2SuccessAction,
  getSubjectV2sErrorAction,
  getSubjectV2sRequestAction,
  getSubjectV2sSuccessAction,
  updateSubjectV2ErrorAction,
  updateSubjectV2RequestAction,
  updateSubjectV2SuccessAction,
} from "./Reducer";
import {
  createSubjectV2Service,
  deleteSubjectV2Service,
  getSubjectV2Service,
  updateSubjectV2Service,
} from "../../service/SubjectV2";
import { SubjectV2ActionTypes } from "../../../domain/SubjectV2";

type ErrorMessage = {
  error: string;
};

function* getSubjectV2({
  payload,
}: ReturnType<typeof getSubjectV2sRequestAction>): Generator {
  try {
    const res = (yield call(getSubjectV2Service, payload)) as AxiosResponse;
    yield put(getSubjectV2sSuccessAction({ subjectV2s: res.data }));
  } catch (err) {
    yield put(getSubjectV2sErrorAction({ message: "404" }));
  }
}

function* createSubjectV2({
  payload,
}: ReturnType<typeof createSubjectV2RequestAction>): Generator {
  try {
    const res = (yield call(createSubjectV2Service, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createSubjectV2SuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createSubjectV2ErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* updateSubjectV2({
    payload,
  }: ReturnType<typeof updateSubjectV2RequestAction>): Generator {
    try {
      const res = (yield call(updateSubjectV2Service, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(updateSubjectV2SuccessAction());
      }
    } catch (err) {
      const error = err as AxiosError<ErrorMessage>;
      if (error.response?.data.error) {
        console.log(error.response?.data.error)
        yield put(updateSubjectV2ErrorAction({ message: error.response?.data.error}));
      }
    }
  }

  function* deleteSubjectV2({
    payload,
  }: ReturnType<typeof deleteSubjectV2RequestAction>): Generator {
    try {
      const res = (yield call(deleteSubjectV2Service, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(deleteSubjectV2SuccessAction());
      }
    } catch (err) {
      yield put(deleteSubjectV2ErrorAction({ message: "404" }));
    }
  }

  function* watchGetSubjectV2s(): Generator {
    yield takeLatest(SubjectV2ActionTypes.GET_SUBJECT_V2S_REQUEST, getSubjectV2);
  }
  
  function* watchCreateSubjectV2(): Generator {
    yield takeLatest(SubjectV2ActionTypes.CREATE_SUBJECT_V2_REQUEST, createSubjectV2);
  }
  
  function* watchUpdateSubjectV2(): Generator {
    yield takeLatest(SubjectV2ActionTypes.UPDATE_SUBJECT_V2_REQUEST, updateSubjectV2);
  }
  
  function* watchDeleteSubjectV2(): Generator {
    yield takeLatest(SubjectV2ActionTypes.DELETE_SUBJECT_V2_REQUEST, deleteSubjectV2);
  }

  export function* subjectV2Saga(): Generator {
    yield all([fork(watchGetSubjectV2s)]);
    yield all([fork(watchCreateSubjectV2)]);
    yield all([fork(watchUpdateSubjectV2)]);
    yield all([fork(watchDeleteSubjectV2)]);
  }
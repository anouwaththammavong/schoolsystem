import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createClassLevelErrorAction,
  createClassLevelRequestAction,
  createClassLevelSuccessAction,
  deleteClassLevelErrorAction,
  deleteClassLevelRequestAction,
  deleteClassLevelSuccessAction,
  getClassLevelsErrorAction,
  getClassLevelsRequestAction,
  getClassLevelsSuccessAction,
  updateClassLevelErrorAction,
  updateClassLevelRequestAction,
  updateClassLevelSuccessAction,
} from "./Reducer";
import {
  createClassLevelService,
  deleteClassLevelService,
  getClassLevelService,
  updateClassLevelService,
} from "../../service/ClassLevel";
import { ClassLevelActionTypes } from "../../../domain/ClassLevel";

type ErrorMessage = {
  error: string;
};

function* getClassLevel({
  payload,
}: ReturnType<typeof getClassLevelsRequestAction>): Generator {
  try {
    const res = (yield call(getClassLevelService, payload)) as AxiosResponse;
    yield put(getClassLevelsSuccessAction({ classLevels: res.data }));
  } catch (err) {
    yield put(getClassLevelsErrorAction({ message: "404" }));
  }
}

function* createClassLevel({
  payload,
}: ReturnType<typeof createClassLevelRequestAction>): Generator {
  try {
    const res = (yield call(createClassLevelService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createClassLevelSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createClassLevelErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* updateClassLevel({
    payload,
  }: ReturnType<typeof updateClassLevelRequestAction>): Generator {
    try {
      const res = (yield call(updateClassLevelService, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(updateClassLevelSuccessAction());
      }
    } catch (err) {
      const error = err as AxiosError<ErrorMessage>;
      if (error.response?.data.error) {
        console.log(error.response?.data.error)
        yield put(updateClassLevelErrorAction({ message: error.response?.data.error}));
      }
    }
  }

  function* deleteClassLevel({
    payload,
  }: ReturnType<typeof deleteClassLevelRequestAction>): Generator {
    try {
      const res = (yield call(deleteClassLevelService, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(deleteClassLevelSuccessAction());
      }
    } catch (err) {
      yield put(deleteClassLevelErrorAction({ message: "404" }));
    }
  }

  function* watchGetClassLevels(): Generator {
    yield takeLatest(ClassLevelActionTypes.GET_CLASS_LEVELS_REQUEST, getClassLevel);
  }
  
  function* watchCreateClassLevel(): Generator {
    yield takeLatest(ClassLevelActionTypes.CREATE_CLASS_LEVEL_REQUEST, createClassLevel);
  }
  
  function* watchUpdateClassLevel(): Generator {
    yield takeLatest(ClassLevelActionTypes.UPDATE_CLASS_LEVEL_REQUEST, updateClassLevel);
  }
  
  function* watchDeleteClassLevel(): Generator {
    yield takeLatest(ClassLevelActionTypes.DELETE_CLASS_LEVEL_REQUEST, deleteClassLevel);
  }

  export function* classLevelSaga(): Generator {
    yield all([fork(watchGetClassLevels)]);
    yield all([fork(watchCreateClassLevel)]);
    yield all([fork(watchUpdateClassLevel)]);
    yield all([fork(watchDeleteClassLevel)]);
  }
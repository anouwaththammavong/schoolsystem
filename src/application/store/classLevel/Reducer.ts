import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  ClassLevelActionTypes,
  ClassLevels,
  CreateClassLevelForm,
  UpdateClassLevelForm,
  initialState,
} from "../../../domain/ClassLevel";

// Get
const getClassLevelsRequestAction = createAction<
  {
    limit: number;
    offset: number;
  },
  ClassLevelActionTypes.GET_CLASS_LEVELS_REQUEST
>(ClassLevelActionTypes.GET_CLASS_LEVELS_REQUEST);
const getClassLevelsSuccessAction = createAction<
  { classLevels: ClassLevels },
  ClassLevelActionTypes.GET_CLASS_LEVELS_SUCCESS
>(ClassLevelActionTypes.GET_CLASS_LEVELS_SUCCESS);
const getClassLevelsErrorAction = createAction<
  { message: string },
  ClassLevelActionTypes.GET_CLASS_LEVELS_ERROR
>(ClassLevelActionTypes.GET_CLASS_LEVELS_ERROR);

// Create Class Level
const createClassLevelRequestAction = createAction<
  { classLevel: CreateClassLevelForm },
  ClassLevelActionTypes.CREATE_CLASS_LEVEL_REQUEST
>(ClassLevelActionTypes.CREATE_CLASS_LEVEL_REQUEST);
const createClassLevelSuccessAction = createAction(
  ClassLevelActionTypes.CREATE_CLASS_LEVEL_SUCCESS
);
const createClassLevelErrorAction = createAction<
  { message: string },
  ClassLevelActionTypes.CREATE_CLASS_LEVEL_ERROR
>(ClassLevelActionTypes.CREATE_CLASS_LEVEL_ERROR);
const createClassLevelResetAction = createAction(
  ClassLevelActionTypes.CREATE_CLASS_LEVEL_RESET
);

// Update Class Level
const updateClassLevelRequestAction = createAction<
  { _id: string; classLevel: UpdateClassLevelForm },
  ClassLevelActionTypes.UPDATE_CLASS_LEVEL_REQUEST
>(ClassLevelActionTypes.UPDATE_CLASS_LEVEL_REQUEST);
const updateClassLevelSuccessAction = createAction(
  ClassLevelActionTypes.UPDATE_CLASS_LEVEL_SUCCESS
);
const updateClassLevelErrorAction = createAction<
  { message: string },
  ClassLevelActionTypes.UPDATE_CLASS_LEVEL_ERROR
>(ClassLevelActionTypes.UPDATE_CLASS_LEVEL_ERROR);
const updateClassLevelResetAction = createAction(
  ClassLevelActionTypes.UPDATE_CLASS_LEVEL_RESET
);

// Delete Class Level
const deleteClassLevelRequestAction = createAction<
  { _id: string },
  ClassLevelActionTypes.DELETE_CLASS_LEVEL_REQUEST
>(ClassLevelActionTypes.DELETE_CLASS_LEVEL_REQUEST);
const deleteClassLevelSuccessAction = createAction(
  ClassLevelActionTypes.DELETE_CLASS_LEVEL_SUCCESS
);
const deleteClassLevelErrorAction = createAction<
  { message: string },
  ClassLevelActionTypes.DELETE_CLASS_LEVEL_ERROR
>(ClassLevelActionTypes.DELETE_CLASS_LEVEL_ERROR);
const deleteClassLevelResetAction = createAction(
  ClassLevelActionTypes.DELETE_CLASS_LEVEL_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Class level
    .addCase(getClassLevelsRequestAction, (state) => {
      (state.classLevels = {
        classLevels: {
          classLevels: [],
          totalCount: 0,
        },
      }),
        (state.isGetClassLevelsLoading = true);
      state.isGetClassLevelsSuccess = false;
      state.getClassLevelsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getClassLevelsSuccessAction, (state, action) => {
      state.classLevels = action.payload.classLevels;
      state.isGetClassLevelsLoading = false;
      state.isGetClassLevelsSuccess = true;
      state.getClassLevelsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getClassLevelsErrorAction, (state, action) => {
      (state.classLevels = {
        classLevels: {
          classLevels: [],
          totalCount: 0,
        },
      }),
        (state.isGetClassLevelsLoading = false);
      state.isGetClassLevelsSuccess = false;
      state.getClassLevelsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create Class Level
    .addCase(createClassLevelRequestAction, (state) => {
      state.isCreateClassLevelLoading = true;
      state.isCreateClassLevelSuccess = false;
      state.createClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createClassLevelSuccessAction, (state) => {
      state.isCreateClassLevelLoading = false;
      state.isCreateClassLevelSuccess = true;
      state.createClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createClassLevelErrorAction, (state, action) => {
      state.isCreateClassLevelLoading = false;
      state.isCreateClassLevelSuccess = false;
      state.createClassLevelError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createClassLevelResetAction, (state) => {
      state.isCreateClassLevelLoading = false;
      state.isCreateClassLevelSuccess = false;
      state.createClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    // Update Class Level
    .addCase(updateClassLevelRequestAction, (state) => {
      state.isUpdateClassLevelLoading = true;
      state.isUpdateClassLevelSuccess = false;
      state.updateClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateClassLevelSuccessAction, (state) => {
      state.isUpdateClassLevelLoading = false;
      state.isUpdateClassLevelSuccess = true;
      state.updateClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateClassLevelErrorAction, (state, action) => {
      state.isUpdateClassLevelLoading = false;
      state.isUpdateClassLevelSuccess = false;
      state.updateClassLevelError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateClassLevelResetAction, (state) => {
      state.isUpdateClassLevelLoading = false;
      state.isUpdateClassLevelSuccess = false;
      state.updateClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete ClassLevel
    .addCase(deleteClassLevelRequestAction, (state) => {
      state.isDeleteClassLevelLoading = true;
      state.isDeleteClassLevelSuccess = false;
      state.deleteClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteClassLevelSuccessAction, (state) => {
      state.isDeleteClassLevelLoading = false;
      state.isDeleteClassLevelSuccess = true;
      state.deleteClassLevelError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteClassLevelErrorAction, (state, action) => {
      state.isDeleteClassLevelLoading = false;
      state.isDeleteClassLevelSuccess = false;
      state.deleteClassLevelError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteClassLevelResetAction, (state) => {
      state.isDeleteClassLevelLoading = false;
      state.isDeleteClassLevelSuccess = false;
      state.deleteClassLevelError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getClassLevelsRequestAction,
  getClassLevelsSuccessAction,
  getClassLevelsErrorAction,
  createClassLevelRequestAction,
  createClassLevelSuccessAction,
  createClassLevelErrorAction,
  createClassLevelResetAction,
  updateClassLevelRequestAction,
  updateClassLevelSuccessAction,
  updateClassLevelErrorAction,
  updateClassLevelResetAction,
  deleteClassLevelRequestAction,
  deleteClassLevelSuccessAction,
  deleteClassLevelErrorAction,
  deleteClassLevelResetAction,
};

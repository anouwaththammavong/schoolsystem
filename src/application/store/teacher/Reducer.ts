import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateTeacher,
  Teacher,
  TeacherActionTypes,
  initialState,
} from "../../../domain/Teacher";

// Get
const getTeachersRequestAction = createAction<{
  limit: number;
  offset: number;
  teacher_id?: string;
},TeacherActionTypes.GET_TEACHERS_REQUEST>(TeacherActionTypes.GET_TEACHERS_REQUEST);
const getTeachersSuccessAction = createAction<
  { teachers: Teacher },
  TeacherActionTypes.GET_TEACHERS_SUCCESS
>(TeacherActionTypes.GET_TEACHERS_SUCCESS);
const getTeachersErrorAction = createAction<
  { message: string },
  TeacherActionTypes.GET_TEACHERS_ERROR
>(TeacherActionTypes.GET_TEACHERS_ERROR);

// Create Teacher
const createTeacherRequestAction = createAction<
  { teacher: CreateTeacher },
  TeacherActionTypes.CREATE_TEACHERS_REQUEST
>(TeacherActionTypes.CREATE_TEACHERS_REQUEST);
const createTeacherSuccessAction = createAction(
  TeacherActionTypes.CREATE_TEACHERS_SUCCESS
);
const CreateTeacherErrorAction = createAction<
  { message: string },
  TeacherActionTypes.CREATE_TEACHERS_ERROR
>(TeacherActionTypes.CREATE_TEACHERS_ERROR);
const createTeacherResetAction = createAction(
  TeacherActionTypes.CREATE_TEACHERS_RESET
);

// Update Teacher
const updateTeacherRequestAction = createAction<
  { _id: string; teacher: CreateTeacher },
  TeacherActionTypes.UPDATE_TEACHERS_REQUEST
>(TeacherActionTypes.UPDATE_TEACHERS_REQUEST);
const updateTeacherSuccessAction = createAction(
  TeacherActionTypes.UPDATE_TEACHERS_SUCCESS
);
const updateTeacherErrorAction = createAction<
  { message: string },
  TeacherActionTypes.UPDATE_TEACHERS_ERROR
>(TeacherActionTypes.UPDATE_TEACHERS_ERROR);
const updateTeacherResetAction = createAction(
  TeacherActionTypes.UPDATE_TEACHERS_RESET
);

// Delete Teacher
const deleteTeacherRequestAction = createAction<
  { _id: string },
  TeacherActionTypes.DELETE_TEACHERS_REQUEST
>(TeacherActionTypes.DELETE_TEACHERS_REQUEST);
const deleteTeacherSuccessAction = createAction(
  TeacherActionTypes.DELETE_TEACHERS_SUCCESS
);
const deleteTeacherErrorAction = createAction<
  { message: string },
  TeacherActionTypes.DELETE_TEACHERS_ERROR
>(TeacherActionTypes.DELETE_TEACHERS_ERROR);
const deleteTeacherResetAction = createAction(
  TeacherActionTypes.DELETE_TEACHERS_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Teacher
    .addCase(getTeachersRequestAction, (state) => {
      (state.teachers = {
        teachers: { teachers: [], totalTeachers: 0 },
      }),
        (state.isGetTeachersLoading = true);
      state.isGetTeachersSuccess = false;
      state.getTeachersError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getTeachersSuccessAction, (state, action) => {
      state.teachers = action.payload.teachers;
      state.isGetTeachersLoading = false;
      state.isGetTeachersSuccess = true;
      state.getTeachersError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getTeachersErrorAction, (state, action) => {
      (state.teachers = {
        teachers: { teachers: [], totalTeachers: 0 },
      }),
        (state.isGetTeachersLoading = false);
      state.isGetTeachersSuccess = false;
      state.getTeachersError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create Teacher
    .addCase(createTeacherRequestAction, (state) => {
      state.isCreateTeacherLoading = true;
      state.isCreateTeacherSuccess = false;
      state.CreateTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createTeacherSuccessAction, (state) => {
      state.isCreateTeacherLoading = false;
      state.isCreateTeacherSuccess = true;
      state.CreateTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(CreateTeacherErrorAction, (state, action) => {
      state.isCreateTeacherLoading = false;
      state.isCreateTeacherSuccess = false;
      state.CreateTeacherError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createTeacherResetAction, (state) => {
      state.isCreateTeacherLoading = false;
      state.isCreateTeacherSuccess = false;
      state.CreateTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    // Update Teacher
    .addCase(updateTeacherRequestAction, (state) => {
      state.isUpdateTeacherLoading = true;
      state.isUpdateTeacherSuccess = false;
      state.updateTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateTeacherSuccessAction, (state) => {
      state.isUpdateTeacherLoading = false;
      state.isUpdateTeacherSuccess = true;
      state.updateTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateTeacherErrorAction, (state, action) => {
      state.isUpdateTeacherLoading = false;
      state.isUpdateTeacherSuccess = false;
      state.updateTeacherError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateTeacherResetAction, (state) => {
      state.isUpdateTeacherLoading = false;
      state.isUpdateTeacherSuccess = false;
      state.updateTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete Teacher
    .addCase(deleteTeacherRequestAction, (state) => {
      state.isDeleteTeacherLoading = true;
      state.isDeleteTeacherSuccess = false;
      state.deleteTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteTeacherSuccessAction, (state) => {
      state.isDeleteTeacherLoading = false;
      state.isDeleteTeacherSuccess = true;
      state.deleteTeacherError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteTeacherErrorAction, (state, action) => {
      state.isDeleteTeacherLoading = false;
      state.isDeleteTeacherSuccess = false;
      state.deleteTeacherError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteTeacherResetAction, (state) => {
      state.isDeleteTeacherLoading = false;
      state.isDeleteTeacherSuccess = false;
      state.deleteTeacherError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getTeachersRequestAction,
  getTeachersSuccessAction,
  getTeachersErrorAction,
  createTeacherRequestAction,
  createTeacherSuccessAction,
  CreateTeacherErrorAction,
  createTeacherResetAction,
  updateTeacherRequestAction,
  updateTeacherSuccessAction,
  updateTeacherErrorAction,
  updateTeacherResetAction,
  deleteTeacherRequestAction,
  deleteTeacherSuccessAction,
  deleteTeacherErrorAction,
  deleteTeacherResetAction,
};

import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  CreateTeacherErrorAction,
  createTeacherRequestAction,
  createTeacherSuccessAction,
  deleteTeacherErrorAction,
  deleteTeacherRequestAction,
  deleteTeacherSuccessAction,
  getTeachersErrorAction,
  getTeachersRequestAction,
  getTeachersSuccessAction,
  updateTeacherErrorAction,
  updateTeacherRequestAction,
  updateTeacherSuccessAction,
} from "./Reducer";
import {
  createTeacherService,
  deleteTeacherService,
  getTeacherService,
  updateTeacherService,
} from "../../service/Teacher";
import { TeacherActionTypes } from "../../../domain/Teacher";

type ErrorMessage = {
  error: string;
}

function* getTeacher({
  payload,
}: ReturnType<typeof getTeachersRequestAction>): Generator {
  try {
    const res = (yield call(getTeacherService, payload)) as AxiosResponse;
    yield put(getTeachersSuccessAction({ teachers: res.data }));
  } catch (err) {
    yield put(getTeachersErrorAction({ message: "404" }));
  }
}

function* createTeacher({
  payload,
}: ReturnType<typeof createTeacherRequestAction>): Generator {
  try {
    const res = (yield call(createTeacherService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createTeacherSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(CreateTeacherErrorAction({ message: error.response?.data.error }));
    }
  }
}

function* updateTeacher({
  payload,
}: ReturnType<typeof updateTeacherRequestAction>): Generator {
  try {
    const res = (yield call(updateTeacherService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateTeacherSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(updateTeacherErrorAction({ message: error.response?.data.error }));
    }
  }
}

function* deleteTeacher({
  payload,
}: ReturnType<typeof deleteTeacherRequestAction>): Generator {
  try {
    const res = (yield call(deleteTeacherService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteTeacherSuccessAction());
    }
  } catch (err) {
    yield put(deleteTeacherErrorAction({ message: "404" }));
  }
}

function* watchGetTeachers(): Generator {
  yield takeLatest(TeacherActionTypes.GET_TEACHERS_REQUEST, getTeacher);
}

function* watchCreateTeacher(): Generator {
  yield takeLatest(TeacherActionTypes.CREATE_TEACHERS_REQUEST, createTeacher);
}

function* watchUpdateTeacher(): Generator {
  yield takeLatest(TeacherActionTypes.UPDATE_TEACHERS_REQUEST, updateTeacher);
}

function* watchDeleteTeacher(): Generator {
  yield takeLatest(TeacherActionTypes.DELETE_TEACHERS_REQUEST, deleteTeacher);
}

export function* teacherSaga(): Generator {
  yield all([fork(watchGetTeachers)]);
  yield all([fork(watchCreateTeacher)]);
  yield all([fork(watchUpdateTeacher)]);
  yield all([fork(watchDeleteTeacher)]);
}

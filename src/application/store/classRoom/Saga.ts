import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createClassRoomErrorAction,
  createClassRoomRequestAction,
  createClassRoomSuccessAction,
  deleteClassRoomErrorAction,
  deleteClassRoomRequestAction,
  deleteClassRoomSuccessAction,
  getClassRoomsErrorAction,
  getClassRoomsRequestAction,
  getClassRoomsSuccessAction,
  updateClassRoomErrorAction,
  updateClassRoomRequestAction,
  updateClassRoomSuccessAction,
} from "./Reducer";
import {
  createClassRoomService,
  deleteClassRoomService,
  getClassRoomService,
  updateClassRoomService,
} from "../../service/ClassRoom";
import { ClassRoomActionTypes } from "../../../domain/ClassRoom";

type ErrorMessage = {
  error: string;
};

function* getClassRoom({
  payload,
}: ReturnType<typeof getClassRoomsRequestAction>): Generator {
  try {
    const res = (yield call(getClassRoomService, payload)) as AxiosResponse;
    yield put(getClassRoomsSuccessAction({ classRooms: res.data }));
  } catch (err) {
    yield put(getClassRoomsErrorAction({ message: "404" }));
  }
}

function* createClassRoom({
  payload,
}: ReturnType<typeof createClassRoomRequestAction>): Generator {
  try {
    const res = (yield call(createClassRoomService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createClassRoomSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createClassRoomErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* updateClassRoom({
    payload,
  }: ReturnType<typeof updateClassRoomRequestAction>): Generator {
    try {
      const res = (yield call(updateClassRoomService, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(updateClassRoomSuccessAction());
      }
    } catch (err) {
      const error = err as AxiosError<ErrorMessage>;
      if (error.response?.data.error) {
        console.log(error.response?.data.error)
        yield put(updateClassRoomErrorAction({ message: error.response?.data.error}));
      }
    }
  }

  function* deleteClassRoom({
    payload,
  }: ReturnType<typeof deleteClassRoomRequestAction>): Generator {
    try {
      const res = (yield call(deleteClassRoomService, payload)) as AxiosResponse;
      if (res.status === 201) {
        yield put(deleteClassRoomSuccessAction());
      }
    } catch (err) {
      yield put(deleteClassRoomErrorAction({ message: "404" }));
    }
  }

  function* watchGetClassRooms(): Generator {
    yield takeLatest(ClassRoomActionTypes.GET_CLASS_ROOMS_REQUEST, getClassRoom);
  }
  
  function* watchCreateClassRoom(): Generator {
    yield takeLatest(ClassRoomActionTypes.CREATE_CLASS_ROOM_REQUEST, createClassRoom);
  }
  
  function* watchUpdateClassRoom(): Generator {
    yield takeLatest(ClassRoomActionTypes.UPDATE_CLASS_ROOM_REQUEST, updateClassRoom);
  }
  
  function* watchDeleteClassRoom(): Generator {
    yield takeLatest(ClassRoomActionTypes.DELETE_CLASS_ROOM_REQUEST, deleteClassRoom);
  }

  export function* classRoomSaga(): Generator {
    yield all([fork(watchGetClassRooms)]);
    yield all([fork(watchCreateClassRoom)]);
    yield all([fork(watchUpdateClassRoom)]);
    yield all([fork(watchDeleteClassRoom)]);
  }
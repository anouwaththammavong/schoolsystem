import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  ClassRoomActionTypes,
  ClassRooms,
  CreateClassRoom,
  UpdateClassRoom,
  initialState,
} from "../../../domain/ClassRoom";

// Get
const getClassRoomsRequestAction = createAction<
  {
    limit: number;
    offset: number;
  },
  ClassRoomActionTypes.GET_CLASS_ROOMS_REQUEST
>(ClassRoomActionTypes.GET_CLASS_ROOMS_REQUEST);
const getClassRoomsSuccessAction = createAction<
  { classRooms: ClassRooms },
  ClassRoomActionTypes.GET_CLASS_ROOMS_SUCCESS
>(ClassRoomActionTypes.GET_CLASS_ROOMS_SUCCESS);
const getClassRoomsErrorAction = createAction<
  { message: string },
  ClassRoomActionTypes.GET_CLASS_ROOMS_ERROR
>(ClassRoomActionTypes.GET_CLASS_ROOMS_ERROR);

// Create Class Room
const createClassRoomRequestAction = createAction<
  { classRoom: CreateClassRoom },
  ClassRoomActionTypes.CREATE_CLASS_ROOM_REQUEST
>(ClassRoomActionTypes.CREATE_CLASS_ROOM_REQUEST);
const createClassRoomSuccessAction = createAction(
  ClassRoomActionTypes.CREATE_CLASS_ROOM_SUCCESS
);
const createClassRoomErrorAction = createAction<
  { message: string },
  ClassRoomActionTypes.CREATE_CLASS_ROOM_ERROR
>(ClassRoomActionTypes.CREATE_CLASS_ROOM_ERROR);
const createClassRoomResetAction = createAction(
  ClassRoomActionTypes.CREATE_CLASS_ROOM_RESET
);

// Update Class Room
const updateClassRoomRequestAction = createAction<
  { _id: string; classRoom: UpdateClassRoom },
  ClassRoomActionTypes.UPDATE_CLASS_ROOM_REQUEST
>(ClassRoomActionTypes.UPDATE_CLASS_ROOM_REQUEST);
const updateClassRoomSuccessAction = createAction(
  ClassRoomActionTypes.UPDATE_CLASS_ROOM_SUCCESS
);
const updateClassRoomErrorAction = createAction<
  { message: string },
  ClassRoomActionTypes.UPDATE_CLASS_ROOM_ERROR
>(ClassRoomActionTypes.UPDATE_CLASS_ROOM_ERROR);
const updateClassRoomResetAction = createAction(
  ClassRoomActionTypes.UPDATE_CLASS_ROOM_RESET
);

// Delete Class Room
const deleteClassRoomRequestAction = createAction<
  { _id: string },
  ClassRoomActionTypes.DELETE_CLASS_ROOM_REQUEST
>(ClassRoomActionTypes.DELETE_CLASS_ROOM_REQUEST);
const deleteClassRoomSuccessAction = createAction(
  ClassRoomActionTypes.DELETE_CLASS_ROOM_SUCCESS
);
const deleteClassRoomErrorAction = createAction<
  { message: string },
  ClassRoomActionTypes.DELETE_CLASS_ROOM_ERROR
>(ClassRoomActionTypes.DELETE_CLASS_ROOM_ERROR);
const deleteClassRoomResetAction = createAction(
  ClassRoomActionTypes.DELETE_CLASS_ROOM_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Class room
    .addCase(getClassRoomsRequestAction, (state) => {
      (state.classRooms = {
        classrooms: {
          classRooms: [],
          totalCount: 0,
        },
      }),
        (state.isGetClassRoomsLoading = true);
      state.isGetClassRoomsSuccess = false;
      state.getClassRoomsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getClassRoomsSuccessAction, (state, action) => {
      state.classRooms = action.payload.classRooms;
      state.isGetClassRoomsLoading = false;
      state.isGetClassRoomsSuccess = true;
      state.getClassRoomsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getClassRoomsErrorAction, (state, action) => {
      (state.classRooms = {
        classrooms: {
          classRooms: [],
          totalCount: 0,
        },
      }),
        (state.isGetClassRoomsLoading = false);
      state.isGetClassRoomsSuccess = false;
      state.getClassRoomsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create Class room
    .addCase(createClassRoomRequestAction, (state) => {
      state.isCreateClassRoomLoading = true;
      state.isCreateClassRoomSuccess = false;
      state.createClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createClassRoomSuccessAction, (state) => {
      state.isCreateClassRoomLoading = false;
      state.isCreateClassRoomSuccess = true;
      state.createClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createClassRoomErrorAction, (state, action) => {
      state.isCreateClassRoomLoading = false;
      state.isCreateClassRoomSuccess = false;
      state.createClassRoomError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createClassRoomResetAction, (state) => {
      state.isCreateClassRoomLoading = false;
      state.isCreateClassRoomSuccess = false;
      state.createClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    // Update Class room
    .addCase(updateClassRoomRequestAction, (state) => {
      state.isUpdateClassRoomLoading = true;
      state.isUpdateClassRoomSuccess = false;
      state.updateClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateClassRoomSuccessAction, (state) => {
      state.isUpdateClassRoomLoading = false;
      state.isUpdateClassRoomSuccess = true;
      state.updateClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateClassRoomErrorAction, (state, action) => {
      state.isUpdateClassRoomLoading = false;
      state.isUpdateClassRoomSuccess = false;
      state.updateClassRoomError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateClassRoomResetAction, (state) => {
      state.isUpdateClassRoomLoading = false;
      state.isUpdateClassRoomSuccess = false;
      state.updateClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete ClassRoom
    .addCase(deleteClassRoomRequestAction, (state) => {
      state.isDeleteClassRoomLoading = true;
      state.isDeleteClassRoomSuccess = false;
      state.deleteClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteClassRoomSuccessAction, (state) => {
      state.isDeleteClassRoomLoading = false;
      state.isDeleteClassRoomSuccess = true;
      state.deleteClassRoomError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteClassRoomErrorAction, (state, action) => {
      state.isDeleteClassRoomLoading = false;
      state.isDeleteClassRoomSuccess = false;
      state.deleteClassRoomError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteClassRoomResetAction, (state) => {
      state.isDeleteClassRoomLoading = false;
      state.isDeleteClassRoomSuccess = false;
      state.deleteClassRoomError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getClassRoomsRequestAction,
  getClassRoomsSuccessAction,
  getClassRoomsErrorAction,
  createClassRoomRequestAction,
  createClassRoomSuccessAction,
  createClassRoomErrorAction,
  createClassRoomResetAction,
  updateClassRoomRequestAction,
  updateClassRoomSuccessAction,
  updateClassRoomErrorAction,
  updateClassRoomResetAction,
  deleteClassRoomRequestAction,
  deleteClassRoomSuccessAction,
  deleteClassRoomErrorAction,
  deleteClassRoomResetAction,
};

import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateSchoolFee,
  SchoolFeeActionTypes,
  SchoolFees,
  UpdateSchoolFee,
  initialState,
} from "../../../domain/SchoolFees";

// Get
const getSchoolFeesRequestAction = createAction<
  {
    limit: number;
    offset: number;
    level_id?: string;
    academic_year_no?: string;
  },
  SchoolFeeActionTypes.GET_SCHOOL_FEES_REQUEST
>(SchoolFeeActionTypes.GET_SCHOOL_FEES_REQUEST);
const getSchoolFeesSuccessAction = createAction<
  { schoolFees: SchoolFees },
  SchoolFeeActionTypes.GET_SCHOOL_FEES_SUCCESS
>(SchoolFeeActionTypes.GET_SCHOOL_FEES_SUCCESS);
const getSchoolFeesErrorAction = createAction<
  { message: string },
  SchoolFeeActionTypes.GET_SCHOOL_FEES_ERROR
>(SchoolFeeActionTypes.GET_SCHOOL_FEES_ERROR);

// Create School Fee
const createSchoolFeeRequestAction = createAction<
  { schoolFee: CreateSchoolFee },
  SchoolFeeActionTypes.CREATE_SCHOOL_FEE_REQUEST
>(SchoolFeeActionTypes.CREATE_SCHOOL_FEE_REQUEST);
const createSchoolFeeSuccessAction = createAction(
  SchoolFeeActionTypes.CREATE_SCHOOL_FEE_SUCCESS
);
const createSchoolFeeErrorAction = createAction<
  { message: string },
  SchoolFeeActionTypes.CREATE_SCHOOL_FEE_ERROR
>(SchoolFeeActionTypes.CREATE_SCHOOL_FEE_ERROR);
const createSchoolFeeResetAction = createAction(
  SchoolFeeActionTypes.CREATE_SCHOOL_FEE_RESET
);

// Update School Fee
const updateSchoolFeeRequestAction = createAction<
  { _id: string; schoolFee: UpdateSchoolFee },
  SchoolFeeActionTypes.UPDATE_SCHOOL_FEE_REQUEST
>(SchoolFeeActionTypes.UPDATE_SCHOOL_FEE_REQUEST);
const updateSchoolFeeSuccessAction = createAction(
  SchoolFeeActionTypes.UPDATE_SCHOOL_FEE_SUCCESS
);
const updateSchoolFeeErrorAction = createAction<
  { message: string },
  SchoolFeeActionTypes.UPDATE_SCHOOL_FEE_ERROR
>(SchoolFeeActionTypes.UPDATE_SCHOOL_FEE_ERROR);
const updateSchoolFeeResetAction = createAction(
  SchoolFeeActionTypes.UPDATE_SCHOOL_FEE_RESET
);

// Delete School Fee
const deleteSchoolFeeRequestAction = createAction<
  { _id: string },
  SchoolFeeActionTypes.DELETE_SCHOOL_FEE_REQUEST
>(SchoolFeeActionTypes.DELETE_SCHOOL_FEE_REQUEST);
const deleteSchoolFeeSuccessAction = createAction(
  SchoolFeeActionTypes.DELETE_SCHOOL_FEE_SUCCESS
);
const deleteSchoolFeeErrorAction = createAction<
  { message: string },
  SchoolFeeActionTypes.DELETE_SCHOOL_FEE_ERROR
>(SchoolFeeActionTypes.DELETE_SCHOOL_FEE_ERROR);
const deleteSchoolFeeResetAction = createAction(
  SchoolFeeActionTypes.DELETE_SCHOOL_FEE_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get School fee
    .addCase(getSchoolFeesRequestAction, (state) => {
      (state.schoolFees = {
        school_fees: {
          schoolFees: [],
          totalCount: 0,
        },
      }),
        (state.isGetSchoolFeesLoading = true);
      state.isGetSchoolFeesSuccess = false;
      state.getSchoolFeesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getSchoolFeesSuccessAction, (state, action) => {
      state.schoolFees = action.payload.schoolFees;
      state.isGetSchoolFeesLoading = false;
      state.isGetSchoolFeesSuccess = true;
      state.getSchoolFeesError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getSchoolFeesErrorAction, (state, action) => {
      (state.schoolFees = {
        school_fees: {
          schoolFees: [],
          totalCount: 0,
        },
      }),
        (state.isGetSchoolFeesLoading = false);
      state.isGetSchoolFeesSuccess = false;
      state.getSchoolFeesError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create School fee
    .addCase(createSchoolFeeRequestAction, (state) => {
      state.isCreateSchoolFeeLoading = true;
      state.isCreateSchoolFeeSuccess = false;
      state.createSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createSchoolFeeSuccessAction, (state) => {
      state.isCreateSchoolFeeLoading = false;
      state.isCreateSchoolFeeSuccess = true;
      state.createSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createSchoolFeeErrorAction, (state, action) => {
      state.isCreateSchoolFeeLoading = false;
      state.isCreateSchoolFeeSuccess = false;
      state.createSchoolFeeError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createSchoolFeeResetAction, (state) => {
      state.isCreateSchoolFeeLoading = false;
      state.isCreateSchoolFeeSuccess = false;
      state.createSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    // Update School fee
    .addCase(updateSchoolFeeRequestAction, (state) => {
      state.isUpdateSchoolFeeLoading = true;
      state.isUpdateSchoolFeeSuccess = false;
      state.updateSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateSchoolFeeSuccessAction, (state) => {
      state.isUpdateSchoolFeeLoading = false;
      state.isUpdateSchoolFeeSuccess = true;
      state.updateSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateSchoolFeeErrorAction, (state, action) => {
      state.isUpdateSchoolFeeLoading = false;
      state.isUpdateSchoolFeeSuccess = false;
      state.updateSchoolFeeError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateSchoolFeeResetAction, (state) => {
      state.isUpdateSchoolFeeLoading = false;
      state.isUpdateSchoolFeeSuccess = false;
      state.updateSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete SchoolFee
    .addCase(deleteSchoolFeeRequestAction, (state) => {
      state.isDeleteSchoolFeeLoading = true;
      state.isDeleteSchoolFeeSuccess = false;
      state.deleteSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteSchoolFeeSuccessAction, (state) => {
      state.isDeleteSchoolFeeLoading = false;
      state.isDeleteSchoolFeeSuccess = true;
      state.deleteSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteSchoolFeeErrorAction, (state, action) => {
      state.isDeleteSchoolFeeLoading = false;
      state.isDeleteSchoolFeeSuccess = false;
      state.deleteSchoolFeeError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteSchoolFeeResetAction, (state) => {
      state.isDeleteSchoolFeeLoading = false;
      state.isDeleteSchoolFeeSuccess = false;
      state.deleteSchoolFeeError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getSchoolFeesRequestAction,
  getSchoolFeesSuccessAction,
  getSchoolFeesErrorAction,
  createSchoolFeeRequestAction,
  createSchoolFeeSuccessAction,
  createSchoolFeeErrorAction,
  createSchoolFeeResetAction,
  updateSchoolFeeRequestAction,
  updateSchoolFeeSuccessAction,
  updateSchoolFeeErrorAction,
  updateSchoolFeeResetAction,
  deleteSchoolFeeRequestAction,
  deleteSchoolFeeSuccessAction,
  deleteSchoolFeeErrorAction,
  deleteSchoolFeeResetAction,
};

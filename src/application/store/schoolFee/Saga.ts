import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createSchoolFeeErrorAction,
  createSchoolFeeRequestAction,
  createSchoolFeeSuccessAction,
  deleteSchoolFeeErrorAction,
  deleteSchoolFeeRequestAction,
  deleteSchoolFeeSuccessAction,
  getSchoolFeesErrorAction,
  getSchoolFeesRequestAction,
  getSchoolFeesSuccessAction,
  updateSchoolFeeErrorAction,
  updateSchoolFeeRequestAction,
  updateSchoolFeeSuccessAction,
} from "./Reducer";
import {
  createSchoolFeeService,
  deleteSchoolFeeService,
  getSchoolFeeService,
  updateSchoolFeeService,
} from "../../service/SchoolFee";
import { SchoolFeeActionTypes } from "../../../domain/SchoolFees";

type ErrorMessage = {
  error: string;
};

function* getSchoolFee({
  payload,
}: ReturnType<typeof getSchoolFeesRequestAction>): Generator {
  try {
    const res = (yield call(getSchoolFeeService, payload)) as AxiosResponse;
    yield put(getSchoolFeesSuccessAction({ schoolFees: res.data }));
  } catch (err) {
    yield put(getSchoolFeesErrorAction({ message: "404" }));
  }
}

function* createSchoolFee({
  payload,
}: ReturnType<typeof createSchoolFeeRequestAction>): Generator {
  try {
    const res = (yield call(createSchoolFeeService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createSchoolFeeSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createSchoolFeeErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* updateSchoolFee({
  payload,
}: ReturnType<typeof updateSchoolFeeRequestAction>): Generator {
  try {
    const res = (yield call(updateSchoolFeeService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateSchoolFeeSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      console.log(error.response?.data.error);
      yield put(
        updateSchoolFeeErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* deleteSchoolFee({
  payload,
}: ReturnType<typeof deleteSchoolFeeRequestAction>): Generator {
  try {
    const res = (yield call(deleteSchoolFeeService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteSchoolFeeSuccessAction());
    }
  } catch (err) {
    yield put(deleteSchoolFeeErrorAction({ message: "404" }));
  }
}

function* watchGetSchoolFees(): Generator {
  yield takeLatest(SchoolFeeActionTypes.GET_SCHOOL_FEES_REQUEST, getSchoolFee);
}

function* watchCreateSchoolFee(): Generator {
  yield takeLatest(
    SchoolFeeActionTypes.CREATE_SCHOOL_FEE_REQUEST,
    createSchoolFee
  );
}

function* watchUpdateSchoolFee(): Generator {
  yield takeLatest(
    SchoolFeeActionTypes.UPDATE_SCHOOL_FEE_REQUEST,
    updateSchoolFee
  );
}

function* watchDeleteSchoolFee(): Generator {
  yield takeLatest(
    SchoolFeeActionTypes.DELETE_SCHOOL_FEE_REQUEST,
    deleteSchoolFee
  );
}

export function* schoolFeeSaga(): Generator {
  yield all([fork(watchGetSchoolFees)]);
  yield all([fork(watchCreateSchoolFee)]);
  yield all([fork(watchUpdateSchoolFee)]);
  yield all([fork(watchDeleteSchoolFee)]);
}

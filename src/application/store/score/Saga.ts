import { AxiosError, AxiosResponse } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createScoreErrorAction,
  createScoreRequestAction,
  createScoreSuccessAction,
  deleteScoreErrorAction,
  deleteScoreRequestAction,
  deleteScoreSuccessAction,
  getScoresErrorAction,
  getScoresRequestAction,
  getScoresSuccessAction,
  updateScoreErrorAction,
  updateScoreRequestAction,
  updateScoreSuccessAction,
} from "./Reducer";
import {
  createScoreService,
  deleteScoreService,
  getScoresService,
  updateScoreService,
} from "../../service/Score";
import { ScoreActionTypes } from "../../../domain/Scores";

type ErrorMessage = {
  error: string;
};

function* getScores({
  payload,
}: ReturnType<typeof getScoresRequestAction>): Generator {
  try {
    const res = (yield call(
      getScoresService,
      payload
    )) as AxiosResponse;
    yield put(
      getScoresSuccessAction({ scores: res.data })
    );
  } catch (err) {
    yield put(getScoresErrorAction({ message: "404" }));
  }
}

function* createScore({
  payload,
}: ReturnType<typeof createScoreRequestAction>): Generator {
  try {
    const res = (yield call(
      createScoreService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(createScoreSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createScoreErrorAction({
          message: error.response?.data.error,
        })
      );
    }
  }
}

function* updateScore({
  payload,
}: ReturnType<typeof updateScoreRequestAction>): Generator {
  try {
    const res = (yield call(
      updateScoreService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateScoreSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        updateScoreErrorAction({
          message: error.response?.data.error,
        })
      );
    }
  }
}

function* deleteScore({
  payload,
}: ReturnType<typeof deleteScoreRequestAction>): Generator {
  try {
    const res = (yield call(
      deleteScoreService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteScoreSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        deleteScoreErrorAction({
          message: error.response?.data.error,
        })
      );
    }
  }
}

function* watchGetScores(): Generator {
  yield takeLatest(
    ScoreActionTypes.GET_SCORES_REQUEST,
    getScores
  );
}

function* watchCreateScore(): Generator {
  yield takeLatest(
    ScoreActionTypes.CREATE_SCORE_REQUEST,
    createScore
  );
}

function* watchUpdateScore(): Generator {
  yield takeLatest(
    ScoreActionTypes.UPDATE_SCORE_REQUEST,
    updateScore
  );
}

function* watchDeleteScore(): Generator {
  yield takeLatest(
    ScoreActionTypes.DELETE_SCORE_REQUEST,
    deleteScore
  );
}

export function* scoreSaga(): Generator {
  yield all([fork(watchGetScores)]);
  yield all([fork(watchCreateScore)]);
  yield all([fork(watchUpdateScore)]);
  yield all([fork(watchDeleteScore)]);
}

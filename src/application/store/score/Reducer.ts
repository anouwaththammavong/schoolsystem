import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateScore,
  ScoreActionTypes,
  Scores,
  UpdateScore,
  initialState,
} from "../../../domain/Scores";

// GET SCORES
const getScoresRequestAction = createAction<
  {
    limit: number,
    offset: number,
    room_id: string,
    month: Date,
    generation: string,
    term?: string
  },
  ScoreActionTypes.GET_SCORES_REQUEST
>(ScoreActionTypes.GET_SCORES_REQUEST);
const getScoresSuccessAction = createAction<
  { scores: Scores },
  ScoreActionTypes.GET_SCORES_SUCCESS
>(ScoreActionTypes.GET_SCORES_SUCCESS);
const getScoresErrorAction = createAction<
  { message: string },
  ScoreActionTypes.GET_SCORES_ERROR
>(ScoreActionTypes.GET_SCORES_ERROR);

// Create SCORES
const createScoreRequestAction = createAction<
  { score: CreateScore },
  ScoreActionTypes.CREATE_SCORE_REQUEST
>(ScoreActionTypes.CREATE_SCORE_REQUEST);
const createScoreSuccessAction = createAction(
  ScoreActionTypes.CREATE_SCORE_SUCCESS
);
const createScoreErrorAction = createAction<
  { message: string },
  ScoreActionTypes.CREATE_SCORE_ERROR
>(ScoreActionTypes.CREATE_SCORE_ERROR);
const createScoreResetAction = createAction(
  ScoreActionTypes.CREATE_SCORE_RESET
);

// Update SCORES
const updateScoreRequestAction = createAction<
  { _id: string; score: UpdateScore },
  ScoreActionTypes.UPDATE_SCORE_REQUEST
>(ScoreActionTypes.UPDATE_SCORE_REQUEST);
const updateScoreSuccessAction = createAction(
  ScoreActionTypes.UPDATE_SCORE_SUCCESS
);
const updateScoreErrorAction = createAction<
  { message: string },
  ScoreActionTypes.UPDATE_SCORE_ERROR
>(ScoreActionTypes.UPDATE_SCORE_ERROR);
const updateScoreResetAction = createAction(
  ScoreActionTypes.UPDATE_SCORE_RESET
);

// Delete SCORES
const deleteScoreRequestAction = createAction<
  { _id: string },
  ScoreActionTypes.DELETE_SCORE_REQUEST
>(ScoreActionTypes.DELETE_SCORE_REQUEST);
const deleteScoreSuccessAction = createAction(
  ScoreActionTypes.DELETE_SCORE_SUCCESS
);
const deleteScoreErrorAction = createAction<
  { message: string },
  ScoreActionTypes.DELETE_SCORE_ERROR
>(ScoreActionTypes.DELETE_SCORE_ERROR);
const deleteScoreResetAction = createAction(
  ScoreActionTypes.DELETE_SCORE_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // GET SCORES
    .addCase(getScoresRequestAction, (state) => {
      (state.scores = {
        scores: {
          scores: [],
          totalScore: 0,
        },
      }),
        (state.isGetScoresLoading = true);
      state.isGetScoresSuccess = false;
      state.getScoresError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getScoresSuccessAction, (state, action) => {
      state.scores = action.payload.scores;
      state.isGetScoresLoading = false;
      state.isGetScoresSuccess = true;
      state.getScoresError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getScoresErrorAction, (state, action) => {
      (state.scores = {
        scores: {
          scores: [],
          totalScore: 0,
        },
      }),
        (state.isGetScoresLoading = false);
      state.isGetScoresSuccess = false;
      state.getScoresError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create SCORES
    .addCase(createScoreRequestAction, (state) => {
      state.isCreateScoreLoading = true;
      state.isCreateScoreSuccess = false;
      state.createScoreError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createScoreSuccessAction, (state) => {
      state.isCreateScoreLoading = false;
      state.isCreateScoreSuccess = true;
      state.createScoreError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createScoreErrorAction, (state, action) => {
      state.isCreateScoreLoading = false;
      state.isCreateScoreSuccess = false;
      state.createScoreError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createScoreResetAction, (state) => {
      state.isCreateScoreLoading = false;
      state.isCreateScoreSuccess = false;
      state.createScoreError = {
        isError: false,
        message: undefined,
      };
    })
    // Update SCORES
    .addCase(updateScoreRequestAction, (state) => {
      state.isUpdateScoreLoading = true;
      state.isUpdateScoreSuccess = false;
      state.updateScoreError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateScoreSuccessAction, (state) => {
      state.isUpdateScoreLoading = false;
      state.isUpdateScoreSuccess = true;
      state.updateScoreError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateScoreErrorAction, (state, action) => {
      state.isUpdateScoreLoading = false;
      state.isUpdateScoreSuccess = false;
      state.updateScoreError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateScoreResetAction, (state) => {
      state.isUpdateScoreLoading = false;
      state.isUpdateScoreSuccess = false;
      state.updateScoreError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete SCORES
    .addCase(deleteScoreRequestAction, (state) => {
      state.isDeleteScoreLoading = true;
      state.isDeleteScoreSuccess = false;
      state.deleteScoreError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteScoreSuccessAction, (state) => {
      state.isDeleteScoreLoading = false;
      state.isDeleteScoreSuccess = true;
      state.deleteScoreError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteScoreErrorAction, (state, action) => {
      state.isDeleteScoreLoading = false;
      state.isDeleteScoreSuccess = false;
      state.deleteScoreError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteScoreResetAction, (state) => {
      state.isDeleteScoreLoading = false;
      state.isDeleteScoreSuccess = false;
      state.deleteScoreError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getScoresRequestAction,
  getScoresSuccessAction,
  getScoresErrorAction,
  createScoreRequestAction,
  createScoreSuccessAction,
  createScoreErrorAction,
  createScoreResetAction,
  updateScoreRequestAction,
  updateScoreSuccessAction,
  updateScoreErrorAction,
  updateScoreResetAction,
  deleteScoreRequestAction,
  deleteScoreSuccessAction,
  deleteScoreErrorAction,
  deleteScoreResetAction
};

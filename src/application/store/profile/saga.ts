import { AxiosError, AxiosResponse } from 'axios';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import { ProfileActionTypes } from '../../../domain/profile';
import { changePassword, getMe, login } from '../../service/profile';
import { changePasswordErrorAction, changePasswordRequestAction, changePasswordSuccessAction, getMeErrorAction, getMeSuccessAction, loginErrorAction, loginRequestAction, loginSuccessAction } from './reducer';

function* handleLogin({ payload }: ReturnType<typeof loginRequestAction>): Generator {
  try {
    const res = (yield call(login, payload)) as AxiosResponse;
    console.log(res.status)
    if (res.status === 200) {
      yield put(loginSuccessAction({
        accessToken: res.data.accessToken,
        // accessToken: res.data.accessToken,
        refreshToken: res.data.refreshToken,
      }))
    }
  } catch (err) {
    const error = err as AxiosError;
    if (error.response?.status === 401) {
      yield put(loginErrorAction(
        {
          message: 'invalid_username_password'
        }
      ))
    } else if (error.response?.status === 422) {
      yield put(loginErrorAction(
        {
          message: 'bad_request'
        }
      ))
    } else if (error.response?.status === 500) {
      yield put(loginErrorAction(
        {
          message: 'server_error'
        }
      ))
    } else {
      yield put(loginErrorAction(
        {
          message: 'connection_error'
        }
      ))
    }
  }
}

function* watchLogin(): Generator {
  yield takeLatest(ProfileActionTypes.LOGIN_AUTH_REQUEST, handleLogin);
}

function* handleGetMe(): Generator {
  try {
    const res = (yield call(getMe)) as AxiosResponse;
    if (res.status === 200) {
      yield put(getMeSuccessAction({ profile: res.data }));
    }
  } catch (err) {
    yield put(getMeErrorAction({ message: '' }));
  }
}

function* handleChangePassword({ payload }: ReturnType<typeof changePasswordRequestAction>): Generator {
  try {
    const res = (yield call(changePassword, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(changePasswordSuccessAction());
    }
  } catch (err) {
    yield put(changePasswordErrorAction({ message: '' }));
  }
}

function* watchGetMe(): Generator {
  yield takeLatest(ProfileActionTypes.GET_ME_REQUEST, handleGetMe);
}

function* watchChangePassword(): Generator {
  yield takeLatest(ProfileActionTypes.CHANGE_PASSWORD_REQUEST, handleChangePassword);
}

export function* profileSaga(): Generator {
  yield all([fork(watchLogin)]);
  yield all([fork(watchGetMe)]);
  yield all([fork(watchChangePassword)]);
}

import { createAction, createReducer } from "@reduxjs/toolkit";
import {
	IAuth,
	IChangePasswordHold,
	IProfile,
	ProfileActionTypes,
	initialState,
} from "../../../domain/profile";

const loginRequestAction = createAction<
	{ auth: IAuth },
	ProfileActionTypes.LOGIN_AUTH_REQUEST
>(ProfileActionTypes.LOGIN_AUTH_REQUEST);

const loginSuccessAction = createAction<
	{
		accessToken: string;
		// accessToken: string;
		refreshToken: string;
	},
	ProfileActionTypes.LOGIN_AUTH_SUCCESS
>(ProfileActionTypes.LOGIN_AUTH_SUCCESS);

const loginErrorAction = createAction<
	{ message: string },
	ProfileActionTypes.LOGIN_AUTH_ERROR
>(ProfileActionTypes.LOGIN_AUTH_ERROR);

const getMeRequestAction = createAction(ProfileActionTypes.GET_ME_REQUEST);

const getMeSuccessAction = createAction<
	{ profile: IProfile },
	ProfileActionTypes.GET_ME_SUCCESS
>(ProfileActionTypes.GET_ME_SUCCESS);

const getMeErrorAction = createAction<
	{ message: string },
	ProfileActionTypes.GET_ME_ERROR
>(ProfileActionTypes.GET_ME_ERROR);

const changePasswordRequestAction = createAction<
	{ changePasswordHold: IChangePasswordHold },
	ProfileActionTypes.CHANGE_PASSWORD_REQUEST
>(ProfileActionTypes.CHANGE_PASSWORD_REQUEST);

const changePasswordSuccessAction = createAction(ProfileActionTypes.CHANGE_PASSWORD_SUCCESS);

const changePasswordErrorAction = createAction<
	{ message: string },
	ProfileActionTypes.CHANGE_PASSWORD_ERROR
>(ProfileActionTypes.CHANGE_PASSWORD_ERROR);

const logoutAction = createAction(ProfileActionTypes.LOGOUT);

const reducer = createReducer(initialState, (builder) => {
	builder
		.addCase(loginRequestAction, (state) => {
			state.isLoginLoading = true;
			state.isLoginSuccess = false;
			state.loginError = {
				isError: false,
				message: undefined,
			};
			state.token = undefined;
		})
		.addCase(loginSuccessAction, (state, action) => {
			state.isLoginLoading = false;
			state.isLoginSuccess = true;
			state.loginError = {
				isError: false,
				message: undefined,
			};
			state.token = {
				accessToken: action.payload.accessToken,
				refreshToken: action.payload.refreshToken
			}
		})
		.addCase(loginErrorAction, (state, action) => {
			state.isLoginLoading = false;
			state.isLoginSuccess = false;
			state.loginError = {
				isError: true,
				message: action.payload.message,
			};
			state.token = undefined;
		})
		.addCase(getMeRequestAction, (state) => {
			state.isGetMeLoading = true;
			state.isGetMeSuccess = false;
			state.getMeError = {
				isError: false,
			};
			state.profile = undefined;
		})
		.addCase(getMeSuccessAction, (state, action) => {
			state.isGetMeLoading = false;
			state.isGetMeSuccess = true;
			state.getMeError = {
				isError: false,
			};
			state.profile = action.payload.profile;
		})
		.addCase(getMeErrorAction, (state, action) => {
			state.isGetMeLoading = false;
			state.isGetMeSuccess = false;
			state.getMeError = {
				isError: true,
				message: action.payload.message,
			};
			state.profile = undefined;
		})
		.addCase(changePasswordRequestAction, (state) => {
			state.isChangePasswordLoading = true;
			state.isChangePasswordSuccess = false;
			state.changePasswordError = {
				isError: false,
			};
		})
		.addCase(changePasswordSuccessAction, (state) => {
			state.isChangePasswordLoading = false;
			state.isChangePasswordSuccess = true;
			state.changePasswordError = {
				isError: false,
			};
		})
		.addCase(changePasswordErrorAction, (state, action) => {
			state.isChangePasswordLoading = false;
			state.isChangePasswordSuccess = false;
			state.changePasswordError = {
				isError: true,
				message: action.payload.message,
			};
		})
		.addCase(logoutAction, (state) => {
			state.profile = undefined;
			state.isLoginSuccess = false;
			state.token = undefined;
		});
});

export default reducer;
export {
	loginRequestAction,
	loginSuccessAction,
	loginErrorAction,
	getMeRequestAction,
	getMeSuccessAction,
	getMeErrorAction,
	changePasswordRequestAction,
	changePasswordSuccessAction,
	changePasswordErrorAction,
	logoutAction
};

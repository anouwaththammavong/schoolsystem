import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateStudentAssessmentForm,
  StudentAssessmentActionTypes,
  StudentAssessments,
  UpdateStudentAssessmentForm,
  initialState,
} from "../../../domain/StudentAssessments";

// GET STUDENT ASSESSMENTS
const getStudentAssessmentsRequestAction = createAction<
  {
    limit: number,
    offset: number,
    level_id: string,
    room_id: string,
    generation: string
  },
  StudentAssessmentActionTypes.GET_STUDENT_ASSESSMENTS_REQUEST
>(StudentAssessmentActionTypes.GET_STUDENT_ASSESSMENTS_REQUEST);
const getStudentAssessmentsSuccessAction = createAction<
  { studentAssessments: StudentAssessments },
  StudentAssessmentActionTypes.GET_STUDENT_ASSESSMENTS_SUCCESS
>(StudentAssessmentActionTypes.GET_STUDENT_ASSESSMENTS_SUCCESS);
const getStudentAssessmentsErrorAction = createAction<
  { message: string },
  StudentAssessmentActionTypes.GET_STUDENT_ASSESSMENTS_ERROR
>(StudentAssessmentActionTypes.GET_STUDENT_ASSESSMENTS_ERROR);

// Create STUDENT ASSESSMENTS
const createStudentAssessmentRequestAction = createAction<
  { studentAssessment: CreateStudentAssessmentForm },
  StudentAssessmentActionTypes.CREATE_STUDENT_ASSESSMENT_REQUEST
>(StudentAssessmentActionTypes.CREATE_STUDENT_ASSESSMENT_REQUEST);
const createStudentAssessmentSuccessAction = createAction(
  StudentAssessmentActionTypes.CREATE_STUDENT_ASSESSMENT_SUCCESS
);
const createStudentAssessmentErrorAction = createAction<
  { message: string },
  StudentAssessmentActionTypes.CREATE_STUDENT_ASSESSMENT_ERROR
>(StudentAssessmentActionTypes.CREATE_STUDENT_ASSESSMENT_ERROR);
const createStudentAssessmentResetAction = createAction(
  StudentAssessmentActionTypes.CREATE_STUDENT_ASSESSMENT_RESET
);

// Update STUDENT ASSESSMENTS
const updateStudentAssessmentRequestAction = createAction<
  { _id: string; studentAssessment: UpdateStudentAssessmentForm },
  StudentAssessmentActionTypes.UPDATE_STUDENT_ASSESSMENT_REQUEST
>(StudentAssessmentActionTypes.UPDATE_STUDENT_ASSESSMENT_REQUEST);
const updateStudentAssessmentSuccessAction = createAction(
  StudentAssessmentActionTypes.UPDATE_STUDENT_ASSESSMENT_SUCCESS
);
const updateStudentAssessmentErrorAction = createAction<
  { message: string },
  StudentAssessmentActionTypes.UPDATE_STUDENT_ASSESSMENT_ERROR
>(StudentAssessmentActionTypes.UPDATE_STUDENT_ASSESSMENT_ERROR);
const updateStudentAssessmentResetAction = createAction(
  StudentAssessmentActionTypes.UPDATE_STUDENT_ASSESSMENT_RESET
);

// Delete STUDENT ASSESSMENTS
const deleteStudentAssessmentRequestAction = createAction<
  { _id: string },
  StudentAssessmentActionTypes.DELETE_STUDENT_ASSESSMENT_REQUEST
>(StudentAssessmentActionTypes.DELETE_STUDENT_ASSESSMENT_REQUEST);
const deleteStudentAssessmentSuccessAction = createAction(
  StudentAssessmentActionTypes.DELETE_STUDENT_ASSESSMENT_SUCCESS
);
const deleteStudentAssessmentErrorAction = createAction<
  { message: string },
  StudentAssessmentActionTypes.DELETE_STUDENT_ASSESSMENT_ERROR
>(StudentAssessmentActionTypes.DELETE_STUDENT_ASSESSMENT_ERROR);
const deleteStudentAssessmentResetAction = createAction(
  StudentAssessmentActionTypes.DELETE_STUDENT_ASSESSMENT_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // GET STUDENT ASSESSMENTS
    .addCase(getStudentAssessmentsRequestAction, (state) => {
      (state.studentAssessments = {
        studentAssessments: {
          studentAssessments: [],
          totalStudents: 0,
        },
      }),
        (state.isGetStudentAssessmentsLoading = true);
      state.isGetStudentAssessmentsSuccess = false;
      state.getStudentAssessmentsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getStudentAssessmentsSuccessAction, (state, action) => {
      state.studentAssessments = action.payload.studentAssessments;
      state.isGetStudentAssessmentsLoading = false;
      state.isGetStudentAssessmentsSuccess = true;
      state.getStudentAssessmentsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getStudentAssessmentsErrorAction, (state, action) => {
      (state.studentAssessments = {
        studentAssessments: {
          studentAssessments: [],
          totalStudents: 0,
        },
      }),
        (state.isGetStudentAssessmentsLoading = false);
      state.isGetStudentAssessmentsSuccess = false;
      state.getStudentAssessmentsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create STUDENT ASSESSMENTS
    .addCase(createStudentAssessmentRequestAction, (state) => {
      state.isCreateStudentAssessmentLoading = true;
      state.isCreateStudentAssessmentSuccess = false;
      state.createStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createStudentAssessmentSuccessAction, (state) => {
      state.isCreateStudentAssessmentLoading = false;
      state.isCreateStudentAssessmentSuccess = true;
      state.createStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createStudentAssessmentErrorAction, (state, action) => {
      state.isCreateStudentAssessmentLoading = false;
      state.isCreateStudentAssessmentSuccess = false;
      state.createStudentAssessmentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createStudentAssessmentResetAction, (state) => {
      state.isCreateStudentAssessmentLoading = false;
      state.isCreateStudentAssessmentSuccess = false;
      state.createStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    // Update STUDENT ASSESSMENTS
    .addCase(updateStudentAssessmentRequestAction, (state) => {
      state.isUpdateStudentAssessmentLoading = true;
      state.isUpdateStudentAssessmentSuccess = false;
      state.updateStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateStudentAssessmentSuccessAction, (state) => {
      state.isUpdateStudentAssessmentLoading = false;
      state.isUpdateStudentAssessmentSuccess = true;
      state.updateStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateStudentAssessmentErrorAction, (state, action) => {
      state.isUpdateStudentAssessmentLoading = false;
      state.isUpdateStudentAssessmentSuccess = false;
      state.updateStudentAssessmentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateStudentAssessmentResetAction, (state) => {
      state.isUpdateStudentAssessmentLoading = false;
      state.isUpdateStudentAssessmentSuccess = false;
      state.updateStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete STUDENT ASSESSMENTS
    .addCase(deleteStudentAssessmentRequestAction, (state) => {
      state.isDeleteStudentAssessmentLoading = true;
      state.isDeleteStudentAssessmentSuccess = false;
      state.deleteStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteStudentAssessmentSuccessAction, (state) => {
      state.isDeleteStudentAssessmentLoading = false;
      state.isDeleteStudentAssessmentSuccess = true;
      state.deleteStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteStudentAssessmentErrorAction, (state, action) => {
      state.isDeleteStudentAssessmentLoading = false;
      state.isDeleteStudentAssessmentSuccess = false;
      state.deleteStudentAssessmentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteStudentAssessmentResetAction, (state) => {
      state.isDeleteStudentAssessmentLoading = false;
      state.isDeleteStudentAssessmentSuccess = false;
      state.deleteStudentAssessmentError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
    getStudentAssessmentsRequestAction,
    getStudentAssessmentsSuccessAction,
    getStudentAssessmentsErrorAction,
    createStudentAssessmentRequestAction,
    createStudentAssessmentSuccessAction,
    createStudentAssessmentErrorAction,
    createStudentAssessmentResetAction,
    updateStudentAssessmentRequestAction,
    updateStudentAssessmentSuccessAction,
    updateStudentAssessmentErrorAction,
    updateStudentAssessmentResetAction,
    deleteStudentAssessmentRequestAction,
    deleteStudentAssessmentSuccessAction,
    deleteStudentAssessmentErrorAction,
    deleteStudentAssessmentResetAction
}
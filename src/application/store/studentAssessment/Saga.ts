import { AxiosResponse } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createStudentAssessmentErrorAction,
  createStudentAssessmentRequestAction,
  createStudentAssessmentSuccessAction,
  deleteStudentAssessmentErrorAction,
  deleteStudentAssessmentRequestAction,
  deleteStudentAssessmentSuccessAction,
  getStudentAssessmentsErrorAction,
  getStudentAssessmentsRequestAction,
  getStudentAssessmentsSuccessAction,
  updateStudentAssessmentErrorAction,
  updateStudentAssessmentRequestAction,
  updateStudentAssessmentSuccessAction,
} from "./Reducer";
import {
  createStudentAssessmentService,
  deleteStudentAssessmentService,
  getStudentAssessmentsService,
  updateStudentAssessmentService,
} from "../../service/studentAssessment";
import { StudentAssessmentActionTypes } from "../../../domain/StudentAssessments";

function* getStudentAssessments({
  payload,
}: ReturnType<typeof getStudentAssessmentsRequestAction>): Generator {
  try {
    const res = (yield call(
      getStudentAssessmentsService,
      payload
    )) as AxiosResponse;
    yield put(
      getStudentAssessmentsSuccessAction({ studentAssessments: res.data })
    );
  } catch (err) {
    yield put(getStudentAssessmentsErrorAction({ message: "404" }));
  }
}

function* createStudentAssessment({
  payload,
}: ReturnType<typeof createStudentAssessmentRequestAction>): Generator {
  try {
    const res = (yield call(
      createStudentAssessmentService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(createStudentAssessmentSuccessAction());
    }
  } catch (err) {
    yield put(createStudentAssessmentErrorAction({ message: "404" }));
  }
}

function* updateStudentAssessment({
  payload,
}: ReturnType<typeof updateStudentAssessmentRequestAction>): Generator {
  try {
    const res = (yield call(
      updateStudentAssessmentService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateStudentAssessmentSuccessAction());
    }
  } catch (err) {
    yield put(updateStudentAssessmentErrorAction({ message: "404" }));
  }
}

function* deleteStudentAssessment({
  payload,
}: ReturnType<typeof deleteStudentAssessmentRequestAction>): Generator {
  try {
    const res = (yield call(
      deleteStudentAssessmentService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteStudentAssessmentSuccessAction());
    }
  } catch (err) {
    yield put(deleteStudentAssessmentErrorAction({ message: "404" }));
  }
}

function* watchGetStudentAssessments(): Generator {
  yield takeLatest(
    StudentAssessmentActionTypes.GET_STUDENT_ASSESSMENTS_REQUEST,
    getStudentAssessments
  );
}

function* watchCreateStudentAssessment(): Generator {
  yield takeLatest(
    StudentAssessmentActionTypes.CREATE_STUDENT_ASSESSMENT_REQUEST,
    createStudentAssessment
  );
}

function* watchUpdateStudentAssessment(): Generator {
  yield takeLatest(
    StudentAssessmentActionTypes.UPDATE_STUDENT_ASSESSMENT_REQUEST,
    updateStudentAssessment
  );
}

function* watchDeleteStudentAssessment(): Generator {
  yield takeLatest(
    StudentAssessmentActionTypes.DELETE_STUDENT_ASSESSMENT_REQUEST,
    deleteStudentAssessment
  );
}


export function* studentAssessmentSaga(): Generator {
  yield all([fork(watchGetStudentAssessments)]);
  yield all([fork(watchCreateStudentAssessment)]);
  yield all([fork(watchUpdateStudentAssessment)]);
  yield all([fork(watchDeleteStudentAssessment)]);
}

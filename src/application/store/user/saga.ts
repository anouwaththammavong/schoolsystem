import { AxiosResponse } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import { UserActionTypes } from "../../../domain/user";
import {
  createUsersService,
  getAllUsersService,
  getUserByIdService,
  getUserClaimItemsService,
  getUserPickUpItemsService,
  getUsersService,
  updateUserService,
  updateUserStatusService,
} from "../../service/user";
import {
  createUserErrorAction,
  createUserRequestAction,
  createUserSuccessAction,
  getAllUsersSuccessAction,
  getUserClaimItemsErrorAction,
  getUserClaimItemsRequestAction,
  getUserClaimItemsSuccessAction,
  getUserErrorAction,
  getUserPickUpItemsErrorAction,
  getUserPickUpItemsRequestAction,
  getUserPickUpItemsSuccessAction,
  getUserRequestAction,
  getUserSuccessAction,
  getUsersErrorAction,
  getUsersRequestAction,
  getUsersSuccessAction,
  updateUserErrorAction,
  updateUserRequestAction,
  updateUserStatusErrorAction,
  updateUserStatusRequestAction,
  updateUserStatusSuccessAction,
  updateUserSuccessAction,
} from "./reducer";

function* getUsers({
  payload,
}: ReturnType<typeof getUsersRequestAction>): Generator {
  try {
    const res = (yield call(getUsersService, payload)) as AxiosResponse;
    yield put(getUsersSuccessAction({ users: res.data }));
  } catch (err) {
    yield put(getUsersErrorAction({ message: "404" }));
  }
}

function* createUser({
  payload,
}: ReturnType<typeof createUserRequestAction>): Generator {
  try {
    const res = (yield call(createUsersService, payload)) as AxiosResponse;
    yield put(createUserSuccessAction());
  } catch (err) {
    yield put(createUserErrorAction({ message: "404" }));
  }
}

function* updateUser({
  payload,
}: ReturnType<typeof updateUserRequestAction>): Generator {
  try {
    const res = (yield call(updateUserService, payload)) as AxiosResponse;
    yield put(updateUserSuccessAction());
  } catch (err) {
    yield put(updateUserErrorAction({ message: "404" }));
  }
}

function* getUserPickUpItems({
  payload,
}: ReturnType<typeof getUserPickUpItemsRequestAction>): Generator {
  try {
    const res = (yield call(
      getUserPickUpItemsService,
      payload
    )) as AxiosResponse;
    yield put(
      getUserPickUpItemsSuccessAction({
        userPickUpItems: res.data.data,
        userPickUpItemsLength: res.data.all,
      })
    );
  } catch (err) {
    yield put(getUserPickUpItemsErrorAction({ message: "404" }));
  }
}

function* getUserClaimItems({
  payload,
}: ReturnType<typeof getUserClaimItemsRequestAction>): Generator {
  try {
    const res = (yield call(
      getUserClaimItemsService,
      payload
    )) as AxiosResponse;
    yield put(
      getUserClaimItemsSuccessAction({
        userClaimItems: res.data.data,
        userClaimItemsLength: res.data.all,
      })
    );
  } catch (err) {
    yield put(getUserClaimItemsErrorAction({ message: "404" }));
  }
}

function* getUser({
  payload,
}: ReturnType<typeof getUserRequestAction>): Generator {
  try {
    const res = (yield call(getUserByIdService, payload)) as AxiosResponse;
    yield put(getUserSuccessAction({ user: res.data }));
  } catch (err) {
    yield put(getUserErrorAction({ message: "404" }));
  }
}

function* getAllUsers(): Generator {
  try {
    const res = (yield call(getAllUsersService)) as AxiosResponse;
    yield put(
      getAllUsersSuccessAction({
        users: res.data.data,
        usersLengthAll: res.data.all,
      })
    );
  } catch (err) {
    yield put(getUsersErrorAction({ message: "404" }));
  }
}

function* updateUserStatus({
  payload,
}: ReturnType<typeof updateUserStatusRequestAction>): Generator {
  try {
    const res = (yield call(updateUserStatusService, payload)) as AxiosResponse;
    yield put(updateUserStatusSuccessAction());
  } catch (err) {
    yield put(updateUserStatusErrorAction({ message: "404" }));
  }
}

function* watchGetUsers(): Generator {
  yield takeLatest(UserActionTypes.GET_USERS_REQUEST, getUsers);
}

function* watchCreateUsers(): Generator {
  yield takeLatest(UserActionTypes.CREATE_USER_REQUEST, createUser);
}

function* watchUpdateUser(): Generator {
  yield takeLatest(UserActionTypes.UPDATE_USER_REQUEST, updateUser);
}

function* watchGetUserPickUpItems(): Generator {
  yield takeLatest(
    UserActionTypes.GET_USER_PICK_UP_ITEMS_REQUEST,
    getUserPickUpItems
  );
}

function* watchGetUserClaimItems(): Generator {
  yield takeLatest(
    UserActionTypes.GET_USER_CLAIM_ITEMS_REQUEST,
    getUserClaimItems
  );
}

function* watchGetUser(): Generator {
  yield takeLatest(UserActionTypes.GET_USER_REQUEST, getUser);
}

function* watchGetAllUsers(): Generator {
  yield takeLatest(UserActionTypes.GET_ALL_USERS_REQUEST, getAllUsers);
}

function* watchUpdateUserStatus(): Generator {
  yield takeLatest(
    UserActionTypes.UPDATE_USER_STATUS_REQUEST,
    updateUserStatus
  );
}

export function* userSaga(): Generator {
  yield all([fork(watchGetUsers)]);
  yield all([fork(watchCreateUsers)]);
  yield all([fork(watchUpdateUser)]);
  yield all([fork(watchGetUserPickUpItems)]);
  yield all([fork(watchGetUserClaimItems)]);
  yield all([fork(watchGetUser)]);
  yield all([fork(watchGetAllUsers)]);
  yield all([fork(watchUpdateUserStatus)]);
}

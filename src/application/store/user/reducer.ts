import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  FilterUser,
  IGetUserClaimItem,
  IGetUserPickUpItem,
  initialState,
  User,
  UserActionTypes,
  UpdateUser,
  ICreateUser,
  Users,
} from "../../../domain/user";

// Get
const getUsersRequestAction = createAction<
  { limit: number; offset: number },
  UserActionTypes.GET_USERS_REQUEST
>(UserActionTypes.GET_USERS_REQUEST);
const getUsersSuccessAction = createAction<
  {
    users: Users;
  },
  UserActionTypes.GET_USERS_SUCCESS
>(UserActionTypes.GET_USERS_SUCCESS);
const getUsersErrorAction = createAction<
  { message: string },
  UserActionTypes.GET_USERS_ERROR
>(UserActionTypes.GET_USERS_ERROR);

// Create
const createUserRequestAction = createAction<
  { user: ICreateUser },
  UserActionTypes.CREATE_USER_REQUEST
>(UserActionTypes.CREATE_USER_REQUEST);
const createUserSuccessAction = createAction(
  UserActionTypes.CREATE_USER_SUCCESS
);
const createUserErrorAction = createAction<
  { message: string },
  UserActionTypes.CREATE_USER_ERROR
>(UserActionTypes.CREATE_USER_ERROR);
const createUserResetAction = createAction(UserActionTypes.CREATE_USER_RESET);

// Update
const updateUserRequestAction = createAction<
  { _id: string; user: UpdateUser },
  UserActionTypes.UPDATE_USER_REQUEST
>(UserActionTypes.UPDATE_USER_REQUEST);
const updateUserSuccessAction = createAction(
  UserActionTypes.UPDATE_USER_SUCCESS
);
const updateUserErrorAction = createAction<
  { message: string },
  UserActionTypes.UPDATE_USER_ERROR
>(UserActionTypes.UPDATE_USER_ERROR);
const updateUserResetAction = createAction(UserActionTypes.UPDATE_USER_RESET);

// Get
const getUserPickUpItemsRequestAction = createAction<
  { id: string; limit: number; offset: number },
  UserActionTypes.GET_USER_PICK_UP_ITEMS_REQUEST
>(UserActionTypes.GET_USER_PICK_UP_ITEMS_REQUEST);
const getUserPickUpItemsSuccessAction = createAction<
  {
    userPickUpItems: IGetUserPickUpItem[];
    userPickUpItemsLength: number;
  },
  UserActionTypes.GET_USER_PICK_UP_ITEMS_SUCCESS
>(UserActionTypes.GET_USER_PICK_UP_ITEMS_SUCCESS);
const getUserPickUpItemsErrorAction = createAction<
  { message: string },
  UserActionTypes.GET_USER_PICK_UP_ITEMS_ERROR
>(UserActionTypes.GET_USER_PICK_UP_ITEMS_ERROR);

// Get
const getUserClaimItemsRequestAction = createAction<
  { id: string; limit: number; offset: number },
  UserActionTypes.GET_USER_CLAIM_ITEMS_REQUEST
>(UserActionTypes.GET_USER_CLAIM_ITEMS_REQUEST);
const getUserClaimItemsSuccessAction = createAction<
  {
    userClaimItems: IGetUserClaimItem[];
    userClaimItemsLength: number;
  },
  UserActionTypes.GET_USER_CLAIM_ITEMS_SUCCESS
>(UserActionTypes.GET_USER_CLAIM_ITEMS_SUCCESS);
const getUserClaimItemsErrorAction = createAction<
  { message: string },
  UserActionTypes.GET_USER_CLAIM_ITEMS_ERROR
>(UserActionTypes.GET_USER_CLAIM_ITEMS_ERROR);

// Get
const getUserRequestAction = createAction<
  { id: string },
  UserActionTypes.GET_USER_REQUEST
>(UserActionTypes.GET_USER_REQUEST);
const getUserSuccessAction = createAction<
  { user: User },
  UserActionTypes.GET_USER_SUCCESS
>(UserActionTypes.GET_USER_SUCCESS);
const getUserErrorAction = createAction<
  { message: string },
  UserActionTypes.GET_USER_ERROR
>(UserActionTypes.GET_USER_ERROR);

// Get
const getAllUsersRequestAction = createAction(
  UserActionTypes.GET_ALL_USERS_REQUEST
);
const getAllUsersSuccessAction = createAction<
  {
    users: User[];
    usersLengthAll: number;
  },
  UserActionTypes.GET_ALL_USERS_SUCCESS
>(UserActionTypes.GET_ALL_USERS_SUCCESS);
const getAllUsersErrorAction = createAction<
  { message: string },
  UserActionTypes.GET_ALL_USERS_ERROR
>(UserActionTypes.GET_ALL_USERS_ERROR);

// Update status
const updateUserStatusRequestAction = createAction<
  { userId: string },
  UserActionTypes.UPDATE_USER_STATUS_REQUEST
>(UserActionTypes.UPDATE_USER_STATUS_REQUEST);
const updateUserStatusSuccessAction = createAction(
  UserActionTypes.UPDATE_USER_STATUS_SUCCESS
);
const updateUserStatusErrorAction = createAction<
  { message: string },
  UserActionTypes.UPDATE_USER_STATUS_ERROR
>(UserActionTypes.UPDATE_USER_STATUS_ERROR);
const updateUserStatusResetAction = createAction(
  UserActionTypes.UPDATE_USER_STATUS_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get
    .addCase(getUsersRequestAction, (state) => {
      state.users = {
        users: [],
        totalUser: 0,
      };
      state.isGetUsersLoading = true;
      state.isGetUsersSuccess = false;
      state.getUsersError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUsersSuccessAction, (state, action) => {
      state.users = action.payload.users;
      state.isGetUsersLoading = false;
      state.isGetUsersSuccess = true;
      state.getUsersError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUsersErrorAction, (state, action) => {
      state.users = {
        users: [],
        totalUser: 0,
      };
      state.isGetUsersLoading = false;
      state.isGetUsersSuccess = false;
      state.getUsersError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create
    .addCase(createUserRequestAction, (state) => {
      state.isCreateUserLoading = true;
      state.isCreateUserSuccess = false;
      state.createUserError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createUserSuccessAction, (state) => {
      state.isCreateUserLoading = false;
      state.isCreateUserSuccess = true;
      state.createUserError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createUserErrorAction, (state, action) => {
      state.isCreateUserLoading = false;
      state.isCreateUserSuccess = false;
      state.createUserError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createUserResetAction, (state) => {
      state.isCreateUserLoading = false;
      state.isCreateUserSuccess = false;
      state.createUserError = {
        isError: false,
        message: undefined,
      };
    })
    // Update
    .addCase(updateUserRequestAction, (state) => {
      state.isUpdateUserLoading = true;
      state.isUpdateUserSuccess = false;
      state.updateUserError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateUserSuccessAction, (state) => {
      state.isUpdateUserLoading = false;
      state.isUpdateUserSuccess = true;
      state.updateUserError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateUserErrorAction, (state, action) => {
      state.isUpdateUserLoading = false;
      state.isUpdateUserSuccess = false;
      state.updateUserError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateUserResetAction, (state) => {
      state.isUpdateUserLoading = false;
      state.isUpdateUserSuccess = false;
      state.updateUserError = {
        isError: false,
        message: undefined,
      };
    })
    // Get
    .addCase(getUserPickUpItemsRequestAction, (state) => {
      state.userPickUpItems = {
        data: [],
        all: 0,
      };
      state.isGetUserPickUpItemsLoading = true;
      state.isGetUserPickUpItemsSuccess = false;
      state.getUserPickUpItemsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUserPickUpItemsSuccessAction, (state, action) => {
      state.userPickUpItems = {
        data: action.payload.userPickUpItems,
        all: action.payload.userPickUpItemsLength,
      };
      state.isGetUserPickUpItemsLoading = false;
      state.isGetUserPickUpItemsSuccess = true;
      state.getUserPickUpItemsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUserPickUpItemsErrorAction, (state, action) => {
      state.userPickUpItems = {
        data: [],
        all: 0,
      };
      state.isGetUserPickUpItemsLoading = false;
      state.isGetUserPickUpItemsSuccess = false;
      state.getUserPickUpItemsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Get
    .addCase(getUserClaimItemsRequestAction, (state) => {
      state.userClaimItems = {
        data: [],
        all: 0,
      };
      state.isGetUserClaimItemsLoading = true;
      state.isGetUserClaimItemsSuccess = false;
      state.getUserClaimItemsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUserClaimItemsSuccessAction, (state, action) => {
      state.userClaimItems = {
        data: action.payload.userClaimItems,
        all: action.payload.userClaimItemsLength,
      };
      state.isGetUserClaimItemsLoading = false;
      state.isGetUserClaimItemsSuccess = true;
      state.getUserClaimItemsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUserClaimItemsErrorAction, (state, action) => {
      state.userClaimItems = {
        data: [],
        all: 0,
      };
      state.isGetUserClaimItemsLoading = false;
      state.isGetUserClaimItemsSuccess = false;
      state.getUserClaimItemsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Get
    .addCase(getUserRequestAction, (state) => {
      state.user = undefined;
      state.isGetUserLoading = true;
      state.isGetUserSuccess = false;
      state.getUserError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUserSuccessAction, (state, action) => {
      state.user = action.payload.user;
      state.isGetUserLoading = false;
      state.isGetUserSuccess = true;
      state.getUserError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getUserErrorAction, (state, action) => {
      state.user = undefined;
      state.isGetUserLoading = false;
      state.isGetUserSuccess = false;
      state.getUserError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Get
    .addCase(getAllUsersRequestAction, (state) => {
      state.isGetAllUsersLoading = true;
      state.isGetAllUsersSuccess = false;
      state.getAllUsersError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getAllUsersSuccessAction, (state, action) => {
      state.allUsers = {
        data: action.payload.users,
        all: action.payload.usersLengthAll,
      };
      state.isGetAllUsersLoading = false;
      state.isGetAllUsersSuccess = true;
      state.getAllUsersError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getAllUsersErrorAction, (state, action) => {
      state.allUsers = {
        data: [],
        all: 0,
      };
      state.isGetAllUsersLoading = false;
      state.isGetAllUsersSuccess = false;
      state.getAllUsersError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Update
    .addCase(updateUserStatusRequestAction, (state) => {
      state.isUpdateUserStatusLoading = true;
      state.isUpdateUserStatusSuccess = false;
      state.updateUserStatusError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateUserStatusSuccessAction, (state) => {
      state.isUpdateUserStatusLoading = false;
      state.isUpdateUserStatusSuccess = true;
      state.updateUserStatusError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateUserStatusErrorAction, (state, action) => {
      state.isUpdateUserStatusLoading = false;
      state.isUpdateUserStatusSuccess = false;
      state.updateUserStatusError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateUserStatusResetAction, (state) => {
      state.isUpdateUserStatusLoading = false;
      state.isUpdateUserStatusSuccess = false;
      state.updateUserStatusError = {
        isError: false,
        message: undefined,
      };
    });
});
export default reducer;
export {
  getUsersRequestAction,
  getUsersSuccessAction,
  getUsersErrorAction,
  createUserRequestAction,
  createUserSuccessAction,
  createUserErrorAction,
  createUserResetAction,
  updateUserRequestAction,
  updateUserSuccessAction,
  updateUserErrorAction,
  updateUserResetAction,
  getUserPickUpItemsRequestAction,
  getUserPickUpItemsSuccessAction,
  getUserPickUpItemsErrorAction,
  getUserClaimItemsRequestAction,
  getUserClaimItemsSuccessAction,
  getUserClaimItemsErrorAction,
  getUserRequestAction,
  getUserSuccessAction,
  getUserErrorAction,
  getAllUsersRequestAction,
  getAllUsersSuccessAction,
  getAllUsersErrorAction,
  updateUserStatusRequestAction,
  updateUserStatusSuccessAction,
  updateUserStatusErrorAction,
  updateUserStatusResetAction,
};

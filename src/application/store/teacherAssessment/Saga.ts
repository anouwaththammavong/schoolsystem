import { AxiosError, AxiosResponse } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createTeacherAssessmentErrorAction,
  createTeacherAssessmentRequestAction,
  createTeacherAssessmentSuccessAction,
  deleteTeacherAssessmentErrorAction,
  deleteTeacherAssessmentRequestAction,
  deleteTeacherAssessmentSuccessAction,
  getTeacherAssessmentsErrorAction,
  getTeacherAssessmentsRequestAction,
  getTeacherAssessmentsSuccessAction,
  updateTeacherAssessmentErrorAction,
  updateTeacherAssessmentRequestAction,
  updateTeacherAssessmentSuccessAction,
} from "./Reducer";
import {
  createTeacherAssessmentService,
  deleteTeacherAssessmentService,
  getTeacherAssessmentsService,
  updateTeacherAssessmentService,
} from "../../service/TeacherAssessment";
import { TeacherAssessmentActionTypes } from "../../../domain/TeacherAssessments";

type ErrorMessage = {
  error: string;
};

function* getTeacherAssessments({
  payload,
}: ReturnType<typeof getTeacherAssessmentsRequestAction>): Generator {
  try {
    const res = (yield call(
      getTeacherAssessmentsService,
      payload
    )) as AxiosResponse;
    yield put(
      getTeacherAssessmentsSuccessAction({ teacherAssessments: res.data })
    );
  } catch (err) {
    yield put(getTeacherAssessmentsErrorAction({ message: "404" }));
  }
}

function* createTeacherAssessment({
  payload,
}: ReturnType<typeof createTeacherAssessmentRequestAction>): Generator {
  try {
    const res = (yield call(
      createTeacherAssessmentService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(createTeacherAssessmentSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createTeacherAssessmentErrorAction({
          message: error.response?.data.error,
        })
      );
    }
  }
}

function* updateTeacherAssessment({
  payload,
}: ReturnType<typeof updateTeacherAssessmentRequestAction>): Generator {
  try {
    const res = (yield call(
      updateTeacherAssessmentService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateTeacherAssessmentSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        updateTeacherAssessmentErrorAction({
          message: error.response?.data.error,
        })
      );
    }
  }
}

function* deleteTeacherAssessment({
  payload,
}: ReturnType<typeof deleteTeacherAssessmentRequestAction>): Generator {
  try {
    const res = (yield call(
      deleteTeacherAssessmentService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteTeacherAssessmentSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        deleteTeacherAssessmentErrorAction({
          message: error.response?.data.error,
        })
      );
    }
  }
}

function* watchGetTeacherAssessments(): Generator {
  yield takeLatest(
    TeacherAssessmentActionTypes.GET_TEACHER_ASSESSMENTS_REQUEST,
    getTeacherAssessments
  );
}

function* watchCreateTeacherAssessment(): Generator {
  yield takeLatest(
    TeacherAssessmentActionTypes.CREATE_TEACHER_ASSESSMENT_REQUEST,
    createTeacherAssessment
  );
}

function* watchUpdateTeacherAssessment(): Generator {
  yield takeLatest(
    TeacherAssessmentActionTypes.UPDATE_TEACHER_ASSESSMENT_REQUEST,
    updateTeacherAssessment
  );
}

function* watchDeleteTeacherAssessment(): Generator {
  yield takeLatest(
    TeacherAssessmentActionTypes.DELETE_TEACHER_ASSESSMENT_REQUEST,
    deleteTeacherAssessment
  );
}

export function* teacherAssessmentSaga(): Generator {
  yield all([fork(watchGetTeacherAssessments)]);
  yield all([fork(watchCreateTeacherAssessment)]);
  yield all([fork(watchUpdateTeacherAssessment)]);
  yield all([fork(watchDeleteTeacherAssessment)]);
}

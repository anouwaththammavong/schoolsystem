import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateTeacherAssessment,
  TeacherAssessmentActionTypes,
  TeacherAssessments,
  UpdateTeacherAssessment,
  initialState,
} from "../../../domain/TeacherAssessments";

// GET TEACHER ASSESSMENTS
const getTeacherAssessmentsRequestAction = createAction<
  {
    limit: number;
    offset: number;
    teacher_id?: string;
    assessment_date?: Date;
  },
  TeacherAssessmentActionTypes.GET_TEACHER_ASSESSMENTS_REQUEST
>(TeacherAssessmentActionTypes.GET_TEACHER_ASSESSMENTS_REQUEST);
const getTeacherAssessmentsSuccessAction = createAction<
  { teacherAssessments: TeacherAssessments },
  TeacherAssessmentActionTypes.GET_TEACHER_ASSESSMENTS_SUCCESS
>(TeacherAssessmentActionTypes.GET_TEACHER_ASSESSMENTS_SUCCESS);
const getTeacherAssessmentsErrorAction = createAction<
  { message: string },
  TeacherAssessmentActionTypes.GET_TEACHER_ASSESSMENTS_ERROR
>(TeacherAssessmentActionTypes.GET_TEACHER_ASSESSMENTS_ERROR);

// Create TEACHER ASSESSMENTS
const createTeacherAssessmentRequestAction = createAction<
  { teacherAssessment: CreateTeacherAssessment },
  TeacherAssessmentActionTypes.CREATE_TEACHER_ASSESSMENT_REQUEST
>(TeacherAssessmentActionTypes.CREATE_TEACHER_ASSESSMENT_REQUEST);
const createTeacherAssessmentSuccessAction = createAction(
  TeacherAssessmentActionTypes.CREATE_TEACHER_ASSESSMENT_SUCCESS
);
const createTeacherAssessmentErrorAction = createAction<
  { message: string },
  TeacherAssessmentActionTypes.CREATE_TEACHER_ASSESSMENT_ERROR
>(TeacherAssessmentActionTypes.CREATE_TEACHER_ASSESSMENT_ERROR);
const createTeacherAssessmentResetAction = createAction(
  TeacherAssessmentActionTypes.CREATE_TEACHER_ASSESSMENT_RESET
);

// Update TEACHER ASSESSMENTS
const updateTeacherAssessmentRequestAction = createAction<
  { _id: string; teacherAssessment: UpdateTeacherAssessment },
  TeacherAssessmentActionTypes.UPDATE_TEACHER_ASSESSMENT_REQUEST
>(TeacherAssessmentActionTypes.UPDATE_TEACHER_ASSESSMENT_REQUEST);
const updateTeacherAssessmentSuccessAction = createAction(
  TeacherAssessmentActionTypes.UPDATE_TEACHER_ASSESSMENT_SUCCESS
);
const updateTeacherAssessmentErrorAction = createAction<
  { message: string },
  TeacherAssessmentActionTypes.UPDATE_TEACHER_ASSESSMENT_ERROR
>(TeacherAssessmentActionTypes.UPDATE_TEACHER_ASSESSMENT_ERROR);
const updateTeacherAssessmentResetAction = createAction(
  TeacherAssessmentActionTypes.UPDATE_TEACHER_ASSESSMENT_RESET
);

// Delete TEACHER ASSESSMENTS
const deleteTeacherAssessmentRequestAction = createAction<
  { _id: string },
  TeacherAssessmentActionTypes.DELETE_TEACHER_ASSESSMENT_REQUEST
>(TeacherAssessmentActionTypes.DELETE_TEACHER_ASSESSMENT_REQUEST);
const deleteTeacherAssessmentSuccessAction = createAction(
  TeacherAssessmentActionTypes.DELETE_TEACHER_ASSESSMENT_SUCCESS
);
const deleteTeacherAssessmentErrorAction = createAction<
  { message: string },
  TeacherAssessmentActionTypes.DELETE_TEACHER_ASSESSMENT_ERROR
>(TeacherAssessmentActionTypes.DELETE_TEACHER_ASSESSMENT_ERROR);
const deleteTeacherAssessmentResetAction = createAction(
  TeacherAssessmentActionTypes.DELETE_TEACHER_ASSESSMENT_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // GET TEACHER ASSESSMENTS
    .addCase(getTeacherAssessmentsRequestAction, (state) => {
      (state.teacherAssessments = {
        teacherAssessments: {
          teacher_assessments: [],
          totalAssessment: 0,
        },
      }),
        (state.isGetTeacherAssessmentsLoading = true);
      state.isGetTeacherAssessmentsSuccess = false;
      state.getTeacherAssessmentsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getTeacherAssessmentsSuccessAction, (state, action) => {
      state.teacherAssessments = action.payload.teacherAssessments;
      state.isGetTeacherAssessmentsLoading = false;
      state.isGetTeacherAssessmentsSuccess = true;
      state.getTeacherAssessmentsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getTeacherAssessmentsErrorAction, (state, action) => {
      (state.teacherAssessments = {
        teacherAssessments: {
          teacher_assessments: [],
          totalAssessment: 0,
        },
      }),
        (state.isGetTeacherAssessmentsLoading = false);
      state.isGetTeacherAssessmentsSuccess = false;
      state.getTeacherAssessmentsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create TEACHER ASSESSMENTS
    .addCase(createTeacherAssessmentRequestAction, (state) => {
      state.isCreateTeacherAssessmentLoading = true;
      state.isCreateTeacherAssessmentSuccess = false;
      state.createTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createTeacherAssessmentSuccessAction, (state) => {
      state.isCreateTeacherAssessmentLoading = false;
      state.isCreateTeacherAssessmentSuccess = true;
      state.createTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createTeacherAssessmentErrorAction, (state, action) => {
      state.isCreateTeacherAssessmentLoading = false;
      state.isCreateTeacherAssessmentSuccess = false;
      state.createTeacherAssessmentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createTeacherAssessmentResetAction, (state) => {
      state.isCreateTeacherAssessmentLoading = false;
      state.isCreateTeacherAssessmentSuccess = false;
      state.createTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    // Update TEACHER ASSESSMENTS
    .addCase(updateTeacherAssessmentRequestAction, (state) => {
      state.isUpdateTeacherAssessmentLoading = true;
      state.isUpdateTeacherAssessmentSuccess = false;
      state.updateTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateTeacherAssessmentSuccessAction, (state) => {
      state.isUpdateTeacherAssessmentLoading = false;
      state.isUpdateTeacherAssessmentSuccess = true;
      state.updateTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateTeacherAssessmentErrorAction, (state, action) => {
      state.isUpdateTeacherAssessmentLoading = false;
      state.isUpdateTeacherAssessmentSuccess = false;
      state.updateTeacherAssessmentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateTeacherAssessmentResetAction, (state) => {
      state.isUpdateTeacherAssessmentLoading = false;
      state.isUpdateTeacherAssessmentSuccess = false;
      state.updateTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete TEACHER ASSESSMENTS
    .addCase(deleteTeacherAssessmentRequestAction, (state) => {
      state.isDeleteTeacherAssessmentLoading = true;
      state.isDeleteTeacherAssessmentSuccess = false;
      state.deleteTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteTeacherAssessmentSuccessAction, (state) => {
      state.isDeleteTeacherAssessmentLoading = false;
      state.isDeleteTeacherAssessmentSuccess = true;
      state.deleteTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteTeacherAssessmentErrorAction, (state, action) => {
      state.isDeleteTeacherAssessmentLoading = false;
      state.isDeleteTeacherAssessmentSuccess = false;
      state.deleteTeacherAssessmentError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteTeacherAssessmentResetAction, (state) => {
      state.isDeleteTeacherAssessmentLoading = false;
      state.isDeleteTeacherAssessmentSuccess = false;
      state.deleteTeacherAssessmentError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getTeacherAssessmentsRequestAction,
  getTeacherAssessmentsSuccessAction,
  getTeacherAssessmentsErrorAction,
  createTeacherAssessmentRequestAction,
  createTeacherAssessmentSuccessAction,
  createTeacherAssessmentErrorAction,
  createTeacherAssessmentResetAction,
  updateTeacherAssessmentRequestAction,
  updateTeacherAssessmentSuccessAction,
  updateTeacherAssessmentErrorAction,
  updateTeacherAssessmentResetAction,
  deleteTeacherAssessmentRequestAction,
  deleteTeacherAssessmentSuccessAction,
  deleteTeacherAssessmentErrorAction,
  deleteTeacherAssessmentResetAction
};

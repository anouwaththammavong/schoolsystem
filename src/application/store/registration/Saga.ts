import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createOldStudentRegistrationErrorAction,
  createOldStudentRegistrationRequestAction,
  createOldStudentRegistrationSuccessAction,
  createNewStudentRegistrationErrorAction,
  createNewStudentRegistrationRequestAction,
  createNewStudentRegistrationSuccessAction,
  deleteRegistrationErrorAction,
  deleteRegistrationRequestAction,
  deleteRegistrationSuccessAction,
  getRegistrationsErrorAction,
  getRegistrationsRequestAction,
  getRegistrationsSuccessAction,
  updateRegistrationErrorAction,
  updateRegistrationRequestAction,
  updateRegistrationSuccessAction,
  updateNewRegistrationRequestAction,
  updateNewRegistrationSuccessAction,
  updateNewRegistrationErrorAction,
} from "./Reducer";
import {
  createOldStudentRegistrationService,
  createNewStudentRegistrationService,
  deleteRegistrationService,
  getRegistrationService,
  updateRegistrationService,
  updateNewRegistrationService,
} from "../../service/Registration";
import { RegistrationActionTypes } from "../../../domain/Registrations";

type ErrorMessage = {
  error: string;
};

function* getRegistration({
  payload,
}: ReturnType<typeof getRegistrationsRequestAction>): Generator {
  try {
    const res = (yield call(getRegistrationService, payload)) as AxiosResponse;
    yield put(getRegistrationsSuccessAction({ registrations: res.data }));
  } catch (err) {
    yield put(getRegistrationsErrorAction({ message: "404" }));
  }
}

function* createOldStudentRegistration({
  payload,
}: ReturnType<typeof createOldStudentRegistrationRequestAction>): Generator {
  try {
    const res = (yield call(createOldStudentRegistrationService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createOldStudentRegistrationSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createOldStudentRegistrationErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* createNewStudentRegistration({
  payload,
}: ReturnType<typeof createNewStudentRegistrationRequestAction>): Generator {
  try {
    const res = (yield call(createNewStudentRegistrationService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(createNewStudentRegistrationSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createNewStudentRegistrationErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* updateRegistration({
  payload,
}: ReturnType<typeof updateRegistrationRequestAction>): Generator {
  try {
    const res = (yield call(updateRegistrationService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateRegistrationSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      console.log(error.response?.data.error);
      yield put(
        updateRegistrationErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* updateNewRegistration({
  payload,
}: ReturnType<typeof updateNewRegistrationRequestAction>): Generator {
  try {
    const res = (yield call(updateNewRegistrationService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateNewRegistrationSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      console.log(error.response?.data.error);
      yield put(
        updateNewRegistrationErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* deleteRegistration({
  payload,
}: ReturnType<typeof deleteRegistrationRequestAction>): Generator {
  try {
    const res = (yield call(deleteRegistrationService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteRegistrationSuccessAction());
    }
  } catch (err) {
    yield put(deleteRegistrationErrorAction({ message: "404" }));
  }
}

function* watchGetRegistrations(): Generator {
  yield takeLatest(RegistrationActionTypes.GET_REGISTRATIONS_REQUEST, getRegistration);
}

function* watchCreateOldStudentRegistration(): Generator {
  yield takeLatest(
    RegistrationActionTypes.CREATE_OLD_STUDENT_REGISTRATION_REQUEST,
    createOldStudentRegistration
  );
}

function* watchCreateNewStudentRegistration(): Generator {
  yield takeLatest(
    RegistrationActionTypes.CREATE_NEW_STUDENT_REGISTRATION_REQUEST,
    createNewStudentRegistration
  );
}

function* watchUpdateRegistration(): Generator {
  yield takeLatest(
    RegistrationActionTypes.UPDATE_REGISTRATION_REQUEST,
    updateRegistration
  );
}

function* watchUpdateNewRegistration(): Generator {
  yield takeLatest(
    RegistrationActionTypes.UPDATE_NEW_REGISTRATION_REQUEST,
    updateNewRegistration
  );
}

function* watchDeleteRegistration(): Generator {
  yield takeLatest(
    RegistrationActionTypes.DELETE_REGISTRATION_REQUEST,
    deleteRegistration
  );
}

export function* registrationSaga(): Generator {
  yield all([fork(watchGetRegistrations)]);
  yield all([fork(watchCreateOldStudentRegistration)]);
  yield all([fork(watchCreateNewStudentRegistration)]);
  yield all([fork(watchUpdateRegistration)]);
  yield all([fork(watchUpdateNewRegistration)]);
  yield all([fork(watchDeleteRegistration)]);
}

import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  CreateOldStudentRegistration,
  CreateNewStudentRegistration,
  RegistrationActionTypes,
  Registrations,
  UpdateRegistration,
  initialState,
  UpdateNewRegistration,
} from "../../../domain/Registrations";

// Get
const getRegistrationsRequestAction = createAction<
  {
    limit: number;
    offset: number;
    new_student: boolean;
    student_id?: string;
    academic_year_no?: string;
    room_id?: string;
    isPaid?: string;
  },
  RegistrationActionTypes.GET_REGISTRATIONS_REQUEST
>(RegistrationActionTypes.GET_REGISTRATIONS_REQUEST);
const getRegistrationsSuccessAction = createAction<
  { registrations: Registrations },
  RegistrationActionTypes.GET_REGISTRATIONS_SUCCESS
>(RegistrationActionTypes.GET_REGISTRATIONS_SUCCESS);
const getRegistrationsErrorAction = createAction<
  { message: string },
  RegistrationActionTypes.GET_REGISTRATIONS_ERROR
>(RegistrationActionTypes.GET_REGISTRATIONS_ERROR);

// Create Old Student Registration
const createOldStudentRegistrationRequestAction = createAction<
  { registration: CreateOldStudentRegistration },
  RegistrationActionTypes.CREATE_OLD_STUDENT_REGISTRATION_REQUEST
>(RegistrationActionTypes.CREATE_OLD_STUDENT_REGISTRATION_REQUEST);
const createOldStudentRegistrationSuccessAction = createAction(
  RegistrationActionTypes.CREATE_OLD_STUDENT_REGISTRATION_SUCCESS
);
const createOldStudentRegistrationErrorAction = createAction<
  { message: string },
  RegistrationActionTypes.CREATE_OLD_STUDENT_REGISTRATION_ERROR
>(RegistrationActionTypes.CREATE_OLD_STUDENT_REGISTRATION_ERROR);
const createOldStudentRegistrationResetAction = createAction(
  RegistrationActionTypes.CREATE_OLD_STUDENT_REGISTRATION_RESET
);

// Create New Student Registration
const createNewStudentRegistrationRequestAction = createAction<
  { registration: CreateNewStudentRegistration },
  RegistrationActionTypes.CREATE_NEW_STUDENT_REGISTRATION_REQUEST
>(RegistrationActionTypes.CREATE_NEW_STUDENT_REGISTRATION_REQUEST);
const createNewStudentRegistrationSuccessAction = createAction(
  RegistrationActionTypes.CREATE_NEW_STUDENT_REGISTRATION_SUCCESS
);
const createNewStudentRegistrationErrorAction = createAction<
  { message: string },
  RegistrationActionTypes.CREATE_NEW_STUDENT_REGISTRATION_ERROR
>(RegistrationActionTypes.CREATE_NEW_STUDENT_REGISTRATION_ERROR);
const createNewStudentRegistrationResetAction = createAction(
  RegistrationActionTypes.CREATE_NEW_STUDENT_REGISTRATION_RESET
);

// Update Registration
const updateRegistrationRequestAction = createAction<
  { _id: string; registration: UpdateRegistration },
  RegistrationActionTypes.UPDATE_REGISTRATION_REQUEST
>(RegistrationActionTypes.UPDATE_REGISTRATION_REQUEST);
const updateRegistrationSuccessAction = createAction(
  RegistrationActionTypes.UPDATE_REGISTRATION_SUCCESS
);
const updateRegistrationErrorAction = createAction<
  { message: string },
  RegistrationActionTypes.UPDATE_REGISTRATION_ERROR
>(RegistrationActionTypes.UPDATE_REGISTRATION_ERROR);
const updateRegistrationResetAction = createAction(
  RegistrationActionTypes.UPDATE_REGISTRATION_RESET
);

// Update New Registration
const updateNewRegistrationRequestAction = createAction<
  { _id: string; registration: UpdateNewRegistration },
  RegistrationActionTypes.UPDATE_NEW_REGISTRATION_REQUEST
>(RegistrationActionTypes.UPDATE_NEW_REGISTRATION_REQUEST);
const updateNewRegistrationSuccessAction = createAction(
  RegistrationActionTypes.UPDATE_NEW_REGISTRATION_SUCCESS
);
const updateNewRegistrationErrorAction = createAction<
  { message: string },
  RegistrationActionTypes.UPDATE_NEW_REGISTRATION_ERROR
>(RegistrationActionTypes.UPDATE_NEW_REGISTRATION_ERROR);
const updateNewRegistrationResetAction = createAction(
  RegistrationActionTypes.UPDATE_NEW_REGISTRATION_RESET
);

// Delete Registration
const deleteRegistrationRequestAction = createAction<
  { _id: string },
  RegistrationActionTypes.DELETE_REGISTRATION_REQUEST
>(RegistrationActionTypes.DELETE_REGISTRATION_REQUEST);
const deleteRegistrationSuccessAction = createAction(
  RegistrationActionTypes.DELETE_REGISTRATION_SUCCESS
);
const deleteRegistrationErrorAction = createAction<
  { message: string },
  RegistrationActionTypes.DELETE_REGISTRATION_ERROR
>(RegistrationActionTypes.DELETE_REGISTRATION_ERROR);
const deleteRegistrationResetAction = createAction(
  RegistrationActionTypes.DELETE_REGISTRATION_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Registration
    .addCase(getRegistrationsRequestAction, (state) => {
      (state.registrations = {
        registrations: {
          registrations: [],
          totalCount: 0,
        },
      }),
        (state.isGetRegistrationsLoading = true);
      state.isGetRegistrationsSuccess = false;
      state.getRegistrationsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getRegistrationsSuccessAction, (state, action) => {
      state.registrations = action.payload.registrations;
      state.isGetRegistrationsLoading = false;
      state.isGetRegistrationsSuccess = true;
      state.getRegistrationsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getRegistrationsErrorAction, (state, action) => {
      (state.registrations = {
        registrations: {
          registrations: [],
          totalCount: 0,
        },
      }),
        (state.isGetRegistrationsLoading = false);
      state.isGetRegistrationsSuccess = false;
      state.getRegistrationsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create Old Student Registration
    .addCase(createOldStudentRegistrationRequestAction, (state) => {
      state.isCreateOldStudentRegistrationLoading = true;
      state.isCreateOldStudentRegistrationSuccess = false;
      state.createOldStudentRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createOldStudentRegistrationSuccessAction, (state) => {
      state.isCreateOldStudentRegistrationLoading = false;
      state.isCreateOldStudentRegistrationSuccess = true;
      state.createOldStudentRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createOldStudentRegistrationErrorAction, (state, action) => {
      state.isCreateOldStudentRegistrationLoading = false;
      state.isCreateOldStudentRegistrationSuccess = false;
      state.createOldStudentRegistrationError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createOldStudentRegistrationResetAction, (state) => {
      state.isCreateOldStudentRegistrationLoading = false;
      state.isCreateOldStudentRegistrationSuccess = false;
      state.createOldStudentRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    // Create New Student Registration
    .addCase(createNewStudentRegistrationRequestAction, (state) => {
      state.isCreateNewStudentRegistrationLoading = true;
      state.isCreateNewStudentRegistrationSuccess = false;
      state.createNewStudentRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createNewStudentRegistrationSuccessAction, (state) => {
      state.isCreateNewStudentRegistrationLoading = false;
      state.isCreateNewStudentRegistrationSuccess = true;
      state.createNewStudentRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createNewStudentRegistrationErrorAction, (state, action) => {
      state.isCreateNewStudentRegistrationLoading = false;
      state.isCreateNewStudentRegistrationSuccess = false;
      state.createNewStudentRegistrationError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createNewStudentRegistrationResetAction, (state) => {
      state.isCreateNewStudentRegistrationLoading = false;
      state.isCreateNewStudentRegistrationSuccess = false;
      state.createNewStudentRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    // Update Registration
    .addCase(updateRegistrationRequestAction, (state) => {
      state.isUpdateRegistrationLoading = true;
      state.isUpdateRegistrationSuccess = false;
      state.updateRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateRegistrationSuccessAction, (state) => {
      state.isUpdateRegistrationLoading = false;
      state.isUpdateRegistrationSuccess = true;
      state.updateRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateRegistrationErrorAction, (state, action) => {
      state.isUpdateRegistrationLoading = false;
      state.isUpdateRegistrationSuccess = false;
      state.updateRegistrationError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateRegistrationResetAction, (state) => {
      state.isUpdateRegistrationLoading = false;
      state.isUpdateRegistrationSuccess = false;
      state.updateRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    // Update New Registration
    .addCase(updateNewRegistrationRequestAction, (state) => {
      state.isUpdateNewRegistrationLoading = true;
      state.isUpdateNewRegistrationSuccess = false;
      state.updateNewRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateNewRegistrationSuccessAction, (state) => {
      state.isUpdateNewRegistrationLoading = false;
      state.isUpdateNewRegistrationSuccess = true;
      state.updateNewRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateNewRegistrationErrorAction, (state, action) => {
      state.isUpdateNewRegistrationLoading = false;
      state.isUpdateNewRegistrationSuccess = false;
      state.updateNewRegistrationError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateNewRegistrationResetAction, (state) => {
      state.isUpdateNewRegistrationLoading = false;
      state.isUpdateNewRegistrationSuccess = false;
      state.updateNewRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete Registration
    .addCase(deleteRegistrationRequestAction, (state) => {
      state.isDeleteRegistrationLoading = true;
      state.isDeleteRegistrationSuccess = false;
      state.deleteRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteRegistrationSuccessAction, (state) => {
      state.isDeleteRegistrationLoading = false;
      state.isDeleteRegistrationSuccess = true;
      state.deleteRegistrationError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteRegistrationErrorAction, (state, action) => {
      state.isDeleteRegistrationLoading = false;
      state.isDeleteRegistrationSuccess = false;
      state.deleteRegistrationError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteRegistrationResetAction, (state) => {
      state.isDeleteRegistrationLoading = false;
      state.isDeleteRegistrationSuccess = false;
      state.deleteRegistrationError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getRegistrationsRequestAction,
  getRegistrationsSuccessAction,
  getRegistrationsErrorAction,
  createOldStudentRegistrationRequestAction,
  createOldStudentRegistrationSuccessAction,
  createOldStudentRegistrationErrorAction,
  createOldStudentRegistrationResetAction,
  createNewStudentRegistrationRequestAction,
  createNewStudentRegistrationSuccessAction,
  createNewStudentRegistrationErrorAction,
  createNewStudentRegistrationResetAction,
  updateRegistrationRequestAction,
  updateRegistrationSuccessAction,
  updateRegistrationErrorAction,
  updateRegistrationResetAction,
  updateNewRegistrationRequestAction,
  updateNewRegistrationSuccessAction,
  updateNewRegistrationErrorAction,
  updateNewRegistrationResetAction,
  deleteRegistrationRequestAction,
  deleteRegistrationSuccessAction,
  deleteRegistrationErrorAction,
  deleteRegistrationResetAction,
};

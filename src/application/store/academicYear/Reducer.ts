import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  AcademicYearActionTypes,
  AcademicYears,
  CreateAcademicYear,
  UpdateAcademicYear,
  initialState,
} from "../../../domain/AcademicYears";

// Get
const getAcademicYearsRequestAction = createAction<
  {
    limit: number;
    offset: number;
  },
  AcademicYearActionTypes.GET_ACADEMIC_YEARS_REQUEST
>(AcademicYearActionTypes.GET_ACADEMIC_YEARS_REQUEST);
const getAcademicYearsSuccessAction = createAction<
  { academic_years: AcademicYears },
  AcademicYearActionTypes.GET_ACADEMIC_YEARS_SUCCESS
>(AcademicYearActionTypes.GET_ACADEMIC_YEARS_SUCCESS);
const getAcademicYearsErrorAction = createAction<
  { message: string },
  AcademicYearActionTypes.GET_ACADEMIC_YEARS_ERROR
>(AcademicYearActionTypes.GET_ACADEMIC_YEARS_ERROR);

// Create Academic Year
const createAcademicYearRequestAction = createAction<
  { academicYear: CreateAcademicYear },
  AcademicYearActionTypes.CREATE_ACADEMIC_YEAR_REQUEST
>(AcademicYearActionTypes.CREATE_ACADEMIC_YEAR_REQUEST);
const createAcademicYearSuccessAction = createAction(
  AcademicYearActionTypes.CREATE_ACADEMIC_YEAR_SUCCESS
);
const createAcademicYearErrorAction = createAction<
  { message: string },
  AcademicYearActionTypes.CREATE_ACADEMIC_YEAR_ERROR
>(AcademicYearActionTypes.CREATE_ACADEMIC_YEAR_ERROR);
const createAcademicYearResetAction = createAction(
  AcademicYearActionTypes.CREATE_ACADEMIC_YEAR_RESET
);

// Update Academic Year
const updateAcademicYearRequestAction = createAction<
  { _id: string; academicYear: UpdateAcademicYear },
  AcademicYearActionTypes.UPDATE_ACADEMIC_YEAR_REQUEST
>(AcademicYearActionTypes.UPDATE_ACADEMIC_YEAR_REQUEST);
const updateAcademicYearSuccessAction = createAction(
  AcademicYearActionTypes.UPDATE_ACADEMIC_YEAR_SUCCESS
);
const updateAcademicYearErrorAction = createAction<
  { message: string },
  AcademicYearActionTypes.UPDATE_ACADEMIC_YEAR_ERROR
>(AcademicYearActionTypes.UPDATE_ACADEMIC_YEAR_ERROR);
const updateAcademicYearResetAction = createAction(
  AcademicYearActionTypes.UPDATE_ACADEMIC_YEAR_RESET
);

// Delete Academic Year
const deleteAcademicYearRequestAction = createAction<
  { _id: string },
  AcademicYearActionTypes.DELETE_ACADEMIC_YEAR_REQUEST
>(AcademicYearActionTypes.DELETE_ACADEMIC_YEAR_REQUEST);
const deleteAcademicYearSuccessAction = createAction(
  AcademicYearActionTypes.DELETE_ACADEMIC_YEAR_SUCCESS
);
const deleteAcademicYearErrorAction = createAction<
  { message: string },
  AcademicYearActionTypes.DELETE_ACADEMIC_YEAR_ERROR
>(AcademicYearActionTypes.DELETE_ACADEMIC_YEAR_ERROR);
const deleteAcademicYearResetAction = createAction(
  AcademicYearActionTypes.DELETE_ACADEMIC_YEAR_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Academic Year
    .addCase(getAcademicYearsRequestAction, (state) => {
      (state.academic_years = {
        academicYears: {
          academicYear: [],
          totalAcademicYears: 0,
        },
      }),
        (state.isGetAcademicYearsLoading = true);
      state.isGetAcademicYearsSuccess = false;
      state.getAcademicYearsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getAcademicYearsSuccessAction, (state, action) => {
      state.academic_years = action.payload.academic_years;
      state.isGetAcademicYearsLoading = false;
      state.isGetAcademicYearsSuccess = true;
      state.getAcademicYearsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getAcademicYearsErrorAction, (state, action) => {
      (state.academic_years = {
        academicYears: {
          academicYear: [],
          totalAcademicYears: 0,
        },
      }),
        (state.isGetAcademicYearsLoading = false);
      state.isGetAcademicYearsSuccess = false;
      state.getAcademicYearsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Create Academic Year
    .addCase(createAcademicYearRequestAction, (state) => {
      state.isCreateAcademicYearLoading = true;
      state.isCreateAcademicYearSuccess = false;
      state.createAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createAcademicYearSuccessAction, (state) => {
      state.isCreateAcademicYearLoading = false;
      state.isCreateAcademicYearSuccess = true;
      state.createAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(createAcademicYearErrorAction, (state, action) => {
      state.isCreateAcademicYearLoading = false;
      state.isCreateAcademicYearSuccess = false;
      state.createAcademicYearError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(createAcademicYearResetAction, (state) => {
      state.isCreateAcademicYearLoading = false;
      state.isCreateAcademicYearSuccess = false;
      state.createAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    // Update Academic Year
    .addCase(updateAcademicYearRequestAction, (state) => {
      state.isUpdateAcademicYearLoading = true;
      state.isUpdateAcademicYearSuccess = false;
      state.updateAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateAcademicYearSuccessAction, (state) => {
      state.isUpdateAcademicYearLoading = false;
      state.isUpdateAcademicYearSuccess = true;
      state.updateAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateAcademicYearErrorAction, (state, action) => {
      state.isUpdateAcademicYearLoading = false;
      state.isUpdateAcademicYearSuccess = false;
      state.updateAcademicYearError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateAcademicYearResetAction, (state) => {
      state.isUpdateAcademicYearLoading = false;
      state.isUpdateAcademicYearSuccess = false;
      state.updateAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete AcademicYear
    .addCase(deleteAcademicYearRequestAction, (state) => {
      state.isDeleteAcademicYearLoading = true;
      state.isDeleteAcademicYearSuccess = false;
      state.deleteAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteAcademicYearSuccessAction, (state) => {
      state.isDeleteAcademicYearLoading = false;
      state.isDeleteAcademicYearSuccess = true;
      state.deleteAcademicYearError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteAcademicYearErrorAction, (state, action) => {
      state.isDeleteAcademicYearLoading = false;
      state.isDeleteAcademicYearSuccess = false;
      state.deleteAcademicYearError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteAcademicYearResetAction, (state) => {
      state.isDeleteAcademicYearLoading = false;
      state.isDeleteAcademicYearSuccess = false;
      state.deleteAcademicYearError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getAcademicYearsRequestAction,
  getAcademicYearsSuccessAction,
  getAcademicYearsErrorAction,
  createAcademicYearRequestAction,
  createAcademicYearSuccessAction,
  createAcademicYearErrorAction,
  createAcademicYearResetAction,
  updateAcademicYearRequestAction,
  updateAcademicYearSuccessAction,
  updateAcademicYearErrorAction,
  updateAcademicYearResetAction,
  deleteAcademicYearRequestAction,
  deleteAcademicYearSuccessAction,
  deleteAcademicYearErrorAction,
  deleteAcademicYearResetAction,
};

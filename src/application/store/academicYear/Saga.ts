import { AxiosResponse, AxiosError } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  createAcademicYearErrorAction,
  createAcademicYearRequestAction,
  createAcademicYearSuccessAction,
  deleteAcademicYearErrorAction,
  deleteAcademicYearRequestAction,
  deleteAcademicYearSuccessAction,
  getAcademicYearsErrorAction,
  getAcademicYearsRequestAction,
  getAcademicYearsSuccessAction,
  updateAcademicYearErrorAction,
  updateAcademicYearRequestAction,
  updateAcademicYearSuccessAction,
} from "./Reducer";
import {
  createAcademicYearService,
  deleteAcademicYearService,
  getAcademicYearService,
  updateAcademicYearService,
} from "../../service/AcademicYear";
import { AcademicYearActionTypes } from "../../../domain/AcademicYears";

type ErrorMessage = {
  error: string;
};

function* getAcademicYear({
  payload,
}: ReturnType<typeof getAcademicYearsRequestAction>): Generator {
  try {
    const res = (yield call(getAcademicYearService, payload)) as AxiosResponse;
    yield put(getAcademicYearsSuccessAction({ academic_years: res.data }));
  } catch (err) {
    yield put(getAcademicYearsErrorAction({ message: "404" }));
  }
}

function* createAcademicYear({
  payload,
}: ReturnType<typeof createAcademicYearRequestAction>): Generator {
  try {
    const res = (yield call(
      createAcademicYearService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(createAcademicYearSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      yield put(
        createAcademicYearErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* updateAcademicYear({
  payload,
}: ReturnType<typeof updateAcademicYearRequestAction>): Generator {
  try {
    const res = (yield call(
      updateAcademicYearService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateAcademicYearSuccessAction());
    }
  } catch (err) {
    const error = err as AxiosError<ErrorMessage>;
    if (error.response?.data.error) {
      console.log(error.response?.data.error);
      yield put(
        updateAcademicYearErrorAction({ message: error.response?.data.error })
      );
    }
  }
}

function* deleteAcademicYear({
  payload,
}: ReturnType<typeof deleteAcademicYearRequestAction>): Generator {
  try {
    const res = (yield call(
      deleteAcademicYearService,
      payload
    )) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteAcademicYearSuccessAction());
    }
  } catch (err) {
    yield put(deleteAcademicYearErrorAction({ message: "404" }));
  }
}

function* watchGetAcademicYears(): Generator {
  yield takeLatest(
    AcademicYearActionTypes.GET_ACADEMIC_YEARS_REQUEST,
    getAcademicYear
  );
}

function* watchCreateAcademicYear(): Generator {
  yield takeLatest(
    AcademicYearActionTypes.CREATE_ACADEMIC_YEAR_REQUEST,
    createAcademicYear
  );
}

function* watchUpdateAcademicYear(): Generator {
  yield takeLatest(
    AcademicYearActionTypes.UPDATE_ACADEMIC_YEAR_REQUEST,
    updateAcademicYear
  );
}

function* watchDeleteAcademicYear(): Generator {
  yield takeLatest(
    AcademicYearActionTypes.DELETE_ACADEMIC_YEAR_REQUEST,
    deleteAcademicYear
  );
}

export function* academicYearSaga(): Generator {
  yield all([fork(watchGetAcademicYears)]);
  yield all([fork(watchCreateAcademicYear)]);
  yield all([fork(watchUpdateAcademicYear)]);
  yield all([fork(watchDeleteAcademicYear)]);
}

import user from "./user/reducer";
import profile from "./profile/reducer";
import student from "./student/Reducer";
import teacher from "./teacher/Reducer";
import subject from "./subject/Reducer";
import studentAbsence from "./studentAbsence/Reducer";
import teacherAbsence from "./teacherAbsence/Reducer";
import studentAssessment from "./studentAssessment/Reducer";
import score from "./score/Reducer";
import classLevel from "./classLevel/Reducer";
import classRoom from "./classRoom/Reducer";
import academicYear from "./academicYear/Reducer";
import schoolFee from "./schoolFee/Reducer";
import subjectV2 from "./subjectV2/Reducer";
import registration from "./registration/Reducer";
import teacherAssessment from "./teacherAssessment/Reducer";

const reducers = {
  user,
  profile,
  student,
  teacher,
  subject,
  studentAbsence,
  teacherAbsence,
  studentAssessment,
  score,
  classLevel,
  classRoom,
  academicYear,
  schoolFee,
  subjectV2,
  registration,
  teacherAssessment
};

export default reducers;

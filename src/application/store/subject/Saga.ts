import { AxiosResponse } from "axios";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  deleteSubjectErrorAction,
  deleteSubjectRequestAction,
  deleteSubjectSuccessAction,
  getSubjectsErrorAction,
  getSubjectsRequestAction,
  getSubjectsSuccessAction,
  updateSubjectErrorAction,
  updateSubjectRequestAction,
  updateSubjectSuccessAction,
} from "./Reducer";
import {
  deleteSubjectService,
  getSubjectService,
  updateSubjectService,
} from "../../service/Subject";
import { SubjectActionTypes } from "../../../domain/Subject";

function* getSubject({
  payload,
}: ReturnType<typeof getSubjectsRequestAction>): Generator {
  try {
    const res = (yield call(getSubjectService, payload)) as AxiosResponse;
    yield put(getSubjectsSuccessAction({ subjects: res.data }));
  } catch (err) {
    yield put(getSubjectsErrorAction({ message: "404" }));
  }
}

function* updateSubject({
  payload,
}: ReturnType<typeof updateSubjectRequestAction>): Generator {
  try {
    const res = (yield call(updateSubjectService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(updateSubjectSuccessAction());
    }
  } catch (err) {
    yield put(updateSubjectErrorAction({ message: "404" }));
  }
}

function* deleteSubject({
  payload,
}: ReturnType<typeof deleteSubjectRequestAction>): Generator {
  try {
    const res = (yield call(deleteSubjectService, payload)) as AxiosResponse;
    if (res.status === 201) {
      yield put(deleteSubjectSuccessAction());
    }
  } catch (err) {
    yield put(deleteSubjectErrorAction({ message: "404" }));
  }
}

function* watchGetSubjects(): Generator {
  yield takeLatest(SubjectActionTypes.GET_Subjects_REQUEST, getSubject);
}

function* watchUpdateSubject(): Generator {
  yield takeLatest(SubjectActionTypes.UPDATE_Subjects_REQUEST, updateSubject);
}

function* watchDeleteSubject(): Generator {
  yield takeLatest(SubjectActionTypes.DELETE_Subjects_REQUEST, deleteSubject);
}

export function* subjectSaga(): Generator {
  yield all([fork(watchGetSubjects)]);
  yield all([fork(watchUpdateSubject)]);
  yield all([fork(watchDeleteSubject)]);
}

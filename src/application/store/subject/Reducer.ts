import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  Subject,
  SubjectActionTypes,
  UpdateSubjectForm,
  initialState,
} from "../../../domain/Subject";

// Get
const getSubjectsRequestAction = createAction<{
  limit: number;
  offset: number;
  class_level: string;
  class_room: string;
  term: string;
  generation: string;
},SubjectActionTypes.GET_Subjects_REQUEST>(SubjectActionTypes.GET_Subjects_REQUEST);
const getSubjectsSuccessAction = createAction<
  { subjects: Subject },
  SubjectActionTypes.GET_Subjects_SUCCESS
>(SubjectActionTypes.GET_Subjects_SUCCESS);
const getSubjectsErrorAction = createAction<
  { message: string },
  SubjectActionTypes.GET_Subjects_ERROR
>(SubjectActionTypes.GET_Subjects_ERROR);

// Update Subject
const updateSubjectRequestAction = createAction<
  { _id: string; subject: UpdateSubjectForm },
  SubjectActionTypes.UPDATE_Subjects_REQUEST
>(SubjectActionTypes.UPDATE_Subjects_REQUEST);
const updateSubjectSuccessAction = createAction(
  SubjectActionTypes.UPDATE_Subjects_SUCCESS
);
const updateSubjectErrorAction = createAction<
  { message: string },
  SubjectActionTypes.UPDATE_Subjects_ERROR
>(SubjectActionTypes.UPDATE_Subjects_ERROR);
const updateSubjectResetAction = createAction(
  SubjectActionTypes.UPDATE_Subjects_RESET
);

// Delete Subject
const deleteSubjectRequestAction = createAction<
  { _id: string },
  SubjectActionTypes.DELETE_Subjects_REQUEST
>(SubjectActionTypes.DELETE_Subjects_REQUEST);
const deleteSubjectSuccessAction = createAction(
  SubjectActionTypes.DELETE_Subjects_SUCCESS
);
const deleteSubjectErrorAction = createAction<
  { message: string },
  SubjectActionTypes.DELETE_Subjects_ERROR
>(SubjectActionTypes.DELETE_Subjects_ERROR);
const deleteSubjectResetAction = createAction(
  SubjectActionTypes.DELETE_Subjects_RESET
);

const reducer = createReducer(initialState, (builder) => {
  builder
    // Get Subject
    .addCase(getSubjectsRequestAction, (state) => {
      (state.subjects = {
        subjects: { subjects: [], totalStudents: 0 },
      }),
        (state.isGetSubjectsLoading = true);
      state.isGetSubjectsSuccess = false;
      state.getSubjectsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getSubjectsSuccessAction, (state, action) => {
      state.subjects = action.payload.subjects;
      state.isGetSubjectsLoading = false;
      state.isGetSubjectsSuccess = true;
      state.getSubjectsError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(getSubjectsErrorAction, (state, action) => {
      (state.subjects = {
        subjects: { subjects: [], totalStudents: 0 },
      }),
        (state.isGetSubjectsLoading = false);
      state.isGetSubjectsSuccess = false;
      state.getSubjectsError = {
        isError: true,
        message: action.payload.message,
      };
    })
    // Update Subject
    .addCase(updateSubjectRequestAction, (state) => {
      state.isUpdateSubjectLoading = true;
      state.isUpdateSubjectSuccess = false;
      state.updateSubjectError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateSubjectSuccessAction, (state) => {
      state.isUpdateSubjectLoading = false;
      state.isUpdateSubjectSuccess = true;
      state.updateSubjectError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(updateSubjectErrorAction, (state, action) => {
      state.isUpdateSubjectLoading = false;
      state.isUpdateSubjectSuccess = false;
      state.updateSubjectError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(updateSubjectResetAction, (state) => {
      state.isUpdateSubjectLoading = false;
      state.isUpdateSubjectSuccess = false;
      state.updateSubjectError = {
        isError: false,
        message: undefined,
      };
    })
    // Delete Subject
    .addCase(deleteSubjectRequestAction, (state) => {
      state.isDeleteSubjectLoading = true;
      state.isDeleteSubjectSuccess = false;
      state.deleteSubjectError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteSubjectSuccessAction, (state) => {
      state.isDeleteSubjectLoading = false;
      state.isDeleteSubjectSuccess = true;
      state.deleteSubjectError = {
        isError: false,
        message: undefined,
      };
    })
    .addCase(deleteSubjectErrorAction, (state, action) => {
      state.isDeleteSubjectLoading = false;
      state.isDeleteSubjectSuccess = false;
      state.deleteSubjectError = {
        isError: true,
        message: action.payload.message,
      };
    })
    .addCase(deleteSubjectResetAction, (state) => {
      state.isDeleteSubjectLoading = false;
      state.isDeleteSubjectSuccess = false;
      state.deleteSubjectError = {
        isError: false,
        message: undefined,
      };
    });
});

export default reducer;
export {
  getSubjectsRequestAction,
  getSubjectsSuccessAction,
  getSubjectsErrorAction,
  updateSubjectRequestAction,
  updateSubjectSuccessAction,
  updateSubjectErrorAction,
  updateSubjectResetAction,
  deleteSubjectRequestAction,
  deleteSubjectSuccessAction,
  deleteSubjectErrorAction,
  deleteSubjectResetAction,
};

import { UpdateSubjectForm } from "../../domain/Subject";
import { axiosInstant } from "./defaultInstance";

export const getSubjectService = (payload: {
  limit: number;
  offset: number;
  class_level: string;
  class_room: string;
  term: string;
  generation: string;
}) => {
  return axiosInstant
    .get(
      `/api/v1/subject/getSubject?class_level=${payload.class_level}&class_room=${payload.class_room}&term=${payload.term}&generation=${payload.generation}&limit=${payload.limit}&offset=${payload.offset}`
    )
    .then((res) => res);
};

export const updateSubjectService = (payload: {
  _id: string;
  subject: UpdateSubjectForm;
}) =>
  axiosInstant
    .put(`/api/v1/subject/updateSubject/${payload._id}`, payload.subject)
    .then((res) => res);

export const deleteSubjectService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`api/v1/subject/deleteSubject/${payload._id}`)
    .then((res) => res);

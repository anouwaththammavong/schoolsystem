import axios, { AxiosRequestConfig } from 'axios';
import { history } from '../store';
import LocalStorageService from './LocalStorageService';
export const axiosInstant = axios;
let isRefreshing = false;
const endpoint = import.meta.env.VITE_API_ENDPOINT || '';
let failedQueue: any = [];
const processQueue = (error: Error, token = null) => {
  failedQueue.forEach((prom: any) => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });
  failedQueue = [];
};
axiosInstant.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    const token = LocalStorageService.getAccessToken();
    if (!isRefreshing) {
      config.url = `${endpoint}${config.url}`;
    }
    if (config.headers) {
      config.headers['cache-control'] = 'no-cache';
      // config.headers['Pragma'] = 'no-cache';

      config.headers['Content-Type'] = 'application/json';
      config.headers['Accept'] = 'application/json';

      const originalRequest = config;
      if (token && originalRequest.url !== `${endpoint}/api/auth/login`) {
        // config.headers['x-access-token'] = `${token}`;
        config.headers['Authorization'] = `Bearer ${token}`;
      }
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);
axiosInstant.interceptors.response.use(
  (response) => {
    return response;
  },
  function (error) {
    const originalRequest = error.config;
    if (error.response !== undefined) {
      switch (error.response.status) {
        case 401:
          if (originalRequest.url === `${endpoint}/api/auth/refresh-token`) {
            history.push('/login');
            return Promise.reject(error);
          }
          if (!originalRequest._retry) {
            if (isRefreshing) {
              return new Promise(function (resolve, reject) {
                failedQueue.push({ resolve, reject });
              })
                .then((token) => {
                  originalRequest.headers['x-access-token'] = token;
                  return axios(originalRequest);
                })
                .catch((err) => {
                  return Promise.reject(err);
                });
            }
            originalRequest._retry = true;
            isRefreshing = true;
            return new Promise(function (resolve, reject) {
              console.log(LocalStorageService.getRefreshToken())
              return axios
                .post(`${endpoint}/api/auth/refresh-token`, {
                  refreshToken: LocalStorageService.getRefreshToken(),
                })
                .then(({ data }) => {
                  window.localStorage.setItem(
                    'accessToken',
                    data.accessToken
                  );
                  window.localStorage.setItem(
                    'refreshToken',
                    data.refreshToken
                  );
                  axios.defaults.headers.common['x-access-token'] =
                    data.accessToken;
                  originalRequest.headers['x-access-token'] =
                    data.accessToken;
                  processQueue(error, data.token);
                  resolve(axios(originalRequest));
                })
                .catch((err) => {
                  processQueue(err, null);
                  reject(err);
                  history.push('/login');
                })
                .then(() => {
                  isRefreshing = false;
                });
            });
          }
          break;
        case 403:
          // history.push('/login');
        default:
          break;
      }
    } else {
      history.push('/login');
    }
    return Promise.reject(error);
  }
);

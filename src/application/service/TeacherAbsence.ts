import {
  CreateTeacherAbsenceForm,
  UpdateTeacherAbsenceForm,
} from "../../domain/TeacherAbsence";
import { axiosInstant } from "./defaultInstance";

export const getTeacherAbsencesService = (payload: {
  limit: number;
  offset: number;
  teacher_id: string;
  absence_date?: Date;
  be_reasonable?: string;
}) => {
  if (payload.absence_date && payload.be_reasonable) {
    return axiosInstant
      .get(
        `/api/v1/teacherAbsence/getTeacherAbsence?absence_date=${payload.absence_date}&teacher_id=${payload.teacher_id}&be_reasonable=${payload.be_reasonable}&limit=${payload.limit}&offset=${payload.offset}`
      )
      .then((res) => res);
  } else if (payload.absence_date || payload.be_reasonable) {
    if (payload.absence_date) {
      return axiosInstant
        .get(
          `/api/v1/teacherAbsence/getTeacherAbsence?absence_date=${payload.absence_date}&teacher_id=${payload.teacher_id}&limit=${payload.limit}&offset=${payload.offset}`
        )
        .then((res) => res);
    } else {
      return axiosInstant
        .get(
          `/api/v1/teacherAbsence/getTeacherAbsence?be_reasonable=${payload.be_reasonable}&teacher_id=${payload.teacher_id}&limit=${payload.limit}&offset=${payload.offset}`
        )
        .then((res) => res);
    }
  } else {
    return axiosInstant
      .get(
        `/api/v1/teacherAbsence/getTeacherAbsence?teacher_id=${payload.teacher_id}&limit=${payload.limit}&offset=${payload.offset}`
      )
      .then((res) => res);
  }
};

export const createTeacherAbsenceService = (payload: {
  teacherAbsence: CreateTeacherAbsenceForm;
}) =>
  axiosInstant
    .post("/api/v1/teacherAbsence/createTeacherAbsence", payload.teacherAbsence)
    .then((res) => res);

export const updateTeacherAbsenceService = (payload: {
  _id: string;
  teacherAbsence: UpdateTeacherAbsenceForm;
}) =>
  axiosInstant
    .put(
      `/api/v1/teacherAbsence/updateTeacherAbsence/${payload._id}`,
      payload.teacherAbsence
    )
    .then((res) => res);

export const deleteTeacherAbsenceService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/teacherAbsence/deleteTeacherAbsence/${payload._id}`)
    .then((res) => res);

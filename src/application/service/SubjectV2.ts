import {
    CreateSubjectV2Form,
    UpdateSubjectV2Form,
  } from "../../domain/SubjectV2";
  import { axiosInstant } from "./defaultInstance";
  
  export const getSubjectV2Service = (payload: {
    limit: number;
    offset: number;
  }) =>
    axiosInstant
      .get(
        `/api/v1/subjectV2/getSubjectV2?limit=${payload.limit}&offset=${payload.offset}`
      )
      .then((res) => res);
  
  export const createSubjectV2Service = (payload: {
    subjectV2: CreateSubjectV2Form;
  }) =>
    axiosInstant
      .post("/api/v1/subjectV2/createSubjectV2", payload.subjectV2)
      .then((res) => res);
  
  export const updateSubjectV2Service = (payload: {
    _id: string;
    subjectV2: UpdateSubjectV2Form;
  }) =>
    axiosInstant
      .put(
        `/api/v1/subjectV2/updateSubjectV2/${payload._id}
      `,
        payload.subjectV2
      )
      .then((res) => res);
  
  export const deleteSubjectV2Service = (payload: { _id: string }) =>
    axiosInstant
      .delete(`/api/v1/subjectV2/deleteSubjectV2/${payload._id}`)
      .then((res) => res);
  
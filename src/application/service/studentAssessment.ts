import {
  CreateStudentAssessmentForm,
  UpdateStudentAssessmentForm,
} from "../../domain/StudentAssessments";
import { axiosInstant } from "./defaultInstance";

export const getStudentAssessmentsService = (payload: {
  limit: number;
  offset: number;
  level_id: string;
  room_id: string;
  generation: string;
}) =>
  axiosInstant
    .get(
      `/api/v1/studentAssessment/getStudentAssessment?level_id=${payload.level_id}&room_id=${payload.room_id}&generation=${payload.generation}&limit=${payload.limit}&offset=${payload.offset}`
    )
    .then((res) => res);

export const createStudentAssessmentService = (payload: {
  studentAssessment: CreateStudentAssessmentForm;
}) =>
  axiosInstant
    .post(
      "/api/v1/studentAssessment/createStudentAssessment",
      payload.studentAssessment
    )
    .then((res) => res);

export const updateStudentAssessmentService = (payload: {
  _id: string;
  studentAssessment: UpdateStudentAssessmentForm;
}) =>
  axiosInstant
    .put(
      `/api/v1/studentAssessment/updateStudentAssessment/${payload._id}`,
      payload.studentAssessment
    )
    .then((res) => res);

export const deleteStudentAssessmentService = (payload: { _id }) =>
  axiosInstant
    .delete(`/api/v1/studentAssessment/deleteStudentAssessment/${payload._id}`)
    .then((res) => res);

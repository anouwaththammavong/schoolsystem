import { CreateStudent } from "../../domain/Student";
import { axiosInstant } from "./defaultInstance";

export const getStudentService = (payload: {
  limit: number;
  offset: number;
  student_id?: string;
  generation?: string;
  level_id?: string;
  new_student?: string;
}) => {
  let url = `/api/v1/student/getStudent?limit=${payload.limit}&offset=${payload.offset}`;
  if (payload.student_id) {
    url += `&student_id=${payload.student_id}`;
  }

  if (payload.generation) {
    url += `&generation=${payload.generation}`;
  }

  if (payload.level_id) {
    url += `&level_id=${payload.level_id}`;
  }

  if (payload.new_student) {
    url += `&new_student=${payload.new_student}`;
  }

  return axiosInstant.get(url).then((res) => res);

  // if (payload.new_student) {
  //   url += `&new_student=${payload.new_student}`
  // }

  // if (payload.student_id && payload.generation) {
  //   return axiosInstant
  //     .get(
  //       `/api/v1/student/getStudent?student_id=${payload.student_id}&generation=${payload.generation}&limit=${payload.limit}&offset=${payload.offset}`
  //     )
  //     .then((res) => res);
  // } else if (payload.student_id || payload.generation) {
  //   if (payload.student_id) {
  //     return axiosInstant
  //       .get(
  //         `/api/v1/student/getStudent?student_id=${payload.student_id}&limit=${payload.limit}&offset=${payload.offset}`
  //       )
  //       .then((res) => res);
  //   } else {
  //     return axiosInstant
  //       .get(
  //         `/api/v1/student/getStudent?generation=${payload.generation}&limit=${payload.limit}&offset=${payload.offset}`
  //       )
  //       .then((res) => res);
  //   }
  // } else {
  //   return axiosInstant
  //     .get(
  //       `/api/v1/student/getStudent?limit=${payload.limit}&offset=${payload.offset}`
  //     )
  //     .then((res) => res);
  // }
};

export const createStudentService = (payload: { student: CreateStudent }) =>
  axiosInstant
    .post("/api/v1/student/createStudent", payload.student)
    .then((res) => res);

export const updateStudentService = (payload: {
  _id: string;
  student: CreateStudent;
}) =>
  axiosInstant
    .put(`/api/v1/student/updateStudent/${payload._id}`, payload.student)
    .then((res) => res);

export const deleteStudentService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/student/deleteStudent/${payload._id}`)
    .then((res) => res);

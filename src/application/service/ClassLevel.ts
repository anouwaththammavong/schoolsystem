import {
  CreateClassLevelForm,
  UpdateClassLevelForm,
} from "../../domain/ClassLevel";
import { axiosInstant } from "./defaultInstance";

export const getClassLevelService = (payload: {
  limit: number;
  offset: number;
}) =>
  axiosInstant
    .get(
      `/api/v1/classlevel/getClassLevel?limit=${payload.limit}&offset=${payload.offset}`
    )
    .then((res) => res);

export const createClassLevelService = (payload: {
  classLevel: CreateClassLevelForm;
}) =>
  axiosInstant
    .post("/api/v1/classLevel/createClassLevel", payload.classLevel)
    .then((res) => res);

export const updateClassLevelService = (payload: {
  _id: string;
  classLevel: UpdateClassLevelForm;
}) =>
  axiosInstant
    .put(
      `/api/v1/classLevel/updateClassLevel/${payload._id}
    `,
      payload.classLevel
    )
    .then((res) => res);

export const deleteClassLevelService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/classLevel/deleteClassLevel/${payload._id}`)
    .then((res) => res);

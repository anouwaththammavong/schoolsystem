import {
  CreateAcademicYear,
  UpdateAcademicYear,
} from "../../domain/AcademicYears";
import { axiosInstant } from "./defaultInstance";

export const getAcademicYearService = (payload: {
  limit: number;
  offset: number;
}) =>
  axiosInstant
    .get(
      `/api/v1/academic/getAcademicYear?limit=${payload.limit}&offset=${payload.offset}`
    )
    .then((res) => res);

export const createAcademicYearService = (payload: {
  academicYear: CreateAcademicYear;
}) =>
  axiosInstant
    .post("/api/v1/academic/createAcademicYear", payload.academicYear)
    .then((res) => res);

export const updateAcademicYearService = (payload: {
  _id: string;
  academicYear: UpdateAcademicYear;
}) =>
  axiosInstant
    .put(
      `/api/v1/academic/updateAcademicYear/${payload._id}
      `,
      payload.academicYear
    )
    .then((res) => res);

export const deleteAcademicYearService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/academic/deleteAcademicYear/${payload._id}`)
    .then((res) => res);

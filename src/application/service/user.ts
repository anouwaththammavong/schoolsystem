import { UpdateUser, ICreateUser } from "../../domain/user";
import { axiosInstant } from "./defaultInstance";

export const getUsersService = (payload: { limit: number; offset: number }) => {
  return axiosInstant
    .get(`/api/auth/getUser?limit=${payload.limit}&offset=${payload.offset}`)
    .then((res) => res);
};

export const createUsersService = (payload: { user: ICreateUser }) =>
  axiosInstant.post("/api/auth/register", payload.user).then((res) => res);

export const updateUserService = (payload: { _id: string; user: UpdateUser }) =>
  axiosInstant
    .put(`/api/auth/updateUser/${payload._id}`, payload.user)
    .then((res) => res);

export const getUserPickUpItemsService = (payload: {
  id: string;
  limit: number;
  offset: number;
}) => {
  return axiosInstant
    .get(
      `/shop/${payload.id}/pickUpItems?limit=${payload.limit}&offset=${payload.offset}`
    )
    .then((res) => res);
};

export const getUserClaimItemsService = (payload: {
  id: string;
  limit: number;
  offset: number;
}) => {
  return axiosInstant
    .get(
      `/shop/${payload.id}/claimItems?limit=${payload.limit}&offset=${payload.offset}`
    )
    .then((res) => res);
};

export const getUserByIdService = (payload: { id: string }) => {
  return axiosInstant.get(`/shop/${payload.id}`).then((res) => res);
};

export const getAllUsersService = () =>
  axiosInstant.get("/allShops").then((res) => res);

export const updateUserStatusService = (payload: { userId: string }) =>
  axiosInstant.put(`/user/${payload.userId}/status`).then((res) => res);

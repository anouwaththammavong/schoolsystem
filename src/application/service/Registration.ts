import {
  CreateOldStudentRegistration,
  CreateNewStudentRegistration,
  UpdateRegistration,
  UpdateNewRegistration,
} from "../../domain/Registrations";
import { axiosInstant } from "./defaultInstance";

export const getRegistrationService = (payload: {
  limit: number;
  offset: number;
  new_student: boolean;
  student_id?: string;
  academic_year_no?: string;
  room_id?: string;
  isPaid?: string;
}) => {
  let url = `/api/v1/registration/getRegistrations?limit=${payload.limit}&offset=${payload.offset}&new_student=${payload.new_student}`;

  if (payload.student_id) {
    url += `&student_id=${payload.student_id}`;
  }

  if (payload.academic_year_no) {
    url += `&academic_year_no=${payload.academic_year_no}`;
  }

  if (payload.room_id) {
    url += `&room_id=${payload.room_id}`;
  }

  if (payload.isPaid) {
    url += `&isPaid=${payload.isPaid}`;
  }

  return axiosInstant.get(url).then((res) => res);
};

export const createOldStudentRegistrationService = (payload: {
  registration: CreateOldStudentRegistration;
}) =>
  axiosInstant
    .post(
      "/api/v1/registration/createOldStudentRegistration",
      payload.registration
    )
    .then((res) => res);

export const createNewStudentRegistrationService = (payload: {
  registration: CreateNewStudentRegistration;
}) =>
  axiosInstant
    .post(
      "/api/v1/registration/createNewStudentRegistration",
      payload.registration
    )
    .then((res) => res);

export const updateRegistrationService = (payload: {
  _id: string;
  registration: UpdateRegistration;
}) =>
  axiosInstant
    .put(
      `/api/v1/registration/updateRegistration/${payload._id}
        `,
      payload.registration
    )
    .then((res) => res);

export const updateNewRegistrationService = (payload: {
  _id: string;
  registration: UpdateNewRegistration;
}) =>
  axiosInstant
    .put(
      `/api/v1/registration/updateNewRegistration/${payload._id}
            `,
      payload.registration
    )
    .then((res) => res);

export const deleteRegistrationService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/registration/deleteRegistration/${payload._id}`)
    .then((res) => res);

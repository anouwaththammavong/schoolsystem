import { CreateScore, UpdateScore } from "../../domain/Scores";
import { axiosInstant } from "./defaultInstance";

export const getScoresService = (payload: {
  limit: number;
  offset: number;
  room_id: string;
  month: Date;
  generation: string;
  term?: string;
}) => {
  // Construct the base URL
  let url = `/api/v1/score/getScores?limit=${payload.limit}&offset=${
    payload.offset
  }&room_id=${
    payload.room_id
  }&month=${payload.month.toISOString()}&generation=${payload.generation}`;

  if (payload.term) {
    url += `&term=${payload.term}`;
  }

  // Make the API request
  return axiosInstant.get(url).then((res) => res);
};

export const createScoreService = (payload: { score: CreateScore }) =>
  axiosInstant
    .post("/api/v1/score/createScore", payload.score)
    .then((res) => res);

export const updateScoreService = (payload: {
  _id: string;
  score: UpdateScore;
}) =>
  axiosInstant
    .put(`/api/v1/score/updateScore/${payload._id}`, payload.score)
    .then((res) => res);

export const deleteScoreService = (payload: { _id:string }) =>
  axiosInstant
    .delete(`/api/v1/score/deleteScore/${payload._id}`)
    .then((res) => res);

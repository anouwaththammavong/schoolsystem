import { CreateSchoolFee, UpdateSchoolFee } from "../../domain/SchoolFees";
import { axiosInstant } from "./defaultInstance";

export const getSchoolFeeService = (payload: {
  limit: number;
  offset: number;
  level_id?: string;
  academic_year_no?: string;
}) => {
  let url = `/api/v1/school_fee/getSchoolFees?limit=${payload.limit}&offset=${payload.offset}`;

  if (payload.level_id) {
    url += `&level_id=${payload.level_id}`;
  }
  if (payload.academic_year_no) {
    url += `&academic_year_no=${payload.academic_year_no}`;
  }

  return axiosInstant.get(url).then((res) => res);
};

export const createSchoolFeeService = (payload: {
  schoolFee: CreateSchoolFee;
}) =>
  axiosInstant
    .post("/api/v1/school_fee/createSchoolFee", payload.schoolFee)
    .then((res) => res);

export const updateSchoolFeeService = (payload: {
  _id: string;
  schoolFee: UpdateSchoolFee;
}) =>
  axiosInstant
    .put(
      `/api/v1/school_fee/updateSchoolFee/${payload._id}
        `,
      payload.schoolFee
    )
    .then((res) => res);

export const deleteSchoolFeeService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/school_fee/deleteSchoolFee/${payload._id}`)
    .then((res) => res);

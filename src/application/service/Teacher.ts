import { CreateTeacher } from "../../domain/Teacher";
import { axiosInstant } from "./defaultInstance";

export const getTeacherService = (payload: {
  limit: number;
  offset: number;
  teacher_id?: string;
}) => {
  if (payload.teacher_id) {
    return axiosInstant
      .get(
        `/api/v1/teacher/getTeacher?teacher_id=${payload.teacher_id}&limit=${payload.limit}&offset=${payload.offset}`
      )
      .then((res) => res);
  } else {
    return axiosInstant.get(
      `/api/v1/teacher/getTeacher?limit=${payload.limit}&offset=${payload.offset}`
    );
  }
};

export const createTeacherService = (payload: { teacher: CreateTeacher }) =>
  axiosInstant
    .post("/api/v1/teacher/createTeacher", payload.teacher)
    .then((res) => res);

export const updateTeacherService = (payload: {
  _id: string;
  teacher: CreateTeacher;
}) =>
  axiosInstant
    .put(`/api/v1/teacher/updateTeacher/${payload._id}`, payload.teacher)
    .then((res) => res);

export const deleteTeacherService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/teacher/deleteTeacher/${payload._id}`)
    .then((res) => res);

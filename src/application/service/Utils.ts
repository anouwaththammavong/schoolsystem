import moment, { Moment } from 'moment';

/**
 * It formats a datetime to the format of DD/MM/YYYY H:mm:ss
 * @param {Moment} datetime - Moment
 * @returns A string.
 */
export const formatDatetimeToInformal = (datetime: Moment) => {
  return moment(datetime, 'YYYY-MM-DD H:mm:ss').format('DD/MM/YYYY H:mm:ss');
};
export const dateTimeInformat = 'DD/MM/YYYY H:mm:ss';

/**
 * It takes a date and formats it to a more informal format.
 * @param {Moment} date - Moment
 * @returns A string.
 */
export const formatDateToInformal = (date: Moment) => {
  return moment(date).format('DD/MM/YYYY');
};
export const dateInformal = 'DD/MM/YYYY';
/**
 * It formats a number into a string with a currency symbol.
 * @param {number} amount - The amount of money to format.
 * @param {string} [currency] - The currency code.
 * @returns A string.
 */
export function formatMoney(amount: number, currency?: string): string {
  if (!amount) {
    amount = 0;
  }
  return `${new Intl.NumberFormat('la-LA').format(amount)} ${currency ? currency : ''
    }`;
}
/**
 * It takes a file and returns a promise that resolves to a base64 string.
 * @param {any} file - The file that you want to upload.
 * @returns A promise.
 */
export const getBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
};

/**
 * "Cut a string to a certain length, if it's longer than the specified length, cut it and add ellipsis
 * at the end."
 *
 * The function is pretty simple, but it's not very readable. Let's break it down into smaller pieces
 * @param {string} text - The string to be cut.
 * @param {number} [quantity=10] - The number of characters to return.
 * @returns A string.
 */
export const cutString = (text: string, quantity: number = 10) => {
  const index = text.length;
  if (index > quantity) {
    return `${text.substring(0, quantity)}...`;
  }
  return text;
};
/**
 * If a > b, return 1. If a < b, return -1. Otherwise, return 0
 * @param {string} a - The first string to compare.
 * @param {string} b - The string to compare to a.
 * @returns 1
 */
export const sortString = (a: string, b: string) => {
  if (a > b) return 1;
  if (a < b) return -1;
  return 0;
};

/**
 * Given a date, return a string that can be used in a query to the API
 * @param {Moment} date - Moment
 * @returns The date in the format YYYY-MM-DDT00:00:00Z
 */
export const formatDateForQuery = (date: Moment) => {
  return `${moment(date).format('YYYY-MM-DDT00:00:00')}Z`;
};

type Color =
  "primary" | "secondary" | "error" | "info" | "success" | "warning" | "default" | undefined

/**
 * If minutes is less than 10, return a string with a 0 prepended to minutes, otherwise return a string
 * with minutes.
 * @param {number} minutes - number - The number of minutes to format.
 * @returns a string.
 */
export function formatMinutes(minutes: number) {
  return minutes < 10 ? `0${minutes}` : `${minutes}`;
}

/**
 * If the number of seconds is less than 10, return a string with a leading zero, otherwise return a
 * string with the number of seconds
 * @param {number} seconds - The number of seconds to format.
 * @returns a string.
 */
export function formatSeconds(seconds: number) {
  return seconds < 10 ? `0${seconds}` : `${seconds}`;
}

// sort table data function
function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

export type Order = 'asc' | 'desc';

export function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (
  a: { [key in Key]: number | string | boolean },
  b: { [key in Key]: number | string | boolean },
) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
export function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

export function randomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

export function getPagedData<T>(data: T[], page: number, rowsPerPage: number): T[] {
  const startIndex = page * rowsPerPage;
  const endIndex = startIndex + rowsPerPage;
  return data.slice(startIndex, endIndex);
}

export const dataURItoBlob = (dataURL: string): Blob | undefined => {
  const arr = dataURL.split(',');
  const mimeMatch = arr && arr[0] && arr[0].match(/:(.*?);/);

  if (mimeMatch) {
    const mime = mimeMatch[1];
    const bstr = atob(arr[1]);
    const n = bstr.length;
    const u8arr = new Uint8Array(n);
    for (let i = 0; i < n; i++) {
      u8arr[i] = bstr.charCodeAt(i);
    }
    return new Blob([u8arr], { type: mime });
  }
  return;
}

export const getTypeArrayBuffer = (arrBuf: string | ArrayBuffer): string => {
  const arrBufString = arrBuf.toString();
  const firstIndex = arrBufString.indexOf(':') + 1;
  const lastIndex = arrBufString.indexOf(';');
  return arrBufString.slice(firstIndex, lastIndex);
}
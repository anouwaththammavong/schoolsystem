import { IAuth, IChangePasswordHold } from '../../domain/profile';
import { axiosInstant } from './defaultInstance';

export const login = (payload: { auth: IAuth; }) =>
  axiosInstant.post('/api/auth/login', payload.auth).then((res) => res);

// http://localhost:8080/api/auth/register

export const getMe = () =>
  axiosInstant.get('/api/v1/profileRoutes/profile').then((res) => res);

export const changePassword = (payload: { changePasswordHold: IChangePasswordHold; }) =>
  axiosInstant.put('/profile/changeProfilePassword', payload.changePasswordHold).then((res) => res);

import moment from 'moment';
import { axiosMockAdapterInstance } from '../ExInstance';

axiosMockAdapterInstance.onGet('/stores').reply(200, {
  stores: [
    {
      id: '1',
      name: 'ໂພນຕ້ອງ',
      description: 'Field 1 description',
      code: '129283983',
      village: 'ໂພນຕ້ອງ',
      district: 'ຈັນທະບູລີ',
      province: 'ນຄລ',
      category: {
        id: 'cate1',
        name: 'ສະໜາມ'
      },
      created_at: moment(),
      place_now: 'ສາງໃຫຍ່'
    },
    {
      id: '2',
      name: 'JA Lighting',
      description: 'Shop 2 description',
      code: '',
      village: 'ດອນໜູນ',
      district: 'ໄຊທານີ',
      province: 'ນຄລ',
      category: {
        id: 'cate2',
        name: 'ຮ້ານຄ້າ'
      },
      created_at: moment(),
      place_now: 'ສະໜາມໂພນຕ້ອງ'
    },
  ],
});

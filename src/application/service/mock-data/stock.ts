import moment from "moment";
import { axiosMockAdapterInstance } from "../ExInstance";

axiosMockAdapterInstance.onGet("/stocks").reply(200, {
  stocks: [
    {
      id: "1",
      code: "3239912",
      date: moment(),
      remark: "Product 1 description",
      store: {
        id: "1",
        name: "ໂພນຕ້ອງ",
        description: "Field 1 description",
        code: "129283983",
        village: "ໂພນຕ້ອງ",
        district: "ຈັນທະບູລີ",
        province: "ນຄລ",
      },
      product_items: [
        {
          id: "cate1",
          name: "ເຄື່ອງມື",
        },
      ],
    },
    {
      id: "2",
      code: "SD3455",
      date: moment(),
      remark: "Product 2 description",
      store: {
        id: "2",
        name: "JA Lighting",
        description: "Shop 2 description",
        code: "",
        village: "ດອນໜູນ",
        district: "ໄຊທານີ",
        province: "ນຄລ",
      },
      product_items: [
        {
          id: "cate1",
          name: "ເຄື່ອງມື",
        },
      ],
    },
  ],
});

import moment from 'moment';
import { axiosMockAdapterInstance } from './../ExInstance';

axiosMockAdapterInstance.onGet('/products').reply(200, {
  products: [
    {
      id: '1',
      name: 'ຄ້ອນ',
      description: 'Product 1 description',
      quantity: 50,
      code: '129283983',
      color: 'ແດງ',
      category: {
        id: 'cate1',
        name: 'ເຄື່ອງມື'
      },
      created_at: moment(),
      place_now: 'ສາງໃຫຍ່'
    },
    {
      id: '2',
      name: 'ຄຸປູນ',
      description: 'Product 2 description',
      quantity: 80,
      code: '',
      color: 'ດຳ',
      category: {
        id: 'cate2',
        name: 'ເຄື່ອງມື'
      },
      created_at: moment(),
      place_now: 'ສະໜາມໂພນຕ້ອງ'
    },
  ],
});

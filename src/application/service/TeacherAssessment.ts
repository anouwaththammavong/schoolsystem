import {
  CreateTeacherAssessment,
  UpdateTeacherAssessment,
} from "../../domain/TeacherAssessments";
import { axiosInstant } from "./defaultInstance";

export const getTeacherAssessmentsService = (payload: {
  limit: number;
  offset: number;
  teacher_id?: string;
  assessment_date?: Date;
}) => {
  // Construct the base URL
  let url = `/api/v1/teacherAssessment/getTeacherAssessment?limit=${payload.limit}&offset=${payload.offset}`;

  // Conditionally append query parameters if they are provided
  if (payload.teacher_id) {
    url += `&teacher_id=${payload.teacher_id}`;
  }

  if (payload.assessment_date) {
    url += `&assessment_date=${payload.assessment_date.toISOString()}`;
  }

  // Make the API request
  return axiosInstant.get(url).then((res) => res);
};

export const createTeacherAssessmentService = (payload: {
  teacherAssessment: CreateTeacherAssessment;
}) =>
  axiosInstant
    .post(
      "/api/v1/teacherAssessment/createTeacherAssessment",
      payload.teacherAssessment
    )
    .then((res) => res);

export const updateTeacherAssessmentService = (payload: {
  _id: string;
  teacherAssessment: UpdateTeacherAssessment;
}) =>
  axiosInstant
    .put(
      `/api/v1/teacherAssessment/updateTeacherAssessment/${payload._id}`,
      payload.teacherAssessment
    )
    .then((res) => res);

export const deleteTeacherAssessmentService = (payload: { _id:string }) =>
  axiosInstant
    .delete(`/api/v1/teacherAssessment/deleteTeacherAssessment/${payload._id}`)
    .then((res) => res);

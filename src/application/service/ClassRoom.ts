import { CreateClassRoom, UpdateClassRoom } from "../../domain/ClassRoom";
import { axiosInstant } from "./defaultInstance";

export const getClassRoomService = (payload: {
  limit: number;
  offset: number;
}) =>
  axiosInstant
    .get(
      `/api/v1/classroom/getClassrooms?limit=${payload.limit}&offset=${payload.offset}`
    )
    .then((res) => res);

export const createClassRoomService = (payload: {
  classRoom: CreateClassRoom;
}) =>
  axiosInstant
    .post("/api/v1/classroom/createClassroom", payload.classRoom)
    .then((res) => res);

export const updateClassRoomService = (payload: {
  _id: string;
  classRoom: UpdateClassRoom;
}) =>
  axiosInstant
    .put(
      `/api/v1/classroom/updateClassroom/${payload._id}
      `,
      payload.classRoom
    )
    .then((res) => res);

export const deleteClassRoomService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/classroom/deleteClassroom/${payload._id}`)
    .then((res) => res);

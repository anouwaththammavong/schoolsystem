export interface ITFLocalStorage {
  token: string;
  // accessToken: string;
  refreshToken: string;
}
const LocalStorageService = (() => {
  function _setToken(tokenObj: ITFLocalStorage) {
    console.log("sssdsadas",tokenObj)
    localStorage.setItem('accessToken', tokenObj.token);
    localStorage.setItem('refreshToken', tokenObj.refreshToken);
  }
  function _getAccessToken() {
    return localStorage.getItem('accessToken');
  }
  function _getExpire() {
    return localStorage.getItem('expire');
  }
  function _getRefreshToken() {
    return localStorage.getItem('refreshToken');
  }
  function _getResetPasswordToken() {
    return localStorage.getItem('reset_password_accessToken');
  }
  function _setResetPasswordToken(token: string) {
    localStorage.setItem('reset_password_accessToken', token);
  }
  function _clearToken() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
  }
  return {
    // getService: _getService,
    setToken: _setToken,
    getAccessToken: _getAccessToken,
    getRefreshToken: _getRefreshToken,
    clearToken: _clearToken,
    getExpire: _getExpire,
    setResetPasswordToken: _setResetPasswordToken,
    getResetPasswordToken: _getResetPasswordToken
  };
})();
export default LocalStorageService;

import {
  CreateStudentAbsenceForm,
  UpdateStudentAbsenceForm,
} from "../../domain/StudentAbsence";
import { axiosInstant } from "./defaultInstance";

export const getAllStudentAbsencesService = (payload: {
  limit: number;
  offset: number;
  room_id: string;
  generation: string;
  student_id?: string;
}) => {
  if (payload.student_id) {
    return axiosInstant
      .get(
        `/api/v1/studentAbsence/getAllStudentAbsence?generation=${payload.generation}&student_id=${payload.student_id}&limit=${payload.limit}&offset=${payload.offset}&room_id=${payload.room_id}`
      )
      .then((res) => res);
  } else {
    return axiosInstant
      .get(
        `/api/v1/studentAbsence/getAllStudentAbsence?generation=${payload.generation}&limit=${payload.limit}&offset=${payload.offset}&room_id=${payload.room_id}`
      )
      .then((res) => res);
  }
};

export const getStudentIdAbsencesService = (payload: {
  limit: number;
  offset: number;
  student_id: string;
  room_id: string;
  absence_date?: Date;
  term?: string;
  be_reasonable?: string;
}) => {
  // Construct the base URL
  let url = `/api/v1/studentAbsence/getStudentAbsence?student_id=${payload.student_id}&limit=${payload.limit}&offset=${payload.offset}&room_id=${payload.room_id}`;

  // Conditionally append query parameters if they are provided
  if (payload.absence_date) {
    url += `&absence_date=${payload.absence_date.toISOString()}`;
  }
  if (payload.term) {
    url += `&term=${payload.term}`;
  }
  if (payload.be_reasonable) {
    url += `&be_reasonable=${payload.be_reasonable}`;
  }

  // Make the API request
  return axiosInstant.get(url).then((res) => res);
};

export const createStudentAbsenceService = (payload: {
  studentAbsence: CreateStudentAbsenceForm;
}) =>
  axiosInstant
    .post("/api/v1/studentAbsence/createStudentAbsence", payload.studentAbsence)
    .then((res) => res);

export const updateStudentAbsenceService = (payload: {
  _id: string;
  studentAbsence: UpdateStudentAbsenceForm;
}) =>
  axiosInstant
    .put(
      `/api/v1/studentAbsence/updateStudentAbsence/${payload._id}`,
      payload.studentAbsence
    )
    .then((res) => res);

export const deleteStudentAbsenceService = (payload: { _id: string }) =>
  axiosInstant
    .delete(`/api/v1/studentAbsence/deleteStudentAbsence/${payload._id}`)
    .then((res) => res);
